import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { ReportComponent } from './report/report.component';
import { ReportFormComponent } from './report-form/report-form.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';

@NgModule({
  declarations: [
    ReportComponent,
    ReportFormComponent,
    BarChartComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    ChartsModule
  ],
  exports: [
    ReportComponent,
    ReportFormComponent,
    BarChartComponent
  ]
})
export class ReportsModule { }
