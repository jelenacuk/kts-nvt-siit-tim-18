
export class ReportEventDate {

    eventId: number;
    eventDateTs: number;
    fromDateTs: number;
    toDateTs: number;
    type: string;

   constructor(eventId?: number, eventDate?: number, fromDate?: number, toDate?: number, reportType?: string) {

        this.eventId = eventId;
        this.eventDateTs = eventDate;
        this.fromDateTs = fromDate;
        this.toDateTs = toDate;
        this.type = reportType;
    }

}
