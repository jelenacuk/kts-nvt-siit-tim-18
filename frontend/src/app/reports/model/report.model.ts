import { ReportData } from './report-data.model';

export class Report {

    locationId: number;
    eventId: number;
    eventDate: Date;
    fromDate: Date;
    toDate: Date;
    reportType: string;
    reportData: ReportData[];

   constructor(locationId: number, eventId: number, eDate: Date, fromDate: Date, toDate: Date, reportType: string) {
        this.locationId = locationId;
        this.eventId = eventId;
        this.eventDate = eDate;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.reportType = reportType;
    }

}
