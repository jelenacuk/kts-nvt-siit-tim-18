
export class ReportEvent {

    eventId: number;
    fromDateTs: number;
    toDateTs: number;
    type: string;

   constructor(eventId?: number, fromDate?: number, toDate?: number, reportType?: string) {

        this.eventId = eventId;
        this.fromDateTs = fromDate;
        this.toDateTs = toDate;
        this.type = reportType;
    }

}
