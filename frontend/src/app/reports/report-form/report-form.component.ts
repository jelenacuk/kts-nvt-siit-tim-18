import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '../../model/location.model';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { EventBasicInfo } from '../../model/event-basic-info.model';
import { EventService } from '../../service/event.service';
import { LocationService } from '../../service/location.service';
import { validateDate, ValidateDate } from '../../validators/date-validator';
import { MatSnackBar } from '@angular/material';
import { ReportLocation } from '../model/report-location.model';
import { ReportEvent } from '../model/report-event.model';
import { ReportEventDate } from '../model/report-event-date.model';
import { UtilService } from '../../service/util.service';
import { atLeastOneRequired } from 'src/app/validators/at-least-one-required.validator';
import { compareDates } from 'src/app/validators/compare-dates.validator';

@Component({
  selector: 'app-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.scss']
})
export class ReportFormComponent implements OnInit {

  @Output()
  sendReportParams = new EventEmitter<string>();
  locationId: number;
  reportForm: FormGroup;

  locationNames: string[] = [];
  eventBasicInfo: EventBasicInfo[] = [];
  reportTypes: string[] = ['daily', 'weekly', 'monthly'];
  eventDates: Date[] = [];

  filteredLocations: Observable<string[]>;
  filteredEvents: Observable<EventBasicInfo[]>;

  constructor(private formBuilder: FormBuilder,
              private utilService: UtilService,
              private locationService: LocationService,
              private eventService: EventService,
              private errorSnackBar: MatSnackBar,
  ) {

    this.createForm();
    this.createFilters();
  }

  ngOnInit() {

    this.getEventBasicInfo();
    this.getLocationNames();

    this.reportForm.controls.location.disable();
    this.reportForm.controls.event.disable();
    this.reportForm.controls.eDate.disable();

  }

  createForm() {
    this.reportForm = this.formBuilder.group({
      fromDate: ['', [Validators.required, validateDate(new Date(), ValidateDate.BEFORE)]],
      toDate: ['', [Validators.required, validateDate(new Date(), ValidateDate.BEFORE)]],
      eDate: ['', []],
      reportType: ['', [Validators.required]],
      location: ['', []],
      event: ['', []]
    });

    this.reportForm.setValidators([
      atLeastOneRequired(
          this.reportForm.get('location'),
          this.reportForm.get('event'),
      ), compareDates(
          this.reportForm.get('fromDate'),
          this.reportForm.get('toDate'))
    ]);
  }

  get fromDate() { return this.reportForm.controls.fromDate.value as Date; }
  get toDate() { return this.reportForm.controls.toDate.value as Date; }
  get eDate() { return this.reportForm.controls.eDate.value as Date; }
  get reportType() { return this.reportForm.controls.reportType.value as string; }
  get selectedLocation() { return this.reportForm.controls.location.value as string; }
  get selectedEvent() { return this.reportForm.controls.event.value as string; }

  // ******************* FILTER EVENTS AND LOCATIONS ******************************

  createFilters() {
    this.filteredLocations = this.reportForm.controls.location.valueChanges
      .pipe(
        startWith(''),
        map(location => location ? this._filterLocations(location) : this.locationNames.slice())
      );

    this.filteredEvents = this.reportForm.controls.event.valueChanges
      .pipe(
        startWith(''),
        map(event => event ? this._filterEvents(event) : this.eventBasicInfo.slice())
      );
  }

  private _filterLocations(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.locationNames.filter(name => name.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterEvents(value: string): EventBasicInfo[] {
    const filterValue = value.toLowerCase();
    return this.eventBasicInfo.filter(event => event.name.toLowerCase().indexOf(filterValue) === 0);
  }

  // ************************ EVENT AND LOCATION CONTROLS ****************************

  enableLocationCtrl_disableEventCtrl() {
    this.reportForm.controls.location.enable();
    this.reportForm.controls.event.disable();
    this.reportForm.controls.eDate.disable();
    this.reportForm.controls.event.setValue('');
    this.reportForm.controls.eDate.setValue(undefined);
  }

  enableEventCtrl_disableLocationCtrl() {
    this.reportForm.controls.event.enable();
    this.reportForm.controls.location.disable();
    this.reportForm.controls.location.setValue('');
  }

  onLocationCtrlChange() {

    if (this.reportForm.controls.location.disabled) {
      this.enableLocationCtrl_disableEventCtrl();
    } else {
      this.enableEventCtrl_disableLocationCtrl();
    }
  }

  onEventCtrlChange() {

    if (this.reportForm.controls.event.disabled) {
      this.enableEventCtrl_disableLocationCtrl();
    } else {
      this.enableLocationCtrl_disableEventCtrl();
    }
  }

  // ***************************  VALIDATIONS  **********************************

  checkEvent() {

    const existingEventId = this.eventNameExists();
    if (existingEventId !== -1) {
      this.reportForm.controls.eDate.enable();
      this.getEventDates(existingEventId);
    } else {
      this.reportForm.controls.eDate.disable();
      this.reportForm.controls.eDate.setValue(undefined);
    }
  }

  eventNameExists(): number {
    const existingEvent: EventBasicInfo[] = this.eventBasicInfo.filter(e => e.name === this.selectedEvent);
    if (existingEvent.length > 0) {
      return existingEvent[0].id;
    } else {
      return -1;
    }
  }

  locationNameExists(): boolean {
    const existingLocation: string[] = this.locationNames.filter(l => l === this.selectedLocation);
    if (existingLocation.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  validateForm(): boolean {
      // make sure the selected location/event name exists
      const eventId = this.eventNameExists();
      const locationExists = this.locationNameExists();
      if (!this.selectedEvent && !this.selectedLocation) {
        this.errorSnackBar.open('You must pick event or location', '', {
          duration: 5000,
          panelClass: ['error-snack-bar']
        });
        return false;
      }

      if (!this.selectedEvent && !locationExists) {
        this.errorSnackBar.open('You must pick an existing location name', '', {
          duration: 5000,
          panelClass: ['error-snack-bar']
        });
        return false;
      } else if (!this.selectedLocation && eventId === -1) {
        this.errorSnackBar.open('You must pick an existing event name', '', {
          duration: 5000,
          panelClass: ['error-snack-bar']
        });
        return false;
      }
      return true;
  }

// ********************************* SUBMIT ************************************

  onReportSubmit() {

    if (this.validateForm()) {

      let reportLocation: ReportLocation = new ReportLocation();
      let reportEvent: ReportEvent = new ReportEvent();
      let reportEventDate: ReportEventDate = new ReportEventDate();
      let params: string;
      const eventId = this.eventNameExists();

      if (this.selectedLocation) {

        reportLocation = new ReportLocation(this.locationId, + this.fromDate, + this.toDate, this.reportType);

        params = this.generateParams(reportLocation);

      } else if (this.selectedEvent && !this.eDate) {
        reportEvent = new ReportEvent(eventId, + this.fromDate, + this.toDate, this.reportType);
        params = this.generateParams(reportEvent);

      } else {
        reportEventDate = new ReportEventDate(eventId, + new Date(this.eDate), + this.fromDate, + this.toDate, this.reportType);
        params = this.generateParams(reportEventDate);

      }

      this.sendReportParams.emit(params);

    }
  }

  generateParams(data: any): string {
    return Object.keys(data).map(key => `${key}=${encodeURIComponent(data[key])}`).join('&');
  }

  // ******************************* GET DATA *********************************

  getEventBasicInfo() {
    this.eventService.getEventsBasicInfo().subscribe(
      (response: EventBasicInfo[]) => {
        this.eventBasicInfo = response;
      }
    );
  }

  getEventDates(id: number) {
    this.eventService.getEventDates(id).subscribe(
      (response: Date[]) => {
        if (response != null) {
          console.log(JSON.stringify(response));
          this.eventDates = response;
        }
      });
  }

  getLocationNames() {
    this.locationService.getAllLocationNames().subscribe(
      (response: string[]) => {
        if (response != null) {
          this.locationNames = response;
        }
      });
  }

  getChosenLocation() {
    if (!this.locationNameExists()) { return; }
    this.locationService.getLocationByName(this.selectedLocation).subscribe(
      (response => {
        console.log('Location id: ' + response.id);
        if (response != null) {
          this.locationId = response.id;
        }
      }));
  }

}
