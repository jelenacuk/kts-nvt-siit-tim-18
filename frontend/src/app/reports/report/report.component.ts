import { Component, OnInit } from '@angular/core';
import { ReportService } from '../../service/report.service';
import { Report } from '../model/report.model';
import { Label } from 'ng2-charts';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  report: Report;

  barChartLabels: Label[];
  soldTicketsData: number[];
  earningsData: number[];
  soldTicketsLabel = 'Sold tickets';
  earningsLabel = 'Earnings';

  constructor(private reportService: ReportService,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  sendReportParams(params: string) {
    // alert('Params from report component: ' + params);
    this.reportService.getReport(params).subscribe(
      (response  => {
        this.report = response;
        this.soldTicketsData = this.report.reportData.map(data => data.soldTickets);
        this.earningsData = this.report.reportData.map(data => data.earnings);
        console.log('Report type: ' + this.report.reportType);
        if (this.report.reportType === 'DAILY') {
          this.barChartLabels = this.getDayLabels(this.report.fromDate, this.report.toDate);
        } else if (this.report.reportType === 'WEEKLY') {
          this.barChartLabels = this.getWeekLabels(this.report.reportData.length);
        } else {
          this.barChartLabels = this.getMonthLabels(this.report.fromDate, this.report.toDate);
        }
      }
    ), (error =>
      this.snackBar.open(error.error.message, '', { duration: 4000,
        panelClass: ['error-snack-bar']
      })
    ));
  }

  getDayLabels(startDate: Date, endDate: Date) {

    const dateArray = [];
    console.log('Start date:' + startDate);
    let currentDate = moment(startDate);
    const stopDate = moment(endDate);

    while (currentDate <= stopDate) {
      dateArray.push(moment(currentDate).format('YYYY-MM-DD'));
      currentDate = moment(currentDate).add(1, 'days');
    }
    console.log(dateArray);
    return dateArray;
  }

  getMonthLabels(startDate: Date, endDate: Date) {
    const currentDate = moment(startDate);
    const stopDate = moment(endDate);

    const result = [];

    while (currentDate.isBefore(stopDate)) {
      result.push(currentDate.format('YYYY-MM'));
      currentDate.add(1, 'month');
    }

    return result;
  }

  // TODO Change this
  getWeekLabels(num: number) {
    const result = [];
    console.log('num' + num);
    for (let i = 1; i < num; i++) {
        result.push('Week ' + i);
    }

    return result;
  }

}
