import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Hall } from '../model/hall.model';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class HallService {

  constructor(private http: HttpClient, private constantsService: ConstantsService) { }

  getHalls(id: number): Observable<Hall> {
    return this.http.get<Hall>(this.constantsService.hallsPath + '/' + id);
  }
  deleteHall(locationid: number, hallid: number): Observable<boolean> {
    return this.http.delete<boolean>(this.constantsService.hallsPath + '/' + locationid + '/' + hallid);
  }
  createHall(locationId: number, hall: Hall): Observable<Hall> {
    return this.http.post<Hall>(this.constantsService.hallsPath + '/' + locationId, hall);
  }
  getHall(locationId: number, hallId: number): Observable<Hall> {
    return this.http.get<Hall>(this.constantsService.hallsPath + '/' + locationId + '/' + hallId);
  }
  updateHall(locationId: number, hall: Hall): Observable<Hall> {
    return this.http.put<Hall>(this.constantsService.hallsPath + '/' + locationId + '/' + hall.id, hall);
  }
}
