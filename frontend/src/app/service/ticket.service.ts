import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Hall } from '../model/hall.model';
import { Section } from '../model/section.model';
import { ConstantsService } from './constants.service';
import { BuyTicket } from '../model/buy-ticket.model';
import { PayPal } from '../model/paypal.model';
import { Seat } from '../model/seat.model';

@Injectable({
    providedIn: 'root'
})
export class TicketService {

    constructor(private http: HttpClient, private constants: ConstantsService) { }

    buyTicket(buyTickets: BuyTicket[]): Observable<any> {

        return this.http.post<any>(this.constants.ticketPath + '/buy-ticket', buyTickets);
    }

    buyReservedTicketCreatePayment(id: number): Observable<any> {
        return this.http.put<any>(this.constants.ticketPath + '/buy-reserved-ticket/' + id, {});
    }

    buyReservedTicketCompletePayment(paypal: PayPal): Observable<any> {
        return this.http.put<any>(this.constants.ticketPath + '/buy-reserved-ticket-complete', paypal);
    }

    buyTicketCompletePayment(paypal: PayPal): Observable<any> {
        return this.http.put<any>(this.constants.ticketPath + '/buy-ticket-complete', paypal);
    }

    reserveTicket(buyTicket: BuyTicket[]): Observable<number> {
        return this.http.post<number>(this.constants.ticketPath + '/reserve-ticket', buyTicket);
    }

    cancelTicket(ids: number[]): Observable<boolean> {
        return this.http.put<boolean>(this.constants.ticketPath + '/cancel-ticket-reservation/' + ids, {});
    }

    getTakenSeats(eventId: number, sectionId: number, tickedDate: string): Observable<Seat[]> {
        return this.http.get<Seat[]>(this.constants.ticketPath + '/get-taken-seats/' + eventId + '&' + sectionId + '&' + tickedDate);
    }

    getTicketCount(eventSectionId: number): Observable<number> {
        return this.http.get<number>(this.constants.ticketPath + '/get-ticket-count/' + eventSectionId);
    }
}
