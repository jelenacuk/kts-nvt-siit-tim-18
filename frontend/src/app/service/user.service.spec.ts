import { TestBed, async, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { ConstantsService } from './constants.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { User } from '../model/user.model';
import { LogIn } from '../model/login.model';
import { PageEvent } from '@angular/material';

describe('UserService', () => {

  let userService: UserService;
  let constantsService: ConstantsService;
  let httpMock: HttpTestingController;
  const newUser = new User();
  newUser.username = 'pera';
  newUser.password = 'password';
  newUser.repeatedPassword = 'password';
  newUser.firstName = 'Pera';
  newUser.lastName = 'Peric';
  newUser.email = 'pera@gmail.com';
  newUser.phone = '023/848-848';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ UserService, ConstantsService ]
    });

    userService = TestBed.get(UserService);
    constantsService = TestBed.get(ConstantsService);
    httpMock = TestBed.get(HttpTestingController);

  });

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it(`registerUser() should save user`, async(inject([HttpTestingController, UserService],
    (httpClient: HttpTestingController, uService: UserService) => {

      const postItem = new User();
      postItem.id = 100;
      postItem.username = 'pera';
      postItem.password = 'password';
      postItem.repeatedPassword = 'password';
      postItem.firstName = 'Pera';
      postItem.lastName = 'Peric';
      postItem.email = 'pera@gmail.com';
      postItem.phone = '023/848-848';

      uService.registerUser(newUser)
        .subscribe((response: any) => {
          expect(response.id).toBe(100);
          expect(response.username).toBe('pera');
          expect(response.firstName).toBe('Pera');
          expect(response.lastName).toBe('Peric');
          expect(response.email).toBe('pera@gmail.com');
          expect(response.phone).toBe('023/848-848');
        });

      const reqest = httpMock.expectOne(constantsService.authPath + '/register');
      expect(reqest.request.method).toBe('POST');
      reqest.flush(postItem);
      httpMock.verify();
    })));

  it(`login() should return token`, async(inject([HttpTestingController, UserService],
    (httpClient: HttpTestingController, uService: UserService) => {

      const loginData = new LogIn('pera', 'pera');
      const jwtToken = 'token';
      uService.login(loginData)
        .subscribe((response: any) => {
          expect(response).toBe('token');
        });
      const reqest = httpMock.expectOne(constantsService.authPath + '/login');
      expect(reqest.request.method).toBe('POST');
      reqest.flush(jwtToken);
      httpMock.verify();
    })));

  it(`getUser() should return logged User data`, async(inject([HttpTestingController, UserService],
      (httpClient: HttpTestingController, uService: UserService) => {

        const loginData = new LogIn('pera', 'pera');
        const jwtToken = 'token';
        uService.getUser()
          .subscribe((response: any) => {
            expect(response.username).toBe('pera');
            expect(response.firstName).toBe('Pera');
            expect(response.lastName).toBe('Peric');
            expect(response.email).toBe('pera@gmail.com');
            expect(response.phone).toBe('023/848-848');
          });
        const reqest = httpMock.expectOne(constantsService.userPath + '/getLogUser');
        expect(reqest.request.method).toBe('GET');
        reqest.flush(newUser);
        httpMock.verify();
      })));

  it(`getUserTickets() should fatch logged User tickets`, async(inject([HttpTestingController, UserService],
        (httpClient: HttpTestingController, uService: UserService) => {

          const userTickets = [
            {
              id: 100,
              dayOfPurchase: '02.02.2020. 12:00',
              row: '6',
              column: 8,
              eventSectionName: 'east',
              eventSectionID: '100',
              price: 1000,
              forDay: new Date('26.02.2020. 12:00'),
              eventName: 'OPENS2019',
              eventID: 100,
              lastDayToPay: '3',
              location: 'Spens',
              bought: true,
              totalSize: 4,
              ticketBackground: 'background.jpg'
            },
            {
              id: 101,
              dayOfPurchase: '02.02.2020. 12:00',
              row: '8',
              column: '6',
              eventSectionName: 'west',
              eventSectionID: 100,
              price: 2000,
              forDay: new Date('28.02.2020. 12:00'),
              eventName: 'Cirkus',
              eventID: '101',
              lastDayToPay: '4',
              location: 'Spens',
              bought: true,
              totalSize: 4,
              ticketBackground: 'background.jpg'
            }
          ];
          const page = new PageEvent();
          page.pageIndex = 0;
          page.pageSize = 4;
          uService.getUserTickets(true, page )
          .subscribe((response: any) => {
            expect(response.length).toBe(2);

            expect(response[0].id).toBe(100);
            expect(response[0].eventName).toBe('OPENS2019');
            expect(response[0].price).toBe(1000);

            expect(response[1].id).toBe(101);
            expect(response[1].eventName).toBe('Cirkus');
            expect(response[1].price).toBe(2000);
          });
          const reqest = httpMock.expectOne(constantsService.userPath + '/getUserTickets/true?page=0&size=4');
          expect(reqest.request.method).toBe('GET');
          reqest.flush(userTickets);
          httpMock.verify();
        })));


});
