import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AddEvent } from '../model/add-event.model';
import { ConstantsService } from './constants.service';
import { PageEvent } from '@angular/material';
import { Event } from '../model/event.model';
import { Section } from '../model/section.model';
import { UpdateEventNameDto } from '../model/update-event-name.model';
import { UpdateEventDescriptionDTO } from '../model/update-event-description.model';
import { ChangeLastDayToPayDTO } from '../model/change-lastDayToPay.model';
import { ChangeEventDateDTO } from '../model/change-event-date.model';
import { ChangeEventCategoryDTO } from '../model/change-event-category.model';
import { Attachment } from '../model/attachment.model';
import { EventBasicInfo } from '../model/event-basic-info.model';
import { EventApplication } from '../model/event-application.model';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    constructor(private http: HttpClient, private constants: ConstantsService) { }

    // Nevermind the eval(eventSections) method, it is safe for usage in this situation.
    addEvent(newEvent: AddEvent, eventSections: string): Observable<number> {
        let appDate: number;
        if (String(newEvent.controllAccess) === 'true') {
            appDate = newEvent.applicationDate.getTime();
        } else {
            appDate = null;
        }

        return this.http.post<number>(this.constants.eventsPath + '/add-event', {
            name: newEvent.name, description: newEvent.description,
            date: newEvent.date.map(x => x.getTime()), salesDate: newEvent.salesDate.getTime(),
            status: 0, lastDayToPay: newEvent.lastDayToPay, controllAccess: newEvent.controllAccess,
            attachments: newEvent.attachments, hallID: newEvent.hallID, categoriesIDs: newEvent.categories.map(x => x.id),
            eventSectionsIDs: eval(eventSections), locationID: newEvent.locationID, background: newEvent.background,
            backgroundName: newEvent.backgroundName, applicationDate: appDate
        });
    }

    getEvents(event: PageEvent): Observable<Event[]> {
        if (!event) {
            return this.http.get<Event[]>(this.constants.eventsPath + '/getPage');
        } else {
            return this.http.get<Event[]>(this.constants.eventsPath + '/getPage' + '?page=' + event.pageIndex + '&size=' + event.pageSize);
        }
    }

    getEventsSearch(page: PageEvent, params: string): Observable<Event[]> {
        return this.http.get<Event[]>(this.constants.eventsPath + '?page=' + page.pageIndex + '&size=' + page.pageSize + '&' + params);
    }

    getEventsBasicInfo(): Observable<EventBasicInfo[]> {
        return this.http.get<EventBasicInfo[]>(this.constants.eventsPath + '/basicInfo');
    }

    getEventDates(id: number): Observable<Date[]> {
        return this.http.get<Date[]>(this.constants.eventsPath + '/' + id + '/eventDates');
    }

    getEvent(id: number): Observable<Event> {
        return this.http.get<Event>(this.constants.eventsPath + '/' + id);
    }

    getEventSection(id: number): Observable<Section> {
        return this.http.get<Section>(this.constants.eventsPath + '/getEventSection/' + id);
    }

    updateEventName(dto: UpdateEventNameDto): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/update-event-name', dto);
    }

    updateEventDescription(dto: UpdateEventDescriptionDTO): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/update-event-description', dto);
    }
    changeEventLastDayToPay(dto: ChangeLastDayToPayDTO): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/change-last-day-to-pay', dto);
    }

    changeEventControllAccess(id: number): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/change-event-controll-access/' + id, {});
    }

    changeEventDate(dto: ChangeEventDateDTO): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/change-event-date', dto);
    }

    addEventCategory(dto: ChangeEventCategoryDTO): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/add-new-category-to-event', dto);
    }

    removeEventCategory(dto: ChangeEventCategoryDTO): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/remove-category-from-event', dto);
    }

    changeTicketBackground(dto: Attachment): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/change-event-ticket-background', dto);
    }

    addAttachment(dto: Attachment): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/add-event-attachment', dto);
    }

    removeAttachment(dto: Attachment): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/remove-event-attachment', dto);
    }

    blockEvent(id: number): Observable<boolean> {
        return this.http.put<boolean>(this.constants.eventsPath + '/block-event/' + id, {});
    }
    addEventApplication(eventId: number): Observable<number> {
        return this.http.put<number>(this.constants.eventsPath + '/add-event-application/' + eventId, {});
    }
    getEventApplications(): Observable<EventApplication[]> {
        return this.http.get<EventApplication[]>(this.constants.eventsPath + '/get-event-application');
    }
    getEventApplicationByEvent(id: number): Observable<EventApplication> {
        return this.http.get<EventApplication>(this.constants.eventsPath + '/get-event-application/' + id);
    }


}
