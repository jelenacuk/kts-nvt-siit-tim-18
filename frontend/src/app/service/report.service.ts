import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Report } from '../reports/model/report.model';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private readonly path = 'http://localhost:8080/api/reports';

  constructor(private http: HttpClient) { }

  getReport(params: string): Observable<Report> {

    return this.http.get<Report>(this.path + '?' + params);
  }

}
