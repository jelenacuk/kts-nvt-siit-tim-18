import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Section } from '../model/section.model';
import { ConstantsService } from './constants.service';

@Injectable({
  providedIn: 'root'
})
export class SectionService {

  constructor(private http: HttpClient, private constants: ConstantsService) { }

  getSection(id: number): Observable<Section> {
    return this.http.get<Section>(this.constants.sectionsPath + '/' + id);
  }
  createSection(hallId: number, sectionDto: Section): Observable<Section> {
    return this.http.post<Section>(this.constants.sectionsPath + '/' + hallId, sectionDto);
  }
  updateSectionPositions(sections: Array<Section>): Observable<boolean> {
    return this.http.put<boolean>(this.constants.localhost + '/api/sectionsPosition', sections);
  }
  updateSection(section: Section): Observable<Section> {
    return this.http.put<Section>(this.constants.sectionsPath + '/' + section.id, section);
  }
  deleteSection(sectionId: number): Observable<Section> {
    return this.http.delete<Section>(this.constants.sectionsPath + '/' + sectionId);
  }
}
