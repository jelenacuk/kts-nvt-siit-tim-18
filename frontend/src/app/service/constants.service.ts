import { Injectable } from '@angular/core';

@Injectable()
export class ConstantsService {
    readonly localhost = 'http://localhost:8080';
    readonly categoriesPath = this.localhost + '/api/categories';
    readonly locationsPath = this.localhost + '/api/locations';
    readonly eventsPath = this.localhost + '/api/events';
    readonly authPath = this.localhost + '/auth';
    readonly userPath = this.localhost + '/api/user';
    readonly ticketPath = this.localhost + '/api/tickets';
    readonly hallsPath = this.localhost + '/api/halls';
    readonly sectionsPath = this.localhost + '/api/sections';
}
