import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../model/user.model';
import {  PageEvent } from '@angular/material';
import { LogIn } from '../model/login.model';
import { UserTicket } from '../model/user-ticket.model';
import { Admin } from '../model/admin.model';
import { ConstantsService } from './constants.service';
import { LoginJwt } from '../model/LoginJwt.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  headers: HttpHeaders = new HttpHeaders({
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient,  private constants: ConstantsService) { }

  registerUser(user: User): Observable<User> {
    return this.http.post<User>(this.constants.authPath + '/register', user);
  }

  login(dto: LogIn): Observable<LoginJwt> {
    return this.http.post<LoginJwt>(this.constants.authPath + '/login', dto);
  }

  getUser(): Observable<User> {
    return this.http.get<User>(this.constants.userPath + '/getLogUser', { headers: this.headers });
  }

  getUserTickets(bought: boolean, event: PageEvent): Observable<UserTicket[]> {
    return this.http.get<UserTicket[]>(this.constants.userPath + '/getUserTickets/' + bought +
    '?page=' + event.pageIndex + '&size=' + event.pageSize, { headers: this.headers });
  }

  editUserProfile(dto: User | Admin): Observable<User | Admin> {
    return this.http.post<User | Admin>(this.constants.userPath + '/editUserProfile', dto, { headers: this.headers });
  }

}
