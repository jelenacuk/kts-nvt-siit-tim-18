import { TestBed, async, inject } from '@angular/core/testing';

import { LocationService } from './location.service';
import { ConstantsService } from './constants.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { PageEvent } from '@angular/material';
import { MaterialModule } from '../material/material.module';
import { Location } from '../model/location.model';

describe('LocationService', () => {
  let locationService: LocationService;
  let constantsService: ConstantsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MaterialModule],
      providers: [LocationService, ConstantsService]
    });
    locationService = TestBed.get(LocationService);
    constantsService = TestBed.get(ConstantsService);
    httpMock = TestBed.get(HttpTestingController);
  });




  it('should be created', () => {
    const service: LocationService = TestBed.get(LocationService);
    expect(service).toBeTruthy();
  });



  it(`getLocations() should return all locations`, async(inject([HttpTestingController, LocationService],
    (httpClient: HttpTestingController, locService: LocationService) => {

      const postItem = [{
        id: 100,
        name: 'Loc1',
        city: 'Beograd',
        address: 'Adresa',
        longitude: 15,
        latitude: 15,
        image: '',
        halls: [],
        active: true,
        totalSize: 1
      },
      ];
      locService.getLocations(null)
        .subscribe((posts: any) => {
          expect(posts.length).toBe(1);
          expect(posts[0].id).toBe(100);
          expect(posts[0].name).toBe('Loc1');
          expect(posts[0].city).toBe('Beograd');
          expect(posts[0].address).toBe('Adresa');
          expect(posts[0].image).toBe('');
          expect(posts[0].active).toBe(true);
          expect(posts[0].totalSize).toBe(1);
        });

      const reqest = httpMock.expectOne(constantsService.locationsPath);
      expect(reqest.request.method).toBe('GET');

      reqest.flush(postItem);
      httpMock.verify();

    })));

  it(`activate location should activate location`, async(inject([HttpTestingController, LocationService],
    (httpClient: HttpTestingController, locService: LocationService) => {
      const postItem = [{
        id: 100,
        name: 'Loc1',
        city: 'Beograd',
        address: 'Adresa',
        longitude: '15',
        latitude: '15',
        image: '',
        halls: [],
        active: true,
        totalSize: 1
      },
      ];

      locService.updateLocation(postItem[0] as Location)
        .subscribe((posts: any) => {
          expect(posts[0].id).toBe(100);
          expect(posts[0].active).toBe(true);
        });

      const reqest = httpMock.expectOne(constantsService.locationsPath);
      expect(reqest.request.method).toBe('PUT');

      reqest.flush(postItem);
      httpMock.verify();

    })));

  it('deactivating location', async(inject([HttpTestingController, LocationService],
    (httpClient: HttpTestingController, locService: LocationService) => {

      const categoryId = 101;
      const postItem = [{
        id: 100,
        name: 'Loc1',
        city: 'Beograd',
        address: 'Adresa',
        longitude: 15,
        latitude: 15,
        image: '',
        halls: [],
        active: false,
        totalSize: 1
      },
      ];

      locService.deleteLocation(100)
        .subscribe((posts: any) => {
          expect(posts[0].active).toBe(false);
          expect(posts[0].name).toBe('Loc1');
        });

      const reqest = httpMock.expectOne(constantsService.locationsPath + '/' + 100);
      expect(reqest.request.method).toBe('DELETE');

      reqest.flush(postItem);
      httpMock.verify();

    })));
});
