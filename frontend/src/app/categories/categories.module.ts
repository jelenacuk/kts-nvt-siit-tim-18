import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoriesManagerComponent } from './categories-manager/categories-manager.component';

@NgModule({
  declarations: [
    CategoriesListComponent,
    AddCategoryComponent,
    CategoriesManagerComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    CategoriesListComponent,
    AddCategoryComponent,
    CategoriesManagerComponent
  ]
})
export class CategoriesModule { }
