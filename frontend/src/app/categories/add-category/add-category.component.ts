import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from '../../model/category.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../service/category.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})

export class AddCategoryComponent implements OnInit {

  category: Category = new Category();
  imagePath: string;
  selectedImage: any;
  addCategoryForm: FormGroup;
  @Input() operation: string;
  @Input() categoryId: number;
  @Output() cancelEvent = new EventEmitter();

  constructor(private categoryService: CategoryService,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.createForm();

    if (this.operation === 'update') {
      this.categoryService.getCategoryById(this.categoryId).subscribe(
        (response => {
          this.category.icon = response.icon;
          this.addCategoryForm.get('name').setValue(response.name);
          this.addCategoryForm.get('color').setValue(response.color);
          this.imagePath = this.category.icon;
        }), (error => {
          this.snackBar.open(error.error.message, '', { duration: 4000,
            panelClass: ['error-snack-bar']
          });
      }));
    }

  }

  createForm() {

    this.addCategoryForm = this.formBuilder.group({
      name: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ]],
      color: ['', [
       // Validators.required
      ]],
      icon: ['', [

      ]]    });

  }

  get icon() { return this.addCategoryForm.controls.icon.value as string; }
  get name() { return this.addCategoryForm.controls.name.value as string; }
  get color() { return this.addCategoryForm.controls.color.value as string; }


  addCategory() {
    if (! this.category.icon) {
      this.snackBar.open('You must pick an icon', '', { duration: 4000,
        panelClass: ['error-snack-bar']
      });
      return;
    }
    console.log('color is: ' + JSON.stringify(this.addCategoryForm.value));
    if (this.addCategoryForm.valid) {

      this.category = new Category(this.addCategoryForm.value, this.category.icon);
      this.categoryService.addCategory(this.category).subscribe(
        (response => {
          this.snackBar.open('Category ' + response.name + ' added', '', { duration: 4000,
            panelClass: ['snack-bar']
          });
          this.cancel();
        }), (error => {
          this.snackBar.open(error.error.message, '', { duration: 4000,
            panelClass: ['error-snack-bar']
          });
        })
      );
    }
  }

  updateCategory() {
    if (! this.category.icon) {
      this.snackBar.open('You must pick an icon', '', { duration: 4000,
        panelClass: ['error-snack-bar']
      });
      return;
    }

    this.category = new Category(this.addCategoryForm.value, this.category.icon);
    this.category.id = this.categoryId;

    if (this.addCategoryForm.valid) {
      this.categoryService.updateCategory(this.category).subscribe(
        (response => {
          this.snackBar.open('Category updated');
          this.cancel();
        }), (error => {
          this.snackBar.open(error.error.message, '', { duration: 4000,
            panelClass: ['error-snack-bar']
          });
        })
      );
    }
  }

  cancel() {
    this.cancelEvent.emit();
  }

  getImage() {
    if (this.imagePath) {
      return 'http://localhost:8080/' + this.imagePath;
    } else {
      return this.selectedImage;
    }
  }

  openInput() {
    document.getElementById('fileInput').click();
  }

  fileChange(files: any) {
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imagePath = null;
      this.selectedImage = reader.result;
      this.category.icon = reader.result.toString();
      this.snackBar.open('Image loaded');
    };
    reader.onerror = error => {
      console.log('Error: ', error);
      this.snackBar.open('error loading image');
    };
  }
}
