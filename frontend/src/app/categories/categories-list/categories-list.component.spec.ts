import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '../../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CategoriesListComponent } from './categories-list.component';
import { CategoryService } from 'src/app/service/category.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { DebugElement } from '@angular/core';

describe('CategoriesListComponent', () => {
  let component: CategoriesListComponent;
  let fixture: ComponentFixture<CategoriesListComponent>;
  let categoryService: any;

  beforeEach(async(() => {

    const categoryServiceMock = {
      getAllCategories: jasmine.createSpy('getAllCategories')
          .and.returnValue(Observable.of([{ id: 100,
                                            name: 'music',
                                            color: '#32a852',
                                            icon: '/category-icons//music.png'
                                          },
                                          { id: 200,
                                            name: 'sports',
                                            color: '#3863ba',
                                            icon: '/category-icons//sports.png'
                                          },
                                          { id: 300,
                                            name: 'cinema',
                                            color: '#8438ba',
                                            icon: '/category-icons//cinema.png' }])),
        deleteCategory: jasmine.createSpy('deleteCategory')
        .and.returnValue(Observable.of(true))
    };

    TestBed.configureTestingModule({
      declarations: [ CategoriesListComponent ],
      imports: [MaterialModule, BrowserAnimationsModule ],
      providers: [{provide: CategoryService, useValue: categoryServiceMock }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoriesListComponent);
    component = fixture.componentInstance;
    categoryService = TestBed.get(CategoryService);
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch the categories list on init', async(() => {
    component.ngOnInit();
    // should subscribe on RegenerateData
    // expect(categoryService.RegenerateData$.subscribe).toHaveBeenCalled();
    expect(categoryService.getAllCategories).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {

        expect(component.categories.length).toBe(3); // mock categoriesService returned 3 empty objects
        fixture.detectChanges(); // synchronize HTML with component data
        const elements: DebugElement[] =
          fixture.debugElement.nativeElement.querySelectorAll('.category-name');
        expect(elements.length).toBe(3);

      });
  }));

  it('should emit add category event', () => {

    spyOn(component.addCategoryEvent, 'emit').and.callThrough();
    component.newCategory();
    const arg: any = (component.addCategoryEvent.emit as any).calls.mostRecent().args[0];
    expect(arg).toEqual(null);

  });

  it('should emit update category event', () => {

    spyOn(component.addCategoryEvent, 'emit').and.callThrough();
    const id = 100;
    component.update(id);
    const arg: any = (component.addCategoryEvent.emit as any).calls.mostRecent().args[0];
    expect(arg).toEqual(id);

  });

  it ('should call delete category', () => {
    const id = 200;
    component.deleteCategory(id);
    expect(categoryService.deleteCategory).toHaveBeenCalledWith(id);
  });
});
