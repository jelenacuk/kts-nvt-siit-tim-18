import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Category } from '../../model/category.model';
import { CategoryService } from '../../service/category.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ConfirmationDialogComponent } from 'src/app/app-common/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  categories: Category[] = [];
  dataSource: MatTableDataSource<Category>;
  displayedColumns = ['name', 'color', 'icon', 'update', 'delete'];

  @Output() addCategoryEvent = new EventEmitter<number>();

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private categoryService: CategoryService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getCategories();

  }

  getCategories() {
    this.categoryService.getAllCategories().subscribe(
      (response: Category[]) => {
        this.categories = response;
        this.dataSource = new MatTableDataSource(this.categories);

      });
  }

  getImage(path: string): string {
    return 'http://localhost:8080/' + path;
  }

  newCategory() {
    this.addCategoryEvent.emit(null);
  }

  delete(category: Category) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
     data: 'Are you sure you want to delete category ' + category.name + '?'
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result === true ) {
        this.deleteCategory(category.id);
       }

    });

  }

  deleteCategory(id: number) {
    this.categoryService.deleteCategory(id).subscribe(
      (response => {
        this.snackBar.open('Category deleted', '', { duration: 3000,
          panelClass: ['snack-bar']
        });
        this.getCategories();
      }), (error => {
        this.snackBar.open(error.error.message, '', { duration: 4000,
          panelClass: ['error-snack-bar']
        });
      }));
  }

  update(id: number) {
    this.addCategoryEvent.emit(id);
  }

}
