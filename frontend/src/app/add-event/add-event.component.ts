import { Component, OnInit } from '@angular/core';
import { AddEvent } from '../model/add-event.model';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Category } from '../model/category.model';
import { Location } from '../model/location.model';
import { Hall } from '../model/hall.model';
import { Section } from '../model/section.model';
import { EventService } from '../service/event.service';
import { CategoryService } from '../service/category.service';
import { LocationService } from '../service/location.service';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})

export class AddEventComponent implements OnInit {

  newEvent: AddEvent = new AddEvent();
  addEventForm: FormGroup;
  datePick: Date;
  categoryPick: Category;
  chosenLocation: string;
  foundLocation: Location;
  chosenHall: Hall;
  chosenSection: Section;
  sectionPrice: number;

  categories: Category[];
  locationNames: string[];


  constructor(private eventService: EventService,
              private categoryService: CategoryService, private locationService: LocationService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {

    this.getCategories();
    this.getLocationNames();

  }

  // Adds chosen date to newEvent date array.
  addDate() {
    if (this.datePick) {
      // Checks if new date is in the past.
      if (this.datePick.getTime() < new Date().getTime()) {
        this.snackBar.open('Event date must be in the future.');
        return;
      }
      if (!this.newEvent.date.includes(this.datePick)) {
        this.newEvent.date.push(this.datePick);
      } else {
        this.snackBar.open('Date already added.');
      }
    } else {
      this.snackBar.open('No date picked.');
    }

  }

  // Removes chosen date from newEvent date array.
  removeDate(i: number) {
    this.newEvent.date.splice(i, 1);
  }

  // Gets Categories from the backend.
  getCategories() {
    this.categoryService.getAllCategories().subscribe(
      (response => {

        if (response != null) {
          this.categories = response;
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  // Get Location names from backend.
  getLocationNames() {
    this.locationService.getAllLocationNames().subscribe(
      (response => {

        if (response != null) {
          this.locationNames = response;
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  // Get Location from backend, by chosen name.
  getChosenLocation() {
    this.chosenHall = null;
    this.newEvent.hallID = null;
    this.chosenSection = null;
    this.newEvent.eventSections = new Map();

    this.locationService.getLocationByName(this.chosenLocation).subscribe(
      (response => {

        if (response != null) {
          this.foundLocation = response;
          this.newEvent.locationID = response.id;
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  getChosenHall() {
    this.newEvent.hallID = this.chosenHall.id;
  }

  openInput() {
    document.getElementById('fileInput').click();
  }
  fileChange(files) {
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.newEvent.background = reader.result.toString();
    };
    this.snackBar.open('Successfully uploaded background.');
    reader.onerror = error => {
    };
  }

  openAttachmentInput() {
    document.getElementById('attachmentInput').click();
  }
  attachmentChange(files) {
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.newEvent.attachments.push(reader.result.toString());
    };

    this.snackBar.open('Successfully uploaded attachment.');
    reader.onerror = error => {
    };
  }

  // Removes chosen attachment from newEvent attachments array.
  removeAttachment(i: number) {
    this.newEvent.attachments.splice(i, 1);
  }

  // Adds chosen Category id to newEvent categories array.
  addCategory() {
    if (this.categoryPick) {

      if (!this.newEvent.categories.includes(this.categoryPick)) {
        this.newEvent.categories.push(this.categoryPick);
      } else {
        this.snackBar.open('Category already added.');
      }
    } else {
      this.snackBar.open('No category picked.');
    }

  }

  // Removes chosen date from newEvent date array.
  removeCategory(i: number) {
    this.newEvent.categories.splice(i, 1);
  }

  // Adds chosen section id and price to newEvent eventSections map.
  addSection() {
    if (this.chosenSection && this.sectionPrice && this.sectionPrice > 0) {

      if (!this.newEvent.eventSections.has(this.chosenSection.id)) {
        this.newEvent.eventSections.set(this.chosenSection.id, this.sectionPrice);
      } else {
        this.snackBar.open('Section already added.');
      }
    } else {
      if (!this.chosenSection) {
        this.snackBar.open('No section picked.');
      } else {
        if (this.sectionPrice < 0) {
          this.snackBar.open('Price must be positive number.');
        } else {
          this.snackBar.open('No price picked.');
        }
      }
    }

  }

  // Removes chosen section from newEvent eventSections map.
  removeSection(i: number) {
    this.newEvent.eventSections.delete(i);
  }

  // Gets newEvent eventSections Map Keys, and removes Iterator.
  // (Iterator was causing some bug).
  getKeys(): number[] {
    return [... this.newEvent.eventSections.keys()];
  }

  getEventSections(): string {
    let es: string;
    es = '({';
    let i = 1;
    this.getKeys().forEach(k => {
      es += '"' + k + '"' + ':' + this.newEvent.eventSections.get(k);
      if (i < this.newEvent.eventSections.size) {
        es += ',';
        i++;
      }
    });

    es += '})';
    return es;
  }

  // Checks if everything is picked and dates are in the future.
  checkAddEvent(): boolean {

    // Checks if last day to pay is negative.
    if (this.newEvent.lastDayToPay < 0) {
      this.snackBar.open('Last day to pay must be positive number.');
      return false;
    }

    // Checks if sales date is in the future.
    if (this.newEvent.salesDate.getTime() < new Date().getTime()) {
      this.snackBar.open('Sales date must be in the future.');
      return false;
    }

    // Checks if date is picked.
    if (this.newEvent.date.length === 0) {
      this.snackBar.open('No date picked.');
      return false;
    }

    // Checks if controll access is picked.
    if (!this.newEvent.controllAccess) {
      this.snackBar.open('Choose control access option.');
      return false;
    }

    // Checks if category is picked.
    if (this.newEvent.categories.length === 0) {
      this.snackBar.open('Category is not picked.');
      return false;
    }

    // Checks if location is picked.
    if (!this.newEvent.locationID) {
      this.snackBar.open('Location is not picked.');
      return false;
    }

    // Checks if hall is picked.
    if (!this.newEvent.hallID) {
      this.snackBar.open('Hall is not picked.');
      return false;
    }

    // Checks if section is picked.
    if (this.newEvent.eventSections.size === 0) {
      this.snackBar.open('Section is not picked.');
      return false;
    }

    return true;
  }

  // Adds new event to the system.
  addEvent() {
    if (this.checkAddEvent()) {
      console.log(this.newEvent);
      // Sets background name to be event name. (because event name is unique.)
      this.newEvent.backgroundName = this.newEvent.name;

      this.eventService.addEvent(this.newEvent, this.getEventSections()).subscribe(
        (response => {
          if (response != null) {
            this.snackBar.open('Event ' + this.newEvent.name + ' successfully added.', '', {
              panelClass: ['success-snack-bar', 'snackBar']
            });
            this.newEvent = new AddEvent();
            this.datePick = null;
            this.categoryPick = null;
            this.chosenLocation = null;
            this.foundLocation = null;
            this.chosenHall = null;
            this.chosenSection = null;
            this.sectionPrice = null;
          }
        }),
        (error => {
          this.snackBar.open(error.error.message, '', {
            panelClass: ['add-event-error-snack-bar', 'snackBar']
          });
        }));
    }
  }
}
