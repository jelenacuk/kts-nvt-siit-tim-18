import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { PayPal } from '../model/paypal.model';

@Injectable({ providedIn: 'root' })
export class PayPalResolver implements Resolve<PayPal> {
  constructor() {}

  resolve( activatedRoute: ActivatedRouteSnapshot) {
    const paymentId = activatedRoute.queryParamMap.get('paymentId');
    let paypal = new PayPal();

    if ( paymentId ) {
        const payerId = activatedRoute.queryParamMap.get('PayerID');
        const cancelIfFail = activatedRoute.queryParamMap.get('cancelIfFail');
        paypal = new PayPal(paymentId, payerId, cancelIfFail === 'true');

    }

    return paypal;
  }
}
