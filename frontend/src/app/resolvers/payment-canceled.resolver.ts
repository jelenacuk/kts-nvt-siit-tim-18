import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { PaymentCanceled } from '../model/payment-canceled.model';

@Injectable({ providedIn: 'root' })
export class PaymentCanceledResolver implements Resolve<PaymentCanceled> {
  constructor() {}

  resolve( activatedRoute: ActivatedRouteSnapshot) {

    const paymentCanceled = activatedRoute.queryParamMap.get('paymentCanceled') ? true : false;
    const deleteReservations = activatedRoute.queryParamMap.get('deleteReservations') === 'true' ? true : false;
    const canceled: PaymentCanceled = new PaymentCanceled(paymentCanceled, deleteReservations);

    return canceled;
  }
}
