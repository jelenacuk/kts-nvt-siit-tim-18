import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Event } from '../../model/event.model';
import { ConstantsService } from '../../service/constants.service';
import { PageEvent, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocationService } from '../../service/location.service';
import { ShowOnMapComponent } from '../../location/show-on-map/show-on-map.component';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})

export class EventCardComponent implements OnInit {

  @Input() events: Array<Event>;
  @Input() page: PageEvent;
  @Output() messageEvent = new EventEmitter<PageEvent>();

  constructor(private constants: ConstantsService,
              private locationService: LocationService,
              private router: Router,
              public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  showOnMap(locationName: string) {
    this.locationService.showOnMap(locationName);
  }

  // Formats date for output.
  formatDate(date: Date): string {
    // This line is neccessary to avoid missing Date method bug.
    const d = new Date(date);
    const month = d.getMonth() + 1;
    return '' + d.getDate() + '/' + month + '/' + d.getFullYear();
  }

  getPicture(picture: string): string {
    return this.constants.localhost + picture;
  }

  sendPage($event) {
    console.log('Sent');
    this.page = $event;
    console.log(this.page);
    this.messageEvent.emit(this.page);
  }

  showDetails(id: number) {
    this.router.navigateByUrl('/show-event/' + id);
  }
}
