import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './users/register/register.component';
import { LoginComponent } from './users/login/login.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { ShowEventComponent } from './show-event/show-event.component';
import { AdminActivateGuard } from './guards/admin-activate.guard';
import { UpdateEventComponent } from './update-event/update-event.component';
import { HomeComponent } from './home/home/home.component';
import { PayPalResolver } from './resolvers/paypal.resolver';
import { UserActivateGuard } from './guards/user-activate.guard';
import {  PaymentCanceledResolver } from './resolvers/payment-canceled.resolver';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'user-profile', component: UserProfileComponent, canActivate: [UserActivateGuard],
                        resolve: { paypal: PayPalResolver, paymentCanceled:  PaymentCanceledResolver}},
  {path: 'admin-profile', component: AdminProfileComponent, canActivate: [AdminActivateGuard]},
  {path: 'show-event/:id', component: ShowEventComponent},
  {path: 'update-event/:id', component: UpdateEventComponent, canActivate: [AdminActivateGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
