import { TestBed, async, inject } from '@angular/core/testing';

import { AdminActivateGuard } from './admin-activate.guard';

describe('AdminActivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminActivateGuard]
    });
  });

  it('should ...', inject([AdminActivateGuard], (guard: AdminActivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
