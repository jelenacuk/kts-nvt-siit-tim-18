import { AbstractControl, ValidatorFn } from '@angular/forms';

export enum ValidateDate {
    BEFORE,
    AFTER
}

// validation function
export function validateDate(compareTo: Date, direction: ValidateDate): ValidatorFn {
    return (c: AbstractControl) => {
        let isValid: boolean;

        if (compareTo === undefined) {
            return null;
        } else if (direction === ValidateDate.BEFORE) {
            isValid = c.value < compareTo;
        } else {
            isValid = c.value > compareTo;
        }

        if (isValid) {
            return null;
        } else {
            return {
                invalidDate: {
                    valid: false
                }
            };
        }
    };
}


