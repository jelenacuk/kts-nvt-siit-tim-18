import {
    ValidationErrors,
    ValidatorFn,
    AbstractControl
} from '@angular/forms';

export function compareDates(startDate: AbstractControl, endDate: AbstractControl): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        if (!startDate.value || !endDate.value) {
            return null;
        }

        if (startDate.value < endDate.value) {
            return null;
        } else {
            return { compareDates: { valid: false } };
        }
    };
}
