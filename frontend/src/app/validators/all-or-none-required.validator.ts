import {
    ValidationErrors,
    ValidatorFn,
    Validators,
    AbstractControl
  } from '@angular/forms';

export function allOrNoneRequired(...controls: AbstractControl[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if ( controls.every(required) || controls.every(notRequired)) {
        return null;
      }
      return { allOrNoneRequired: { valid: false } };
    };
  }

function required(control: AbstractControl) {
    return Validators.required(control);
}

function notRequired(control: AbstractControl) {
    return !Validators.required(control);
}
