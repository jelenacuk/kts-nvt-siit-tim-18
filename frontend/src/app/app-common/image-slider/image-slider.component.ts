import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.scss']
})
export class ImageSliderComponent implements OnInit {

  @Input() firstImage: string;
  @Input() imagePaths: string[] = [];
  numbers: number[];

  constructor() { }

  ngOnInit() {
    this.numbers = Array(this.imagePaths.length ).fill(0).map((x, i) => i);
  }

  getImage(path: string): string {
    return 'http://localhost:8080' + path;
  }

}
