import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Form, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Input() loggedIn: boolean;
  @Input() homepage: boolean;
  @Output()
  sendSearchData = new EventEmitter<string>();
  generalSearchForm: FormGroup;

  constructor(public router: Router,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    console.log(this.loggedIn);
    if (this.homepage) {
      this.createForm();
    }
  }

  createForm() {
      this.generalSearchForm = this.formBuilder.group({
        searchData: ['', []]
      });
  }

  generalSearch() {
    this.sendSearchData.emit(this.generalSearchForm.controls.searchData.value as string);
  }

  onLogOut() {
    localStorage.setItem('token', '');
    localStorage.setItem('role', '');
    this.router.navigate(['/login']);
  }

  goToProfile() {
    const role = localStorage.getItem('role');
    if (role === 'REGISTERED') {
      this.router.navigateByUrl('/user-profile');
    } else if (role === 'ADMIN') {
      this.router.navigateByUrl('/admin-profile');
    }
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  goToLogIn() {
    this.router.navigate(['/login']);
  }

  goToRegister() {
    this.router.navigate(['/register']);
  }

}
