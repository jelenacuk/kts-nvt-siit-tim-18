import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from 'src/app/model/category.model';

@Component({
  selector: 'app-categories-toolbar',
  templateUrl: './categories-toolbar.component.html',
  styleUrls: ['./categories-toolbar.component.scss']
})
export class CategoriesToolbarComponent implements OnInit {

  @Input()
  categories: Category[] =  [];

  @Output()
  sendCategoryChoosen = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  categoryChoosen(categoryId: number) {
    this.sendCategoryChoosen.emit(categoryId);
  }

  getImage(path: string): string {
    return 'http://localhost:8080/' + path;
  }

}
