import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';

import { NavbarComponent } from './navbar/navbar.component';
import { CategoriesToolbarComponent } from './categories-toolbar/categories-toolbar.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';

@NgModule({
  declarations: [
    NavbarComponent,
    CategoriesToolbarComponent,
    ConfirmationDialogComponent,
    ImageSliderComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    NavbarComponent,
    CategoriesToolbarComponent,
    ConfirmationDialogComponent,
    ImageSliderComponent
  ],
   entryComponents: [ConfirmationDialogComponent ],
})
export class AppCommonModule { }
