import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from '../service/event.service';
import { MatSnackBar } from '@angular/material';
import { Event } from '../model/event.model';
import { Category } from '../model/category.model';
import { CategoryService } from '../service/category.service';
import { ConstantsService } from '../service/constants.service';
import { Attachment } from '../model/attachment.model';

@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.scss']
})
export class UpdateEventComponent implements OnInit {

  event: Event;

  datePick: Date;
  datesDTO: Array<number> = new Array();
  categoryPick: Category;
  categories: Category[];


  constructor(private router: Router, private eventService: EventService,
              private snackBar: MatSnackBar, private categoryService: CategoryService,
              private constants: ConstantsService) { }

  ngOnInit() {

    const splited = this.router.url.split('/');
    const id = splited[splited.length - 1];
    this.eventService.getEvent(+id).subscribe(
      (response) => {
        if (response != null) {
          this.event = response;
          for (const date of response.date) {
            const d: Date = new Date(date);
            this.datesDTO.push(d.getTime());
          }
        }
      }
    );
    this.getCategories();
  }

  updateEventName() {
    const updateEventDTO = { name: this.event.name, id: this.event.id};
    this.eventService.updateEventName(updateEventDTO).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('Name successfully updated!');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  updateEventDescription() {
    const updateEventDTO = {description: this.event.description, id: this.event.id};
    this.eventService.updateEventDescription(updateEventDTO).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('Description successfully updated!');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  changeLastDayToPay() {
    const updateEventDTO = {lastDayToPay: this.event.lastDayToPay, id: this.event.id};
    this.eventService.changeEventLastDayToPay(updateEventDTO).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('LastDayToPay successfully updated!');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  blockEvent() {
    this.eventService.blockEvent(this.event.id).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('Event is blocked!');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      })
    );
  }

  changeEventControllAccess() {
    this.eventService.changeEventControllAccess(this.event.id).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('ControllAccess successfuly changed!');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  addDate() {
    if (this.datePick) {
      // Checks if new date is in the past.
      if (this.datePick.getTime() < new Date().getTime()) {
        this.snackBar.open('Event date must be in the future.');
        return;
      }
      if (!this.datesDTO.includes(this.datePick.getTime())) {
        this.event.date.push(this.datePick);
        this.datesDTO.push(this.datePick.getTime());
      } else {
        this.snackBar.open('Date already added.');
      }
    } else {
      this.snackBar.open('No date picked.');
    }
  }

  removeDate(i: number) {
    this.event.date.splice(i, 1);
    this.datesDTO.splice(i, 1);
    this.snackBar.open('Date removed');
  }

  changeEventDate() {
    const dto = {date: this.datesDTO, id: this.event.id};
    this.eventService.changeEventDate(dto).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('Dates successfuly changed!');
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }


  addCategory() {
    if (this.categoryPick) {
      if (!this.event.categories.includes(this.categoryPick)) {
        const dto = {categoryId: this.categoryPick.id, eventId: this.event.id};
        this.eventService.addEventCategory(dto).subscribe(
          (response => {
            if (response) {
              this.event.categories.push(this.categoryPick);
              this.snackBar.open('Category successfuly updated!');
            }
          }),
          (error => {
            this.snackBar.open(error.error.message);
          }));
      } else {
        this.snackBar.open('Category already added.');
      }
    } else {
      this.snackBar.open('No category picked.');
    }

  }

  // Removes chosen date from newEvent date array.
  removeCategory(i: number) {

    const dto = {categoryId: this.categories[i].id, eventId: this.event.id};
    this.eventService.removeEventCategory(dto).subscribe(
      (response => {
        if (response) {
          this.event.categories.splice(i, 1);
          this.snackBar.open('Category successfully removed!');
        }
      }),
      (error => {
          this.snackBar.open(error.error.message);
      }));
  }

  // Gets Categories from the backend.
  getCategories() {
    this.categoryService.getAllCategories().subscribe(
      (response: Category[]) => {

        if (response != null) {
          this.categories = response;
        }
      });
  }
  openInput() {
    document.getElementById('fileInput').click();
  }
  changeTicketBackground(files) {
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.event.background = reader.result.toString();
      const dto = new Attachment(this.event.id,  this.event.background );
      this.eventService.changeTicketBackground(dto).subscribe(
        (response => {
          if (response) {
            this.snackBar.open('Background changed!');
          }
        }),
        (error => {
          this.snackBar.open(error.error.message);
          console.log(error.error.message);
        }));

    };
    reader.onerror = error => {
      console.log('Error: ', error);
    };
  }

  openAttachmentInput() {
    document.getElementById('attachmentInput').click();
  }

  addAttachment(files) {
    if (files.length > 1) {
      this.snackBar.open('You can add one file at a time.');
      return;
    }
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const attachmentName = reader.result.toString();
      console.log(attachmentName);
      /*
      const dto = new Attachment(this.event.id,  attachmentName );
      this.eventService.addAttachment(dto).subscribe(
        (response => {
          if (response) {
            this.snackBar.open('Attachment added!');
            this.event.attachments.push(attachmentName);
          }
        }),
        (error => {
          this.snackBar.open(error.error.message);
          console.log(error.error.message);
        }));
        */
    };
    reader.onerror = error => {
      console.log('Error: ', error);
    };
  }
  removeAttachment(i: number) {
    const dto = new Attachment(this.event.id,  this.event.attachments[i] );
    this.eventService.removeAttachment(dto).subscribe(
      (response => {
        if (response) {
          this.snackBar.open('Attachment removed!');
          this.event.attachments.splice(i, 1);
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
        console.log(error.error.message);
      })
    );
  }

  getPicture(picture: string): string {
    return this.constants.localhost + picture;
  }
}
