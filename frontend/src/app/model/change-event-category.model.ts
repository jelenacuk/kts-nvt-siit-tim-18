export class ChangeEventCategoryDTO {

    eventId: number;
    categoryId: number;

    constructor(id: number, categoryId: number) {
        this.eventId = id;
        this.categoryId = categoryId;
    }
}