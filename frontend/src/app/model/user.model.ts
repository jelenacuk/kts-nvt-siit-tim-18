import { UserTicket } from './user-ticket.model';

export class User {
    id: number;
    username: string;
    newPassword: string;
    password: string;
    repeatedPassword: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    paypal: string;
    reservedTickets: Array<UserTicket>;
    boughtTickets: Array<UserTicket>;
}
