export class ChangeEventDateDTO {

    id: number;
    date: Array<number>;

    constructor(id: number, date: Array<number>) {
        this.id = id;
        this.date = date;
    }
}
