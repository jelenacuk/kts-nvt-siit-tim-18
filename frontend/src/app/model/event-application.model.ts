export class EventApplication {

    id: number;
    userId: number;
    eventId: number;
    queueNumber: number;
    downInterval: number;
    upInterval: number;
    eventName: string;
}
