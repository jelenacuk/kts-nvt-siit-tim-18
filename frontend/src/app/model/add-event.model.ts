import { Category } from './category.model';

export class AddEvent {

    name: string;
    description: string;
    date: Date[];
    salesDate: Date;
    applicationDate: Date;
    status: number;
    lastDayToPay: number;
    controllAccess: boolean;
    attachments: string[];
    locationID: number;
    hallID: number;
    eventSections: Map<number, number>;
    categories: Category[];
    background: string;
    backgroundName: string;

    constructor() {
        this.date = new Array();
        this.attachments = new Array();
        this.categories = new Array();
        this.eventSections = new Map();
        this.background = null;
        this.backgroundName = '';
    }

}
