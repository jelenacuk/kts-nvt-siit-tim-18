export class UserTicket {

    id: number;
    dayOfPurchase: string;
    row: number;
    column: number;
    eventSectionName: string;
    eventSectionID: number;
    price: number;
    forDay: string;
    eventName: string;
    eventID: number;
    lastDayToPay: string;
    location: string;
    bought: boolean;
    totalSize: number;
    ticketBackground: string;
}
