export class PaymentCanceled {
    canceled: boolean;
    deleteReservations: boolean;

    constructor(canceled?: boolean, deleteReservations?: boolean) {
      this.canceled = canceled;
      this.deleteReservations = deleteReservations;
    }
}