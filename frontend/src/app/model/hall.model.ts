import { Section } from './section.model';

export class Hall{

    id: number;
    name: string;
    sections: Section[];
    active: boolean;


}
    