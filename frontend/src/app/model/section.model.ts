export class Section {

    id: number;
    name: string;
    rows: number;
    columns: number;
    numerated: boolean;
    numberOfSeats: number;
    active: boolean;
    top: number;
    left: number;
    // For EventSection
    price: number;
}
