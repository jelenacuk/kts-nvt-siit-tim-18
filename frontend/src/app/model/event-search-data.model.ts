export class EventSearchData {

    eventName: string;
    city: string;
    locationName: string;
    fromDateTs: Date;
    toDateTs: Date;
    fromPrice: number;
    toPrice: number;

    constructor(obj?: any) {
        this.eventName = obj && obj.eventName || null;
        this.city = obj && obj.city || null;
        this.locationName = obj && obj.locationName || null;
        this.fromDateTs = obj && obj.fromDateTs || null;
        this.toDateTs = obj && obj.toDateTs || null;
        this.fromPrice = obj && obj.fromPrice || null;
        this.toPrice = obj && obj.toPrice || null;
    }
}
