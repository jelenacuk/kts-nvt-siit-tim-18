export class Admin {

    username: string;
    newPassword: string;
    password: string;
    repeatedPassword: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
}
