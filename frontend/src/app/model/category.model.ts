export class Category {

    id: number;
    name: string;
    color: string;
    icon: string;

    constructor(obj?: any, selectedIcon?: string) {
        this.id = obj && obj.id || null;
        this.name = obj && obj.name || '';
        this.color = obj && obj.color || '#FAEBD7';
        this.icon =  selectedIcon || '';
    }
}
