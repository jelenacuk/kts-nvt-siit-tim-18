export class PayPal {
    paymentID: string;
    payerID: string;
    cancelIfFail: boolean;
    reservationIds: number[];

    constructor(paymentId?: string, payerId?: string, cancelIfFail?: boolean, ids?: number[]) {
      this.paymentID = paymentId ? paymentId : null;
      this.payerID = payerId ? payerId : null;
      this.cancelIfFail = cancelIfFail;
      this.reservationIds = ids ? ids : null;
    }
}
