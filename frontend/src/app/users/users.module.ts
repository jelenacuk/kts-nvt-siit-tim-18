import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserTicketsComponent } from './user-ticket/user-ticket.component';
import { AppCommonModule } from '../app-common/app-common.module';
import { UserApplicationsComponent } from './user-applications/user-applications.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    UserProfileComponent,
    UserInfoComponent,
    UserTicketsComponent,
    UserApplicationsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    AppCommonModule,
    FormsModule
  ],
  exports: [
    LoginComponent,
    RegisterComponent,
    UserProfileComponent,
    UserInfoComponent,
    UserTicketsComponent,
    UserApplicationsComponent
  ]
})
export class UsersModule { }
