import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../model/user.model';
import { Admin } from '../../model/admin.model';
import { Validators, FormGroup, FormBuilder,  } from '@angular/forms';
import { UserService } from '../../service/user.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  @Input() user: User | Admin;
  editForm: FormGroup;
  currentPassword: string;
  newPassword = '';
  repeatedPassword = '';
  role: string;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.role = localStorage.getItem('role');
    this.editFormSetUp();
  }


  getUserData() {
    this.currentPassword = this.editForm.controls.currentPassword.value;
    this.repeatedPassword = this.editForm.controls.repeatedPassword.value;
    this.newPassword = this.editForm.controls.newPassword.value;
    this.user.firstName = this.editForm.controls.firstName.value;
    this.user.lastName = this.editForm.controls.lastName.value;
    this.user.phone = this.editForm.controls.phone.value;
    this.user.email = this.editForm.controls.email.value;
  }

  editUserProfile() {
    this.getUserData();
    if (this.repeatedPassword !== this.newPassword) {
      this.snackBar.open('Password is wrong repeated!');
      return;
    }
    if (this.newPassword.match(/[\<\>!@#\$%^&\*,]+/i)) {
      this.snackBar.open('Password: Illegal characters found!');
      return;
    }
    if (this.newPassword !== '' && this.newPassword.length < 4) {
      this.snackBar.open('Password must have min 4 characters!');
      return;
    }
    this.user.newPassword = this.newPassword;
    this.user.repeatedPassword = this.repeatedPassword;
    this.user.password = this.currentPassword;
    this.userService.editUserProfile(this.user).subscribe(
      (response => {
        this.snackBar.open('Successfuly updated.');
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
    this.newPassword = '';
    this.repeatedPassword = '';
  }

  editFormSetUp() {

    this.editForm = this.formBuilder.group({
       currentPassword: ['', [
        Validators.required,
        Validators.minLength(4),
      ]],
      newPassword: ['', [
      ]],
      repeatedPassword: ['', [
      ]],
      firstName: [this.user.firstName, [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern('[a-z A-Z]*')
      ]],
      lastName: [this.user.lastName, [
        Validators.required,
        Validators.maxLength(20),
        Validators.pattern('[a-z A-Z]*')
      ]],
      phone: [this.user.phone, [
        Validators.required
      ]],
      email: [this.user.email, [
        Validators.required,
        Validators.email
      ]],

    });
  }


}
