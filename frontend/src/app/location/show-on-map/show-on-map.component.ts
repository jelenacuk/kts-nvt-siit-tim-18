import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-show-on-map',
  templateUrl: './show-on-map.component.html',
  styleUrls: ['./show-on-map.component.scss']
})
export class ShowOnMapComponent implements OnInit {

  public placemarkProperties = {
    hintContent: 'Your location'
  };

  public placemarkOptions = {
    iconLayout: 'default#image',
    iconImageHref: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png',
    iconImageSize: [32, 32]
  };

  constructor(public dialogRef: MatDialogRef<ShowOnMapComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {latitude: number, longitude: number}) { }

  ngOnInit() {

  }

}
