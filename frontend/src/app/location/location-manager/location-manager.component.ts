import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-location-manager',
  templateUrl: './location-manager.component.html',
  styleUrls: ['./location-manager.component.scss']
})
export class LocationManagerComponent implements OnInit {
  selectedScreen = 0;
  locationid: number = null;
  hallId: number = null;
  sectionId: number = null;
  operation: string = null;
  constructor() { }

  ngOnInit() {
  }
  locationInfo(id: number) {
    this.locationid = id;
    this.selectedScreen = 1;
  }
  locationCreate() {
    console.log(2);
    this.operation = 'create';
    this.selectedScreen = 2;
  }
  locationBack() {
    this.selectedScreen = 0;
  }
  updateLocation() {
    this.operation = 'update';
    this.selectedScreen = 2;
  }
  hallInfo(id: number) {
    this.hallId = id;
    this.selectedScreen = 4;
  }
  createHall() {
    this.operation = 'create';
    this.selectedScreen = 5;
  }
  backToLocationInfo() {
    this.selectedScreen = 1;
  }
  updateHall() {
    this.operation = 'update';
    this.selectedScreen = 5;
  }
  createSection() {
    this.operation = 'create';
    this.selectedScreen = 8;
  }
  sectionInfo(id: number) {
    this.sectionId = id;
    this.selectedScreen = 7;
  }
  backToHallInfo() {
    this.selectedScreen = 4;
  }
  updateSection() {
    this.operation = 'update';
    this.selectedScreen = 8;
  }
  backToSectionInfo() {
    this.selectedScreen = 7;
  }

}
