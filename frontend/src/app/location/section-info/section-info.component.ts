import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Section } from '../../model/section.model';
import { Router } from '@angular/router';
import { SectionService } from '../../service/section.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-section-info',
  templateUrl: './section-info.component.html',
  styleUrls: ['./section-info.component.scss']
})
export class SectionInfoComponent implements OnInit {
  section: Section = new Section();
  @Input() sectionId: number;
  @Output() backToHallInfoEvent = new EventEmitter();
  @Output() updateSectionEvent = new EventEmitter();
  constructor(private sectionService: SectionService, private router: Router, private snackBar: MatSnackBar) { }
  ngOnInit() {
    this.sectionService.getSection(this.sectionId).subscribe(
      (response => {
        this.section = response;
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }
  back() {
    this.backToHallInfoEvent.emit();
  }
  updateSection() {
    this.updateSectionEvent.emit();
  }
  getSectionStatus(): string {
    if (this.section.active) {
      return 'Active';
    }
    return 'Not active';
  }
  deactivateSection() {
    this.sectionService.deleteSection(this.section.id).subscribe(
      (response => {
        this.section = response;
        this.snackBar.open('Section has been deactivated');
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }
  activate() {
    this.section.active = true;
    this.sectionService.updateSection(this.section).subscribe(
      (response => {
        this.snackBar.open('Section has been activated');
      }),
      (error => {
        this.snackBar.open('Error activating section');
      })
    );
  }


}
