import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Section } from '../../model/section.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SectionService } from '../../service/section.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-section-create',
  templateUrl: './section-create.component.html',
  styleUrls: ['./section-create.component.scss']
})
export class SectionCreateComponent implements OnInit {

  numerated = 'true';
  section: Section = new Section();
  sectionForm: FormGroup;
  @Input() operation: string;
  @Input() hallId: string;
  @Input() sectionId: number;
  @Output() backToSectionInfoEvent = new EventEmitter();
  @Output() backToHallInfoEvent = new EventEmitter();

  constructor(private formBuilder: FormBuilder, private sectionService: SectionService, private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.sectionForm = this.formBuilder.group({
      name: ['', [
        Validators.required
      ]],
      rows: ['', [
        Validators.required,
      ]],
      columns: ['', [
        Validators.required,
      ]],
      seats: ['', [
        Validators.required,
      ]],
      numerated: ['', [
        Validators.required,
      ]]
    });
    this.sectionForm.get('numerated').setValue('true');
    if (this.operation === 'update') {
      this.sectionService.getSection(this.sectionId).subscribe(
        (response => {
          this.section = response;
          this.setSecion();
          if (this.section.numerated) {
            this.sectionForm.get('numerated').setValue('true');
          } else {
            this.sectionForm.get('numerated').setValue('false');
          }
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    }
  }
  numeratedSelected(): boolean {
    return this.sectionForm.controls.numerated.value === 'true';
  }
  onSectionSubmit() {
    this.getSection();
    this.section.numerated = this.sectionForm.controls.numerated.value === 'true';
    this.section.active = true;
    if (!this.section.numerated) {
      this.section.rows = this.section.numberOfSeats;
      this.section.columns = 1;
    } else {
      this.section.numberOfSeats = this.section.rows * this.section.columns;
    }
    this.sectionService.createSection(+this.hallId, this.section).subscribe(
      (response => {
        this.backToHallInfoEvent.emit();
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }
  onSectionUpdate() {
    if (this.sectionForm.valid) {
      if (this.numeratedSelected()) {
        if (this.section.rows > 0 && this.section.columns > 0) {
          this.update();
        }
      } else {
        if (this.section.numberOfSeats > 0) {
          this.update();
        }
      }
    }

  }
  update() {
    this.getSection();
    this.section.numerated = this.sectionForm.controls.numerated.value === 'true';
    this.section.active = true;
    if (!this.section.numerated) {
      this.section.rows = this.section.numberOfSeats;
      this.section.columns = 1;
    } else {
      this.section.numberOfSeats = this.section.rows * this.section.columns;
    }
    this.sectionService.updateSection(this.section).subscribe(
      (response => {
        this.backToSectionInfoEvent.emit();
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      }
    );
  }

  cancel() {
    if (this.operation === 'create') {
      this.backToHallInfoEvent.emit();
    } else {
      this.backToSectionInfoEvent.emit();
    }
  }
  getSection() {
    this.section.name = this.sectionForm.controls.name.value;
    if (this.numeratedSelected()) {
      this.section.rows = this.sectionForm.controls.rows.value;
      this.section.columns = this.sectionForm.controls.columns.value;
    } else {
      this.section.numberOfSeats = this.sectionForm.controls.seats.value;
    }
  }
  setSecion() {
    this.sectionForm.get('name').setValue(this.section.name);
    this.sectionForm.get('rows').setValue(this.section.rows);
    this.sectionForm.get('columns').setValue(this.section.columns);
    this.sectionForm.get('seats').setValue(this.section.numberOfSeats);
    this.sectionForm.get('numerated').setValue(this.getNumerated());
  }
  getNumerated(): string {
    if (this.section.numerated) {
      return 'true';
    }
    return 'false';
  }

}
