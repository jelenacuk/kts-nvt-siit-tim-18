import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { LocationService } from '../../service/location.service';
import { Router } from '@angular/router';
import { Location } from '../../model/location.model';
import { MatTableDataSource, MatSnackBar } from '@angular/material';
import { Hall } from '../../model/hall.model';
import { HallService } from '../../service/hall.service';
import { Location as BackLocation } from '@angular/common';

@Component({
  selector: 'app-location-info',
  templateUrl: './location-info.component.html',
  styleUrls: ['./location-info.component.scss']
})
export class LocationInfoComponent implements OnInit {
  location: Location = new Location();
  dataSource: MatTableDataSource<Hall>;
  halls: Array<Hall>;
  displayedColumns = ['name', 'section_number', 'active', 'deactivate'];
  @Input() locationId: number;
  @Output() backToLocationEvent = new EventEmitter();
  @Output() updateLocationEvent = new EventEmitter();
  @Output() hallInfoEvent = new EventEmitter();
  @Output() createHallEvent = new EventEmitter();

  constructor(private locationService: LocationService, private router: Router, private hallService: HallService,
              private backLocation: BackLocation, private snackBar: MatSnackBar) { }
  ngOnInit() {
    this.locationService.getLocation(this.locationId).subscribe(
      (response => {
        this.location = response;
        this.sortById();
        this.dataSource = new MatTableDataSource(this.location.halls);
      }),
      (error) => {
        this.snackBar.open(error.error.message);
      });
  }
  deactivate(element: Hall) {
    element.active = false;
    this.hallService.deleteHall(this.location.id, element.id).subscribe(
      (response => {
        if (response) {
          element.active = false;
        }
      }),
      (error) => {
        element.active = true;
        this.snackBar.open(error.error.message);
      }
    );
  }
  activate(element: Hall) {
    element.active = true;
    this.hallService.updateHall(this.locationId, element).subscribe(
      (response => {
        this.snackBar.open('Hall has been activated');
      }),
      (error => {
        this.snackBar.open('Eror activating hall');
      })
    );
  }
  getStatus(element: Hall): string {
    if (element.active) {
      return 'Active';
    }
    return 'Not active';
  }
  info(element: Hall) {
    this.hallInfoEvent.emit(element.id);
  }
  back() {
    this.backToLocationEvent.emit();
  }
  getImage(): string {
    return 'http://localhost:8080' + this.location.image;
  }
  createHall() {
    this.createHallEvent.emit();
  }
  updateLocation() {
    this.updateLocationEvent.emit();
  }
  getLocationStatus(): string {
    if (this.location.active) {
      return 'Active';
    }
    return 'Not active';
  }
  sortById() {
    this.location.halls = this.location.halls.sort((t1, t2) => {
      if (t1.id >= t2.id) {
        return 1;
      }
      return -1;
    });
  }

}
