import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HallCreateComponent } from './hall-create.component';

describe('HallCreateComponent', () => {
  let component: HallCreateComponent;
  let fixture: ComponentFixture<HallCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HallCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HallCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
