import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Hall } from '../../model/hall.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HallService } from '../../service/hall.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-hall-create',
  templateUrl: './hall-create.component.html',
  styleUrls: ['./hall-create.component.scss']
})
export class HallCreateComponent implements OnInit {
  hall: Hall = new Hall();
  hallForm: FormGroup;
  @Input() operation: string;
  @Input() locationId: number;
  @Input() hallId: number;
  @Output() backToLocationInfoEvent = new EventEmitter();
  @Output() backToHallInfoEvent = new EventEmitter();
  constructor(private formBuilder: FormBuilder, private hallService: HallService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.hallForm = this.formBuilder.group({
      name: ['', [
        Validators.required
      ]]
    });

    if (this.operation === 'update') {
      this.hallService.getHall(this.locationId, this.hallId).subscribe(
        (response => {
          this.hall = response;
          this.hallForm.get('name').setValue(this.hall.name);
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    }
  }
  onHallSubmit() {
    if (this.hallForm.valid) {
      this.getHall();
      this.hall.active = true;
      this.hallService.createHall(this.locationId, this.hall).subscribe(
        (response => {
          this.backToLocationInfoEvent.emit();
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    }
  }
  onHallUpdate() {
    if (this.hallForm.valid) {
      this.getHall();
      this.hall.active = true;
      this.hallService.updateHall(this.locationId, this.hall).subscribe(
        (response => {
          this.backToHallInfoEvent.emit();
        }),
        (error) => {
          this.snackBar.open(error.error.message);
        }
      );
    }
  }
  cancel() {
    if (this.operation === 'create') {
      this.backToLocationInfoEvent.emit();
    } else {
      this.backToHallInfoEvent.emit();
    }
  }
  getHall() {
    this.hall.name = this.hallForm.controls.name.value;
  }

}
