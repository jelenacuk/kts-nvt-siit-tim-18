import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Event } from '../model/event.model';
import { Section } from '../model/section.model';
import { BuyTicket } from '../model/buy-ticket.model';
import { TicketService } from '../service/ticket.service';
import { fabric } from 'fabric';
import { Seat } from '../model/seat.model';
import { EventApplication } from '../model/event-application.model';
import { EventService } from '../service/event.service';


@Component({
    selector: 'app-buy-ticket',
    templateUrl: './buy-ticket.component.html',
    styleUrls: ['./buy-ticket.component.scss']
})

export class BuyTicketComponent implements OnInit {

    @Input() event: Event;
    @Input() eventSections: Section[];
    @Output() closeComponent = new EventEmitter();

    canvas;
    role: string;
    newTicket: BuyTicket = new BuyTicket();
    datePick: Date;
    sectionPick: Section;
    takenSeats: Seat[];
    selectedSeats: Seat[];
    numberOfTickets: number;
    eventApplication: EventApplication;
    ticketCount: number;

    constructor(private snackBar: MatSnackBar, private ticketService: TicketService, private eventService: EventService) {
    }

    ngOnInit() {
        this.canvas = new fabric.Canvas('canvas', {});
        this.canvas.setWidth(0);
        this.canvas.setHeight(0);
        this.newTicket.eventID = this.event.id;
        this.role = localStorage.getItem('role');
        this.selectedSeats = [];

        if (this.role === 'REGISTERED' && this.event.controllAccess) {
            this.getEventApplication();
        }

    }

    getEventApplication() {
        this.eventService.getEventApplicationByEvent(this.event.id).subscribe(
            (response => {
                if (response != null) {
                    this.eventApplication = response;
                }
            }),
            (error => {
                this.snackBar.open(error.error.message);
            }));
    }

    addEventApplication() {
        this.eventService.addEventApplication(this.event.id).subscribe(
            (response => {
                if (response != null) {
                    this.snackBar.open('Successfully added event application with number: ' + response);
                    this.getEventApplication();
                }
            }),
            (error => {
                this.snackBar.open(error.error.message);
            }));
    }

    getTicketCount(eventSectionId: number) {
        this.ticketService.getTicketCount(eventSectionId).subscribe(
            (response => {
                if (response != null) {
                    console.log(response);
                    this.ticketCount = response;
                }
            }),
            (error => {
                this.snackBar.open(error.error.message);
            }));
    }

    chooseDate() {
        // This line is neccessary to avoid missing Date method bug.
        const d = new Date(this.datePick);
        this.newTicket.forDay = d.getTime();

        if (this.sectionPick) {
            this.chooseSection();
        }
    }

    chooseSection() {
        this.newTicket.eventSectionID = this.sectionPick.id;
        if (this.datePick) {
            const d = new Date(this.datePick);
            const year = d.getFullYear();

            let month = (1 + d.getMonth()).toString();
            month = month.length > 1 ? month : '0' + month;

            let day = d.getDate().toString();
            day = day.length > 1 ? day : '0' + day;

            const stringDate = day + '-' + month + '-' + year;
            if (this.sectionPick.numerated) {
                this.ticketService.getTakenSeats(this.event.id, this.sectionPick.id, stringDate).subscribe(
                    (response => {
                        this.takenSeats = response;
                        this.drawSections();
                    })
                );
            } else {
                this.canvas.clear();
                this.canvas.setWidth(0);
                this.canvas.setHeight(0);
            }
        }

        this.selectedSeats = [];
        this.getTicketCount(this.sectionPick.id);
    }

    emitCloseComponent() {
        this.closeComponent.emit();
    }

    buyTicket() {

        const buyTickets = [];
        if (!this.sectionPick.numerated) {
            for (let i = 0; i < this.numberOfTickets; i++) {
                buyTickets.push(new BuyTicket(this.newTicket, 0, 0));
            }
        } else {
            for (const seat of this.selectedSeats) {
                buyTickets.push(new BuyTicket(this.newTicket, seat.row, seat.column));
            }
        }

        this.ticketService.buyTicket(buyTickets).subscribe(
            (response => {

                if (response != null) {
                    localStorage.setItem('buyReservationIds', JSON.stringify(response.reservationIds));
                    if (!this.sectionPick.numerated) {
                        this.ticketCount += buyTickets.length;
                    }
                    window.location = response.redirect_url;

                }
            }),
            (error => {
                this.snackBar.open(error.error.message);
            }));



        this.selectedSeats = [];
    }

    reserveTicket() {
        const reserveTickets = [];
        if (!this.sectionPick.numerated) {
            for (let i = 0; i < this.numberOfTickets; i++) {
                reserveTickets.push(new BuyTicket(this.newTicket, 0, 0));
            }
        } else {
            for (const seat of this.selectedSeats) {
                reserveTickets.push(new BuyTicket(this.newTicket, seat.row, seat.column));
            }
        }

        this.ticketService.reserveTicket(reserveTickets).subscribe(
            (response => {
                if (response != null) {
                    this.snackBar.open('Ticket for event: ' + this.event.name + ' successfully reserved.');
                    if (!this.sectionPick.numerated) {
                        this.ticketCount += reserveTickets.length;
                    }
                }
            }),
            (error => {
                this.snackBar.open(error.error.message);
            }));



        this.selectedSeats = [];
    }
    drawSections() {
        this.canvas.clear();
        this.canvas.setWidth((this.sectionPick.columns * 20) + 20);
        this.canvas.setHeight((this.sectionPick.rows * 20) + 20);
        for (let i = 0; i < this.sectionPick.rows; i++) {
            for (let j = 0; j < this.sectionPick.columns; j++) {
                let color = 'green';
                const taken = this.checkIfTaken(i, j, this.takenSeats);
                if (taken) {
                    color = 'red';
                }
                const rect = new fabric.Rect({
                    top: (i + 1) * 20, left: (j + 1) * 20,
                    width: 20, height: 20, fill: color
                });
                rect.lockMovementX = true;
                rect.lockMovementY = true;
                rect.setControlsVisibility({
                    mt: false,
                    mb: false,
                    ml: false,
                    mr: false,
                    bl: false,
                    br: false,
                    tl: false,
                    tr: false,
                    mtr: false,
                });
                rect.on('mousedown', () => {
                    if (!taken) {
                        const selected = this.checkIfTaken(i, j, this.selectedSeats);
                        if (selected) {
                            this.removeSeat(i, j, this.selectedSeats);
                            rect.setColor('green');
                            this.canvas.renderAll();
                        } else {
                            const s = new Seat();
                            s.row = i;
                            s.column = j;
                            this.selectedSeats.push(s);
                            rect.setColor('yellow');
                            this.canvas.renderAll();
                        }
                    }
                });
                this.canvas.add(rect);
            }
        }
    }
    checkIfTaken(row: number, col: number, listOfSeats: Seat[]): boolean {
        for (const seat of listOfSeats) {
            if (seat.row === row && seat.column === col) {
                return true;
            }
        }
        return false;
    }
    removeSeat(row: number, col: number, listOfSeats: Seat[]) {
        for (let i = 0; i < listOfSeats.length; i++) {
            if (listOfSeats[i].row === row && listOfSeats[i].column === col) {
                listOfSeats.splice(i, 1);
                return;
            }
        }
    }
    getSelectedSeatsString(): string {
        let output = '';
        for (const seat of this.selectedSeats) {
            output = output + '(' + (seat.row + 1) + ',' + (seat.column + 1) + '),';
        }
        return output;
    }
}
