import { Component, OnInit } from '@angular/core';
import { Event } from '../model/event.model';
import { Admin } from '../model/admin.model';
import { EventService } from '../service/event.service';
import { PageEvent, MatTabChangeEvent, MatSnackBar } from '@angular/material';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-admin-profile-card',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})

export class AdminProfileComponent implements OnInit {

  events: Event[];
  page: PageEvent = new PageEvent();
  user: Admin;
  role: string;
  selectedTab = 0;

  constructor(private eventService: EventService, private userService: UserService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.page.pageIndex = 0;
    this.page.pageSize = 3;
    this.getUser();
    this.role = localStorage.getItem('role');

  }

  receivePage($event) {
    this.page = $event;
    this.getEvents();
  }

  getEvents() {
    this.eventService.getEvents(this.page).subscribe(
      (response => {

        if (response != null) {
          this.events = response;
          if (this.events.length === 0) {
            this.page.length = 0;
          } else {
            this.page.length = this.events[0].totalSize;
          }
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  getUser() {
    this.userService.getUser().subscribe(
      (response => {
        if (response) {
          this.user = response;
        }
      }),
      (error => {
        this.snackBar.open(error.error.message);
      }));
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent) {
    this.selectedTab = tabChangeEvent.index;
    if (tabChangeEvent.index === 1) {
      this.getEvents();
    }
  }
}
