import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Event } from '../model/event.model';
import { EventService } from '../service/event.service';
import { ConstantsService } from '../service/constants.service';
import { Section } from '../model/section.model';
import { Location } from '@angular/common';
import { LocationService } from '../service/location.service';
@Component({
  selector: 'app-show-event',
  templateUrl: './show-event.component.html',
  styleUrls: ['./show-event.component.scss']
})

export class ShowEventComponent implements OnInit {

  event: Event;
  eventSections: Section[];
  role: string;
  ticketComponent: boolean;

  constructor(private snackBar: MatSnackBar,
              private router: Router,
              private eventService: EventService,
              private constants: ConstantsService,
              private backLocation: Location,
              private locationService: LocationService) {
  }

  ngOnInit() {
    this.ticketComponent = false;
    const splited = this.router.url.split('/');
    const id = splited[splited.length - 1];
    this.eventService.getEvent(+id).subscribe(
      (response => {
        console.log(response);
        if (response != null) {
          this.event = response;
          this.eventSections = new Array<Section>();
          this.getSections(response);
        }

      }
      ),
      (error => {
        this.snackBar.open(error.error.message);
      }));

    this.role = localStorage.getItem('role');

  }

  recieveCloseComponent() {
    this.ticketComponent = false;
  }

  showOnMap(locationName: string) {
    this.locationService.showOnMap(locationName);
  }

  // Formats date for output.
  formatDate(date: Date): string {
    // This line is neccessary to avoid missing Date method bug.
    const d = new Date(date);
    const month = d.getMonth() + 1;
    return '' + d.getDate() + '/' + month + '/' + d.getFullYear();
  }

  getPicture(picture: string): string {
    return this.constants.localhost + picture;
  }

  getSections(r: Event) {
    r.eventSectionsIDs.forEach(e => {
      this.eventService.getEventSection(e).subscribe(
        (response => {
          if (response != null) {
            this.eventSections.push(response);
          }
        }),
        (error => {
          this.snackBar.open(error.error.message);
        })
      );
    });
  }

  chooseSeat() {
    this.ticketComponent = true;
  }

  // Returns user to the previous page.
  getBack() {
    this.backLocation.back();
  }

  block(e: Event) {
    this.eventService.blockEvent(e.id).subscribe(
      (response => {
      if (response != null && response === true) {
        this.snackBar.open('Event successfully blocked.');
        e.status = 1;
      }
    }),
    (error => {
      this.snackBar.open(error.error.message);
    }));
  }

  update(id: number){
    this.router.navigateByUrl('/update-event/' + id);
  }
}
