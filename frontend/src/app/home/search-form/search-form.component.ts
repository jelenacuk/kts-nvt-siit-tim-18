import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '../../service/util.service';
import { allOrNoneRequired } from 'src/app/validators/all-or-none-required.validator';
import { compareDates } from 'src/app/validators/compare-dates.validator';
import { EventSearchData } from 'src/app/model/event-search-data.model';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnChanges {

  @Output()
  sendSearchParams = new EventEmitter<string>();
  @Input() searchData: EventSearchData;

  searchForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private utilService: UtilService) {
  }

  ngOnInit() {
    this.createForm();
  }

  ngOnChanges() {
    this.ngOnInit();
  }

  createForm() {
    this.searchForm = this.formBuilder.group({
      eventName: [this.searchData.eventName, []],
      city: [this.searchData.city, []],
      locationName: [this.searchData.locationName, []],
      fromDateTs: [this.searchData.fromDateTs, []],
      toDateTs: [this.searchData.toDateTs, []],
      fromPrice: [this.searchData.fromPrice, [ Validators.min(0)]],
      toPrice: [this.searchData.toPrice, [Validators.min(0)]]
    }, { updateOn: 'submit'});

    this.searchForm.setValidators([
     allOrNoneRequired(
          this.searchForm.get('fromDateTs'),
          this.searchForm.get('toDateTs'),
      ), compareDates(
        this.searchForm.get('fromDateTs'),
        this.searchForm.get('toDateTs'))
    ]);

  }

  onSearchSubmit() {
    localStorage.setItem('searchData', JSON.stringify(this.searchForm.value));
    localStorage.setItem('searchCategory', null);
    const params: string = this.utilService.generateParams(this.searchForm.value);
    this.sendSearchParams.emit(params);
  }

}
