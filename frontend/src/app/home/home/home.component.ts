import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { EventService } from '../../service/event.service';
import { CategoryService } from '../../service/category.service';
import { Event } from '../../model/event.model';
import { Category } from 'src/app/model/category.model';
import { EventSearchData } from 'src/app/model/event-search-data.model';
import { UtilService } from 'src/app/service/util.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  events: Event[];
  categories: Category[];
  page: PageEvent = new PageEvent();
  params: string;
  loggedIn: boolean;
  searchData: EventSearchData = new EventSearchData();
  searchCategory: number;
  generalSearch: string;

  constructor(private eventService: EventService,
              private categoryService: CategoryService,
              private utilService: UtilService) { }

  ngOnInit() {
    // TODO Izdvoj ovo u posebnu funkciju i pozivaj je u getEvents
    this.getCategories();
    this.loggedIn =  localStorage.getItem('token') ? true : false;
    this.checkParams();
  }

  receivePage($event) {
    this.page = $event;
    localStorage.setItem('homepagePage', JSON.stringify(this.page));
    this.checkParams();
  }

  checkParams() {
    const searchData = JSON.parse(localStorage.getItem('searchData'));
    this.searchData = searchData ? searchData : new EventSearchData();
    this.searchCategory = +localStorage.getItem('searchCategory');
    this.generalSearch = JSON.parse(localStorage.getItem('generalSearch'));
    this.page = JSON.parse(localStorage.getItem('homepagePage'));

    if (!this.page) {
      this.page = new PageEvent();
      this.page.pageIndex = 0;
      this.page.pageSize = 8;
    }

    if (this.searchCategory) {
      this.getEvents('categoryId=' + [this.searchCategory]);
    } else if (this.generalSearch) {
      this.getEvents('searchString=' + this.generalSearch);
    } else if (this.searchData) {
      this.getEvents(this.utilService.generateParams(this.searchData));
    } else {
      this.getEvents('');
    }
  }

  getEvents(params: string) {
    console.log('page ' + JSON.stringify(this.page) + ' params: ' + params);
    this.eventService.getEventsSearch(this.page, params).subscribe(
      (response: Event[]) => {

        if (response != null) {
          this.events = response;
          if (this.events.length === 0) {
            this.page.length = 0;
          } else {
            this.page.length = this.events[0].totalSize;
          }
        }
      });
  }

  getCategories() {

    this.categoryService.getAllCategories().subscribe(
      (response: Category[]) => {

        if (response != null) {
          this.categories = response;
        }
      });
  }

  sendCategoryChoosen(categoryId: number) {
    if (categoryId === -1) {
      this.setSearchData(new EventSearchData(), null, null);
      this.page.pageIndex = 0;
      this.getEvents('');
    } else {
      this.setSearchData(new EventSearchData(), categoryId, null);
      this.page.pageIndex = 0;
      this.getEvents('categoryId=' + [categoryId]);
    }
  }

  sendSearchData(searchData: string) {
    this.setSearchData(new EventSearchData(), null, searchData.trim());
    this.page.pageIndex = 0;
    this.getEvents('searchString=' + [searchData.trim()]);
  }

  sendSearchFormParams(params: string) {
    this.page.pageIndex = 0;
    this.getEvents(params);
  }

  setSearchData(newSearchData: EventSearchData, newSearchCategory: number, generalSearch: string) {

      localStorage.setItem('searchData', JSON.stringify(newSearchData));
      localStorage.setItem('searchCategory', JSON.stringify(newSearchCategory));
      localStorage.setItem('generalSearch', JSON.stringify(generalSearch));
      this.searchData = JSON.parse(localStorage.getItem('searchData'));
      this.searchCategory = JSON.parse(localStorage.getItem('searchCategory'));
      this.generalSearch = JSON.parse(localStorage.getItem('generalSearch'));
  }

}
