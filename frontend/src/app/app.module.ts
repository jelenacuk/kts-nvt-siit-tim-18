import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ConstantsService } from './service/constants.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AddEventComponent } from './add-event/add-event.component';
import { LocationService } from './service/location.service';
import { TokenInterceptorService } from './service/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { EventService } from './service/event.service';
import { CategoryService } from './service/category.service';
import { HallService } from './service/hall.service';
import { ShowEventComponent } from './show-event/show-event.component';
import { BuyTicketComponent } from './buy-ticket/buy-ticket.component';
import { UpdateEventComponent } from './update-event/update-event.component';
import { CategoriesModule } from './categories/categories.module';
import { ReportsModule } from './reports/reports.module';
import { HomeModule } from './home/home.module';
import { AppCommonModule } from './app-common/app-common.module';
import { EventsModule } from './events/events.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { UsersModule } from './users/users.module';
import { LocationModule } from './location/location.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AngularYandexMapsModule } from 'angular8-yandex-maps';

@NgModule({
  declarations: [
    AppComponent,
    AddEventComponent,
    AdminProfileComponent,
    ShowEventComponent,
    BuyTicketComponent,
    UpdateEventComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    RouterModule,
    HttpClientModule,
    CategoriesModule,
    ReportsModule,
    AppCommonModule,
    HomeModule,
    EventsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    UsersModule,
    LocationModule,
    MDBBootstrapModule,
    AngularYandexMapsModule.forRoot('18116907-79b6-47b3-97aa-0db7c335b7e0')

  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    LocationService,
    ConstantsService,
    EventService,
    CategoryService,
    HallService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
