insert into hall (id, name,active) values (10, "Hall1",true);
insert into hall (id, name,active) values (20, "Hall2",true);

insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active) values (10, "east", 100, 100, true, 10000,true);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active) values (11, "east2", 100, 100, true, 10000,true);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active) values (12, "east3", 100, 100, true, 10000,true);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active) values (20, "west", 100, 100, true, 10000,true);

insert into location (id, name, city, address, longitude, latitude, image,active) values (10, "Spens", "Novi Sad", "Sutjeska 2", 45.247167, 19.845349, "",true);
insert into location (id, name, city, address, longitude, latitude, image,active) values (20, "Beogradska arena", "Beograd", "Bulevar Arsenija Carnojevica 58", 44.814344, 20.421300, "",true);

INSERT INTO location_halls (location_id, halls_id) VALUES (10, 10);
INSERT INTO location_halls (location_id, halls_id) VALUES (20, 20);

INSERT INTO hall_sections (hall_id, sections_id) VALUES (10, 10);
INSERT INTO hall_sections (hall_id, sections_id) VALUES (10, 11);
INSERT INTO hall_sections (hall_id, sections_id) VALUES (20, 20);
INSERT INTO registered_user (id, confirmed, email, first_name, last_name, password, phone, username) VALUES (100, 1, 'pera@pera.com', 'pera', 'peric','$2a$10$7LMHVtrTCqRxJnW38x1NfemALnmu3uLO6eUG3eGSSXYdjYOYBC6Py','nema', 'pera');
INSERT INTO authority (id, name) VALUES (20, 'admin');
INSERT INTO user_authority (user_id,authority_id) values(10,20);
