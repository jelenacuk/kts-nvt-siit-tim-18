insert into hall (id, name,active) values (10, "Hall1",true);
insert into hall (id, name,active) values (11, "existing",true);
insert into hall (id, name,active) values (20, "Hall2",true);
insert into hall (id, name,active) values (21, "Hall3",true);
insert into hall (id, name,active) values (22, "Hall4",true);

insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active,top,left_position) values (10, "east", 100, 100, true, 10000,true,0,0);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active,top,left_position) values (11, "east2", 100, 100, true, 10000,true,0,0);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active,top,left_position) values (12, "east3", 100, 100, true, 10000,true,0,0);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active,top,left_position) values (20, "west", 100, 100, true, 10000,true,0,0);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats,active,top,left_position) values (22, "west", 100, 100, true, 10000,true,0,0);

insert into location (id, name, city, address, longitude, latitude, image,active) values (10, "Spens", "Novi Sad", "Sutjeska 2", 45.247167, 19.845349, "",true);
insert into location (id, name, city, address, longitude, latitude, image,active) values (20, "Beogradska arena", "Beograd", "Bulevar Arsenija Carnojevica 58", 44.814344, 20.421300, "",true);
insert into location (id, name, city, address, longitude, latitude, image,active) values (21, "Beogradska arena2", "Beograd", "Bulevar Arsenija Carnojevica 58", 44.814344, 20.421300, "",true);
insert into location (id, name, city, address, longitude, latitude, image,active) values (22, "4", "Beograd", "Bulevar Arsenija Carnojevica 58", 44.814344, 20.421300, "",true);

INSERT INTO location_halls (location_id, halls_id) VALUES (10, 10);
INSERT INTO location_halls (location_id, halls_id) VALUES (10, 11);
INSERT INTO location_halls (location_id, halls_id) VALUES (20, 20);
INSERT INTO location_halls (location_id, halls_id) VALUES (21, 21);
INSERT INTO location_halls (location_id, halls_id) VALUES (22, 22);

INSERT INTO hall_sections (hall_id, sections_id) VALUES (10, 10);
INSERT INTO hall_sections (hall_id, sections_id) VALUES (10, 11);
INSERT INTO hall_sections (hall_id, sections_id) VALUES (20, 20);
INSERT INTO hall_sections (hall_id, sections_id) VALUES (22, 22);

INSERT INTO registered_user (id, confirmed, email, first_name, last_name, password, phone, username) VALUES (10, 1, 'pera@pera.com', 'pera', 'peric','$2a$10$7LMHVtrTCqRxJnW38x1NfemALnmu3uLO6eUG3eGSSXYdjYOYBC6Py','nema', 'pera');
INSERT INTO authority (id, name) VALUES (20, 'ADMIN');
INSERT INTO user_authority (user_id,authority_id) values(10,20);

INSERT INTO event (id, controll_access, description, last_day_to_pay, name, sales_date, status, ticket_background, hall_id, location_id)
	values (100, true, 'Novi Sad- omladinska prestonica Evrope.', 3, 'OPENS2019', '2020-03-01 20:00:00', 0, 'background', 22, 22);
insert into event_date (event_id, date) values (100, '2021-03-06 20:00:00');
