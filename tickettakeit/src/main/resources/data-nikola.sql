/* Adds categories */
INSERT INTO `tickettakeit`.`category` (`id`, `color`, `icon`, `name`) VALUES ('100', 'blue', 'Test Icon 1', 'Music Festival');
INSERT INTO `tickettakeit`.`category` (`id`, `color`, `icon`, `name`) VALUES ('200', 'red', 'Test Icon 2', 'Art Exibition');

/* Adds locations */
INSERT INTO `tickettakeit`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('100', 'Address 1', 'Novi Sad', 'Test Image 1', '19.859807', '45.255826', 'Location 1', TRUE);
INSERT INTO `tickettakeit`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('200', 'Address 2', 'Belgrade', 'Test Image 2', '20.455915', '44.788317', 'Location 2', TRUE);

/* Adds halls */
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('100', 'Hall 1', TRUE);
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('200', 'Hall 2', TRUE);
INSERT INTO `tickettakeit`.`hall` (`id`, `name`, `active`) VALUES ('300', 'Hall 3', TRUE);

/* Adds sections */
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('100', '10', TRUE, 'Section 1', '100', '10',TRUE,0,0);
INSERT INTO `tickettakeit`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`,`active`,`left_position`,`top`) VALUES ('200', '0', FALSE, 'Section 2', '100', '0',TRUE,0,0);

/* Adds halls to locations */
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('100', '200');
INSERT INTO `tickettakeit`.`location_halls` (`location_id`, `halls_id`) VALUES ('200', '300');

/* Adds sections to halls */
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('100', '200');

/* Adds registered users */
INSERT INTO `tickettakeit`.`registered_user` (`id`, `confirmed`, `email`, `first_name`, `last_name`, `password`, `phone`, `username`, `paypal`) VALUES ('100', TRUE, 'user@mail.com', 'User', 'Useric', '$2y$12$7agwOLLBYv6.GzIxJnK3QOd/yeRMfYb1A6TIHJf.nS2UDfDOD0MJu', '03024104', 'user', 'nema');
/* Adds admin user */
INSERT INTO `tickettakeit`.`registered_user` (`id`, `confirmed`, `email`, `first_name`, `last_name`, `password`, `phone`, `username`, `paypal`) VALUES ('200', TRUE, 'admin@mail.com', 'Admin', 'Adminic', '$2y$12$7agwOLLBYv6.GzIxJnK3QOd/yeRMfYb1A6TIHJf.nS2UDfDOD0MJu', '03024104', 'admin', 'nema');

/* Adds authorities */
INSERT INTO `tickettakeit`.`authority` (`id`, `name`) VALUES ('100', 'REGISTERED');
INSERT INTO `tickettakeit`.`authority` (`id`, `name`) VALUES ('200', 'ADMIN');

/* Adds authorities to users */
INSERT INTO `tickettakeit`.`user_authority` (`user_id`, `authority_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`user_authority` (`user_id`, `authority_id`) VALUES ('200', '200');

/* Adds events, sale has began */
INSERT INTO `tickettakeit`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('100', FALSE, 'Event Desc', '5', 'Event 1', '2020-1-1', '0', '100', '100');

/* Adds categories to events */
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`event_categories` (`event_id`, `categories_id`) VALUES ('100', '200');

/* Adds dates to events */
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('100', '2020-10-10');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('100', '2020-11-10');
INSERT INTO `tickettakeit`.`event_date` (`event_id`, `date`) VALUES ('100', '2020-12-10');

/* Adds event sections */
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`) VALUES ('100', '10', TRUE, 'Section 1', '100', '200', '10');
INSERT INTO `tickettakeit`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`) VALUES ('200', '0', FALSE, 'Section 2', '100', '100', '0');

/* Adds event sections to events */
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('100', '100');
INSERT INTO `tickettakeit`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('100', '200');

/* Adds Ticket */
INSERT INTO `tickettakeit`.`ticket` (`id`, `bought`, `code`, `seat_column`, `day_of_purchase`, `for_day`, `price`, `seat_row`, `event_id`, `event_section_id`, `user_id`) VALUES ('100', TRUE, '10 10 Test', '5', '2020-1-3', '2020-11-10', '200', '5', '100', '100', '100');

/* Adds Ticket to it's User */
INSERT INTO `tickettakeit`.`user_tickets` (`user_id`, `tickets_id`) VALUES ('100', '100');

