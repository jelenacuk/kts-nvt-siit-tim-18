package com.tickettakeit.tickettakeit.controller;

import java.io.UnsupportedEncodingException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.exceptions.AccountIsNotActiveException;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserAlredyExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;
import com.tickettakeit.tickettakeit.service.UserService;

@RestController
@RequestMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {

	@Autowired
	private UserService userService;

	// LogIn method checks if user exists, generates and returns jwt token
	@PostMapping(value = "/login")
	@CrossOrigin()
	public ResponseEntity<JwtResponseDTO> createAuthenticationToken(@RequestBody LogInDTO authenticationRequest)
			throws UserNotExistsException, AccountIsNotActiveException {
		String jwt = userService.createAuthenticationToken(authenticationRequest);
		return new ResponseEntity<JwtResponseDTO>(new JwtResponseDTO(jwt), HttpStatus.OK);
	}

	@PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<UserDTO> register(@RequestBody @Valid UserDTO dto)
			throws UserAlredyExistsException, PasswordConfirmationException {

		UserDTO user = userService.register(dto);
		return new ResponseEntity<>(user, HttpStatus.OK);
		
	}

	// Recieves enoceded username and switches variable confirmed to true so user
	// can log in
	@GetMapping(value = "/registrationConfirmation/{encoded}")
	public RedirectView confirm(@PathVariable("encoded") String encoded)
			throws UnsupportedEncodingException, UserNotExistsException {
		boolean ok = userService.activateAccount(encoded);
		RedirectView view = null;
		if (ok) {
			view = new RedirectView("http://localhost:4200/login");
		}
		return view;
	}

}
