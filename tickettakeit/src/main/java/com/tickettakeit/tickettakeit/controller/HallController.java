package com.tickettakeit.tickettakeit.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.converter.HallConverter;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.exceptions.HallAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallCreatingException;
import com.tickettakeit.tickettakeit.exceptions.HallDeleteException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallUpdateException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.service.HallService;
import com.tickettakeit.tickettakeit.service.LocationService;

@RestController
public class HallController {
	@Autowired
	private HallService hallService;
	@Autowired
	LocationService locationService;

	// Gets all halls for location
	@GetMapping(value = "/api/halls/{locationID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<List<HallDTO>> getHalls(@PathVariable("locationID") Long locationID)
			throws LocationNotExistsException {
		List<HallDTO> halls = hallService.findHallsFromLocation(locationID).stream()
				.map(hall -> HallConverter.toDto(hall)).collect(Collectors.toList());
		return new ResponseEntity<>(halls, HttpStatus.OK);
	}

	// Creates hall if none exists with same name on that location
	@PostMapping(value = "/api/halls/{locationID}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<HallDTO> createHall(@PathVariable("locationID") Long locationID,@RequestBody HallDTO hallDTO)
			throws LocationNotExistsException, HallAlreadyExistsException, HallCreatingException {

		return new ResponseEntity<>(HallConverter.toDto(hallService.createHall(locationID, hallDTO)),
				HttpStatus.OK);
	}

	// Update hall
	@PutMapping(value = "/api/halls/{locationID}/{hallID}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<HallDTO> updateHall(@PathVariable("locationID") Long locationID,@PathVariable("hallID") Long hallID, @RequestBody HallDTO hallDTO)
			throws  HallNotExistsException, HallUpdateException {
		return new ResponseEntity<>(HallConverter.toDto(hallService.updateHall(locationID, hallID, hallDTO)),
				HttpStatus.OK);
	}

	// Gets one hall if exists
	@GetMapping(value = "/api/halls/{locationID}/{hallID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<HallDTO> getHall(@PathVariable("locationID") Long locationID,
			@PathVariable("hallID") Long hallID) throws  HallNotExistsException {
		return new ResponseEntity<>(HallConverter.toDto(hallService.findHallByLocationAndId(locationID, hallID)),
				HttpStatus.OK);
	}

	// Delete hall if exists
	@DeleteMapping(value = "/api/halls/{locationID}/{hallID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Boolean> deleteHall(@PathVariable("locationID") Long locationID,
			@PathVariable("hallID") Long hallID) throws HallNotExistsException, HallDeleteException {
		hallService.deleteHall(locationID, hallID);
		return new ResponseEntity<>(new Boolean(true), HttpStatus.OK);
	}

}
