package com.tickettakeit.tickettakeit.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.dto.ReportDTO;
import com.tickettakeit.tickettakeit.dto.ReportDataDTO;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.exceptions.InvalidReportTypeException;
import com.tickettakeit.tickettakeit.model.REPORT_TYPE;
import com.tickettakeit.tickettakeit.service.ReportService;

@RestController
@RequestMapping(value = "/api/reports", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReportController {

	@Autowired
	public ReportService reportService;

	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping()
	@CrossOrigin()
	public ResponseEntity<ReportDTO> getReport(@RequestParam(required = false) Long locationId,
			@RequestParam(required = false) Long eventId, @RequestParam(required = false) Long eventDateTs,
			@RequestParam() Long fromDateTs, @RequestParam() Long toDateTs, @RequestParam() String type)
			throws InvalidDateException, InvalidReportTypeException {

		Date eventDate = eventDateTs == null ? null : new Date(eventDateTs);

		Date fromDate = new Date(fromDateTs);
		Date toDate = new Date(toDateTs);

		ReportDTO reportDto = new ReportDTO(locationId, eventId, eventDate, fromDate, toDate,
				REPORT_TYPE.strToEnum(type), null);
		ArrayList<ReportDataDTO> reportData = reportService.createReports(reportDto);
		reportDto.setReportData(reportData);

		return new ResponseEntity<>(reportDto, HttpStatus.OK);

	}

}
