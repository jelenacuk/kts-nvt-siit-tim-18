package com.tickettakeit.tickettakeit.controller;

import java.io.IOException;
import java.net.MalformedURLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.DocumentException;
import com.paypal.base.rest.PayPalRESTException;
import com.tickettakeit.tickettakeit.dto.BuyTicketDTO;
import com.tickettakeit.tickettakeit.dto.PayPalDTO;
import com.tickettakeit.tickettakeit.dto.SeatDTO;
import com.tickettakeit.tickettakeit.dto.TicketDTO;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationHasPassed;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionDoesNotBelongsToTheEventException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.GivenDateDoesntExistException;
import com.tickettakeit.tickettakeit.exceptions.NoLoggedUserException;
import com.tickettakeit.tickettakeit.exceptions.NoTicketsLeftException;
import com.tickettakeit.tickettakeit.exceptions.NotYourTimeYet;
import com.tickettakeit.tickettakeit.exceptions.ReservationNotAvailableException;
import com.tickettakeit.tickettakeit.exceptions.SaleHasNotYetBeganException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsBeyondBoundariesException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.TicketIsBoughtException;
import com.tickettakeit.tickettakeit.exceptions.TicketNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserDoesNotHaveTheTicketException;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.service.TicketService;

@RestController
@RequestMapping(value = "/api/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
public class TicketController {
	@Autowired
	TicketService ticketService;

	// Returns ticket for given ticket ID. If no ticket is found throws
	// TicketNotExistsException
	@GetMapping(value = "/{ticketID}")
	@CrossOrigin()
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<TicketDTO> getTicket(@PathVariable("ticketID") Long ticketID)
			throws TicketNotExistsException {

		TicketDTO foundTicket = ticketService.getTicket(ticketID);

		return new ResponseEntity<>(foundTicket, HttpStatus.OK);
	}

	@GetMapping(value = "/get-taken-seats/{eventId}&{sectionId}&{stringDate}")
	@CrossOrigin()
	public @ResponseBody ResponseEntity<List<SeatDTO>> getTakenSeats(@PathVariable("eventId") Long eventId,
			@PathVariable("sectionId") Long sectionId, @PathVariable("stringDate") String stringDate) {
		List<SeatDTO> seats = ticketService.getTakenSeats(eventId, sectionId, stringDate);

		return new ResponseEntity<>(seats, HttpStatus.OK);
	}

	// Buys new ticket -> paypal create payment
	@PostMapping(value = "/buy-ticket", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<Map<String, Object>> buyTicketCreatePayment(
			@RequestBody @Valid List<BuyTicketDTO> buyTicketDtos)
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException, SeatIsTakenException,
			SeatIsBeyondBoundariesException, NoTicketsLeftException, SalesDateHasPassedException,
			EventIsBlockedException, SaleHasNotYetBeganException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, ReservationNotAvailableException, PayPalRESTException,
			NotYourTimeYet, EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {

		Map<String, Object> response = ticketService.buyTicketsCreatePayment(buyTicketDtos);

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	// Buys new ticket -> paypal complete payment
	@PutMapping(value = "/buy-ticket-complete", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<Map<String, Object>> buyTicketCompletePayment(
			@Valid @RequestBody PayPalDTO paypalDto) throws DocumentException, IOException, EventIsBlockedException,
			SalesDateHasPassedException, SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException,
			PayPalRESTException, NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {

		Map<String, Object> result = null;

		try {
			// if payment completion fails ...
			result = ticketService.buyReservedTicketCompletePayment(paypalDto);
		} catch (Exception e) {
			for (Long id : paypalDto.getReservationIds()) {
				// ... delete all reservations
				ticketService.cancelTicketReservation(id);
			}
		}

		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);

	}

	// Reserves new Ticket
	@PostMapping(value = "/reserve-ticket", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<List<Long>> reserveTicket(@RequestBody @Valid List<BuyTicketDTO> buyTicketDtos)
			throws NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			ReservationNotAvailableException, SeatIsTakenException, NoTicketsLeftException, EventIsBlockedException,
			SalesDateHasPassedException, GivenDateDoesntExistException, SaleHasNotYetBeganException,
			EventSectionDoesNotBelongsToTheEventException, SeatIsBeyondBoundariesException, NotYourTimeYet,
			EventApplicationHasPassed, EventApplicationNotFoundForGivenUser {

		List<Ticket> tickets = new ArrayList<>();
		for (BuyTicketDTO dto : buyTicketDtos) {
			Ticket t = ticketService.reserveTicket(dto);
			tickets.add(t);
		}

		List<Long> ids = tickets.stream().map(Ticket::getId).collect(Collectors.toList());

		return new ResponseEntity<List<Long>>(ids, HttpStatus.OK);

	}

	// Confirms ticket reservation -> paypal create payment
	@PutMapping(value = "/buy-reserved-ticket/{ticketID}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<Map<String, Object>> buyReservedTicketCreatePayment(
			@PathVariable("ticketID") Long ticketID) throws MalformedURLException, DocumentException, IOException,
			NoLoggedUserException, TicketNotExistsException, EventIsBlockedException, SalesDateHasPassedException,
			SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException, PayPalRESTException {

		List<Long> ids = new ArrayList<>();
		ids.add(ticketID);

		Map<String, Object> result = ticketService.buyReservedTicketCreatePayment(ids);

		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);

	}

	// Confirms ticket reservation -> paypal complete payment
	@PutMapping(value = "/buy-reserved-ticket-complete", consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('REGISTERED')")
	public @ResponseBody ResponseEntity<Map<String, Object>> buyReservedTicketCompletePayment(
			@Valid @RequestBody PayPalDTO paypalDto) throws MalformedURLException, DocumentException, IOException,
			NoLoggedUserException, TicketNotExistsException, EventIsBlockedException, SalesDateHasPassedException,
			SaleHasNotYetBeganException, UserDoesNotHaveTheTicketException, PayPalRESTException {

		Map<String, Object> result = ticketService.buyReservedTicketCompletePayment(paypalDto);
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);

	}

	// Deletes Ticket reservation with given ID.
	@PutMapping(value = "/cancel-ticket-reservation/{ticketIds}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public @ResponseBody ResponseEntity<Boolean> cancelTicket(@PathVariable("ticketIds") List<Long> ticketIds)
			throws NoLoggedUserException, TicketNotExistsException, TicketIsBoughtException {

		for (Long id : ticketIds) {
			ticketService.cancelTicketReservation(id);
		}

		return new ResponseEntity<>(true, HttpStatus.OK);
	}

	// Gets number of Tickets for given EventSection.
	@GetMapping(value = "/get-ticket-count/{eventSectionId}")
	@CrossOrigin()
	public @ResponseBody ResponseEntity<Integer> getTicketCount(@PathVariable("eventSectionId") Long eventSectionId) {

		int ticketCount = ticketService.countSoldTickets(eventSectionId);

		return new ResponseEntity<>(ticketCount, HttpStatus.OK);
	}

}
