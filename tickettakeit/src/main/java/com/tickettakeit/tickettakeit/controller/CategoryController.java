package com.tickettakeit.tickettakeit.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.converter.CategoryConverter;
import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.ObjectCannotBeDeletedException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.service.CategoryService;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	 
	@GetMapping()
	public ResponseEntity<List<CategoryDTO>> getCategories(){
		List<CategoryDTO> categoryDTOList = categoryService.findAll().stream().map(category -> {
			return CategoryConverter.convertToCategoryDTO(category);
		}).collect(Collectors.toList());

		return new ResponseEntity<>(categoryDTOList, HttpStatus.OK);
	} 
	
	@GetMapping(value="/{categoryId}")
	public ResponseEntity<CategoryDTO> getCategory(@PathVariable("categoryId") Long categoryId) throws CategoryNotExistsException{
		
		CategoryDTO categoryDto = CategoryConverter.convertToCategoryDTO(categoryService.findById(categoryId)); 
		return new ResponseEntity<>(categoryDto, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CategoryDTO> addCategory(@RequestBody @Valid CategoryDTO categoryDto) throws CategoryAlreadyExistsException, FileNotFoundException, IOException{

		Category newCategory = categoryService.addCategory(categoryDto);
		return new ResponseEntity<>(CategoryConverter.convertToCategoryDTO(newCategory), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@PutMapping(value="/{categoryId}", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CategoryDTO> updateCategory(@RequestBody @Valid CategoryDTO categoryDto) throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException{
		
		Category updatedCategory = categoryService.updateCategory(categoryDto);
		return new ResponseEntity<>(CategoryConverter.convertToCategoryDTO(updatedCategory), HttpStatus.OK);
				
	} 
	
	@PreAuthorize("hasAuthority('ADMIN')")
	@DeleteMapping(value="/{categoryId}")
	public ResponseEntity<Boolean> deleteCategory(@PathVariable("categoryId") Long categoryId) throws CategoryNotExistsException, ObjectCannotBeDeletedException{
		
		categoryService.deleteCategory(categoryId);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}
	
	
}
