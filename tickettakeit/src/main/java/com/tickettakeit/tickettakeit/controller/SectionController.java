package com.tickettakeit.tickettakeit.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.converter.SectionConverter;
import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.SectionAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionCreatingException;
import com.tickettakeit.tickettakeit.exceptions.SectionDeleteException;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionUpdateException;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.service.SectionService;

@RestController
public class SectionController {
	@Autowired()
	private SectionService sectionService;

	// creates new section if there is no other section with same name
	@PostMapping(value = "/api/sections/{hallID}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<SectionDTO> createSection(@PathVariable("hallID") Long hallID,
			@RequestBody SectionDTO section) throws SectionAlreadyExistsException, SectionCreatingException {
		Section sec;
		sec = sectionService.createSection(hallID, section);
		SectionDTO newSection = SectionConverter.toDto(sec);
		return new ResponseEntity<>(newSection, HttpStatus.OK);
	}

	// Gets one page of sections
	@GetMapping(value = "/api/sections", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<List<SectionDTO>> getSections(Pageable pageable) {
		List<SectionDTO> sectionsDTOList = sectionService.findAll(pageable).stream().map(section -> {
			return SectionConverter.toDto(section);
		}).collect(Collectors.toList());
		return new ResponseEntity<>(sectionsDTOList, HttpStatus.OK);
	}

	// Gets one section
	@GetMapping(value = "/api/sections/{sectionID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	public ResponseEntity<SectionDTO> getSection(@PathVariable("sectionID") Long sectionID)
			throws SectionNotExistsException {
		Section s = sectionService.findOne(sectionID);
		return new ResponseEntity<>(SectionConverter.toDto(s), HttpStatus.OK);
	}

	// Update section
	@PutMapping(value = "/api/sections/{sectionID}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<SectionDTO> updateSection(@PathVariable("sectionID") Long sectionID,
			@RequestBody SectionDTO sectionDTO) throws SectionNotExistsException, SectionUpdateException {
		return new ResponseEntity<>(SectionConverter.toDto(sectionService.updateSection(sectionID, sectionDTO)),
				HttpStatus.OK);
	}

	// Update section
	@PutMapping(value = "/api/sectionsPosition", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<Boolean> updateSectionPositions(@RequestBody List<SectionDTO> sectionDTO) {
		sectionService.updateSectionPositions(sectionDTO);
		return new ResponseEntity<>(true, HttpStatus.OK);
	}

	// Delete section
	@DeleteMapping(value = "/api/sections/{sectionID}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAuthority('ADMIN')")
	public ResponseEntity<SectionDTO> deleteSection(@PathVariable("sectionID") Long sectionID)
			throws SectionNotExistsException, SectionDeleteException {
		Section section = sectionService.getSection(sectionID);
		Section s = sectionService.remove(section.getId());
		return new ResponseEntity<>(SectionConverter.toDto(s), HttpStatus.OK);
	}

}
