package com.tickettakeit.tickettakeit.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.dto.UserTicketDTO;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;
import com.tickettakeit.tickettakeit.service.UserService;

@RestController
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping(value = "/getLogUser", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	@PreAuthorize("hasAnyAuthority('REGISTERED','ADMIN')")
	// Method returns logged user info
	public @ResponseBody ResponseEntity<UserDTO> getLogUser() throws Exception {

		UserDTO dto = userService.getProfile();
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping(value = "/getUserTickets/{bought}", produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	@PreAuthorize("hasAuthority('REGISTERED')")
	// Method returns logged user info
	public @ResponseBody ResponseEntity<List<UserTicketDTO>> getUserTickets(Pageable pageable, @PathVariable("bought") boolean bought) throws Exception {
		
		List<UserTicketDTO> dto = userService.getUserTickets(pageable, bought);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PostMapping(value = "/editUserProfile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin()
	@PreAuthorize("hasAnyAuthority('REGISTERED','ADMIN')")
	// Method for changing user profile information
	public @ResponseBody ResponseEntity<Boolean> editUserProfile(@RequestBody @Valid UserDTO dto)
			throws PasswordConfirmationException, UserNotExistsException {
		boolean success = userService.editUserProfile(dto);
		return new ResponseEntity<>(success, HttpStatus.OK);
	}

}
