package com.tickettakeit.tickettakeit.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class RegisteredUser extends User {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "paypal", unique = false, nullable = true)
	private String paypal;
	@OneToMany
	private Set<Ticket> tickets = new HashSet<Ticket>();
	
	public RegisteredUser() {
		// TODO Auto-generated constructor stub
	}
	
	public RegisteredUser(String username,String email, String phone, String password, String firstName, String lastName, boolean active) {
		super(username, password, email, phone, firstName, lastName, active);
	}

	public RegisteredUser(Long id, String username, String password, String email, String phone, String firstName,
			String lastName) {
		super(id, username, password, email, phone, firstName, lastName);
	}

	public RegisteredUser(Long id, String username, String password, String email, String phone, String firstName,
			String lastName, String paypal, Set<Ticket> tickets) {
		super(id, username, password, email, phone, firstName, lastName);
		this.paypal = paypal;
		this.tickets = tickets;
	}
	
	public RegisteredUser(String username, String password, String email, String phone, String firstName,
			String lastName) {
		super(username, password, email, phone, firstName, lastName);
	}

	public String getPaypal() {
		return paypal;
	}

	public void setPaypal(String paypal) {
		this.paypal = paypal;
	}

	public Set<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(Set<Ticket> tickets) {
		this.tickets = tickets;
	}

}
