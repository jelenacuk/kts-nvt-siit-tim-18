package com.tickettakeit.tickettakeit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.tickettakeit.tickettakeit.dto.SectionDTO;

@Entity
public class Section {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	@Column(name = "seat_rows", unique = false, nullable = false)
	private int rows;
	@Column(name = "seat_columns", unique = false, nullable = false)
	private int columns;
	@Column(name = "isNumerated", unique = false, nullable = false)
	private boolean isNumerated;
	@Column(name = "numberOfSeats", unique = false, nullable = false)
	private int numberOfSeats;
	@Column(name = "active")
	private Boolean active;
	@Column(name= "leftPosition")
	private int leftPosition;
	@Column(name="top")
	private int top;
	

	public Section() {
		super();
	}

	public Section(Long id, String name, int rows, int columns, boolean isNumerated, int numberOfSeats,Boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.rows = rows;
		this.columns = columns;
		this.isNumerated = isNumerated;
		this.numberOfSeats = numberOfSeats;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public boolean isNumerated() {
		return isNumerated;
	}

	public void setNumerated(boolean isNumerated) {
		this.isNumerated = isNumerated;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public int getLeft() {
		return leftPosition;
	}

	public void setLeft(int left) {
		this.leftPosition = left;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}
	

}
