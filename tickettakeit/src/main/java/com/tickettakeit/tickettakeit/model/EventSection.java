package com.tickettakeit.tickettakeit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EventSection {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	@Column(name = "seat_rows", unique = false, nullable = false)
	private int rows;
	@Column(name = "seat_columns", unique = false, nullable = false)
	private int columns;
	@Column(name = "isNumerated", unique = false, nullable = false)
	private boolean isNumerated;
	@Column(name = "numberOfSeats", unique = false, nullable = false)
	private int numberOfSeats;
	@Column(name = "price", unique = false, nullable = false)
	private float price;
	@Column(name="left_position")
	private int left;
	@Column(name="top")
	private int top;
	public EventSection() {
		super();
	}

	public EventSection(Long id, String name, int rows, int columns, boolean isNumerated, int numberOfSeats,
			float price) {
		super();
		this.id = id;
		this.name = name;
		this.rows = rows;
		this.columns = columns;
		this.isNumerated = isNumerated;
		this.numberOfSeats = numberOfSeats;
		this.price = price;
	}

	// For testing of Event Service.
	public EventSection(String name, int rows, int columns, boolean isNumerated, int numberOfSeats, float price) {
		this.name = name;
		this.rows = rows;
		this.columns = columns;
		this.isNumerated = isNumerated;
		this.numberOfSeats = numberOfSeats;
		this.price = price;
	}

	public EventSection(Section section, Float price) {
		this.name = section.getName();
		this.rows = section.getRows();
		this.columns = section.getColumns();
		this.isNumerated = section.isNumerated();
		this.numberOfSeats = section.getNumberOfSeats();
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public boolean isNumerated() {
		return isNumerated;
	}

	public void setNumerated(boolean isNumerated) {
		this.isNumerated = isNumerated;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}
	

}
