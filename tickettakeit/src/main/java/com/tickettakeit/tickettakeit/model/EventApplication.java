package com.tickettakeit.tickettakeit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class EventApplication {
	@Id
	@GeneratedValue
	private Long id;
	@OneToOne
	private RegisteredUser user;
	@OneToOne
	private Event event;
	@Column(name = "queueNumber", unique = false, nullable = false)
	private int queueNumber;

	public EventApplication(Long id, RegisteredUser user, Event event, int queueNumber) {
		this.id = id;
		this.user = user;
		this.event = event;
		this.queueNumber = queueNumber;
	}

	public EventApplication(RegisteredUser user, Event event, int queueNumber) {
		this.user = user;
		this.event = event;
		this.queueNumber = queueNumber;
	}

	public EventApplication() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RegisteredUser getUser() {
		return user;
	}

	public void setUser(RegisteredUser user) {
		this.user = user;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public int getQueueNumber() {
		return queueNumber;
	}

	public void setQueueNumber(int queueNumber) {
		this.queueNumber = queueNumber;
	}

}
