package com.tickettakeit.tickettakeit.model;

import com.tickettakeit.tickettakeit.exceptions.InvalidReportTypeException;

public enum REPORT_TYPE {
	DAILY, WEEKLY, MONTHLY;
	
	
	public static REPORT_TYPE strToEnum(String enumStr) throws InvalidReportTypeException {
		switch(enumStr.toLowerCase()) {
		case "daily":
			return DAILY;
		case "weekly":
			return WEEKLY;
		case "monthly":
			return MONTHLY;
		default:
			throw new InvalidReportTypeException();
		}
	}
}

