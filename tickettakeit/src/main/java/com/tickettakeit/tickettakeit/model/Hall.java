package com.tickettakeit.tickettakeit.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Hall {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name", unique = false, nullable = false)
	private String name;

	@OneToMany
	private Set<Section> sections;
	
	@Column(name = "active")
	private boolean active;

	public Hall() {
		super();
	}

	public Hall(Long id, String name, Set<Section> sections,boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.sections = sections;
		this.active = active;
	}

	// For testing repository.
	public Hall(String name) {
		this.name = name;
		this.sections = new HashSet<Section>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Section> getSections() {
		return sections;
	}

	public void setSections(Set<Section> sections) {
		this.sections = sections;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	

}
