package com.tickettakeit.tickettakeit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Ticket {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "code", unique = false, nullable = false)
	private String code;
	@Column(name = "dayOfPurchase", unique = false, nullable = true)
	private Date dayOfPurchase;
	@Column(name = "bought", unique = false, nullable = false)
	private boolean bought;
	@Column(name = "seat_row", unique = false, nullable = false)
	private int row;
	@Column(name = "seat_column", unique = false, nullable = false)
	private int column;
	@Column(name = "price", unique = false, nullable = false)
	private float price;
	@Column(name = "forDay", unique = false, nullable = false)
	private Date forDay;
	@OneToOne
	private EventSection eventSection;
	@OneToOne
	private Event event;
	@OneToOne
	private RegisteredUser user;

	public Ticket() {
	}

	public Ticket(Long id, String code, Date dayOfPurchase, boolean bought, int row, int column, float price,
			Date forDay, EventSection eventSection, Event event, RegisteredUser user) {
		this.id = id;
		this.code = code;
		this.dayOfPurchase = dayOfPurchase;
		this.bought = bought;
		this.row = row;
		this.column = column;
		this.price = price;
		this.forDay = forDay;
		this.eventSection = eventSection;
		this.event = event;
		this.user = user;
	}

	// for testing purposes
	public Ticket(Date dayOfPurchase, boolean bought, int row, int column, Date forDay, Event event) {

		this.dayOfPurchase = dayOfPurchase;
		this.bought = bought;
		this.row = row;
		this.column = column;
		this.forDay = forDay;
		this.event = event;
	}

	// for testing purposes
	public Ticket(Date dayOfPurchase, boolean bought, int row, int column, Date forDay, Event event, float price) {

		this.dayOfPurchase = dayOfPurchase;
		this.bought = bought;
		this.row = row;
		this.column = column;
		this.forDay = forDay;
		this.event = event;
		this.price = price;
	}

	// for testing purposes
	public Ticket(Date dayOfPurchase, boolean bought, int row, int column, Date forDay, Event event, float price, String code) {

		this.dayOfPurchase = dayOfPurchase;
		this.bought = bought;
		this.row = row;
		this.column = column;
		this.forDay = forDay;
		this.event = event;
		this.price = price;
		this.code = code;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDayOfPurchase() {
		return dayOfPurchase;
	}

	public void setDayOfPurchase(Date dayOfPurchase) {
		this.dayOfPurchase = dayOfPurchase;
	}

	public boolean isBought() {
		return bought;
	}

	public void setBought(boolean bought) {
		this.bought = bought;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Date getForDay() {
		return forDay;
	}

	public void setForDay(Date forDay) {
		this.forDay = forDay;
	}

	public EventSection getEventSection() {
		return eventSection;
	}

	public void setEventSection(EventSection eventSection) {
		this.eventSection = eventSection;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public RegisteredUser getUser() {
		return user;
	}

	public void setUser(RegisteredUser user) {
		this.user = user;
	}

}
