package com.tickettakeit.tickettakeit.model;

import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.tickettakeit.tickettakeit.converter.HallConverter;
import com.tickettakeit.tickettakeit.dto.LocationDTO;

@Entity
public class Location {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	@Column(name = "city", unique = false, nullable = false)
	private String city;
	@Column(name = "address", unique = false, nullable = false)
	private String address;
	@Column(name = "longitude", unique = false, nullable = false)
	private double longitude;
	@Column(name = "latitude", unique = false, nullable = false)
	private double latitude;
	@Column(name = "image", unique = false, nullable = false)
	private String image;
	@OneToMany
	private Set<Hall> halls;
	@Column(name = "active")
	private boolean active;

	public Location() {
		super();
	}

	public Location(Long id, String name, String city, String address, double longitude, double latitude, String image,
			Set<Hall> halls,boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.address = address;
		this.longitude = longitude;
		this.latitude = latitude;
		this.image = image;
		this.halls = halls;
		this.active = active;
	}

	// For testing of Event Service.
	public Location(String name, String city, String address, double longitude, double latitude, String image,
			Set<Hall> halls,Boolean active) {
		this.name = name;
		this.city = city;
		this.address = address;
		this.longitude = longitude;
		this.latitude = latitude;
		this.image = image;
		this.halls = halls;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Set<Hall> getHalls() {
		return halls;
	}

	public void setHalls(Set<Hall> halls) {
		this.halls = halls;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	

}
