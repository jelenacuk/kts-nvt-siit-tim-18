package com.tickettakeit.tickettakeit.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import com.tickettakeit.tickettakeit.dto.UpdateEventDTO;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

public class UpdateEventConverter {
	public static Event ConvertFromDTO(UpdateEventDTO ue, Event event, Hall hall, ArrayList<Category> categories,
			ArrayList<EventSection> eventSections, Location location) {

		event.setName(ue.getName());
		event.setDescription(ue.getDescription());
		event.setDate(ue.getDate().stream().map(s -> new Date(s)).collect(Collectors.toSet()));
		event.setAttachments(ue.getAttachments().stream().collect(Collectors.toSet()));
		event.setHall(hall);
		event.setCategories(categories.stream().collect(Collectors.toSet()));
		event.setEventSections(eventSections.stream().collect(Collectors.toSet()));
		event.setLocation(location);

		return event;
	}
	private UpdateEventConverter() {
		
	}
}
