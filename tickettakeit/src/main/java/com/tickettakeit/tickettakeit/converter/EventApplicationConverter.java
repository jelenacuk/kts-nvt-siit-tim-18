package com.tickettakeit.tickettakeit.converter;

import java.util.Date;

import com.tickettakeit.tickettakeit.dto.EventApplicationDTO;
import com.tickettakeit.tickettakeit.model.EventApplication;

public class EventApplicationConverter {
	// Converts EventApplication to DTO
	public static EventApplicationDTO convertToDTO(EventApplication e, Date downInterval, Date upInterval) {
		EventApplicationDTO dto = new EventApplicationDTO();

		dto.setId(e.getId());
		dto.setUserId(e.getUser().getId());
		dto.setEventId(e.getEvent().getId());
		dto.setQueueNumber(e.getQueueNumber());
		dto.setDownInterval(downInterval.getTime());
		dto.setUpInterval(upInterval.getTime());
		dto.setEventName(e.getEvent().getName());

		return dto;
	}
	private EventApplicationConverter() {
		
	}
}
