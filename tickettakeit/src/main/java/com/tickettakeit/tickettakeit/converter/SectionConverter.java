package com.tickettakeit.tickettakeit.converter;

import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.model.Section;

public class SectionConverter {
	public static SectionDTO toDto(Section section) {
		SectionDTO dto = new SectionDTO();
		dto.setId(section.getId());
		dto.setName(section.getName());
		dto.setRows(section.getRows());
		dto.setColumns(section.getColumns());
		dto.setNumerated(section.isNumerated());
		dto.setNumberOfSeats(section.getNumberOfSeats());
		dto.setActive(section.getActive());
		dto.setLeft(section.getLeft());
		dto.setTop(section.getTop());
		return dto;
	}

	public static Section fromDto(SectionDTO dto) {
		Section section = new Section();
		section.setId(dto.getId());
		section.setName(dto.getName());
		section.setRows(dto.getRows());
		section.setColumns(dto.getColumns());
		section.setNumerated(dto.isNumerated());
		section.setNumberOfSeats(dto.getNumberOfSeats());
		section.setActive(dto.isActive());
		section.setLeft(dto.getLeft());
		section.setTop(dto.getTop());
		return section;
	}
	private SectionConverter() {
		
	}
}
