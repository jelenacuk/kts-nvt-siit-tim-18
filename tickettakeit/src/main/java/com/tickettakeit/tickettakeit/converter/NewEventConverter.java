package com.tickettakeit.tickettakeit.converter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.tickettakeit.tickettakeit.dto.NewEventWithBackgroundDTO;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

public class NewEventConverter {
	public static Event ConvertFromDTO(NewEventWithBackgroundDTO dto, Hall hall, Set<Category> categories,
			Set<EventSection> eventSections, Location location) {
		Event e = new Event();

		e.setName(dto.getName());
		e.setDescription(dto.getDescription());
		e.setDate(new HashSet<Date>());

		for (Long d : dto.getDate()) {
			e.getDate().add(new Date(d));
		}

		e.setSalesDate(new Date(dto.getSalesDate()));
		e.setStatus(EventStatus.NORMAL);
		e.setLastDayToPay(dto.getLastDayToPay());
		e.setControllAccess(dto.isControllAccess());
		e.setAttachments(new HashSet<String>());
		e.setHall(hall);
		e.setCategories(categories);
		e.setEventSections(eventSections);
		e.setLocation(location);

		return e;
	}
	private NewEventConverter() {
		
	}
}
