package com.tickettakeit.tickettakeit.converter;

import java.text.SimpleDateFormat;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.dto.UserTicketDTO;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.model.User;

public class UserConverter {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy. hh:mm");

	// Converts UserDTO to User.
	public static RegisteredUser convertFromDTO(UserDTO dto) {
		RegisteredUser user = new RegisteredUser();
		user.setUsername(dto.getUsername());
		user.setFirstName(dto.getFirstName());
		user.setLastName(dto.getLastName());
		user.setPhone(dto.getPhone());
		user.setEmail(dto.getEmail());
		BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
		user.setPassword(bc.encode(dto.getPassword()));
		user.setPaypal(dto.getPaypal());
		return user;
	}

	public static UserDTO convertToDTO(User user) {
		UserDTO dto = new UserDTO(user.getUsername(), user.getPassword(), user.getEmail(), user.getFirstName(),
				user.getLastName(), user.getPhone());
		dto.setRepeatedPassword(user.getPassword());
		RegisteredUser reg = (RegisteredUser) user;
		dto.setPaypal(reg.getPaypal());
		return dto; 
	}
	
	public static UserTicketDTO getUserTicketDTO(Ticket ticket) {
		UserTicketDTO dto = new UserTicketDTO();
		dto.setId(ticket.getId());
		dto.setBought(ticket.isBought());
		dto.setColumn(ticket.getColumn());
		dto.setRow(ticket.getRow());
		dto.setEventID(ticket.getEvent().getId());
		dto.setEventName(ticket.getEvent().getName());
		dto.setEventSectionName(ticket.getEventSection().getName());
		dto.setForDay(sdf.format(ticket.getForDay()));
		dto.setDayOfPurchase(sdf.format(ticket.getDayOfPurchase()));
		dto.setLocation(ticket.getEvent().getLocation().getName());
		dto.setPrice(ticket.getPrice());
		dto.setTicketBackground(ticket.getEvent().getTicketBackground());
		dto.setEventSectionID(ticket.getEventSection().getId());
		return dto;
	}
	private UserConverter() {
		
	}

}
