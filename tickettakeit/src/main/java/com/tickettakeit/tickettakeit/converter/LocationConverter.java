package com.tickettakeit.tickettakeit.converter;

import java.util.HashSet;
import java.util.stream.Collectors;

import com.tickettakeit.tickettakeit.dto.LocationDTO;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

public class LocationConverter {
	public static LocationDTO toDto(Location location) {
		LocationDTO dto = new LocationDTO();
		dto.setId(location.getId());
		dto.setName(location.getName());
		dto.setCity(location.getCity());
		dto.setAddress(location.getAddress());
		dto.setLongitude(location.getLongitude());
		dto.setLatitude(location.getLatitude());
		dto.setImage(location.getImage());
		dto.setHalls(location.getHalls().stream().map(hall -> HallConverter.toDto(hall)).collect(Collectors.toSet()));
		dto.setActive(location.getActive());
		return dto;
	}

	public static Location fromDto(LocationDTO dto) {
		Location location = new Location();
		location.setId(dto.getId());
		location.setName(dto.getName());
		location.setCity(dto.getCity());
		location.setAddress(dto.getAddress());
		location.setLongitude(dto.getLongitude());
		location.setLatitude(dto.getLatitude());
		if(dto.getImage()!=null) {
			location.setImage(dto.getImage());
		}else {
			location.setImage("");
		}
		if(dto.getHalls()!=null) {
			location.setHalls(dto.getHalls().stream().map(hall -> HallConverter.fromDto(hall)).collect(Collectors.toSet()));
		}else {
			location.setHalls(new HashSet<Hall>());
		}
		location.setActive(dto.getActive());
		return location;
	}
	private LocationConverter() {
		
	}

}
