package com.tickettakeit.tickettakeit.converter;

import java.util.HashSet;
import java.util.stream.Collectors;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Section;

public class HallConverter {
	public static HallDTO toDto(Hall hall) {
		HallDTO dto = new HallDTO();
		dto.setId(hall.getId());
		dto.setName(hall.getName());
		dto.setSections(hall.getSections().stream().map(section -> SectionConverter.toDto(section))
				.collect(Collectors.toList()));
		dto.setActive(hall.getActive());
		return dto;
	}

	public static Hall fromDto(HallDTO dto) {
		Hall hall = new Hall();
		hall.setName(dto.getName());
		hall.setId(dto.getId());
		if (dto.getSections() != null) {
			hall.setSections(dto.getSections().stream().map(section -> SectionConverter.fromDto(section))
					.collect(Collectors.toSet()));
		} else {
			hall.setSections(new HashSet<Section>());
		}

		hall.setActive(dto.getActive());
		return hall;
	}
	private HallConverter() {
		
	}

}
