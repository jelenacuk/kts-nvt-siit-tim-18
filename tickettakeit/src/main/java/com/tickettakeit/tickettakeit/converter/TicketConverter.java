package com.tickettakeit.tickettakeit.converter;

import com.tickettakeit.tickettakeit.dto.TicketDTO;
import com.tickettakeit.tickettakeit.model.Ticket;

public class TicketConverter {
	public static TicketDTO convertToDTO(Ticket t) {
		TicketDTO dto = new TicketDTO();

		dto.setId(t.getId());
		dto.setCode(t.getCode());
		dto.setBought(t.isBought());
		dto.setRow(t.getRow());
		dto.setColumn(t.getColumn());
		dto.setPrice(t.getPrice());
		dto.setForDay(t.getForDay().getTime());
		dto.setEventSectionID(t.getEventSection().getId());
		dto.setEventID(t.getEvent().getId());

		return dto;
	}
	private TicketConverter() {
		
	}
}
