package com.tickettakeit.tickettakeit.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.converter.SectionConverter;
import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.SectionAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionCreatingException;
import com.tickettakeit.tickettakeit.exceptions.SectionDeleteException;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionUpdateException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.SectionRepository;

@Service
public class SectionService {

	@Autowired
	SectionRepository repository;
	@Autowired
	HallService hallService;
	@Autowired
	EventService eventService;

	public Section findOne(Long id) throws SectionNotExistsException {
		return repository.findById(id).orElseThrow(() -> new SectionNotExistsException(id.toString()));
	}

	public Optional<Section> findById(Long id) {
		return repository.findById(id);
	}

	public List<Section> findAll() {
		return repository.findAll();
	}

	public List<Section> findByName(String name) {
		return repository.findByName(name);
	}

	public List<Section> findAll(Pageable page) {
		Page<Section> p = repository.findAll(page);
		return p.toList();
	}

	public Section save(Section section) {
		return repository.save(section);
	}

	public List<Section> saveAll(List<Section> section) {
		return repository.saveAll(section);
	}

	public Section remove(Long id) throws SectionNotExistsException, SectionDeleteException {
		Hall hall = repository.findHallBySectionId(id).orElseThrow(() -> new SectionNotExistsException(id.toString()));
		List<Event> events = eventService.selectEventsFromHall(hall.getId()).orElseGet(() -> null);
		if (events != null) {
			Date today = new Date();
			for (Event event : events) {
				Set<Date> dates = event.getDate();
				for (Date d : dates) {
					if (d.after(today)) {
						throw new SectionDeleteException();
					}
				}
			}
		}
		Section section = findOne(id);
		section.setActive(false);
		return repository.save(section);
	}

	public Section createSection(Long hallId, SectionDTO section)
			throws SectionAlreadyExistsException, SectionCreatingException {
		Hall hall = hallService.findOne(hallId);
		List<Section> listOfSections = hall.getSections().stream().filter(s -> s.getName().equals(section.getName()))
				.collect(Collectors.toList());
		if (listOfSections.size() > 0) {
			throw new SectionAlreadyExistsException(section.getName());
		}
		if (section.getNumberOfSeats() < 0 || section.getName().equals("") || section.getColumns() < 0
				|| section.getRows() < 0) {
			throw new SectionCreatingException("Error creating section");
		}
		Section s;
		try {
			s = SectionConverter.fromDto(section);
			s = repository.save(s);
			hall.getSections().add(s);
			hallService.save(hall);
		} catch (Exception e) {
			throw new SectionCreatingException(section.getName());
		}
		return s;
	}

	public Section getSection(Long sectionID) throws SectionNotExistsException {
		return Optional.ofNullable(findOne(sectionID))
				.orElseThrow(() -> new SectionNotExistsException(sectionID.toString()));
	}

	public Section updateSection(Long sectionID, SectionDTO sectionDTO)
			throws SectionNotExistsException, SectionUpdateException {
		Section section = repository.findById(sectionID)
				.orElseThrow(() -> new SectionNotExistsException(sectionID.toString()));
		if (sectionDTO.getNumberOfSeats() < 0 || sectionDTO.getName().equals("") || sectionDTO.getColumns() < 0
				|| sectionDTO.getRows() < 0) {
			throw new SectionUpdateException("Error updating section");
		}
		section.setName(sectionDTO.getName());
		section.setColumns(sectionDTO.getColumns());
		section.setNumberOfSeats(sectionDTO.getNumberOfSeats());
		section.setNumerated(sectionDTO.isNumerated());
		section.setRows(sectionDTO.getRows());
		section.setActive(sectionDTO.isActive());
		Section s;
		try {
			s = repository.save(section);
		} catch (Exception e) {

			throw new SectionUpdateException(section.getName());
		}
		return s;
	}
	public void updateSectionPositions(List<SectionDTO> dto){
		for (SectionDTO sectionDTO : dto) {
			Section s = repository.getOne(sectionDTO.getId());
			s.setTop(sectionDTO.getTop());
			s.setLeft(sectionDTO.getLeft());
			repository.save(s);
		}
	}
}
