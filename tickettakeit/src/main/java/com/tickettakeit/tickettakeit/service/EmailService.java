package com.tickettakeit.tickettakeit.service;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.model.User;

@Service
public class EmailService {
	private static final String FROM = "tickettakeit.kts.nvt@gmail.com";

	@Autowired
	private JavaMailSender javaMailSender;

	// Sends email with encoded username after registration
	@Async
	public boolean sendRegistrationEmail(User user) throws MailException, InterruptedException {

		System.out.println("Sending activation e-mail to " + user.getEmail());
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom(FROM);
		mail.setSubject("TicketTakeit: Account conformation");
		String url = "";
		try {
			url = Base64.getEncoder().encodeToString(user.getUsername().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		mail.setText("Go to: " + "http://localhost:8080/auth/registrationConfirmation" + "/" + url
				+ " to activate your account.");
		javaMailSender.send(mail);
		System.out.println("E-mail sent!");
		return true;
	}

	// Sends reminder for the upcoming event
	@Async
	public void sendUpcomingEventReminderEmail(Ticket ticket) throws MailException, InterruptedException {

		User user = ticket.getUser();

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom(FROM);
		mail.setSubject("TicketTakeit: Upcoming event - " + ticket.getEvent().getName());

		mail.setText("Hello " + user.getFirstName() + ", \n\n " + ticket.getEvent().getName()
				+ " is only two days away! We hope you are getting excited for the event! " + "\n\nEvent name: "
				+ ticket.getEvent().getName() + "\nDate: " + ticket.getForDay() + "\nLocation: "
				+ ticket.getEvent().getLocation().getName() + "\nSeat row: " + ticket.getRow() + "\nSeat column: "
				+ ticket.getColumn());
		javaMailSender.send(mail);

	}

	// Sends reminder for the payment deadline
	@Async
	public void sendDeadlineReminderEmail(Ticket ticket, Date deadline) throws MailException, InterruptedException {

		User user = ticket.getUser();

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom(FROM);
		mail.setSubject("TicketTakeit: Payment deadline - " + ticket.getEvent().getName());

		mail.setText("Hello " + user.getFirstName() + ", \n\n " + " Please note that the payment deadline for "
				+ ticket.getEvent().getName() + " ticket expires at " + deadline
				+ ". \n If you do not make the payment by then, your reservation will be canceled. ");

		javaMailSender.send(mail);

	}

	// Sends notifications for canceled reservations
	@Async
	public void sendCanceledReservationEmail(Ticket ticket) throws MailException, InterruptedException {

		User user = ticket.getUser();

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom(FROM);
		mail.setSubject("TicketTakeit: Reservation canceled - " + ticket.getEvent().getName());

		mail.setText("Hello " + user.getFirstName() + ", \n\n " + " Your reservation for " + ticket.getEvent().getName()
				+ " event is canceled because the payment deadline has expired. \n\nEvent name: "
				+ ticket.getEvent().getName() + "\nDate: " + ticket.getForDay() + "\nLocation: "
				+ ticket.getEvent().getLocation().getName());

		javaMailSender.send(mail);

	}
	
	// Send ticket as pdf attachment
	@Async
	public void sendTicket(Ticket ticket, List<String> paths) {

		User user = ticket.getUser();

		MimeMessage message = javaMailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(FROM);
			helper.setTo(user.getEmail());
			helper.setSubject("TicketTakeIt tickets" );
			helper.setText("Hello " + user.getFirstName() + ", \n\n Your ticket is ready!");

			for (String path : paths) {
				FileSystemResource file = new FileSystemResource(path);
				helper.addAttachment(file.getFilename(), file);
			}

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		javaMailSender.send(message);
	}

}
