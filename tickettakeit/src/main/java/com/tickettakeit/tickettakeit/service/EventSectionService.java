package com.tickettakeit.tickettakeit.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.repository.EventSectionRepository;

@Service
public class EventSectionService {
	@Autowired
	EventSectionRepository repository;

	public EventSection findByID(Long id) {
		return repository.getOne(id);
	}

	public Optional<EventSection> findByIdOptional(Long id) {

		return repository.findById(id);
	}

	public EventSection save(EventSection eventSection) {
		return repository.save(eventSection);
	}
}
