package com.tickettakeit.tickettakeit.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.codec.Base64;
import com.tickettakeit.tickettakeit.converter.EventConverter;
import com.tickettakeit.tickettakeit.converter.NewEventConverter;
import com.tickettakeit.tickettakeit.dto.AddNewAttachmentDTO;
import com.tickettakeit.tickettakeit.dto.ChangeEventCategoryDTO;
import com.tickettakeit.tickettakeit.dto.ChangeEventDateDTO;
import com.tickettakeit.tickettakeit.dto.ChangeLastDayToPayDTO;
import com.tickettakeit.tickettakeit.dto.EventBasicInfoDTO;
import com.tickettakeit.tickettakeit.dto.EventDTO;
import com.tickettakeit.tickettakeit.dto.NewEventWithBackgroundDTO;
import com.tickettakeit.tickettakeit.dto.RemoveAttachmentDTO;
import com.tickettakeit.tickettakeit.dto.UpdateEventDescriptionDTO;
import com.tickettakeit.tickettakeit.dto.UpdateEventNameDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventDateBeforeSalesDateException;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventIsOverException;
import com.tickettakeit.tickettakeit.exceptions.EventNameIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.HallIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.exceptions.LocationIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SectionIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.EventRepository;

@Service
public class EventService {
	@Autowired
	HallService hallService;
	@Autowired
	CategoryService categoryService;
	@Autowired
	EventSectionService eventSectionService;
	@Autowired
	LocationService locationService;
	@Autowired
	FileUploadService fileUploadService;
	@Autowired
	SectionService sectionService;

	@Autowired
	EventRepository repository;

	public Optional<Event> findByID(Long id) {
		return repository.findById(id);
	}

	public Event findOne(Long id) {
		return repository.getOne(id);
	}

	public Page<Event> findAll(Pageable page) {
		return repository.findAll(page);
	}

	public Page<Event> generalSearch(Pageable page, String searchString) {
		return repository.generalSearch(page, searchString);
	}

	public List<Event> findByHall(Long id) {
		Hall hall = hallService.findOne(id);
		return repository.findByHall(hall);
	}

	public List<EventBasicInfoDTO> findAllBasicInfo() {
		return repository.findAllBasicInfo();
	}

	public Set<Date> findEventDates(Long id) {
		return repository.findById(id).get().getDate();
	}

	public Page<Event> detailedSearch(Pageable pageable, String eventName, String city, String locationName,
			float fromPrice, float toPrice, Date fromDate, Date toDate, List<Long> categoriesId)
			throws InvalidDateException {
		Page<Event> events;
		if (fromDate != null && toDate != null) {
			if (fromDate.after(toDate)) {
				throw new InvalidDateException("From date must be before to date.");
			}
		}

		if (categoriesId == null && fromDate == null && toDate == null) {
			events = repository.findByNameCityLocationPrice(pageable, eventName, city, locationName, fromPrice,
					toPrice);
		} else if (categoriesId == null) {
			events = repository.findByNameCityLocationPriceDate(pageable, eventName, city, locationName, fromPrice,
					toPrice, fromDate, toDate);
		} else if (fromDate == null && toDate == null) {
			events = repository.findByNameCityLocationPriceCategory(pageable, eventName, city, locationName, fromPrice,
					toPrice, categoriesId);
		} else {
			events = repository.detailedSearch(pageable, eventName, city, locationName, fromPrice, toPrice, fromDate,
					toDate, categoriesId);
		}

		return events;
	}

	public EventDTO getEvent(Long eventID) throws EventNotExistsException {
		Optional<Event> foundEvent = repository.findById(eventID);
		if (!foundEvent.isPresent()) {
			throw new EventNotExistsException(eventID);
		}

		// Creates EventDTO and fills its data with Event data.
		return EventConverter.convertToEventDTO(foundEvent.get());
	}

	// Adds new Event to the system, Hall, Location, Section and Category must
	// already exist.
	public Event addEvent(@Valid NewEventWithBackgroundDTO ne)
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {

		// Checks if sales date is before today.
		if ((new Date(ne.getSalesDate()).before(new Date()))) {
			throw new SalesDateHasPassedException();
		}

		// Because of the previous check, dates can't be before today.
		// Checks if dates are before sales date
		for (Long d : ne.getDate()) {
			if (d < ne.getSalesDate()) {
				throw new EventDateBeforeSalesDateException();
			}
		}

		// Checks if Event with given name already exists.
		Optional<Event> sameNameEvents = repository.findByName(ne.getName());
		if (sameNameEvents.isPresent()) {
			throw new EventNameIsTakenException();
		}

		// Checks if hall with given ID exists.
		Optional<Hall> hall = hallService.findById(ne.getHallID());
		if (!hall.isPresent()) {
			throw new HallNotExistsException(ne.getHallID());
		}
		if (!hall.get().getActive()) {
			throw new HallIsBlocked(hall.get().getId());
		}

		// Checks if location with given ID exists.
		Optional<Location> location = locationService.findById(ne.getLocationID());
		if (!location.isPresent()) {
			throw new LocationNotExistsException(ne.getLocationID());
		}
		if (!location.get().getActive()) {
			throw new LocationIsBlocked(location.get().getId());
		}

		// Checks if Hall belongs to found location.
		if (!location.get().getHalls().contains(hall.get())) {
			throw new HallNotExistsException(hall.get().getId());
		}

		// Checks if every category with given ID exists.
		ArrayList<Category> categories = checkIfCategoriesExist(ne.getCategoriesIDs());

		// Checks if every section with given ID exists and if it belongs to the
		// given Hall.
		ArrayList<EventSection> eventSections = checkIfSectionsExist(ne.getEventSectionsIDs(), hall.get());

		// Checks if Hall is taken for the given day.
		List<Event> foundByHall = findByHall(ne.getHallID());
		if (!foundByHall.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			// For every date in new event.
			for (Date eventDate : ne.getDate().stream().map(l -> new Date(l)).collect(Collectors.toSet())) {
				// For every found Event in given Hall.
				for (Event e : foundByHall) {
					// Checks if found Event is BLOCKED
					if (!e.getStatus().equals(EventStatus.BLOCKED)) {
						// For every date of found Event in given Hall check if it is the day.
						for (Date d : e.getDate()) {
							if ((sdf.format(d)).equals(sdf.format(eventDate))) {
								throw new HallIsTakenException(ne.getHallID(), sdf.format(d));
							}
						}
					}
				}
			}
		}

		// Converts NewEventDTO to Event.
		Event newEvent = NewEventConverter.ConvertFromDTO(ne, hall.get(),
				categories.stream().collect(Collectors.toSet()), eventSections.stream().collect(Collectors.toSet()),
				location.get());

		if (newEvent.isControllAccess()) {
			newEvent.setApplicationDate(new Date(ne.getApplicationDate()));
		} else {
			newEvent.setApplicationDate(null);
		}

		// Saves new attachment
		for (String a : ne.getAttachments()) {

			// Save file
			byte[] imageByte = Base64.decode((a.split(","))[1]);
			String directory = "/images/";
			Long uniqueator = new Date().getTime();
			String attachmentName = directory + newEvent.getName() + uniqueator + ".jpg";
			File f = new File(directory);
			f.mkdirs();
			try (FileOutputStream ff = new FileOutputStream(attachmentName)) {
				ff.write(imageByte);
			}
			newEvent.getAttachments().add(attachmentName);
		}

		// Save file
		if (ne.getBackground() != null) {
			byte[] imageByte = Base64.decode((ne.getBackground().split(","))[1]);
			String directory = "/images";
			File f = new File(directory);
			f.mkdirs();
			try (FileOutputStream ff = new FileOutputStream(directory + "/" + ne.getBackgroundName() + ".jpg")){
				ff.write(imageByte);
			}
			newEvent.setTicketBackground("/images/" + ne.getBackgroundName() + ".jpg");
		} else {
			newEvent.setTicketBackground("/images/" + "defaultTicketBackground" + ".jpg");
		}

		// Saves new EventSections to database.
		for (EventSection es : eventSections) {
			eventSectionService.save(es);
		}

		// Saves new event to database and returns it.
		return repository.save(newEvent);
	}

	// Changes Event status to blocked, blocked Event is logically deleted.
	// Blocked Event cannot be reversed.
	public boolean blockEvent(Long id) throws EventNotExistsException, SalesDateHasPassedException {
		// Checks if Event with given ID exists.
		Optional<Event> foundEvent = findByID(id);
		if (!foundEvent.isPresent()) {
			throw new EventNotExistsException(id);
		}

		// Checks if today is before sales date.
		if (foundEvent.get().getSalesDate().before(new Date())) {
			throw new SalesDateHasPassedException();
		}

		// Blocks Event and saves it.
		foundEvent.get().setStatus(EventStatus.BLOCKED);
		repository.save(foundEvent.get());

		return true;
	}

	// Updates Event name attribute.
	public boolean updateEventName(UpdateEventNameDTO dto)
			throws EventNotExistsException, EventNameIsTakenException, EventIsBlockedException, EventIsOverException {
		// Checks if name attribute is not empty.
		if (dto.getName().equals("".trim())) {
			return false;
		}

		// Checks if Event with given ID exists.

		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if Event is OVER
		if (event.get().getStatus().equals(EventStatus.OVER)) {
			throw new EventIsOverException(event.get().getId());
		}

		// Checks if there is another Event with same NEW name.
		Optional<Event> sameNameEvents = repository.findByName(dto.getName());

		if (sameNameEvents.isPresent() && (!sameNameEvents.get().getName().equals(event.get().getName()))) {
			throw new EventNameIsTakenException();
		}

		// Sets new name and saves Event.
		event.get().setName(dto.getName());
		repository.save(event.get());

		return true;
	}

	// Updates Event description attribute
	public boolean updateEventDescription(UpdateEventDescriptionDTO dto)
			throws EventNotExistsException, EventIsBlockedException, EventIsOverException {
		// Checks if description attribute is not empty.
		if (dto.getDescription().equals("".trim())) {
			return false;
		}

		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if Event is OVER
		if (event.get().getStatus().equals(EventStatus.OVER)) {
			throw new EventIsOverException(event.get().getId());
		}

		// Sets new description and saves Event.
		event.get().setDescription(dto.getDescription());
		repository.save(event.get());

		return true;
	}

	// Adds new attachment to Event attachments attribute, and saves it on server.
	public boolean addNewAttachment(AddNewAttachmentDTO dto)
			throws EventNotExistsException, IOException, EventIsBlockedException {
		// Checks if attachment attribute is not empty.
		if (dto.getAttachment() == null) {
			return false;
		}

		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Saves new attachment
		// Save file
		byte[] imageByte = Base64.decode((dto.getAttachment().split(","))[1]);
		String directory = "/images/";
		Long uniqueator = new Date().getTime();
		String attachmentName = directory + event.get().getName() + uniqueator + ".jpg";
		File f = new File(directory);
		f.mkdirs();
		try (FileOutputStream ff = new FileOutputStream(attachmentName)) {
			ff.write(imageByte);
		}
		event.get().getAttachments().add(attachmentName);

		// Saves event.
		repository.save(event.get());

		return true;
	}

	// Removes attachment with given name.
	public boolean removeAttachment(RemoveAttachmentDTO dto) throws EventNotExistsException, EventIsBlockedException {
		// Checks if attachment attribute is not empty.
		if (dto.getAttachment().equals("".trim())) {
			return false;
		}

		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Removes attachment with given name from list of attachments.
		for (String a : event.get().getAttachments()) {
			if (a.equals(dto.getAttachment())) {
				// To do: remove attachment from folder!
				event.get().getAttachments().remove(a);
				break;
			}
		}

		// Saves event.
		repository.save(event.get());

		return true;
	}

	// Changes Event control access. Only available before sales date!
	public boolean changeEventControlAccess(Long id)
			throws EventNotExistsException, SalesDateHasPassedException, EventIsBlockedException, EventIsOverException {
		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(id);
		if (!event.isPresent()) {
			throw new EventNotExistsException(id);
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if sales date has passed.
		if (event.get().getSalesDate().before(new Date())) {
			throw new SalesDateHasPassedException();
		}

		// Changes Event control access.
		if (event.get().isControllAccess()) {
			event.get().setControllAccess(false);
		} else {
			event.get().setControllAccess(true);
		}

		// Saves Event.
		repository.save(event.get());

		return true;
	}

	// Adds and saves new Ticket background. Only available before sales date!
	public boolean changeEventTicketBackground(AddNewAttachmentDTO dto)
			throws EventNotExistsException, IOException, SalesDateHasPassedException, EventIsBlockedException {
		// Checks if attachment attribute is not empty.
		if (dto.getAttachment().equals("".trim())) {
			return false;
		}

		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if sales date has passed.
		if (event.get().getSalesDate().before(new Date())) {
			throw new SalesDateHasPassedException();
		}

		// Saves new Ticket background.
		if (dto.getAttachment() != null) {
			// Save file
			byte[] imageByte = Base64.decode((dto.getAttachment().split(","))[1]);
			String directory = "/images";
			File f = new File(directory);
			f.mkdirs();
			try (FileOutputStream ff = new FileOutputStream(directory + "/" + event.get().getName() + ".jpg")) {
				ff.write(imageByte);
			}

			String attachmentName = "/images/" + event.get().getName() + ".jpg";

			// Changes Ticket background and saves Event.
			event.get().setTicketBackground(attachmentName);
			repository.save(event.get());

			return true;
		}

		return false;
	}

	// Changes Event dates to new ones. Only available before sales date!
	// New dates must be after the sales date.
	public boolean changeEventDate(ChangeEventDateDTO dto)
			throws EventNotExistsException, SalesDateHasPassedException, EventIsBlockedException, HallIsTakenException {
		// Checks if list of dates is empty.
		if (dto.getDate().isEmpty()) {
			return false;
		}

		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if sales date has passed.
		if (event.get().getSalesDate().before(new Date())) {
			throw new SalesDateHasPassedException();
		}

		// Checks if date is before sales date.
		HashSet<Date> newDate = new HashSet<Date>();
		for (Long d : dto.getDate()) {
			if (event.get().getSalesDate().after(new Date(d))) {
				return false;
			}
			newDate.add(new Date(d));
		}

		// Checks if Hall is taken for the given day.
		List<Event> foundByHall = findByHall(event.get().getHall().getId());
		if (!foundByHall.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			// For every date in new event.
			for (Date eventDate : dto.getDate().stream().map(l -> new Date(l)).collect(Collectors.toSet())) {
				// For every found Event in given Hall.
				for (Event e : foundByHall) {
					if (e.getId() == dto.getId()) {
						continue;
					}
					// Checks if found Event is BLOCKED
					if (!e.getStatus().equals(EventStatus.BLOCKED)) {
						// For every date of found Event in given Hall check if it is the day.
						for (Date d : e.getDate()) {
							if ((sdf.format(d)).equals(sdf.format(eventDate))) {
								throw new HallIsTakenException(event.get().getHall().getId(), sdf.format(d));
							}
						}
					}
				}
			}
		}

		// Sets new date and saves Event.
		event.get().setDate(newDate);
		repository.save(event.get());

		return true;
	}

	// Changes lastDayToPay attribute of Event. Only available before sales date.
	public boolean changeLastDayToPay(ChangeLastDayToPayDTO dto)
			throws EventNotExistsException, SalesDateHasPassedException, EventIsBlockedException {
		// Checks if lastDayToPay is less than 0.

		if (dto.getLastDayToPay() < 0) {
			return false;
		}

		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if sales date has passed.
		if (event.get().getSalesDate().before(new Date())) {
			throw new SalesDateHasPassedException();
		}

		// Checks if new last day to pay is before sales date.
		// Sorts dates from set to list.
		ArrayList<Date> sortedDates = new ArrayList<Date>(new TreeSet<Date>(event.get().getDate()));
		// Checks only the closest date, if it passes, others will pass too.
		Date lastDayToPayDate = new Date(sortedDates.get(0).getTime() - dto.getLastDayToPay() * (1000 * 60 * 60 * 24));
		if (lastDayToPayDate.before(event.get().getSalesDate())) {
			return false;
		}

		// Changes lastDayToPay and saves Event.
		event.get().setLastDayToPay(dto.getLastDayToPay());
		repository.save(event.get());

		return true;
	}

	// Adds new Category (already existing) to Event.
	public boolean addNewCategoryToEvent(ChangeEventCategoryDTO dto)
			throws EventNotExistsException, CategoryNotExistsException, EventIsBlockedException, EventIsOverException {
		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getEventId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getEventId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if Event is OVER
		if (event.get().getStatus().equals(EventStatus.OVER)) {
			throw new EventIsOverException(event.get().getId());
		}

		// Checks if Category with given ID exists.
		Optional<Category> category = categoryService.findByIdOptional(dto.getCategoryId());
		if (!category.isPresent()) {
			throw new CategoryNotExistsException(dto.getCategoryId());
		}

		// Adds category to Event and saves it.
		event.get().getCategories().add(category.get());
		repository.save(event.get());

		return true;
	}

	// Removes Category from Event.
	public boolean removeCategoryFromEvent(ChangeEventCategoryDTO dto)
			throws EventNotExistsException, CategoryNotExistsException, EventIsBlockedException, EventIsOverException {
		// Checks if Event with given ID exists.
		Optional<Event> event = repository.findById(dto.getEventId());
		if (!event.isPresent()) {
			throw new EventNotExistsException(dto.getEventId());
		}

		// Checks if Event is BLOCKED
		if (event.get().getStatus().equals(EventStatus.BLOCKED)) {
			throw new EventIsBlockedException(event.get().getId());
		}

		// Checks if Event is OVER
		if (event.get().getStatus().equals(EventStatus.OVER)) {
			throw new EventIsOverException(event.get().getId());
		}

		// Checks if Category with given ID exists.
		Optional<Category> category = categoryService.findByIdOptional(dto.getCategoryId());
		if (!category.isPresent()) {
			throw new CategoryNotExistsException(dto.getCategoryId());
		}

		// Removes Category from Event and saves it.
		event.get().getCategories().remove(category.get());
		repository.save(event.get());

		return true;
	}

	// Checks if every category with given ID exists.
	private ArrayList<Category> checkIfCategoriesExist(ArrayList<Long> ids) throws CategoryNotExistsException {
		ArrayList<Category> categories = new ArrayList<Category>();
		for (Long catID : ids) {
			Optional<Category> foundCategory = categoryService.findByIdOptional(catID);
			if (!foundCategory.isPresent()) {
				throw new CategoryNotExistsException(catID);
			}
			categories.add(foundCategory.get());
		}

		return categories;
	}

	// Checks if every section with given ID exists and if it belongs to given Hall.
	private ArrayList<EventSection> checkIfSectionsExist(Map<Long, Float> ids, Hall hall)
			throws SectionNotExistsException, SectionIsBlocked {
		ArrayList<EventSection> eventSections = new ArrayList<>();
		for (Map.Entry<Long, Float> section : ids.entrySet()) {
			Optional<Section> foundSection = sectionService.findById(section.getKey());
			if (!foundSection.isPresent()) {
				throw new SectionNotExistsException("Section with ID: " + section.getKey() + " does not exist.");
			}
			if (!foundSection.get().getActive()) {
				throw new SectionIsBlocked(foundSection.get().getId());
			}
			if (!hall.getSections().contains(foundSection.get())) {
				throw new SectionNotExistsException(
						"Section with ID: " + section.getKey() + " does not exist in Hall with ID: " + hall.getId());
			}
			EventSection newEventSection = new EventSection(foundSection.get(), section.getValue());
			eventSections.add(newEventSection);
		}

		return eventSections;
	}

	public Event save(Event event) {
		return repository.save(event);
	}

	public Optional<List<Event>> selectEventsFromLocation(Long locationId) {
		return repository.selectEventsFromLocation(locationId);
	}

	public Optional<List<Event>> selectEventsFromHall(Long hallId) {
		return repository.selectEventsFromHall(hallId);
	}
}
