package com.tickettakeit.tickettakeit.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.codec.Base64;
import com.tickettakeit.tickettakeit.converter.CategoryConverter;
import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.ObjectCannotBeDeletedException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.repository.CategoryRepository;
import com.tickettakeit.tickettakeit.repository.EventRepository;

@Service
public class CategoryService {
	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	EventRepository eventRepository;

	public Category findById(Long id) throws CategoryNotExistsException {
		Optional<Category> categoryOpt = categoryRepository.findById(id);
		if (!categoryOpt.isPresent()) {
			throw new CategoryNotExistsException(id);
		}
		return categoryOpt.get();
	}

	public Optional<Category> findByIdOptional(Long id) {
		return categoryRepository.findById(id);
	}

	public Category findByName(String name) {
		return categoryRepository.findByName(name);
	}

	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	public Category save(Category category) {
		return categoryRepository.save(category);
	}

	public void remove(Long id) {
		categoryRepository.deleteById(id);
	}

	public Category addCategory(CategoryDTO categoryDto)
			throws CategoryAlreadyExistsException, IOException {
		Category newCategory = CategoryConverter.convertFromDTO(categoryDto);

		// Check if category with the same name already exists
		if (categoryRepository.findByName(newCategory.getName()) != null) {
			throw new CategoryAlreadyExistsException(newCategory.getName());
		}

		if (newCategory.getIcon() != null) {
			byte[] imageByte = Base64.decode((newCategory.getIcon().split(","))[1]);
			String directory = "/category_icons";
			File f = new File(directory);
			f.mkdirs();
			try(FileOutputStream fileOutputStream = new FileOutputStream(directory + "/" + newCategory.getName() + ".jpg")){
				fileOutputStream.write(imageByte);
			}

			newCategory.setIcon("category_icons/" + newCategory.getName() + ".jpg");
		}

		return save(newCategory);

	}

	public Category updateCategory(CategoryDTO categoryDto)
			throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {
		Category updateCategory = findById(categoryDto.getId());

		// If the name is changed, check if the new one is unique
		if (!categoryDto.getName().equals(updateCategory.getName())) {
			if (findByName(categoryDto.getName()) != null) {
				throw new CategoryAlreadyExistsException(categoryDto.getName());
			}
		}

		if (categoryDto.getIcon() != null) {
			if (!categoryDto.getIcon().equals(updateCategory.getIcon())) {
				byte[] imageByte = Base64.decode((categoryDto.getIcon().split(","))[1]);
				String directory = "/category_icons";
				File f = new File(directory);
				f.mkdirs();
				try (FileOutputStream fileOutputStream = new FileOutputStream(
						directory + "/" + categoryDto.getName() + ".jpg")) {
					fileOutputStream.write(imageByte);
				}
				categoryDto.setIcon("category_icons/" + categoryDto.getName() + ".jpg");
			}
		}

		updateCategory = CategoryConverter.convertFromDTO(categoryDto);

		// update category
		return save(updateCategory);
	}

	public boolean deleteCategory(Long id) throws CategoryNotExistsException, ObjectCannotBeDeletedException {
		// check if category with the given id exists
		Category category = findById(id);

		// check if there are events of this category
		ArrayList<Long> idList = new ArrayList<>();
		idList.add(id);
		List<Event> foundEvent = eventRepository.findByCategory(PageRequest.of(1, 1), idList).getContent();
		if (foundEvent.size() > 0) {
			throw new ObjectCannotBeDeletedException("Category " + category.getName()
					+ " cannot be deleted because there are events associated with it.");
		}

		// delete category
		remove(category.getId());
		return true;

	}

}
