package com.tickettakeit.tickettakeit.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import com.tickettakeit.tickettakeit.dto.PayPalDTO;
import com.tickettakeit.tickettakeit.model.Ticket;

@Service
public class PayPalService {

	@Value("${paypal.client}")
	String clientId;

	@Value("${paypal.secret}")
	String clientSecret;

	public Payment createPaymentObjectForTickets(List<Ticket> tickets, String returnUrl, String cancelUrl) {

		Amount amount = new Amount();
		amount.setCurrency("EUR");

		ItemList itemList = new ItemList();
		ArrayList<Item> items = new ArrayList<>();

		for (Ticket t : tickets) {

			Item item = new Item();
			item.setDescription("TicketTakeIt");
			item.setName(t.getEvent().getName() + " ticket");
			item.setCurrency("EUR");
			item.setPrice(t.getPrice() + "");
			item.setQuantity("1");
			items.add(item);
		}

		itemList.setItems(items);

		Transaction transaction = new Transaction();
		transaction.setItemList(itemList);

		amount.setTotal(String
				.valueOf(itemList.getItems().stream().mapToDouble(item -> Double.parseDouble(item.getPrice())).sum()));
		transaction.setAmount(amount);
		List<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = new Payment();
		payment.setIntent("sale");
		payment.setPayer(payer);
		payment.setTransactions(transactions);

		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(cancelUrl);
		redirectUrls.setReturnUrl(returnUrl);
		payment.setRedirectUrls(redirectUrls);

		return payment;

	}

	public Map<String, Object> createPayPalPayment(Payment payment) throws PayPalRESTException {

		Payment createdPayment;
		Map<String, Object> response = new HashMap<String, Object>();

		String redirectUrl = "";
		APIContext context = new APIContext(clientId, clientSecret, "sandbox");
		createdPayment = payment.create(context);
		if (createdPayment != null) {
			List<Links> links = createdPayment.getLinks();
			for (Links link : links) {
				if (link.getRel().equals("approval_url")) {
					redirectUrl = link.getHref();
					break;
				}
			}
			response.put("status", "success");
			response.put("redirect_url", redirectUrl);
		}
		return response;
	}

	public Map<String, Object> completePayment(PayPalDTO paypalDto) throws PayPalRESTException {

		Map<String, Object> response = new HashMap<>();
		Payment payment = new Payment();
		payment.setId(paypalDto.getPaymentID());

		PaymentExecution paymentExecution = new PaymentExecution();
		paymentExecution.setPayerId(paypalDto.getPayerID());

		APIContext context = new APIContext(clientId, clientSecret, "sandbox");
		Payment createdPayment = payment.execute(context, paymentExecution);
		if (createdPayment != null) {
			response.put("status", "success");
		}

		return response;
	}

}
