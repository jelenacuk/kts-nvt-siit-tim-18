package com.tickettakeit.tickettakeit.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Service;

@Service
public class FileUploadService {

	// save uploaded file to new location
	public  void savePicture(InputStream uploadedInputStream, String uploadedFileLocation) {
		try {
			try(OutputStream out = new FileOutputStream(new File(uploadedFileLocation))){
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = uploadedInputStream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
