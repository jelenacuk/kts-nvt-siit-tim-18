package com.tickettakeit.tickettakeit.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tickettakeit.tickettakeit.config.JwtTokenUtil;
import com.tickettakeit.tickettakeit.converter.UserConverter;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.dto.UserTicketDTO;
import com.tickettakeit.tickettakeit.exceptions.AccountIsNotActiveException;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserAlredyExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;
import com.tickettakeit.tickettakeit.model.Authority;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.model.User;
import com.tickettakeit.tickettakeit.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository repository;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUserDetailsService jwtUserDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private AuthorityService authorityService;

	@Autowired
	private EmailService emailService;
	
	@Autowired
	private TicketService ticketService;

	public User findOne(Long id) {
		return repository.getOne(id);
	}

	public Optional<User> findOneByUsername(String username) {
		return repository.findOneByUsername(username);
	}

	public List<User> findAll() {
		return repository.findAll();
	}

	public User save(User user) {
		return repository.save(user);
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	public void delete(User entity) {
		repository.delete(entity);
	}

	public UserDTO getProfile() throws UserNotExistsException {
		User loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new UserNotExistsException("");
		}
		return UserConverter.convertToDTO(loggedUser);
	}
	
	public List<UserTicketDTO> getUserTickets(Pageable page, boolean bought) throws UserNotExistsException {
		User loggedUser = getLoggedUser();
		if (loggedUser == null) {
			throw new UserNotExistsException("");
		}
		Page<Ticket> tickets = ticketService.findTicketsByUser(page, loggedUser.getId(), bought);
		return tickets.stream().map(ticket -> {
			UserTicketDTO dto = UserConverter.getUserTicketDTO(ticket);
			dto.setTotalSize(tickets.getTotalElements());
			return dto;
		}).collect(Collectors.toList());
	}

	public boolean editUserProfile(UserDTO dto) throws PasswordConfirmationException, UserNotExistsException {

		RegisteredUser loggedUser = (RegisteredUser) findOneByUsername(dto.getUsername())
				.orElseThrow(() -> new UserNotExistsException(dto.getUsername()));
		if (loggedUser == null) {
			throw new UserNotExistsException("");
		}
		BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
		if (bc.matches(dto.getPassword(),loggedUser.getPassword() ) == false) {
			throw new PasswordConfirmationException();
		}
		// Checks if password is valid
		if (!dto.getNewPassword().equals(dto.getRepeatedPassword())) {
			throw new PasswordConfirmationException();
		}
		if (!dto.getNewPassword().equals("")) {
			loggedUser.setPassword(bc.encode(dto.getNewPassword()));
		}
		loggedUser.setEmail(dto.getEmail());
		loggedUser.setFirstName(dto.getFirstName());
		loggedUser.setLastName(dto.getLastName());
		loggedUser.setPhone(dto.getPhone());
		loggedUser.setPaypal(dto.getPaypal());
		save(loggedUser);
		return true;
	}

	// Checks if user exists, generates and returns jwt token
	public String createAuthenticationToken(LogInDTO authenticationRequest)
			throws UserNotExistsException, AccountIsNotActiveException {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException | InternalAuthenticationServiceException e) {
			throw new UserNotExistsException(authenticationRequest.getUsername());
		}
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		User user = (User) userDetails;
		// Checks if account is activated
		if (!user.isConfirmed()) {
			throw new AccountIsNotActiveException();
		}
		return jwtTokenUtil.generateToken(userDetails);
	}

	public UserDTO register(UserDTO dto) throws UserAlredyExistsException, PasswordConfirmationException {

		// Checks if password is valid
		if (!dto.getPassword().equals(dto.getRepeatedPassword())) {
			throw new PasswordConfirmationException();
		}
		// Checks if username is taken
		Optional<User> optinalUser = findOneByUsername(dto.getUsername());
		if (optinalUser.isPresent()) {
			throw new UserAlredyExistsException(dto.getUsername());
		}
		// Create new user and save to database
		RegisteredUser user = UserConverter.convertFromDTO(dto);
		Authority authority = authorityService.findByID(1l);
		ArrayList<Authority> authorities = new ArrayList<>();
		authorities.add(authority);
		user.setAuthorities(authorities);
		save(user);
		// sends email
		try {
			emailService.sendRegistrationEmail(user);
		} catch (MailException | InterruptedException e) {
			e.printStackTrace();
		}
		return UserConverter.convertToDTO(user);
	}

	// Recieves enoceded username and switches variable confirmed to true so user
	// can log in
	public boolean activateAccount(String encoded) throws UnsupportedEncodingException, UserNotExistsException {
		byte[] decoded = Base64.getDecoder().decode(encoded);
		String username = new String(decoded, "UTF-8");
		RegisteredUser user = (RegisteredUser) findOneByUsername(username)
				.orElseThrow(() -> new UserNotExistsException(username));
		user.setConfirmed(true);
		save(user);
		return true;
	}

	// Returns currently logged user
	public RegisteredUser getLoggedUser() throws UserNotExistsException {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String username = authentication.getName();
			return (RegisteredUser) findOneByUsername(username)
					.orElseThrow(() -> new UserNotExistsException(username));
		}
		return null;
	}

}
