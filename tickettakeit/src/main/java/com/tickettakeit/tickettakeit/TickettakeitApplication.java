package com.tickettakeit.tickettakeit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TickettakeitApplication {

	public static void main(String[] args) {
		SpringApplication.run(TickettakeitApplication.class, args);
	}

}
