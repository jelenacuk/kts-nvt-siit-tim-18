package com.tickettakeit.tickettakeit.exceptions;

public class LocationUpdateException extends Exception {
	public LocationUpdateException(String message) {
		super(message);
	}

}
