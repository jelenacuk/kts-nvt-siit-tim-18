package com.tickettakeit.tickettakeit.exceptions;

public class HallNotExistsException extends Exception {
	public HallNotExistsException(Long id) {
		super("Hall with id: " + id + " not exists");
	}

}
