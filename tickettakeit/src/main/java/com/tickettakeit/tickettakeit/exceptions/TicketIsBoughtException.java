package com.tickettakeit.tickettakeit.exceptions;

public class TicketIsBoughtException extends Exception {
	public TicketIsBoughtException() {
		super("Ticket is already bought.");
	}
}
