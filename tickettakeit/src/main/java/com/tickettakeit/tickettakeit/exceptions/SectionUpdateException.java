package com.tickettakeit.tickettakeit.exceptions;

public class SectionUpdateException extends Exception {
	public SectionUpdateException(String message) {
		super(message);
	}
}
