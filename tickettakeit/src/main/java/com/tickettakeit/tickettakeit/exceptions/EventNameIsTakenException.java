package com.tickettakeit.tickettakeit.exceptions;

public class EventNameIsTakenException extends Exception {
	public EventNameIsTakenException() {
		super("Event name is taken.");
	}
}
