package com.tickettakeit.tickettakeit.exceptions;

public class HallIsTakenException extends Exception {
	public HallIsTakenException(Long id, String date) {
		super("Hall with ID: " + id + "is taken for the date: " + date);
	}
}
