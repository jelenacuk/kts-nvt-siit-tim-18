package com.tickettakeit.tickettakeit.exceptions;

public class EventSectionNotExistsException extends Exception {
	public EventSectionNotExistsException(Long eventSectionName) {
		super(eventSectionName + " does not exist");
	}
}
