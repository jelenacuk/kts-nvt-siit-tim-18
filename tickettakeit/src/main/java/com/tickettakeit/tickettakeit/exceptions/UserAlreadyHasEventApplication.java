package com.tickettakeit.tickettakeit.exceptions;

public class UserAlreadyHasEventApplication extends Exception {
	public UserAlreadyHasEventApplication(Long userId) {
		super("User with ID: " + userId + " already has event application.");
	}
}
