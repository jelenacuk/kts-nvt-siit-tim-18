package com.tickettakeit.tickettakeit.exceptions;

public class SectionDeleteException extends Exception {
	public SectionDeleteException() {
		super("Event for this section exists");
	}

}
