package com.tickettakeit.tickettakeit.exceptions;

public class EventIsBlockedException extends Exception {
	public EventIsBlockedException(Long id) {
		super("Event with ID: " + id + " is blocked.");
	}
}
