package com.tickettakeit.tickettakeit.exceptions;

public class LocationCreatingException extends Exception {
	public LocationCreatingException(String message) {
		super(message);
	}

}
