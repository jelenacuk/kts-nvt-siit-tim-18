package com.tickettakeit.tickettakeit.exceptions;

// idk if this has any use :'(
public class MissingAttributeException extends Exception {
	public MissingAttributeException(String attribute) {
		super("Attribute " + attribute + " is missing.");
	}
}
