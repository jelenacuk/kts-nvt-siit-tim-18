package com.tickettakeit.tickettakeit.exceptions;

public class EventSectionDoesNotBelongsToTheEventException extends Exception {
	public EventSectionDoesNotBelongsToTheEventException(Long eventSectionId, Long eventIds) {
		super("EventSection: " + eventSectionId + " does not belongs to the Event: " + eventIds);
	}
}
