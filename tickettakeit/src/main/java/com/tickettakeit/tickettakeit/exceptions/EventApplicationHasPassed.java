package com.tickettakeit.tickettakeit.exceptions;

import java.util.Date;

public class EventApplicationHasPassed extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7021382514058065948L;

	public EventApplicationHasPassed(Date downInterval, Date upInterval) {
		super("Your event application has passed" + downInterval + "   " + upInterval);
	}
}
