package com.tickettakeit.tickettakeit.exceptions;

public class LocationDeleteException extends Exception {
	public LocationDeleteException() {
		super("Event for this location already exists");
	}

}
