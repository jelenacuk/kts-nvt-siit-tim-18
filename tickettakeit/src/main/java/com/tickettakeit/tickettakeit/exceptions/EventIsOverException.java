package com.tickettakeit.tickettakeit.exceptions;

public class EventIsOverException extends Exception {
	public EventIsOverException(Long id) {
		super("Event with ID: " + id + " is over.");
	}
}
