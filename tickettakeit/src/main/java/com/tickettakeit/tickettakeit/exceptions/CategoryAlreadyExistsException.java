package com.tickettakeit.tickettakeit.exceptions;

public class CategoryAlreadyExistsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CategoryAlreadyExistsException(String name) {
		super("Category with the name: " + name + " already exists. ");
	}

}
