package com.tickettakeit.tickettakeit.exceptions;

public class EventNotExistsException extends Exception {
	public EventNotExistsException(Long eventID) {
		super("Event with ID: " + eventID + " does not exist.");
	}
}
