package com.tickettakeit.tickettakeit.exceptions;

public class LocationAlreadyExistsExceptions extends Exception {
	public LocationAlreadyExistsExceptions(String name) {
		super("Location " + name + " already exists");
	}
	public LocationAlreadyExistsExceptions(Long id) {
		super("Location " + id + " already exists");
	}

}
