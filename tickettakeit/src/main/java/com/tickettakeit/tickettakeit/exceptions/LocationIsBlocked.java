package com.tickettakeit.tickettakeit.exceptions;

public class LocationIsBlocked extends Exception {
	public LocationIsBlocked(Long id) {
		super("Location with ID: " + id + " is blocked.");
	}
}
