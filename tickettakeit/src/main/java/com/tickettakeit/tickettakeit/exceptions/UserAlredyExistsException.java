package com.tickettakeit.tickettakeit.exceptions;

public class UserAlredyExistsException extends Exception {
	public UserAlredyExistsException(String username) {
		super("User with username: " + username + " already exists.");
	}
}
