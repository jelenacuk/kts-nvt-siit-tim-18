package com.tickettakeit.tickettakeit.exceptions;

public class EventApplicationNotFound extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1760731922609767499L;

	public EventApplicationNotFound(Long id) {
		super("Event application with ID: " + id + " not found.");
	}
}
