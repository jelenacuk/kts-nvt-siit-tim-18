package com.tickettakeit.tickettakeit.exceptions;

public class NoTicketsLeftException extends Exception {
	public NoTicketsLeftException() {
		super("There are no tickets left.");
	}
}
