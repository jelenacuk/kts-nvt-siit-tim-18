package com.tickettakeit.tickettakeit.exceptions;

public class SaleHasNotYetBeganException extends Exception {
	public SaleHasNotYetBeganException() {
		super("Sale has not yet began.");
	}
}
