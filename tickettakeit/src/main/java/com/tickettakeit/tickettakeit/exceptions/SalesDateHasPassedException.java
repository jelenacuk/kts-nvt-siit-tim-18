package com.tickettakeit.tickettakeit.exceptions;

public class SalesDateHasPassedException extends Exception {
	public SalesDateHasPassedException() {
		super("You can't update after sales date!");
	}
}
