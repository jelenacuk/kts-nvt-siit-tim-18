package com.tickettakeit.tickettakeit.exceptions;

public class HallAlreadyExistsException extends Exception {
	public HallAlreadyExistsException(String name) {
		super("Hall with name: " + name+" already exists on that location");
	}

}
