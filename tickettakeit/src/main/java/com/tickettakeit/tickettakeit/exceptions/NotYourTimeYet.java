package com.tickettakeit.tickettakeit.exceptions;

import java.util.Date;

public class NotYourTimeYet extends Exception {
	public NotYourTimeYet(Date downInterval, Date upInterval) {
		super("It is not your time yet. " + downInterval + "   " + upInterval);
	}
}
