package com.tickettakeit.tickettakeit.exceptions;

public class SeatIsTakenException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2892337249591168846L;

	public SeatIsTakenException(int row, int column) {
		super("Seat(row, column): " + row + ", " + column + " is already taken");
	}

}
