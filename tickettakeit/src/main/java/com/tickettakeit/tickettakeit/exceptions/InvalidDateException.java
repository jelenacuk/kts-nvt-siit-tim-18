package com.tickettakeit.tickettakeit.exceptions;

public class InvalidDateException extends Exception  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3704674373846385911L;

	public InvalidDateException(String message) {
		super(message);
	}
}
