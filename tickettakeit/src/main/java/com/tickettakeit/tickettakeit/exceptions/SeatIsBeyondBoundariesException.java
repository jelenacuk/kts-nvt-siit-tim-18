package com.tickettakeit.tickettakeit.exceptions;

public class SeatIsBeyondBoundariesException extends Exception {
	public SeatIsBeyondBoundariesException(int row, int column) {
		super("Seat(row, column): " + row + ", " + column + " is beyond boundaries");
	}
}
