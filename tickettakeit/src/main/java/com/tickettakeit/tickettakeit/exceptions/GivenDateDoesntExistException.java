package com.tickettakeit.tickettakeit.exceptions;

import java.util.Date;

public class GivenDateDoesntExistException extends Exception {
	public GivenDateDoesntExistException(Date date, Long eventId) {
		super("Date: " + date + "does not belong to Event: " + eventId + "dates");
	}
}
