package com.tickettakeit.tickettakeit.exceptions;

public class TicketNotExistsException extends Exception {
	public TicketNotExistsException(Long id) {
		super("Ticket with ID: " + id + " does not exist.");
	}
}
