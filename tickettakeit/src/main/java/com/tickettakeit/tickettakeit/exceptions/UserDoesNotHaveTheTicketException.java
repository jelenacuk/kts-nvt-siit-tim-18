package com.tickettakeit.tickettakeit.exceptions;

public class UserDoesNotHaveTheTicketException extends Exception {
	public UserDoesNotHaveTheTicketException(Long userId, Long ticketId) {
		super("User: " + userId + " does not have a Ticket: " + ticketId);
	}
}
