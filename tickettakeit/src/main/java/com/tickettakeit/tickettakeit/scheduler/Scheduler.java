package com.tickettakeit.tickettakeit.scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.repository.TicketRepository;
import com.tickettakeit.tickettakeit.service.EmailService;
import com.tickettakeit.tickettakeit.service.TicketService;

@Component
public class Scheduler {
	
	@Autowired 
	TicketService ticketService;
	
	@Autowired 
	TicketRepository ticketRepository;
	
	@Autowired
	private EmailService emailService;
	
	@Scheduled(cron = "${reminders.cron}") 
	public void sendUpcomingEventReminders() throws MailException, InterruptedException {
		
		// get tickets for the upcoming events
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, 2);
		Date fromDate = calendar.getTime();
		calendar.add(Calendar.DATE, 1);
		Date toDate = calendar.getTime();
		
		List<Ticket> tickets = ticketRepository.findBoughtTicketsForDay(fromDate, toDate);
		
		// send email reminders for upcoming events
		for(Ticket t : tickets) {
	
			emailService.sendUpcomingEventReminderEmail(t);
	
		}
		
	}
	
	@Scheduled(cron ="${reminders.cron}")
	public void checkPaymentDeadline() throws MailException, InterruptedException {
		
		List<Ticket> tickets = ticketRepository.findByBought(false); 
		
		for(Ticket t : tickets) {
			//calculate deadline
			DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(t.getForDay());
			calendar.add(Calendar.DATE, 0 - t.getEvent().getLastDayToPay());
			
			Date deadline = calendar.getTime();
			
			calendar.add(Calendar.DATE, -5);
			Date expiresSoon = calendar.getTime();
			Date today = new Date();
			
			// if the deadline has expired
			if(dateTimeComparator.compare(today, deadline) > 0) {
				// cancel reservation and send email notification
				ticketService.cancelTicket(t.getId());
				emailService.sendCanceledReservationEmail(t);
							
			}else if(dateTimeComparator.compare(today, expiresSoon) == 0) { 
				// if the deadline expires soon send email reminder
				
				emailService.sendDeadlineReminderEmail(t, deadline);
	
			}
		}

	}

}
