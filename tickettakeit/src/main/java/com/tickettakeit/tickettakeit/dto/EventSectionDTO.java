package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class EventSectionDTO {
	private Long id;
	@NotEmpty
	private String name;
	@NotNull
	private int rows;
	@NotNull
	private int columns;
	@NotNull
	private boolean isNumerated;
	@NotNull
	private int numberOfSeats;
	@NotNull
	private float price;
	private boolean active;
	private int left;
	private int top;

	public EventSectionDTO() {
	}

	public EventSectionDTO(Long id, @NotEmpty String name, @NotNull int rows, @NotNull int columns,
			@NotNull boolean isNumerated, @NotNull int numberOfSeats, @NotNull float price, boolean active, int left,
			int top) {
		this.id = id;
		this.name = name;
		this.rows = rows;
		this.columns = columns;
		this.isNumerated = isNumerated;
		this.numberOfSeats = numberOfSeats;
		this.price = price;
		this.active = active;
		this.left = left;
		this.top = top;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public boolean isNumerated() {
		return isNumerated;
	}

	public void setNumerated(boolean isNumerated) {
		this.isNumerated = isNumerated;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

}
