package com.tickettakeit.tickettakeit.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class LocationDTO {

	private Long id;
	@NotEmpty
	private String name;
	@NotEmpty
	private String city;
	@NotEmpty
	private String address;
	@NotNull
	private double longitude;
	@NotNull
	private double latitude;
	@NotNull
	private String image;
	private Set<HallDTO> halls;
	private boolean active;
	private long totalSize;

	public LocationDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Set<HallDTO> getHalls() {
		return halls;
	}

	public void setHalls(Set<HallDTO> halls) {
		this.halls = halls;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}
	
	

}
