package com.tickettakeit.tickettakeit.dto;

public class JwtResponseDTO {

	private  String token;
	
	public JwtResponseDTO() {
		super();
	}

	public JwtResponseDTO(String jwttoken) {
		this.token = jwttoken;
	}

	public String getToken() {
		return this.token;
	}
}
