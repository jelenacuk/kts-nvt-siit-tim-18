package com.tickettakeit.tickettakeit.dto;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tickettakeit.tickettakeit.converter.CategoryConverter;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;

public class EventDTO {
	@NotNull
	private Long id;
	@NotNull
	@Size(min = 2, max = 100)
	private String name;
	@NotNull
	@Size(min = 1, max = 300)
	private String description;
	@NotNull
	private ArrayList<Date> date;
	@NotNull
	private Long salesDate;
	private Long applicationDate;
	@NotNull
	private int status;
	@NotNull
	private int lastDayToPay;
	@NotNull
	private boolean controllAccess;
	@NotNull
	private ArrayList<String> attachments;
	@NotNull
	private Long hallID;
	@NotNull
	private ArrayList<CategoryDTO> categories;
	@NotNull
	private ArrayList<Long> eventSectionsIDs;
	@NotNull
	private Long locationID;

	private String locationName;

	private String hallName;

	private long totalSize;

	private String background;

	public EventDTO() {
	}

	@SuppressWarnings("unchecked")
	public EventDTO(Event e) {
		this.id = e.getId();
		this.name = e.getName();
		this.description = e.getDescription();
		this.date = new ArrayList<>();
		this.salesDate = e.getSalesDate().getTime();
		this.status = e.getStatus().ordinal();
		this.lastDayToPay = e.getLastDayToPay();
		this.controllAccess = e.isControllAccess();
		/* this.attachments = (ArrayList<String>) e.getAttachments(); */
		this.hallID = e.getHall().getId();
		this.categories = new ArrayList<>();
		for (Category c : e.getCategories()) {
			CategoryDTO dto = CategoryConverter.convertToCategoryDTO(c);
			categories.add(dto);
		}
		this.eventSectionsIDs = new ArrayList<>();
		this.locationID = e.getLocation().getId();
		this.locationName = e.getLocation().getName();
		this.background = e.getTicketBackground();
		this.hallName = e.getHall().getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Date> getDate() {
		return date;
	}

	public void setDate(ArrayList<Date> date) {
		this.date = date;
	}

	public Long getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Long salesDate) {
		this.salesDate = salesDate;
	}

	public Long getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Long applicationDate) {
		this.applicationDate = applicationDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getLastDayToPay() {
		return lastDayToPay;
	}

	public void setLastDayToPay(int lastDayToPay) {
		this.lastDayToPay = lastDayToPay;
	}

	public boolean isControllAccess() {
		return controllAccess;
	}

	public void setControllAccess(boolean controllAccess) {
		this.controllAccess = controllAccess;
	}

	public ArrayList<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}

	public Long getHallID() {
		return hallID;
	}

	public void setHallID(Long hallID) {
		this.hallID = hallID;
	}

	public ArrayList<CategoryDTO> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<CategoryDTO> categories) {
		this.categories = categories;
	}

	public ArrayList<Long> getEventSectionsIDs() {
		return eventSectionsIDs;
	}

	public void setEventSectionsIDs(ArrayList<Long> eventSectionsIDs) {
		this.eventSectionsIDs = eventSectionsIDs;
	}

	public Long getLocationID() {
		return locationID;
	}

	public void setLocationID(Long locationID) {
		this.locationID = locationID;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

}
