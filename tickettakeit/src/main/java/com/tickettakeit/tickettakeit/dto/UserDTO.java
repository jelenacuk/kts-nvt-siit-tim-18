package com.tickettakeit.tickettakeit.dto;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTO {

	@NotNull
	@Size(min = 4)
	private String username;
	@NotNull
	@Size(min = 4)
	private String password;
	private String repeatedPassword;
	private String newPassword;
	@NotNull
	@Email
	private String email;
	@NotNull
	@Pattern(regexp = "[a-z A-Z]*", message = "First name must contain only letters")
	@Size(min = 2, max = 50)
	private String firstName;
	@NotNull
	@Pattern(regexp = "[a-z A-Z]*", message = "Last name must contain only letters")
	@Size(min = 2, max = 50)
	private String lastName;
	@NotNull
	private String phone;
	private String paypal;
	private List<UserTicketDTO> boughtTickets;
	private List<UserTicketDTO> reservedTickets;

	public UserDTO() {
	}

	public UserDTO(String username, String password, String email, String firstName, String lastName, String phone) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}

	public UserDTO(String username, String password, String email, String firstName, String lastName, String phone,
			String paypal) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.paypal = paypal;
	}

	public String getUsername() {
		return username;
	}

	

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRepeatedPassword() {
		return repeatedPassword;
	}

	public void setRepeatedPassword(String repeatedPassword) {
		this.repeatedPassword = repeatedPassword;
	}

	public String getPaypal() {
		return paypal;
	}

	public void setPaypal(String paypal) {
		this.paypal = paypal;
	}

	public List<UserTicketDTO> getBoughtTickets() {
		return boughtTickets;
	}

	public void setBoughtTickets(List<UserTicketDTO> boughtTickets) {
		this.boughtTickets = boughtTickets;
	}

	public List<UserTicketDTO> getReservedTickets() {
		return reservedTickets;
	}

	public void setReservedTickets(List<UserTicketDTO> reservedTicket) {
		this.reservedTickets = reservedTicket;
	}

}
