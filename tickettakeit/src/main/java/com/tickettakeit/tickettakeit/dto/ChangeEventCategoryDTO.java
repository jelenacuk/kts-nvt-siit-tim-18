package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;

public class ChangeEventCategoryDTO {
	@NotNull
	private Long eventId;
	@NotNull
	private Long categoryId;

	public ChangeEventCategoryDTO(Long eventId, Long category) {
		this.eventId = eventId;
		this.categoryId = category;
	}

	public ChangeEventCategoryDTO() {
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

}
