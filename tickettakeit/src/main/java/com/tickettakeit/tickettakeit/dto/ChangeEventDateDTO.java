package com.tickettakeit.tickettakeit.dto;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

public class ChangeEventDateDTO {
	@NotNull
	private Long id;
	@NotNull
	private ArrayList<Long> date;

	public ChangeEventDateDTO(Long id, ArrayList<Long> date) {
		this.id = id;
		this.date = date;
	}

	public ChangeEventDateDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArrayList<Long> getDate() {
		return date;
	}

	public void setDate(ArrayList<Long> date) {
		this.date = date;
	}

}
