package com.tickettakeit.tickettakeit.dto;

public class NewEventLocationDTO {
	private Long id;
	private String name;

	public NewEventLocationDTO() {
	}

	public NewEventLocationDTO(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
