package com.tickettakeit.tickettakeit.dto;

import java.util.ArrayList;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewEventWithBackgroundDTO {
	@NotNull
	@Size(min = 4, max = 100)
	private String name;
	@NotNull
	@Size(min = 1, max = 300)
	private String description;
	@NotNull
	@Size(min = 1)
	private ArrayList<Long> date;
	@NotNull
	private Long salesDate;
	private Long applicationDate;
	@NotNull
	private int status;
	@NotNull
	private int lastDayToPay;
	@NotNull
	private boolean controllAccess;
	private ArrayList<String> attachments;
	@NotNull
	private Long hallID;
	@NotNull
	private ArrayList<Long> categoriesIDs;
	@NotNull
	@Size(min = 1)
	private Map<Long, Float> eventSectionsIDs;
	@NotNull
	private Long locationID;
	private String background;
	private String backgroundName;

	public NewEventWithBackgroundDTO(@NotNull @Size(min = 4, max = 100) String name,
			@NotNull @Size(min = 1, max = 300) String description, @NotNull @Size(min = 1) ArrayList<Long> date,
			@NotNull Long salesDate, Long applicationDate, @NotNull int status, @NotNull int lastDayToPay,
			@NotNull boolean controllAccess, ArrayList<String> attachments, @NotNull Long hallID,
			@NotNull ArrayList<Long> categoriesIDs, @NotNull @Size(min = 1) Map<Long, Float> eventSectionsIDs,
			@NotNull Long locationID, String background, String backgroundName) {
		this.name = name;
		this.description = description;
		this.date = date;
		this.salesDate = salesDate;
		this.applicationDate = applicationDate;
		this.status = status;
		this.lastDayToPay = lastDayToPay;
		this.controllAccess = controllAccess;
		this.attachments = attachments;
		this.hallID = hallID;
		this.categoriesIDs = categoriesIDs;
		this.eventSectionsIDs = eventSectionsIDs;
		this.locationID = locationID;
		this.background = background;
		this.backgroundName = backgroundName;
	}

	public NewEventWithBackgroundDTO(NewEventDTO ne, String background, String backgroundName) {
		super();
		this.name = ne.getName();
		this.description = ne.getDescription();
		this.date = ne.getDate();
		this.salesDate = ne.getSalesDate();
		this.status = ne.getStatus();
		this.lastDayToPay = ne.getLastDayToPay();
		this.controllAccess = ne.isControllAccess();
		this.attachments = ne.getAttachments();
		this.hallID = ne.getHallID();
		this.categoriesIDs = ne.getCategoriesIDs();
		this.eventSectionsIDs = ne.getEventSectionsIDs();
		this.locationID = ne.getLocationID();
		this.background = background;
		this.backgroundName = backgroundName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Long> getDate() {
		return date;
	}

	public void setDate(ArrayList<Long> date) {
		this.date = date;
	}

	public Long getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Long salesDate) {
		this.salesDate = salesDate;
	}

	public Long getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Long applicationDate) {
		this.applicationDate = applicationDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getLastDayToPay() {
		return lastDayToPay;
	}

	public void setLastDayToPay(int lastDayToPay) {
		this.lastDayToPay = lastDayToPay;
	}

	public boolean isControllAccess() {
		return controllAccess;
	}

	public void setControllAccess(boolean controllAccess) {
		this.controllAccess = controllAccess;
	}

	public ArrayList<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}

	public Long getHallID() {
		return hallID;
	}

	public void setHallID(Long hallID) {
		this.hallID = hallID;
	}

	public ArrayList<Long> getCategoriesIDs() {
		return categoriesIDs;
	}

	public void setCategoriesIDs(ArrayList<Long> categoriesIDs) {
		this.categoriesIDs = categoriesIDs;
	}

	public Map<Long, Float> getEventSectionsIDs() {
		return eventSectionsIDs;
	}

	public void setEventSectionsIDs(Map<Long, Float> eventSectionsIDs) {
		this.eventSectionsIDs = eventSectionsIDs;
	}

	public Long getLocationID() {
		return locationID;
	}

	public void setLocationID(Long locationID) {
		this.locationID = locationID;
	}

	public NewEventWithBackgroundDTO() {
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getBackgroundName() {
		return backgroundName;
	}

	public void setBackgroundName(String backgroundName) {
		this.backgroundName = backgroundName;
	}

	public @Valid NewEventDTO getNewEventDTO() {
		return new NewEventDTO(name, description, date, salesDate, status, lastDayToPay, controllAccess, attachments,
				hallID, categoriesIDs, eventSectionsIDs, locationID);
	}

	@Override
	public String toString() {
		return "NewEventWithBackgroundDTO [name=" + name + ", description=" + description + ", date=" + date
				+ ", salesDate=" + salesDate + ", status=" + status + ", lastDayToPay=" + lastDayToPay
				+ ", controllAccess=" + controllAccess + ", attachments=" + attachments + ", hallID=" + hallID
				+ ", categoriesIDs=" + categoriesIDs + ", eventSectionsIDs=" + eventSectionsIDs + ", locationID="
				+ locationID + ", background=" + background + ", backgroundName=" + backgroundName + "]";
	}

}
