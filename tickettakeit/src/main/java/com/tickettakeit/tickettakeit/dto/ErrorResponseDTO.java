package com.tickettakeit.tickettakeit.dto;

import java.util.List;

public class ErrorResponseDTO {
	private List<ErrorDetailsDTO> errors;

	public ErrorResponseDTO() {
		super();
	}

	public ErrorResponseDTO(List<ErrorDetailsDTO> errors) {
		super();
		this.errors = errors;
	}

	public List<ErrorDetailsDTO> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorDetailsDTO> errors) {
		this.errors = errors;
	}

}
