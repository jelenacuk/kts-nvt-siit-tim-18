package com.tickettakeit.tickettakeit.dto;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateEventDTO {
	@NotNull
	private Long id;
	@NotNull
	@Size(min = 2, max = 20)
	private String name;
	@NotNull
	@Size(min = 1, max = 100)
	private String description;
	@NotNull
	private ArrayList<Long> date;
	@NotNull
	private ArrayList<String> attachments;
	@NotNull
	private Long hallID;
	@NotNull
	private ArrayList<Long> categoriesIDs;
	@NotNull
	private ArrayList<Long> eventSectionsIDs;
	@NotNull
	private Long locationID;

	public UpdateEventDTO() {
	}

	public UpdateEventDTO(Long id, String name, String description, ArrayList<Long> date, ArrayList<String> attachments,
			Long hallID, ArrayList<Long> categoriesIDs, ArrayList<Long> eventSectionsIDs, Long locationID) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.date = date;
		this.attachments = attachments;
		this.hallID = hallID;
		this.categoriesIDs = categoriesIDs;
		this.eventSectionsIDs = eventSectionsIDs;
		this.locationID = locationID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Long> getDate() {
		return date;
	}

	public void setDate(ArrayList<Long> date) {
		this.date = date;
	}

	public ArrayList<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}

	public Long getHallID() {
		return hallID;
	}

	public void setHallID(Long hallID) {
		this.hallID = hallID;
	}

	public ArrayList<Long> getCategoriesIDs() {
		return categoriesIDs;
	}

	public void setCategoriesIDs(ArrayList<Long> categoriesIDs) {
		this.categoriesIDs = categoriesIDs;
	}

	public ArrayList<Long> getEventSectionsIDs() {
		return eventSectionsIDs;
	}

	public void setEventSectionsIDs(ArrayList<Long> eventSectionsIDs) {
		this.eventSectionsIDs = eventSectionsIDs;
	}

	public Long getLocationID() {
		return locationID;
	}

	public void setLocationID(Long locationID) {
		this.locationID = locationID;
	}

}
