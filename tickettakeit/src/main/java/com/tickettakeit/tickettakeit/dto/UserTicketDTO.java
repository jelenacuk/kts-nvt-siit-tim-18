package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserTicketDTO {

	@NotNull
	private Long id;
	@NotNull
	private boolean bought;
	@NotNull
	@Size(min = 0)
	private int row;
	@NotNull
	@Size(min = 0)
	private int column;
	@NotNull
	@Size(min = 0)
	private double price;
	@NotNull
	private String forDay;
	@NotNull
	private String dayOfPurchase;
	@NotNull
	private Long eventID;
	@NotNull
	private String eventName;
	@NotNull
	private String location;
	@NotNull
	private String eventSectionName;
	private Long totalSize;
	private String ticketBackground;
	private Long eventSectionID;
	
	public UserTicketDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isBought() {
		return bought;
	}

	public void setBought(boolean bought) {
		this.bought = bought;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getForDay() {
		return forDay;
	}

	public void setForDay(String forDay) {
		this.forDay = forDay;
	}

	public String getDayOfPurchase() {
		return dayOfPurchase;
	}

	public void setDayOfPurchase(String dayOfPurchase) {
		this.dayOfPurchase = dayOfPurchase;
	}

	public Long getEventID() {
		return eventID;
	}

	public void setEventID(Long eventID) {
		this.eventID = eventID;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEventSectionName() {
		return eventSectionName;
	}

	public void setEventSectionName(String eventSectionName) {
		this.eventSectionName = eventSectionName;
	}

	public Long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Long totalSize) {
		this.totalSize = totalSize;
	}

	public String getTicketBackground() {
		return ticketBackground;
	}

	public void setTicketBackground(String ticketBackground) {
		this.ticketBackground = ticketBackground;
	}

	public Long getEventSectionID() {
		return eventSectionID;
	}

	public void setEventSectionID(Long eventSectionID) {
		this.eventSectionID = eventSectionID;
	}
	
	
}
