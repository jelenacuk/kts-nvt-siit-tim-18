package com.tickettakeit.tickettakeit.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PayPalDTO {
	
	@NotEmpty
    private String paymentID;
    @NotEmpty
    private String payerID;
    @NotNull
    private List<Long> reservationIds;
    private boolean cancelIfFail;
    
    
    public PayPalDTO() {
		super();
	}


	public PayPalDTO(@NotEmpty String paymentID, @NotEmpty String payerID, @NotNull List<Long> reservationIds,
			boolean cancelIfFail) {
		super();
		this.paymentID = paymentID;
		this.payerID = payerID;
		this.reservationIds = reservationIds;
		this.cancelIfFail = cancelIfFail;
	}


	@Override
	public String toString() {
		return "PayPalDTO [paymentID=" + paymentID + ", payerID=" + payerID + ", reservationIds=" + reservationIds
				+ ", cancelIfFail=" + cancelIfFail + "]";
	}


	public String getPaymentID() {
		return paymentID;
	}


	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}


	public String getPayerID() {
		return payerID;
	}


	public void setPayerID(String payerID) {
		this.payerID = payerID;
	}


	public List<Long> getReservationIds() {
		return reservationIds;
	}


	public void setReservationIds(List<Long> reservationIds) {
		this.reservationIds = reservationIds;
	}


	public boolean isCancelIfFail() {
		return cancelIfFail;
	}


	public void setCancelIfFail(boolean cancelIfFail) {
		this.cancelIfFail = cancelIfFail;
	}
    
	
}
