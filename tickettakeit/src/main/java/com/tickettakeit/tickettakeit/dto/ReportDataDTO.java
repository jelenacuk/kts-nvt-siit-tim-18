package com.tickettakeit.tickettakeit.dto;

public class ReportDataDTO {
	private double earnings;
	private int soldTickets;
	
	public ReportDataDTO() {
		super();
	}

	public ReportDataDTO(double earnings, int soldTickets) {
		super();
		this.earnings = earnings;
		this.soldTickets = soldTickets;
	}

	public double getEarnings() {
		return earnings;
	}

	public void setEarnings(double earnings) {
		this.earnings = earnings;
	}

	public int getSoldTickets() {
		return soldTickets;
	}

	public void setSoldTickets(int soldTickets) {
		this.soldTickets = soldTickets;
	}
	
}
