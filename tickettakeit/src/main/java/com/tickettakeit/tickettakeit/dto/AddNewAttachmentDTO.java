package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;

public class AddNewAttachmentDTO {
	@NotNull
	private Long id;

	@NotNull
	private String attachment;

	public AddNewAttachmentDTO(@NotNull Long id, @NotNull String attachmentName, @NotNull String attachment) {
		this.id = id;
		this.attachment = attachment;
	}

	public AddNewAttachmentDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

}
