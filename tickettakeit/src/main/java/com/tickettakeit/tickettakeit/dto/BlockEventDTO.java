package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;

public class BlockEventDTO {
	@NotNull
	private Long id;
	@NotNull
	private int status;

	public BlockEventDTO() {
	}

	public BlockEventDTO(Long id, int blocked) {
		this.id = id;
		this.status = blocked;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int blocked) {
		this.status = blocked;
	}

}
