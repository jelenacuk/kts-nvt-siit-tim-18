package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;

public class ChangeLastDayToPayDTO {
	@NotNull
	private Long id;
	@NotNull
	private int lastDayToPay;

	public ChangeLastDayToPayDTO(Long id, int lastDayToPay) {
		this.id = id;
		this.lastDayToPay = lastDayToPay;
	}

	public ChangeLastDayToPayDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLastDayToPay() {
		return lastDayToPay;
	}

	public void setLastDayToPay(int lastDayToPay) {
		this.lastDayToPay = lastDayToPay;
	}

}
