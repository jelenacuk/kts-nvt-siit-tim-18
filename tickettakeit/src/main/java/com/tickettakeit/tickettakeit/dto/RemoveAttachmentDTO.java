package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;

public class RemoveAttachmentDTO {
	private Long id;
	@NotNull
	private String attachment;

	public RemoveAttachmentDTO(Long id, String attachmentName) {
		this.id = id;
		this.attachment = attachmentName;
	}

	public RemoveAttachmentDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	

}
