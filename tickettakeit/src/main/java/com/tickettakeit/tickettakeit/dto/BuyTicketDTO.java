package com.tickettakeit.tickettakeit.dto;

import javax.validation.constraints.NotNull;

public class BuyTicketDTO {
	@NotNull
	private int row;
	@NotNull
	private int column;
	@NotNull
	private Long forDay;
	@NotNull
	private Long eventSectionID;
	@NotNull
	private Long eventID;

	public BuyTicketDTO() {
	}

	public BuyTicketDTO(int row, int column, Long forDay, Long eventSectionID, Long eventID) {
		this.row = row;
		this.column = column;
		this.forDay = forDay;
		this.eventSectionID = eventSectionID;
		this.eventID = eventID;
	}

	@Override
	public String toString() {
		return "BuyTicketDTO [row=" + row + ", column=" + column + ", forDay=" + forDay + ", eventSectionID="
				+ eventSectionID + ", eventID=" + eventID + "]";
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public Long getForDay() {
		return forDay;
	}

	public void setForDay(Long forDay) {
		this.forDay = forDay;
	}

	public Long getEventSectionID() {
		return eventSectionID;
	}

	public void setEventSectionID(Long eventSectionID) {
		this.eventSectionID = eventSectionID;
	}

	public Long getEventID() {
		return eventID;
	}

	public void setEventID(Long eventID) {
		this.eventID = eventID;
	}

}
