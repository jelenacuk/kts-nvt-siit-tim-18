package com.tickettakeit.tickettakeit.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventApplication;
import com.tickettakeit.tickettakeit.model.RegisteredUser;

@Repository
public interface EventApplicationRepository extends JpaRepository<EventApplication, Long> {
	Optional<EventApplication> findById(Long id);

	int countByEvent(Event e);

	Optional<EventApplication> findByUserAndEvent(RegisteredUser user, Event event);

	List<EventApplication> findByUser(RegisteredUser user);
}
