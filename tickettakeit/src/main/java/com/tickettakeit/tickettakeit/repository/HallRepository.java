package com.tickettakeit.tickettakeit.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.model.Hall;


@Repository
public interface HallRepository extends JpaRepository<Hall, Long> {
	@Query("SELECT hall FROM Location l JOIN l.halls hall where l.id = :locationId order by hall.id asc")
	public Optional<List<Hall>> findHallsFromLocation(Long locationId);

	@Query("SELECT DISTINCT hall FROM Location l JOIN l.halls hall where l.id = :locationId AND hall.id = :hallId")
	public Optional<Hall> findHallsFromLocationAndHall(Long locationId, Long hallId);

	Optional<Hall> findById(Long id);


}
