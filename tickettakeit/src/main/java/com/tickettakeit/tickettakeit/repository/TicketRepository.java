package com.tickettakeit.tickettakeit.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.dto.SeatDTO;
import com.tickettakeit.tickettakeit.model.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
	@Query("SELECT t FROM Ticket t WHERE t.eventSection.id = :id and t.row = :row and t.column = :column")
	Ticket findTicketByRowColumnEventSectionID(@Param("id") Long id, @Param("row") Integer row,
			@Param("column") Integer column);

	@Query("SELECT t FROM Ticket t WHERE t.eventSection.id = :id and t.forDay = :date and t.row = :row and t.column = :column")
	Ticket findTicketByDateRowColumnEventSectionID(@Param("date") Date date, @Param("id") Long id,
			@Param("row") Integer row, @Param("column") Integer column);

	@Query("SELECT COUNT(t) FROM Ticket t WHERE t.eventSection.id = :id")
	int countSoldTickets(@Param("id") Long id);

	@Query("SELECT t FROM Ticket t WHERE t.event.location.id = :locationId AND t.dayOfPurchase BETWEEN :fromDate AND :toDate")
	List<Ticket> soldTicketsForLocation(Long locationId, Date fromDate, Date toDate);

	@Query("SELECT t FROM Ticket t WHERE t.event.id = :eventId AND t.dayOfPurchase BETWEEN :fromDate AND :toDate")
	List<Ticket> soldTicketsForEvent(Long eventId, Date fromDate, Date toDate);

	@Query("SELECT t FROM Ticket t WHERE t.event.id = :eventId AND t.forDay = :eventDate AND t.dayOfPurchase BETWEEN :fromDate AND :toDate")
	List<Ticket> soldTicketsForEventDay(Long eventId, Date eventDate, Date fromDate, Date toDate);

	@Query("SELECT t FROM Ticket t WHERE t.forDay BETWEEN  :fromDay AND :toDay AND t.bought = true")
	List<Ticket> findBoughtTicketsForDay(Date fromDay, Date toDay);

	@Query("SELECT t FROM Ticket t WHERE t.user.id = :userId  AND t.bought = :bought")
	Page<Ticket> findTicketsByUser(Pageable pageable, Long userId, boolean bought);

	List<Ticket> findByBought(Boolean bought);

	@Query("SELECT NEW com.tickettakeit.tickettakeit.dto.SeatDTO(t.row, t.column,t.forDay) FROM Ticket t WHERE t.event.id = :eventId and t.eventSection.id = :sectionId")
	List<SeatDTO> getTakenSeats(Long eventId, Long sectionId);

	Optional<Ticket> findById(Long id);
}
