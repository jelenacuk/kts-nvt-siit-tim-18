package com.tickettakeit.tickettakeit.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Section;

@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {
	public List<Section> findByName(String name);

	public Optional<Section> findById(Long id);

	@Query("SELECT DISTINCT hall from Hall hall JOIN hall.sections section where section.id = :sectionId")//
	public Optional<Hall> findHallBySectionId(Long sectionId);
}
