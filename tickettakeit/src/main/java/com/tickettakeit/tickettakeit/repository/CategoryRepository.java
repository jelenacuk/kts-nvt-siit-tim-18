package com.tickettakeit.tickettakeit.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tickettakeit.tickettakeit.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	Category findByName(String name);
	
	Optional<Category> findById(Long id);
}
