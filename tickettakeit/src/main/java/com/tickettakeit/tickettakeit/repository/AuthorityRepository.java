package com.tickettakeit.tickettakeit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tickettakeit.tickettakeit.model.Authority;

public interface AuthorityRepository  extends JpaRepository<Authority, Long>{

}
