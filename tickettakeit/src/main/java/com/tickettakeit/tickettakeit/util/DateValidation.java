package com.tickettakeit.tickettakeit.util;

import java.util.Date;

import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;

public class DateValidation {

	public static boolean checkInThePast(Date fromDate, Date toDate) throws InvalidDateException {

		Date now = new Date();
		if (toDate.before(fromDate)) {
			throw new InvalidDateException("From date must be before to date.");
		}
		if (now.before(toDate)) {
			throw new InvalidDateException("Dates must be in the past.");
		}

		return true;
	}

	public static boolean checkInTheFuture(Date fromDate, Date toDate) throws InvalidDateException {

		Date now = new Date();
		if (toDate.before(fromDate)) {
			throw new InvalidDateException("From date must be before to date.");
		}
		if (now.after(fromDate)) {
			throw new InvalidDateException("Dates must be in the future.");
		}

		return true;
	}
	private DateValidation() {
		
	}
}
