insert into hall (id, name, active) values (100, 'Hall1', true);
insert into hall (id, name, active) values (101, 'Hall2', true);
insert into hall (id, name, active) values (102, 'Hall3', true);

insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats) values (100, 'east', 100, 100, true, 10000);
insert into section (id, name, seat_rows, seat_columns, is_numerated, number_of_seats) values (101, 'west', 100, 100, true, 10000);

insert into location (id, name, city, address, longitude, latitude, image, active) values (100, 'Spens', 'Novi Sad', 'Sutjeska 2', 45.247167, 19.845349, '', true);
insert into location (id, name, city, address, longitude, latitude, image, active) values (101, 'Beogradska arena', 'Beograd', 'Bulevar Arsenija Carnojevica 58', 44.814344, 20.421300, '', true);
insert into location (id, name, city, address, longitude, latitude, image, active) values (102, 'Sava centar', 'Beograd', 'Bulevar Arsenija Carnojevica 58', 44.814344, 20.421300, '', true);
insert into location (id, name, city, address, longitude, latitude, image, active) values (103, 'Location1', 'Novi Sad', 'Sutjeska 2', 45.247167, 19.845349, '', true);

insert into category (id, name, color, icon) values (100, 'fun', 'color1', 'icon1');
insert into category (id, name, color, icon) values (101, 'sports', 'color2', 'icon2');
insert into category (id, name, color, icon) values (102, 'category for deletion', 'color3', 'icon3');

insert into event_section (id, name, price, seat_rows, seat_columns, is_numerated, number_of_seats, left_position, top) values (100, 'east', 100, 100, 100, true, 1000, 0, 0);
insert into event_section (id, name, price, seat_rows, seat_columns, is_numerated, number_of_seats, left_position, top) values (101, 'west', 80,  100, 100, true, 1000, 0, 0);
insert into event_section (id, name, price, seat_rows, seat_columns, is_numerated, number_of_seats, left_position, top) values (102, 'east', 90, 100, 100, true, 1000, 0, 0);
insert into event_section (id, name, price, seat_rows, seat_columns, is_numerated, number_of_seats, left_position, top) values (103, 'west', 50,  100, 100, true, 1000, 0, 0);
insert into event_section (id, name, price, seat_rows, seat_columns, is_numerated, number_of_seats, left_position, top) values (104, 'east', 90, 100, 100, true, 1000, 0, 0);
insert into event_section (id, name, price, seat_rows, seat_columns, is_numerated, number_of_seats, left_position, top) values (105, 'west', 50,  100, 100, true, 1000, 0, 0);

insert into event (id, name, description, sales_date, status, last_day_to_pay, controll_access, ticket_background, hall_id, location_id )
	values (100, 'Event1', '', '2020-11-06 20:00:00', 0, 10, false, 'red-concert-tickets.jpg', 100, 101 );
insert into event (id, name, description, sales_date, status, last_day_to_pay, controll_access, ticket_background, hall_id, location_id )
	values (101, 'Event2', '', '2020-06-01 20:30:00', 0, 10, false, '',  100, 101 );
insert into event (id, name, description, sales_date, status, last_day_to_pay, controll_access, ticket_background, hall_id, location_id )
	values (102, 'Event3', '', '2020-11-28 19:00:00', 0, 20, false, '', 100, 100 );
insert into event (id, name, description, sales_date, status, last_day_to_pay, controll_access, ticket_background, hall_id, location_id )
	values (103, 'Event4', '', '2020-11-28 19:00:00', 0, 20, false, '', 100, 103 );
	
insert into event_date (event_id, date) values (100, '2020-12-06 20:30:00');
insert into event_date (event_id, date) values (101, '2020-06-04 20:30:00');
insert into event_date (event_id, date) values (102, '2020-12-28 19:00:00');
insert into event_date (event_id, date) values (103, '2020-12-28 19:00:00');

insert into event_categories (event_id, categories_id) values (100, 100);
insert into event_categories (event_id, categories_id) values (101, 100);
insert into event_categories (event_id, categories_id) values (102, 101);
insert into event_categories (event_id, categories_id) values (103, 101);

insert into event_event_sections (event_id, event_sections_id) values (100, 100);
insert into event_event_sections (event_id, event_sections_id) values (100, 101);
insert into event_event_sections (event_id, event_sections_id) values (101, 102);
insert into event_event_sections (event_id, event_sections_id) values (101, 103);
insert into event_event_sections (event_id, event_sections_id) values (102, 104);
insert into event_event_sections (event_id, event_sections_id) values (103, 105);

INSERT INTO registered_user (id, confirmed, email, first_name, last_name, password, phone, username) VALUES (100, 1, 'pera@pera.com', 'pera', 'peric','$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq','nema', 'pera');
INSERT INTO registered_user (id, confirmed, email, first_name, last_name, password, phone, username) VALUES (200, 1, 'pera@pera.com', 'pera', 'peric','$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq','nema', 'pera_user');
INSERT INTO authority (id, name) VALUES (100, 'ADMIN');
INSERT INTO authority (id, name) VALUES (200, 'REGISTERED');
INSERT INTO user_authority (user_id,authority_id) values(100, 100);
INSERT INTO user_authority (user_id,authority_id) values(200, 200);

insert into ticket (id, bought, code, seat_column, seat_row, day_of_purchase, for_day, price, event_id, event_section_id, user_id) values 
				   (100, true, 24324, 12, 12, '2019-10-10 20:30:00', '2020-12-06 20:30:00', 100, 100, 100, 200);
insert into ticket (id, bought, code, seat_column, seat_row, day_of_purchase, for_day, price, event_id, event_section_id, user_id) values 
				   (101, true, 24324, 12, 12, '2019-10-20 20:30:00', '2020-10-05 20:00:00', 120, 100, 100, 200);
insert into ticket (id, bought, code, seat_column, seat_row, day_of_purchase, for_day, price, event_id, event_section_id, user_id) values 
				   (102, false, 24324, 12, 12, null, '2019-11-28 20:00:00', 100, 102, 100, 200);
insert into ticket (id, bought, code, seat_column, seat_row, day_of_purchase, for_day, price, event_id, event_section_id, user_id) values 
				   (103, true, 24324, 12, 12, '2019-10-10 20:30:00', '2019-10-05 20:00:00', 110, 102, 100, 200);
				   
