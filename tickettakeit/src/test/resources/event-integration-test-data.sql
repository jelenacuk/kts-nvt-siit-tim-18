/* Adds categories */
INSERT INTO `tickettakeit_test`.`category` (`id`, `color`, `icon`, `name`) VALUES ('10', 'Blue', 'Test Icon 1', 'Music Festival');
INSERT INTO `tickettakeit_test`.`category` (`id`, `color`, `icon`, `name`) VALUES ('20', 'Red', 'Test Icon 2', 'Art Exibition');

/* Adds locations */
INSERT INTO `tickettakeit_test`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('10', 'Address 1', 'Novi Sad', 'Test Image 1', '19.859807', '45.255826', 'Location 1', TRUE);
INSERT INTO `tickettakeit_test`.`location` (`id`, `address`, `city`, `image`, `latitude`, `longitude`, `name`, `active`) VALUES ('20', 'Address 2', 'Belgrade', 'Test Image 2', '20.455915', '44.788317', 'Location 2', TRUE);

/* Adds halls */
INSERT INTO `tickettakeit_test`.`hall` (`id`, `name`, `active`) VALUES ('10', 'Hall 1', TRUE);
INSERT INTO `tickettakeit_test`.`hall` (`id`, `name`, `active`) VALUES ('20', 'Hall 2', TRUE);
INSERT INTO `tickettakeit_test`.`hall` (`id`, `name`, `active`) VALUES ('30', 'Hall 3', TRUE);

/* Adds sections */
INSERT INTO `tickettakeit_test`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`, `active`,`left_position`,`top`) VALUES ('10', '10', TRUE, 'Section 1', '100', '10', TRUE,0,0);
INSERT INTO `tickettakeit_test`.`section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `seat_rows`, `active`,`left_position`,`top`) VALUES ('20', '0', FALSE, 'Section 2', '100', '0', TRUE,0,0);

/* Adds halls to locations */
INSERT INTO `tickettakeit_test`.`location_halls` (`location_id`, `halls_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`location_halls` (`location_id`, `halls_id`) VALUES ('10', '20');
INSERT INTO `tickettakeit_test`.`location_halls` (`location_id`, `halls_id`) VALUES ('20', '30');

/* Adds sections to halls */
INSERT INTO `tickettakeit_test`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`hall_sections` (`hall_id`, `sections_id`) VALUES ('10', '20');

/* Adds registered users */
INSERT INTO `tickettakeit_test`.`registered_user` (`id`, `confirmed`, `email`, `first_name`, `last_name`, `password`, `phone`, `username`, `paypal`) VALUES ('10', TRUE, 'user@mail.com', 'User', 'Useric', '$2y$12$7agwOLLBYv6.GzIxJnK3QOd/yeRMfYb1A6TIHJf.nS2UDfDOD0MJu', '03024104', 'user', 'nema');

/* Adds authorities */
INSERT INTO `tickettakeit_test`.`authority` (`id`, `name`) VALUES ('10', 'REGISTERED');
INSERT INTO `tickettakeit_test`.`authority` (`id`, `name`) VALUES ('20', 'ADMIN');

/* Adds authorities to users */
INSERT INTO `tickettakeit_test`.`user_authority` (`user_id`, `authority_id`) VALUES ('10', '20');

/* Adds events */
INSERT INTO `tickettakeit_test`.`event` (`id`, `controll_access`, `description`, `last_day_to_pay`, `name`, `sales_date`, `status`, `hall_id`, `location_id`) VALUES ('10', FALSE, 'Event Desc', '5', 'Event 1', '2020-9-10', '0', '10', '10');

/* Adds categories to events */
INSERT INTO `tickettakeit_test`.`event_categories` (`event_id`, `categories_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`event_categories` (`event_id`, `categories_id`) VALUES ('10', '20');

/* Adds dates to events */
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('10', '2020-10-10');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('10', '2020-10-11');
INSERT INTO `tickettakeit_test`.`event_date` (`event_id`, `date`) VALUES ('10', '2020-10-12');

/* Adds event sections */
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`,`left_position`,`top`) VALUES ('10', '10', TRUE, 'Section 1', '100', '200', '10',0,0);
INSERT INTO `tickettakeit_test`.`event_section` (`id`, `seat_columns`, `is_numerated`, `name`, `number_of_seats`, `price`, `seat_rows`,`left_position`,`top`) VALUES ('20', '0', FALSE, 'Section 2', '100', '100', '0',0,0);

/* Adds event sections to events */
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('10', '10');
INSERT INTO `tickettakeit_test`.`event_event_sections` (`event_id`, `event_sections_id`) VALUES ('10', '20');
