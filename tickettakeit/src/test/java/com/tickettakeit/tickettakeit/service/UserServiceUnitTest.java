package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.config.JwtTokenUtil;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.exceptions.AccountIsNotActiveException;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserAlredyExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;
import com.tickettakeit.tickettakeit.model.Authority;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.model.User;
import com.tickettakeit.tickettakeit.repository.UserRepository;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
public class UserServiceUnitTest {

	// @SpyBean
	@Autowired
	UserService userService;

	@MockBean
	UserRepository repository;

	@MockBean
	private AuthenticationManager authenticationManager;

	@MockBean
	private JwtUserDetailsService jwtUserDetailsService;

	@MockBean
	private JwtTokenUtil jwtTokenUtil;

	@MockBean
	private AuthorityService authorityService;

	@MockBean
	private EmailService emailService;

	private User user;
	private UserDTO userDTO;
	private LogInDTO loginDTO;
	private Authority authority;
	private User userToEdit;
	private BCryptPasswordEncoder encoder;

	@org.junit.Before
	public void setUp() throws MailException, InterruptedException {

		user = new RegisteredUser("mira", "mira", "mira@gmail.com", "023-848-848", "Mira", "Miric", true);
		userDTO = new UserDTO("mira", "mira", "mira@gmail.com", "Mira", "Miric", "023-848-848", "paypal");
		userDTO.setRepeatedPassword("mira");
		authority = new Authority(1l, "REGISTERED");
		encoder = new BCryptPasswordEncoder();
		loginDTO = new LogInDTO("mira", "mira");
		String bcryptHashedPassword = "$2a$04$j7IaKvVnaqtinjM.hVH5r.nsyS8/cmLN/oANc7EKkgMkjgDBaN6Xm";
		userToEdit = new RegisteredUser("mira", bcryptHashedPassword, "mira@gmail.com", "023/848-848", "Mira", "Miric");
	}

//REGISTRATION

	@Test
	public void whenRegister_Successful_True()
			throws MailException, InterruptedException, UserAlredyExistsException, PasswordConfirmationException {

		Mockito.when(authorityService.findByID(authority.getId())).thenReturn(authority);
		Mockito.when(emailService.sendRegistrationEmail(user)).thenReturn(true);

		userService.register(userDTO);
		ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
		verify(repository, times(1)).save(argument.capture());
		User savedUser = argument.getValue();
		assertEquals(userDTO.getUsername(), savedUser.getUsername());
		assertEquals(userDTO.getEmail(), savedUser.getEmail());
		assertEquals(userDTO.getFirstName(), savedUser.getFirstName());
		assertEquals(userDTO.getLastName(), savedUser.getLastName());
		assertTrue(encoder.matches(userDTO.getPassword(), savedUser.getPassword()));
		assertEquals(1, savedUser.getAuthorities().size());
		assertEquals(true, savedUser.getAuthorities().contains(authority));
		assertEquals(false, savedUser.isConfirmed());
	}

	@Test(expected = UserAlredyExistsException.class)
	public void whenRegister_ExistingUser_thenUserAlreadyExistException()
			throws UserAlredyExistsException, PasswordConfirmationException {

		Mockito.doReturn(Optional.of(user)).when(repository).findOneByUsername(userDTO.getUsername());
		UserDTO result = userService.register(userDTO);
		assertEquals(null, result);

	}

	@Test(expected = PasswordConfirmationException.class)
	public void whenRegister_WrongRepeatedPassword_throwPasswordConfirmationException()
			throws UserAlredyExistsException, PasswordConfirmationException {

		String password = userDTO.getPassword();
		userDTO.setRepeatedPassword(userDTO.getPassword() + "WrongRepaeted");
		UserDTO result = userService.register(userDTO);
		assertEquals(null, result);
		// return to correct value
		userDTO.setRepeatedPassword(password);

	}

//LOGIN

	@Test
	public void whenLogin_Successful_ReturnJwtToken() throws UserNotExistsException, AccountIsNotActiveException {

		UserDetails userDetails = user;
		Mockito.when(jwtUserDetailsService.loadUserByUsername(loginDTO.getUsername())).thenReturn(userDetails);
		Mockito.when(jwtTokenUtil.generateToken(userDetails)).thenReturn("jwtToken");
		String generatedToken = userService.createAuthenticationToken(loginDTO);

		// checks if method passes valid username to
		// jwtUserDetailsService.loadByUsername
		ArgumentCaptor<String> argument1 = ArgumentCaptor.forClass(String.class);
		verify(jwtUserDetailsService, times(1)).loadUserByUsername(argument1.capture());
		String sentUsername = argument1.getValue();
		assertEquals(loginDTO.getUsername(), sentUsername);

		// checks if passes valid UserDetails to jwtTokenUtil.generateToken
		ArgumentCaptor<UserDetails> argument2 = ArgumentCaptor.forClass(UserDetails.class);
		verify(jwtTokenUtil, times(1)).generateToken(argument2.capture());
		User sentUserDetails = (User) argument2.getValue();
		assertEquals(loginDTO.getUsername(), sentUserDetails.getUsername());

		assertEquals("jwtToken", generatedToken);
	}

	@Test(expected = AccountIsNotActiveException.class)
	public void whenLogin_NoActivatedAccount_throwAccountIsNotActiveException()
			throws UserNotExistsException, AccountIsNotActiveException {

		// crete user and set confirmed attribute (last parameter in constructor) on
		// false
		UserDetails userDetails = new RegisteredUser("mira", "mira", "mira@gmail.com", "023-848-848", "Mira", "Miric",
				false);
		Mockito.when(jwtUserDetailsService.loadUserByUsername(loginDTO.getUsername())).thenReturn(userDetails);
		userService.createAuthenticationToken(loginDTO);
	}

	@Test(expected = UserNotExistsException.class)
	public void whenLogin_BadCredentials_throwUserNotExistsException()
			throws UserNotExistsException, AccountIsNotActiveException {

		Mockito.when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class)))
				.thenThrow(BadCredentialsException.class);
		userService.createAuthenticationToken(loginDTO);
	}

//ACTIVATE ACCOUNT

	@Test
	public void activateAccount_Successful_True() throws UnsupportedEncodingException, UserNotExistsException {

		user.setConfirmed(false);
		// Base64 converter for username: 'mira'
		String encodedUsername = "bWlyYQ==";
		Mockito.doReturn(Optional.of(user)).when(repository).findOneByUsername("mira");
		Boolean activated = userService.activateAccount(encodedUsername);
		assertEquals(true, activated);
		assertEquals(true, user.isConfirmed());

	}

	@Test(expected = UserNotExistsException.class)
	public void whenActivateAccount_WrongUsername_throwUserNotExistsException()

			throws UnsupportedEncodingException, UserNotExistsException {

		Boolean activated = userService.activateAccount("");
		assertEquals(false, activated);

	}

//EDIT PROFILE

	@Test
	public void whenEditProfile_Successful_True() throws PasswordConfirmationException, UserNotExistsException {
		
		UserDTO editDTO = new UserDTO(userToEdit.getUsername(), "mira", "New" + userToEdit.getEmail(),
				userToEdit.getFirstName() + "New", userToEdit.getLastName() + "New", userToEdit.getPhone() + "New",
				"paypal");
		editDTO.setNewPassword(userToEdit.getPassword() + "New");
		editDTO.setRepeatedPassword(userToEdit.getPassword() + "New");

		Mockito.doReturn(Optional.of(userToEdit)).when(repository).findOneByUsername(editDTO.getUsername());
		boolean ok = userService.editUserProfile(editDTO);
		assertEquals(true, ok);

		assertEquals(editDTO.getUsername(), userToEdit.getUsername());
		assertEquals(editDTO.getEmail(), userToEdit.getEmail());
		assertEquals(editDTO.getFirstName(), userToEdit.getFirstName());
		assertEquals(editDTO.getLastName(), userToEdit.getLastName());
		assertTrue(encoder.matches(editDTO.getNewPassword(), userToEdit.getPassword()));

	}

	@Test(expected = PasswordConfirmationException.class)
	public void whenEditProfile_WrongRepeatedPassword_throwPasswordConfirmationError()
			throws PasswordConfirmationException, UserNotExistsException {

		UserDTO editDTO = new UserDTO(userToEdit.getUsername(), userToEdit.getPassword(),
				"New" + userToEdit.getEmail(), userToEdit.getFirstName() + "New", userToEdit.getLastName() + "New",
				userToEdit.getPhone() + "New", "paypal");
		editDTO.setRepeatedPassword(editDTO.getNewPassword() + "Different");
		Mockito.doReturn(Optional.of(userToEdit)).when(repository).findOneByUsername(editDTO.getUsername());
		boolean ok = userService.editUserProfile(editDTO);
		assertEquals(false, ok);
	}
	
	@Test(expected = PasswordConfirmationException.class)
	public void whenEditProfile_WrongCurrentPassword_throwPasswordConfirmationError()
			throws PasswordConfirmationException, UserNotExistsException {

		String currentPassword = userToEdit.getPassword() + "wrong";
		UserDTO editDTO = new UserDTO(userToEdit.getUsername(), currentPassword,
				"New" + userToEdit.getEmail(), userToEdit.getFirstName() + "New", userToEdit.getLastName() + "New",
				userToEdit.getPhone() + "New", "paypal");
		Mockito.doReturn(Optional.of(userToEdit)).when(repository).findOneByUsername(editDTO.getUsername());
		boolean ok = userService.editUserProfile(editDTO);
		assertEquals(false, ok);
	}

	@Test(expected = UserNotExistsException.class)
	public void whenEditingProfile_InvalidUsername_throwUserNotExistException()
			throws PasswordConfirmationException, UserNotExistsException {

		UserDTO editDTO = new UserDTO(userToEdit.getUsername(), userToEdit.getPassword(),
				"New" + userToEdit.getEmail(), userToEdit.getFirstName() + "New", userToEdit.getLastName() + "New",
				userToEdit.getPhone() + "New", "paypal");
		boolean ok = userService.editUserProfile(editDTO);
		assertEquals(false, ok);
	}
	

//GET PROFILE

	@Test
	public void whenGetProfile_Successful_returnUserProfile() throws UserNotExistsException {
		UserService userService = Mockito.spy(UserService.class);
		Mockito.doReturn(user).when(userService).getLoggedUser();
		UserDTO returnedUser = userService.getProfile();
		assertEquals(user.getUsername(), returnedUser.getUsername());
		assertEquals(user.getPassword(), returnedUser.getPassword());
		assertEquals(user.getFirstName(), returnedUser.getFirstName());
		assertEquals(user.getLastName(), returnedUser.getLastName());
		assertEquals(user.getEmail(), returnedUser.getEmail());
		assertEquals(user.getPhone(), returnedUser.getPhone());
	}

	@Test(expected = UserNotExistsException.class)
	public void whenGetProfile_InvalidCredentials_throwUserNotExistsException() throws UserNotExistsException {
		
		UserService userService = Mockito.spy(UserService.class);
		Mockito.doReturn(null).when(userService).getLoggedUser();
		UserDTO returnedUser = userService.getProfile();
		assertEquals(null, returnedUser);
	}
}
