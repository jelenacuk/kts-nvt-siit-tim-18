package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.converter.LocationConverter;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.dto.LocationDTO;
import com.tickettakeit.tickettakeit.exceptions.LocationAlreadyExistsExceptions;
import com.tickettakeit.tickettakeit.exceptions.LocationCreatingException;
import com.tickettakeit.tickettakeit.exceptions.LocationDeleteException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationUpdateException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.EventRepository;
import com.tickettakeit.tickettakeit.repository.LocationRepository;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
@Transactional
@Rollback(value = true)
public class LocationServiceUnitTest {
	@Autowired
	private LocationService service;
	@MockBean
	private LocationRepository repository;
	@MockBean
	private EventService eventService;

	@Before
	public void setUp() {
		Optional<Location> l = Optional.of(new Location("Spens", "Novi Sad", "Adresa", 10, 11, "", new HashSet<>(),true));
		l.get().setImage(null);
		l.get().setId(1l);
		Mockito.when(repository.findById(l.get().getId())).thenReturn(l);
		List<Location> found = new ArrayList<Location>();
		found.add(l.get());
		Mockito.when(repository.findByNameAndCityAndAddress("Spens", "Novi Sad", "Adresa")).thenReturn(found);
		Mockito.when(repository.findById(1l)).thenReturn(l);
		Mockito.when(repository.save(ArgumentMatchers.any(Location.class))).thenReturn(l.get());

	}

	@Test
	public void whencreateLocation_thenCreateLocation() throws LocationAlreadyExistsExceptions, LocationCreatingException, FileNotFoundException, IOException {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("Ulica 11");
		dto.setCity("Beograd");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(null);
		dto.setImage(null);
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Kombank arena");
		dto.setId(2l);
		Mockito.when(repository.save(ArgumentMatchers.any(Location.class))).thenReturn(LocationConverter.fromDto(dto));
		List<Location> locations = new ArrayList<Location>();
		locations.add(LocationConverter.fromDto(dto));
		Mockito.when(repository.findAll()).thenReturn(locations);

		Location l = service.createLocation(dto);
		List<Location> list = service.findAll();
		assertEquals(1, list.size());
		assertEquals("Kombank arena", l.getName());

	}

	@Test(expected = LocationAlreadyExistsExceptions.class)
	public void whencreateLocation_thenAlredyExists() throws LocationAlreadyExistsExceptions, LocationCreatingException, FileNotFoundException, IOException {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("Adresa");
		dto.setCity("Novi Sad");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(null);
		dto.setImage(null);
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Spens");
		dto.setId(1l);
		service.createLocation(dto);
	}
	@Test
	public void whenupdateLocation_thenUpdate() throws LocationNotExistsException, LocationUpdateException, FileNotFoundException, IOException {
		Location l = new Location("Arena","Beograd","Adresa",0,0,null,new HashSet<Hall>(),true);
		Location location = service.save(l);
		LocationDTO dto = LocationConverter.toDto(location);
		dto.setName("ArenaUpdated");
		Mockito.when(repository.save(ArgumentMatchers.any(Location.class))).thenReturn(LocationConverter.fromDto(dto));
		Location location2 = service.updateLocation(dto);
		assertEquals("ArenaUpdated", location2.getName());
	}
	@Test(expected = LocationNotExistsException.class)
	public void whenupdateLocation_thenNotExist() throws LocationNotExistsException, LocationUpdateException, FileNotFoundException, IOException {
		Location loc = new Location("Arena","Beograd","Adresa",0,0,"image",new HashSet<Hall>(),true);
		LocationDTO dto = LocationConverter.toDto(loc);
		dto.setId(999l);
		Location location2 = service.updateLocation(dto);
	}
	
	@Test
	public void whenremoveLocation_thenRemove() throws LocationDeleteException, LocationNotExistsException {
		Optional<Location> l = Optional.of(new Location("Spens", "Novi Sad", "Adresa", 10, 11, "", new HashSet<>(),true));
		l.get().setId(10l);
		l.get().setActive(false);
		Mockito.when(repository.findById(10l)).thenReturn(l);
		Mockito.when(repository.save(ArgumentMatchers.any(Location.class))).thenReturn(l.get());
		Location loc = service.removeLocation(10l);
		assertFalse(loc.getActive());
	}
	@Test(expected = LocationNotExistsException.class)
	public void whenremoveLocation_LocationNotExist() throws LocationDeleteException, LocationNotExistsException {
		service.removeLocation(1115l);
	}
	@Test(expected = LocationDeleteException.class)
	public void whenremoveLocation_thenEventExist() throws ParseException, LocationDeleteException, LocationNotExistsException {
		Optional<Location> l = Optional.of(new Location("Spens", "Novi Sad", "Adresa", 10, 11, "", new HashSet<>(),true));
		l.get().setId(10l);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date d = sdf.parse("11-11-2025");
		Set<Date> dates = new HashSet<Date>();
		dates.add(d);
		Event e = new Event(1l, "Event", "Opis",dates , new Date(), new Date(), EventStatus.NORMAL, 2, false, "Str", null, null, null, null, null);
		List<Event> events = new ArrayList<Event>();
		events.add(e);
		Mockito.when(eventService.selectEventsFromLocation(10l)).thenReturn(Optional.of(events));
		Mockito.when(repository.findById(10l)).thenReturn(l);
		service.removeLocation(10l);
	}
	
	

}
