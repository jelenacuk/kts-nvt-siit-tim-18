package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.EventDTO;
import com.tickettakeit.tickettakeit.dto.NewEventWithBackgroundDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventDateBeforeSalesDateException;
import com.tickettakeit.tickettakeit.exceptions.EventNameIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.HallIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.HallIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.HallNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SectionIsBlocked;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.model.Event;

@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:event-integration-test.properties")
public class EventServiceIntegrationTest {

	@Autowired
	EventService eventService;

	private NewEventWithBackgroundDTO newEventDTO;
	private String name;
	private String description;
	private ArrayList<Long> date;
	private Long salesDate;
	private int status;
	private int lastDayToPay;
	private boolean controlAccess;
	private ArrayList<String> attachments;
	private Long hallID;
	private ArrayList<Long> categoriesIDs;
	private HashMap<Long, Float> eventSectionsIDs;
	private Long locationID;
	private Date today;

	@Before
	public void setUp() {
		// Sets acceptable data for adding new Event.
		today = new Date();

		name = "Event 2";
		description = "Event Desc 2";
		date = new ArrayList<Long>();
		date.add(today.getTime() + 10 * (1000 * 60 * 60 * 24));
		date.add(today.getTime() + 11 * (1000 * 60 * 60 * 24));
		salesDate = today.getTime() + 5 * (1000 * 60 * 60 * 24);
		status = 0;
		lastDayToPay = 5;
		controlAccess = false;
		attachments = new ArrayList<String>();
		hallID = 10L;
		categoriesIDs = new ArrayList<Long>();
		categoriesIDs.add(10L);
		categoriesIDs.add(20L);
		eventSectionsIDs = new HashMap<Long, Float>();
		eventSectionsIDs.put(10L, new Float(150));
		eventSectionsIDs.put(20L, new Float(250));
		locationID = 10L;

		newEventDTO = new NewEventWithBackgroundDTO(name, description, date, salesDate, null, status, lastDayToPay,
				controlAccess, attachments, hallID, categoriesIDs, eventSectionsIDs, locationID, null, name);

	}

	@Test
	public void whenGetEvent_thenReturnEventDTO() throws EventNotExistsException {
		String name = "Event 1";
		String description = "Event Desc";
		ArrayList<Long> date = new ArrayList<Long>();
		date.add(1602280800000L);
		date.add(1602367200000L);
		date.add(1602453600000L);
		Long salesDate = 1599688800000L;
		int status = 0;
		int lastDayToPay = 5;
		boolean controlAccess = false;
		// ArrayList<String> attachments = new ArrayList<String>();
		Long hallID = 10L;
		ArrayList<Long> categoriesIDs = new ArrayList<Long>();
		categoriesIDs.add(10L);
		categoriesIDs.add(20L);
		ArrayList<Long> eventSectionsIDs = new ArrayList<Long>();
		eventSectionsIDs.add(10L);
		eventSectionsIDs.add(20L);
		Long locationID = 10L;

		EventDTO eventDTO = eventService.getEvent(10L);

		assertEquals(name, eventDTO.getName());
		assertEquals(description, eventDTO.getDescription());
		assertEquals(date.size(), eventDTO.getDate().size());
		assertEquals(salesDate, eventDTO.getSalesDate());
		assertEquals(status, eventDTO.getStatus());
		assertEquals(lastDayToPay, eventDTO.getLastDayToPay());
		assertEquals(controlAccess, eventDTO.isControllAccess());
		assertTrue(eventDTO.getAttachments().isEmpty());
		assertEquals(hallID, eventDTO.getHallID());
		assertEquals(categoriesIDs.size(), eventDTO.getCategories().size());
		assertEquals(eventSectionsIDs.size(), eventDTO.getEventSectionsIDs().size());
		assertTrue(eventDTO.getEventSectionsIDs().contains(eventSectionsIDs.get(0)));
		assertTrue(eventDTO.getEventSectionsIDs().contains(eventSectionsIDs.get(1)));
		assertEquals(locationID, eventDTO.getLocationID());
	}

	@Test(expected = EventNotExistsException.class)
	public void whenGetEventWrongId_thenThrowEventNotExistsException() throws EventNotExistsException {
		// There is no Event with ID 100L in database.
		eventService.getEvent(100L);
	}

	@Test
	public void whenAddEventWithoutBackground_thenReturnEvent() throws IOException, HallNotExistsException,
			LocationNotExistsException, CategoryNotExistsException, HallIsTakenException, EventNameIsTakenException,
			SectionNotExistsException, SalesDateHasPassedException, EventDateBeforeSalesDateException,
			EventNotExistsException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {

		Event addedEvent = eventService.addEvent(newEventDTO);

		assertEquals(name, addedEvent.getName());
		assertEquals(description, addedEvent.getDescription());
		assertEquals(date.size(), addedEvent.getDate().size());
		assertTrue(addedEvent.getDate().contains(new Date(date.get(0))));
		assertTrue(addedEvent.getDate().contains(new Date(date.get(1))));
		assertEquals(salesDate, addedEvent.getSalesDate().getTime());
		assertEquals(status, addedEvent.getStatus().ordinal());
		assertEquals(lastDayToPay, addedEvent.getLastDayToPay());
		assertEquals(controlAccess, addedEvent.isControllAccess());
		assertTrue(addedEvent.getAttachments().isEmpty());
		assertEquals(hallID, addedEvent.getHall().getId());
		assertEquals(categoriesIDs.size(), addedEvent.getCategories().size());
		assertEquals(locationID, addedEvent.getLocation().getId());
	}

	@Test(expected = SalesDateHasPassedException.class)
	public void whenAddEventSalesDateBeforeToday_thenSalesDateBeforeTodayException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Sets sales date 1 day before today.
		newEventDTO.setSalesDate(today.getTime() - (1000 * 60 * 60 * 24));

		eventService.addEvent(newEventDTO);

		// Return to old values.
		newEventDTO.setSalesDate(salesDate);
	}

	@Test(expected = EventDateBeforeSalesDateException.class)
	public void whenAddEventDateBeforeToday_EventDateBeforeSalesDateException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds date 1 day before today.
		Long dateBeforeToday = new Date().getTime() - (1000 * 60 * 60 * 24);
		newEventDTO.getDate().add(dateBeforeToday);

		eventService.addEvent(newEventDTO);

		// Return to old values.
		newEventDTO.getDate().remove(dateBeforeToday);

	}

	@Test(expected = EventDateBeforeSalesDateException.class)
	public void whenAddEventDateBeforeSalesDate_EventDateBeforeSalesDateException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds date 1 day before sales date.
		Long dateBeforeSalesDate = new Date().getTime() - 4 * (1000 * 60 * 60 * 24);
		newEventDTO.getDate().add(dateBeforeSalesDate);

		eventService.addEvent(newEventDTO);

		// Return to old values.
		newEventDTO.getDate().remove(dateBeforeSalesDate);

	}

	@Test(expected = EventNameIsTakenException.class)
	public void whenAddEventNameExists_EventNameIsTakenException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Sets new Event name to existing one.
		newEventDTO.setName("Event 1");

		eventService.addEvent(newEventDTO);

		// Returns new Event name to old one.
		newEventDTO.setName(name);
	}

	@Test(expected = HallNotExistsException.class)
	public void whenAddEventHallIsMissing_thenThrowHallNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Changes Hall so it doesn't exist.
		newEventDTO.setHallID(15L);

		eventService.addEvent(newEventDTO);

		// Return to old values.
		newEventDTO.setHallID(hallID);
	}

	@Test(expected = LocationNotExistsException.class)
	public void whenAddEventLocationIsMissing_thenThrowLocationNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Changes Location so it doesn't exist.
		newEventDTO.setLocationID(15L);

		eventService.addEvent(newEventDTO);

		// Return to old values.
		newEventDTO.setHallID(locationID);
	}

	@Test(expected = CategoryNotExistsException.class)
	public void whenAddEventCategoryIsMissing_thenThrowCategoryNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds Category that doesn't exist.
		categoriesIDs.add(15L);

		eventService.addEvent(newEventDTO);

		// Return to old values.
		categoriesIDs.remove(2);
	}

	@Test(expected = SectionNotExistsException.class)
	public void whenAddEventEventSectionIsMissing_thenThrowEventSectionNotExistsException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Adds EventSection that doesn't exist.
		eventSectionsIDs.put(15L, new Float(150));

		eventService.addEvent(newEventDTO);

		// Return to old values.
		eventSectionsIDs.remove(15L);
	}

	@Test(expected = HallIsTakenException.class)
	public void whenAddEventHallIsTaken_thenThrowHallIsTakenExceptionException()
			throws IOException, HallNotExistsException, LocationNotExistsException, CategoryNotExistsException,
			HallIsTakenException, EventNameIsTakenException, SectionNotExistsException, SalesDateHasPassedException,
			EventDateBeforeSalesDateException, HallIsBlocked, LocationIsBlocked, SectionIsBlocked {
		// Sets new Event date to be conflicting with existing one.
		date.add(1602280800000L);

		eventService.addEvent(newEventDTO);

		// Removes new added date.
		date.remove(2);
	}
}
