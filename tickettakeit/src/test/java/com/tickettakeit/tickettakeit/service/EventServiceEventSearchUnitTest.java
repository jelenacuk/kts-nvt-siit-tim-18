package com.tickettakeit.tickettakeit.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.repository.EventRepository;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
public class EventServiceEventSearchUnitTest {

	@Autowired
	private EventService eventService;

	@MockBean
	private EventRepository eventRepository;

	private static PageRequest pageRequest = PageRequest.of(0, 4);

	private ArrayList<Long> categoryIds = new ArrayList<>();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Before
	public void setUp() {

		// Set up event categories
		Category category1 = new Category("Category 1", "", "");
		Category category2 = new Category("Category 2", "", "");
		// Category category3 = new Category("Category 3", "", "");

		// Set up event sections
		EventSection es1 = new EventSection("east", 10, 10, true, 0, 100);
		EventSection es2 = new EventSection("west", 10, 10, true, 0, 150);
		EventSection es3 = new EventSection("south", 10, 10, true, 0, 200);
		EventSection es4 = new EventSection("north", 10, 10, true, 0, 250);

		Set<EventSection> sections1 = new HashSet<>();
		sections1.add(es1);

		Set<EventSection> sections2 = new HashSet<>();
		sections2.add(es2);

		Set<EventSection> sections3 = new HashSet<>();
		sections3.add(es3);
		sections3.add(es4);

		// Set up locations
		Location location1 = new Location("Location 1", "City 1", "Address 1", 100, 100, "", null,true);
		Location location2 = new Location("Location 2", "City 1", "Address 2", 100, 100, "", null,true);

		// Set up halls
		Hall hall1 = new Hall("Hall 1");
		// Hall hall2 = new Hall("Hall 2");

		// Set up events
		Event event1 = new Event("Event 1", EventStatus.NORMAL, 0, false, hall1);
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime());
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime());
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 22).getTime());
		event1.getCategories().add(category1);
		event1.setLocation(location1);

		Event event2 = new Event("Event 2", EventStatus.NORMAL, 0, false, hall1);
		event2.getDate().add(new GregorianCalendar(2020, Calendar.APRIL, 20).getTime());
		event2.getCategories().add(category1);
		event2.getCategories().add(category2);
		event2.setLocation(location2);

		Event event3 = new Event("Event 3", EventStatus.NORMAL, 0, false, hall1);
		event3.getCategories().add(category1);
		event3.setLocation(location2);

		Event event4 = new Event("Event 4", EventStatus.NORMAL, 0, false, hall1);

		event1.setEventSections(sections1);
		event2.setEventSections(sections2);
		event3.setEventSections(sections3);

		// results
		List<Event> events = new ArrayList<Event>();
		events.add(event1);
		events.add(event2);
		events.add(event3);
		events.add(event4);

		List<Event> eventsLocationSearch = new ArrayList<Event>();
		eventsLocationSearch.add(event1);
		eventsLocationSearch.add(event2);
		eventsLocationSearch.add(event3);

		List<Event> detailedSearch1 = new ArrayList<Event>();
		detailedSearch1.add(event1);

		List<Event> detailedSearch2 = new ArrayList<Event>();
		detailedSearch2.add(event1);
		detailedSearch2.add(event2);

		// general search
		Mockito.when(eventRepository.generalSearch(pageRequest, "Event")).thenReturn(new PageImpl(events));
		Mockito.when(eventRepository.generalSearch(pageRequest, "Location"))
				.thenReturn(new PageImpl(eventsLocationSearch));
		Mockito.when(eventRepository.generalSearch(pageRequest, "City")).thenReturn(new PageImpl(eventsLocationSearch));
		Mockito.when(eventRepository.generalSearch(pageRequest, "ajgparijg"))
				.thenReturn(new PageImpl(new ArrayList<Event>()));
		Mockito.when(eventRepository.generalSearch(pageRequest, "")).thenReturn(new PageImpl(events));

		// detailed search
		Mockito.when(eventRepository.detailedSearch(pageRequest, "Event 1", "City 1", "Location 1", 0, 2000,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds))
				.thenReturn(new PageImpl(detailedSearch1));
		Mockito.when(eventRepository.detailedSearch(pageRequest, "Event ", "City ", "Location", 0, 2000,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds))
				.thenReturn(new PageImpl(detailedSearch2));
		Mockito.when(eventRepository.detailedSearch(pageRequest, "ahodghro", "City 1", "Location 1", 0, 2000,
				new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds))
				.thenReturn(new PageImpl(new ArrayList<Event>()));
		Mockito.when(eventRepository.detailedSearch(pageRequest, "Event 1", "adfhoaw", "Location 1", 0, 2000,
				new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds))
				.thenReturn(new PageImpl(new ArrayList<Event>()));
	}

	// ***** generalSearch *****

	@Test
	@Transactional
	public void generalSearch_matchEventName() {
		String searchStr = "Event";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertThat(result).hasSize(4).allMatch(event -> event.getName().contains(searchStr));

	}

	@Test
	@Transactional
	public void generalSearch_matchLocationName() {
		String searchStr = "Location";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertThat(result).hasSize(3).allMatch(event -> event.getLocation().getName().contains(searchStr));

	}

	@Test
	@Transactional
	public void generalSearch_matchCity() {
		String searchStr = "City";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertThat(result).hasSize(3).allMatch(event -> event.getLocation().getCity().contains(searchStr));

	}

	@Test
	@Transactional
	public void generalSearch_emptyString() {
		String searchStr = "";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertThat(result).hasSize(4);

	}

	@Test
	@Transactional
	public void generalSearch_noResults() {
		String searchStr = "ajgparijg";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertThat(result).hasSize(0);

	}

	// ***** detailedSearch *****

	@Test
	@Transactional
	public void detailedSearch_matchAll() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "Event 1", "City 1", "Location 1", 0,
				2000, new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		assertThat(foundEvents).hasSize(1).allMatch(event -> event.getName().equals("Event 1"));
	}

	@Test
	@Transactional
	public void detailedSearch_matchSubstrings() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "Event ", "City ", "Location", 0, 2000,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.MAY, 20).getTime(), categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		assertThat(foundEvents).hasSize(2)
				.allMatch(event -> event.getName().equals("Event 1") || event.getName().equals("Event 2"));
	}

	@Test
	@Transactional
	public void detailedSearch_noMatchingEventName() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "ahodghro", "City 1", "Location 1", 0,
				2000, new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		assertThat(foundEvents).hasSize(0);

	}

	@Test
	public void detailedSearch_noMatchingCity() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "Event 1", "adfhoaw", "Location 1", 0,
				2000, new GregorianCalendar(2029, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2029, Calendar.MAY, 20).getTime(), categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		assertThat(foundEvents).hasSize(0);

	}
}
