package com.tickettakeit.tickettakeit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:ana-test.properties")
public class EventServiceEventSearchIntegrationTest {

	@Autowired
	EventService eventService;

	private static PageRequest pageRequest = PageRequest.of(0, 4);
	private static List<Long> categoryIds = new ArrayList<>();
	private static Date fromDate = new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime();
	private static Date toDate = new GregorianCalendar(2022, Calendar.MAY, 20).getTime();

	@Before
	public void setUp() {
		categoryIds.add(100L);
		categoryIds.add(101L);
	}

	// ***** generalSearch *****

	@Test
	public void generalSearch_matchEventName_Multiple() {
		String searchStr = "Event";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result =  resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(4).allMatch(event -> event.getName().contains(searchStr));

	}

	@Test
	public void generalSearch_matchEventName_One() {
		String searchStr = "Event2";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(1);
		assertEquals(searchStr, result.get(0).getName());

	}

	@Test
	public void generalSearch_matchEventName_NoResults() {
		String searchStr = "30urwihfpghv";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();
		
		assertNotNull(result);
		assertThat(result).hasSize(0);

	}

	@Test
	public void generalSearch_matchLocationName() {
		String searchStr = "Spens";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();
		
		assertNotNull(result);
		assertThat(result).hasSize(1).allMatch(event -> event.getLocation().getName().contains(searchStr));

	}

	@Test
	public void generalSearch_matchLocationName_NoResults() {
		String searchStr = "Spenss";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(0);

	}

	@Test
	public void generalSearch_matchCity() {
		String searchStr = "Beograd";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(2).allMatch(event -> event.getLocation().getCity().contains(searchStr));

	}

	@Test
	public void generalSearch_matchCity_NoResults() {
		String searchStr = "Kraljevo";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(0);

	}

	@Test
	public void generalSearch_matchEventNameAndLocationName() {
		String searchStr = "1";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(2).allMatch(
				event -> event.getLocation().getName().contains(searchStr) || event.getName().contains(searchStr));

	}

	@Test
	public void generalSearch_emptyString() {
		String searchStr = "";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(4);

	}

	@Test
	public void generalSearch_noResults() {
		String searchStr = "argt45";
		Page<Event> resultPage = eventService.generalSearch(pageRequest, searchStr);
		List<Event> result = resultPage.toList();

		assertNotNull(result);
		assertThat(result).hasSize(0);

	}

	// ***** detailedSearch *****

	@Test
	@Transactional
	public void detailedSearch_matchAll() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "Event1", "Beograd", "Beogradska arena", 0,
				2000, fromDate, toDate, categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(1).allMatch(event -> event.getName().equals("Event1"));
	}

	@Test
	@Transactional
	public void detailedSearch_matchSubstrings() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "Event", "Beograd", "", 0, 2000, fromDate,
				toDate, categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(2)
				.allMatch(event -> event.getName().equals("Event1") || event.getName().equals("Event2"));
	}

	@Test
	@Transactional
	public void detailedSearch_matchEmptyStrings() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "", "", 0, 2000, fromDate, toDate,
				categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(4);
	}

	@Test
	@Transactional
	public void detailedSearch_matchCategory() throws InvalidDateException {
		List<Long> category = new ArrayList<>();
		category.add(100L);

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "", "", 0, 2000, fromDate, toDate,
				category);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(2).allMatch(event -> event.getCategories().stream().map(Category::getId)
				.collect(Collectors.toSet()).contains(category.get(0)));
	}

	@Test
	@Transactional
	public void detailedSearch_noMatchingCategory() throws InvalidDateException {
		List<Long> category = new ArrayList<>();
		category.add(11111L);

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "", "", 0, 2000, fromDate, toDate,
				category);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(0);
	}

	@Test
	@Transactional
	public void detailedSearch_noMatchingPrice() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "", "", 0, 2, fromDate, toDate,
				categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(0);
	}

	@Test
	@Transactional
	public void detailedSearch_noMatchingDates() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "", "", 0, 2,
				new GregorianCalendar(2024, Calendar.FEBRUARY, 20).getTime(),
				new GregorianCalendar(2024, Calendar.MAY, 20).getTime(), categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(0);
	}

	@Test
	@Transactional
	public void detailedSearch_noMatchingEventName() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "ahodghro", "", "", 0, 2000, fromDate,
				toDate, categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(0);

	}

	@Test
	public void detailedSearch_noMatchingCity() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "adfhoaw", "", 0, 2000, fromDate, toDate,
				categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(0);

	}

	@Test
	public void detailedSearch_noMatchingLocationName() throws InvalidDateException {

		Page<Event> foundEventsPage = eventService.detailedSearch(pageRequest, "", "", "hgho", 0, 2000, fromDate, toDate,
				categoryIds);
		List<Event> foundEvents = foundEventsPage.toList();
		
		assertNotNull(foundEvents);
		assertThat(foundEvents).hasSize(0);

	}

	@Test(expected = InvalidDateException.class)
	public void detailedSearch_invalidDates() throws InvalidDateException {

		eventService.detailedSearch(pageRequest, "", "", "", 0, 2000, toDate, fromDate, categoryIds);

	}

}
