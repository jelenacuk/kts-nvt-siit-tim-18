package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.converter.LocationConverter;
import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.dto.LocationDTO;
import com.tickettakeit.tickettakeit.exceptions.LocationAlreadyExistsExceptions;
import com.tickettakeit.tickettakeit.exceptions.LocationCreatingException;
import com.tickettakeit.tickettakeit.exceptions.LocationDeleteException;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.LocationUpdateException;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test-mile.properties")
@Transactional
@Rollback(value = true)
public class LocationServiceIntegrationTest {
	@Autowired
	private LocationService service;


	@Test
	public void whencreateLocation_thenCreateLocation() throws LocationAlreadyExistsExceptions, LocationCreatingException, FileNotFoundException, IOException {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("Ulica 11");
		dto.setCity("Beograd");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(null);
		dto.setImage(null);
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Kombank arena");

		Location location = service.createLocation(dto);
		assertEquals(dto.getName(), location.getName());

	}

	@Test(expected = LocationAlreadyExistsExceptions.class)
	public void whencreateLocation_thenAlredyExists() throws LocationAlreadyExistsExceptions, LocationCreatingException, FileNotFoundException, IOException {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("Sutjeska 2");
		dto.setCity("Novi Sad");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(null);
		dto.setImage("");
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Spens");
		dto.setId(1l);
		service.createLocation(dto);
	}
	@Test
	public void whenupdateLocation_thenUpdate() throws LocationNotExistsException, LocationUpdateException, FileNotFoundException, IOException {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("adreasa");
		dto.setName("ArenaUpdated");
		dto.setCity("Novi Sad");
		dto.setId(20l);
		dto.setHalls(new HashSet<HallDTO>());
		Location location2 = service.updateLocation(dto);
		assertEquals("ArenaUpdated", location2.getName());
	}
	@Test(expected = LocationNotExistsException.class)
	public void whenupdateLocation_thenNotExist() throws LocationNotExistsException, LocationUpdateException, FileNotFoundException, IOException {
		Location loc = new Location("Arena","Beograd","Adresa",0,0,"image",new HashSet<Hall>(),true);
		LocationDTO dto = LocationConverter.toDto(loc);
		dto.setId(999l);
		Location location2 = service.updateLocation(dto);
	}
	@Test(expected = LocationNotExistsException.class)
	public void whendeleteLocation_thenNotExist() throws LocationNotExistsException, LocationUpdateException, LocationDeleteException {
		Location location2 = service.removeLocation(23l);
		assertFalse(location2.getActive());
	}
	@Test
	public void whendeleteLocation_thenDelete() throws LocationNotExistsException, LocationUpdateException, LocationDeleteException {
		Location location2 = service.removeLocation(21l);
		assertFalse(location2.getActive());
	}
	@Test(expected = LocationDeleteException.class)
	public void whendeleteLocation_thenHasEvent() throws LocationNotExistsException, LocationUpdateException, LocationDeleteException {
		Location location2 = service.removeLocation(22l);
		assertFalse(location2.getActive());
	}
	
}
