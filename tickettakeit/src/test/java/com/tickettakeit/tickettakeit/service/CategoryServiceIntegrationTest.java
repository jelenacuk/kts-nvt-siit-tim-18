package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.ObjectCannotBeDeletedException;
import com.tickettakeit.tickettakeit.model.Category;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:ana-test.properties")
public class CategoryServiceIntegrationTest {

	@Autowired
	private CategoryService categoryService;

	// ***** addCategory *****

	@Test
	@Transactional
	public void addCategory_success() throws CategoryAlreadyExistsException, FileNotFoundException, IOException {

		CategoryDTO dto = new CategoryDTO(null, "New category", "color", "icon, path");
		Category savedCategory = categoryService.addCategory(dto);

		assertNotNull(savedCategory);
		assertEquals(dto.getName(), savedCategory.getName());
		assertEquals(dto.getColor(), savedCategory.getColor());
		assertEquals("category_icons/New category.jpg", savedCategory.getIcon());

	}

	@Test(expected = CategoryAlreadyExistsException.class)
	@Transactional
	public void addCategory_nameExists() throws CategoryAlreadyExistsException, FileNotFoundException, IOException {

		CategoryDTO dto = new CategoryDTO(null, "fun", "color", "icon path");
		categoryService.addCategory(dto);

	}

	// ***** updateCategory *****

	@Test
	@Transactional
	public void updateCategory_success() throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {

		CategoryDTO dto = new CategoryDTO(100L, "fun updated", "color updated", "icon, path updated");
		Category realUpdated = categoryService.updateCategory(dto);

		assertEquals(dto.getName(), realUpdated.getName());
		assertEquals(dto.getColor(), realUpdated.getColor());
		assertEquals(dto.getIcon(), realUpdated.getIcon());
	}

	@Test(expected = CategoryNotExistsException.class)
	@Transactional
	public void updateCategory_categoryNotExists() throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {

		CategoryDTO dto = new CategoryDTO(13332L, "Category 2 updated", "color updated", "icon path updated");
		categoryService.updateCategory(dto);

	}

	@Test(expected = CategoryAlreadyExistsException.class)
	@Transactional
	public void updateCategory_nameExists() throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {

		CategoryDTO dto = new CategoryDTO(101L, "fun", "color updated", "icon path updated");
		categoryService.updateCategory(dto);

	}
	

	// ***** deleteCategory *****
	
	@Test(expected = CategoryNotExistsException.class)
	@Transactional
	public void deleteCategory_success() throws CategoryNotExistsException, CategoryAlreadyExistsException, ObjectCannotBeDeletedException {
		
		assertTrue(categoryService.deleteCategory(102L));

		categoryService.findById(102L);
	}
	
	@Test(expected = ObjectCannotBeDeletedException.class)
	@Transactional
	public void deleteCategory_eventsExist() throws CategoryNotExistsException, ObjectCannotBeDeletedException {
		
		categoryService.deleteCategory(100L);
	}
	
	@Test(expected = CategoryNotExistsException.class)
	@Transactional
	public void deleteCategory_categoryNotExists() throws CategoryNotExistsException, ObjectCannotBeDeletedException {

		categoryService.deleteCategory(111L);

	}
	
}
