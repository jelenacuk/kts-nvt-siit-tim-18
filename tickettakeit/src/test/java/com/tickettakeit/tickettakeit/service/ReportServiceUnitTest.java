package com.tickettakeit.tickettakeit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.ReportDTO;
import com.tickettakeit.tickettakeit.dto.ReportDataDTO;
import com.tickettakeit.tickettakeit.exceptions.InvalidDateException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.REPORT_TYPE;
import com.tickettakeit.tickettakeit.model.Ticket;
import com.tickettakeit.tickettakeit.repository.TicketRepository;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
public class ReportServiceUnitTest {

	@MockBean
	private TicketRepository ticketRepository;

	@Autowired
	private ReportService reportService;

	@Before
	public void setUp() {

		// Set up locations
		Location location1 = new Location("Location 1", "City 1", "Address 1", 100, 100, "", null, true);
		Location location2 = new Location("Location 2", "City 1", "Address 2", 100, 100, "", null, true);

		// Set up halls
		Hall hall1 = new Hall("Hall 1");
		// Hall hall2 = new Hall("Hall 2");

		// Set up events
		Event event1 = new Event("Event 1", EventStatus.NORMAL, 0, false, hall1);
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime());
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime());
		event1.getDate().add(new GregorianCalendar(2020, Calendar.FEBRUARY, 22).getTime());
		event1.setLocation(location1);

		Event event2 = new Event("Event 2", EventStatus.NORMAL, 0, false, hall1);
		event2.getDate().add(new GregorianCalendar(2020, Calendar.APRIL, 20).getTime());
		event2.setLocation(location2);

		Event event3 = new Event("Event 3", EventStatus.NORMAL, 0, false, hall1);
		event3.setLocation(location2);

		//Event event4 = new Event("Event 4", EventStatus.NORMAL, 0, false, hall1);

		// Date dayOfPurchase, boolean bought, int row, int column, Date forDay, Event
		// event
		Ticket t1 = new Ticket(new GregorianCalendar(2019, Calendar.OCTOBER, 20).getTime(), true, 10, 10,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(), event1, 100);
		Ticket t2 = new Ticket(new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(), true, 11, 10,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(), event1, 120);
		Ticket t3 = new Ticket(null, false, 11, 10, new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime(),
				event1);
		//Ticket t4 = new Ticket(new GregorianCalendar(2019, Calendar.APRIL, 20).getTime(), true, 11, 10,
				//new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(), event3);

		// soldTicketsForLocation - for positive test
		List<Ticket> result1 = new ArrayList<Ticket>();
		result1.add(t1);
		result1.add(t2);

		Mockito.when(
				ticketRepository.soldTicketsForLocation(1L, new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
						new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime()))
				.thenReturn(result1);

		// soldTicketsForLocation - for negative test
		Mockito.when(
				ticketRepository.soldTicketsForLocation(4L, new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
						new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime()))
				.thenReturn(new ArrayList<Ticket>());

		// soldTicketsForEvent - for positive test
		List<Ticket> result2 = new ArrayList<Ticket>();
		result2.add(t2);

		List<Ticket> result2_1 = new ArrayList<Ticket>();
		result2_1.add(t1);

		Mockito.when(
				ticketRepository.soldTicketsForEvent(1L, new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
						new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime()))
				.thenReturn(result2);
		Mockito.when(
				ticketRepository.soldTicketsForEvent(1L, new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
						new GregorianCalendar(2019, Calendar.SEPTEMBER, 21).getTime()))
				.thenReturn(result2);
		Mockito.when(
				ticketRepository.soldTicketsForEvent(1L, new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
						new GregorianCalendar(2019, Calendar.SEPTEMBER, 23).getTime()))
				.thenReturn(result2);
		Mockito.when(
				ticketRepository.soldTicketsForEvent(1L, new GregorianCalendar(2019, Calendar.SEPTEMBER, 1).getTime(),
						new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime()))
				.thenReturn(result2);
		Mockito.when(
				ticketRepository.soldTicketsForEvent(1L, new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime(),
						new GregorianCalendar(2019, Calendar.NOVEMBER, 1).getTime()))
				.thenReturn(result2_1);

		// soldTicketsForEvent - for negative test
		Mockito.when(
				ticketRepository.soldTicketsForEvent(4L, new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
						new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime()))
				.thenReturn(new ArrayList<Ticket>());

		// soldTicketsForEventDay - for positive test
		List<Ticket> result3 = new ArrayList<Ticket>();
		result3.add(t3);

		Mockito.when(ticketRepository.soldTicketsForEventDay(1L,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime(),
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime())).thenReturn(result3);

		// soldTicketsForEventDay - for negative test

		Mockito.when(ticketRepository.soldTicketsForEventDay(1L,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 22).getTime(),
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime())).thenReturn(new ArrayList<Ticket>());
	}

	// ***** createReportData *****
	@Test
	@Transactional
	public void createReportData_ForLocation_Success() {

		ReportDataDTO dto = reportService.createReportData(1L, null, null,
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());

		assertNotNull(dto);
		assertEquals(2, dto.getSoldTickets());
		assertEquals(220, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForLocation_NoTickets() {

		ReportDataDTO dto = reportService.createReportData(4L, null, null,
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());

		assertNotNull(dto);
		assertEquals(0, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEvent_Success() {

		ReportDataDTO dto = reportService.createReportData(null, 1L, null,
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime());

		assertNotNull(dto);
		assertEquals(1, dto.getSoldTickets());
		assertEquals(120, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEvent_NoTickets() {

		ReportDataDTO dto = reportService.createReportData(null, 4L, null,
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());

		assertNotNull(dto);
		assertEquals(0, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createReportData_ForEventDay_Success() {

		ReportDataDTO dto = reportService.createReportData(null, 1L,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 21).getTime(),
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());

		assertNotNull(dto);
		assertEquals(1, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0); // ticket is not paid

	}

	@Test
	@Transactional
	public void createReportData_ForEventDay_NoTickets() {

		ReportDataDTO dto = reportService.createReportData(null, 1L,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 22).getTime(),
				new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());

		assertNotNull(dto);
		assertEquals(0, dto.getSoldTickets());
		assertEquals(0, dto.getEarnings(), 0);

	}

	// **** createDailyReport *****
	@Test
	@Transactional
	public void createDailyReport_Success() {

		ArrayList<ReportDataDTO> dtos = reportService.dailyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 27).getTime());

		assertNotNull(dtos);
		assertEquals(7, dtos.size());
		assertEquals(1, dtos.get(0).getSoldTickets());
		assertEquals(120, dtos.get(0).getEarnings(), 0);
		dtos.remove(0);
		assertThat(dtos).hasSize(6).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	@Test
	@Transactional
	public void createDailyReport_NoTickets() {

		ArrayList<ReportDataDTO> dtos = reportService.dailyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.MARCH, 20).getTime(),
				new GregorianCalendar(2019, Calendar.MARCH, 27).getTime());

		assertNotNull(dtos);
		assertThat(dtos).hasSize(7).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// **** createWeeklyReport *****
	@Test
	@Transactional
	public void createWeeklyReport_Success() {

		ArrayList<ReportDataDTO> dtos = reportService.weeklyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 27).getTime());

		assertNotNull(dtos);
		assertEquals(2, dtos.size());
		assertEquals(1, dtos.get(0).getSoldTickets());
		assertEquals(120, dtos.get(0).getEarnings(), 0);
		dtos.remove(0);
		assertThat(dtos).hasSize(1).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	@Test
	@Transactional
	public void createWeeklyReport_NoTickets() {

		ArrayList<ReportDataDTO> dtos = reportService.weeklyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.MARCH, 20).getTime(),
				new GregorianCalendar(2019, Calendar.MARCH, 27).getTime());

		assertNotNull(dtos);
		assertThat(dtos).hasSize(2).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// **** createMonthlyReport *****
	@Test
	@Transactional
	public void createMonthlyReport_Success_OneMonth() {

		ArrayList<ReportDataDTO> dtos = reportService.monthlyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime());

		assertNotNull(dtos);
		assertEquals(1, dtos.size());
		assertEquals(1, dtos.get(0).getSoldTickets());
		assertEquals(120, dtos.get(0).getEarnings(), 0);

	}

	@Test
	@Transactional
	public void createMonthlyReport_Success_ThreeMonths() {

		ArrayList<ReportDataDTO> dtos = reportService.monthlyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.DECEMBER, 1).getTime());

		assertNotNull(dtos);
		assertEquals(3, dtos.size());
		assertEquals(1, dtos.get(0).getSoldTickets());
		assertEquals(120, dtos.get(0).getEarnings(), 0);
		assertEquals(1, dtos.get(1).getSoldTickets());
		assertEquals(100, dtos.get(1).getEarnings(), 0);
		assertEquals(0, dtos.get(2).getSoldTickets());
		assertEquals(0, dtos.get(2).getEarnings(), 0);

	}

	// TODO Granicni slucajevi sa datumima

	@Test
	@Transactional
	public void createMonthlyReport_NoTickets() {

		ArrayList<ReportDataDTO> dtos = reportService.monthlyReport(null, 1L, null,
				new GregorianCalendar(2019, Calendar.FEBRUARY, 1).getTime(),
				new GregorianCalendar(2019, Calendar.MARCH, 1).getTime());

		assertNotNull(dtos);
		assertThat(dtos).hasSize(1).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// ***** createReports *****

	// daily report
	@Test
	@Transactional
	public void createReports_Daily_Success() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 27).getTime(), REPORT_TYPE.DAILY, null);

		ArrayList<ReportDataDTO> result = reportService.createReports(reportDto);

		assertNotNull(result);
		assertEquals(7, result.size());
		assertEquals(1, result.get(0).getSoldTickets());
		assertEquals(120, result.get(0).getEarnings(), 0);
		result.remove(0);
		assertThat(result).hasSize(6).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// weekly report
	@Test
	@Transactional
	public void createReports_Weekly_Success() throws InvalidDateException {
		ReportDTO reportDto = new ReportDTO(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 27).getTime(), REPORT_TYPE.WEEKLY, null);

		ArrayList<ReportDataDTO> result = reportService.createReports(reportDto);

		assertNotNull(result);
		assertEquals(2, result.size());
		assertEquals(1, result.get(0).getSoldTickets());
		assertEquals(120, result.get(0).getEarnings(), 0);
		result.remove(0);
		assertThat(result).hasSize(1).allMatch(d -> d.getSoldTickets() == 0 && d.getEarnings() == 0);

	}

	// monthly report
	@Test
	@Transactional
	public void createReports_Monthly_Success() throws InvalidDateException {
		ReportDTO reportDto = new ReportDTO(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 1).getTime(), REPORT_TYPE.MONTHLY, null);

		ArrayList<ReportDataDTO> result = reportService.createReports(reportDto);

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(1, result.get(0).getSoldTickets());
		assertEquals(120, result.get(0).getEarnings(), 0);

	}

	// invalid dates -> toDate in the future
	@Test(expected = InvalidDateException.class)
	@Transactional
	public void createReports_toDateInTheFuture() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2022, Calendar.SEPTEMBER, 27).getTime(), REPORT_TYPE.DAILY, null);

		reportService.createReports(reportDto);

	}

	// invalid dates -> fromDate in the future
	@Test(expected = InvalidDateException.class)
	@Transactional
	public void createReports_fromDateInTheFuture() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 1L, null,
				new GregorianCalendar(2022, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2022, Calendar.SEPTEMBER, 27).getTime(), REPORT_TYPE.DAILY, null);

		reportService.createReports(reportDto);

	}

	// invalid dates -> fromDate after toDate
	@Test(expected = InvalidDateException.class)
	@Transactional
	public void createReports_fromDateAfterToDate() throws InvalidDateException {

		ReportDTO reportDto = new ReportDTO(null, 1L, null,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 27).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(), REPORT_TYPE.DAILY, null);

		reportService.createReports(reportDto);

	}
}
