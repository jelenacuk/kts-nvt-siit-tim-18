package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.SectionAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionCreatingException;
import com.tickettakeit.tickettakeit.exceptions.SectionDeleteException;
import com.tickettakeit.tickettakeit.exceptions.SectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.SectionUpdateException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Section;
import com.tickettakeit.tickettakeit.repository.SectionRepository;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
@Transactional
@Rollback(value = true)
public class SectionServiceUnitTest {
	@MockBean
	SectionRepository repository;
	@Autowired
	SectionService service;
	@MockBean
	HallService hallService;
	@MockBean
	private EventService eventService;

	@Before
	public void setUp() {
		Hall hall = new Hall(1l, "Hall1", new HashSet<Section>(),true);
		Section sec = new Section(1l, "S", 1, 1, false, 1,true);
		hall.getSections().add(sec);
		Mockito.when(hallService.findOne(1l)).thenReturn(hall);
	}

	@Test
	public void whencreateSection_thenCreateSection() throws SectionAlreadyExistsException, SectionCreatingException {
		Section s = new Section(1l, "Section", 10, 10, false, 100,true);
		Mockito.when(repository.findById(1l)).thenReturn(Optional.of(s));
		Mockito.when(repository.save(ArgumentMatchers.any(Section.class))).thenReturn(s);

		SectionDTO dto = new SectionDTO(null, "Section", 10, 10, false, 100,true);
		Section section = service.createSection(1l, dto);
		assertEquals("Section", section.getName());
		assertEquals(2, hallService.findOne(1l).getSections().size());
	}

	@Test(expected = SectionAlreadyExistsException.class)
	public void whenCreateSection_SectionExists() throws SectionAlreadyExistsException, SectionCreatingException {
		Section s = new Section(1l, "S", 10, 10, false, 100,true);
		Mockito.when(repository.findById(1l)).thenReturn(Optional.of(s));
		Mockito.when(repository.save(ArgumentMatchers.any(Section.class))).thenReturn(s);

		SectionDTO dto = new SectionDTO(null, "S", 10, 10, false, 100,true);
		Section section = service.createSection(1l, dto);
		assertEquals(s.getName(), section.getName());
	}
	
	@Test
	public void whenUpdateSection_thenUpdate() throws SectionNotExistsException, SectionUpdateException {
		Section s = new Section(1l, "S", 10, 10, false, 100,true);
		Mockito.when(repository.findById(1l)).thenReturn(Optional.of(s));
		Mockito.when(repository.save(ArgumentMatchers.any(Section.class))).thenReturn(s);
		SectionDTO dto = new SectionDTO(null, "Section", 10, 10, false, 100,true);
		Section section = service.updateSection(1l, dto);
		assertEquals("Section", repository.findById(1l).get().getName());
	}
	
	@Test(expected = SectionNotExistsException.class)
	public void whenUpdateSection_thenNotExist() throws SectionNotExistsException, SectionUpdateException {
		Section s = new Section(1l, "S", 10, 10, false, 100,true);
		Mockito.when(repository.findById(1l)).thenReturn(Optional.of(s));
		Mockito.when(repository.save(ArgumentMatchers.any(Section.class))).thenReturn(s);
		SectionDTO dto = new SectionDTO(null, "Section", 10, 10, false, 100,true);
		Section section = service.updateSection(5l, dto);
		assertEquals("Section", repository.findById(1l).get().getName());
	}
	
	@Test
	public void whenRemove_thenRemove() throws SectionNotExistsException, SectionDeleteException {
		Section sec = new Section(10l, "Sec", 5, 5, true, 25, false);
		Hall h = new Hall(10l,"Hall",new HashSet<Section>(),true);
		Mockito.when(repository.save(ArgumentMatchers.any(Section.class))).thenReturn(sec);
		Mockito.when(repository.findHallBySectionId(10l)).thenReturn(Optional.of(h));
		Mockito.when(repository.findById(10l)).thenReturn(Optional.of(sec));
		Section s = service.remove(10l);
		assertFalse(s.getActive());
		
	}
	@Test(expected = SectionNotExistsException.class)
	public void whenRemove_thenNotExists() throws SectionNotExistsException, SectionDeleteException {
		service.remove(55552l);
	}
	@Test(expected = SectionDeleteException.class)
	public void whenRemove_thenHasEvent() throws ParseException, SectionNotExistsException, SectionDeleteException {
		Hall h = new Hall(10l,"Hall",new HashSet<Section>(),true);
		Section sec = new Section(10l, "Sec", 5, 5, true, 25, false);
		Mockito.when(repository.findHallBySectionId(10l)).thenReturn(Optional.of(h));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date d = sdf.parse("11-11-2025");
		Set<Date> dates = new HashSet<Date>();
		dates.add(d);
		Event e = new Event(1l, "Event", "Opis",dates , new Date(),new Date(), EventStatus.NORMAL, 2, false, "Str", null, null, null, null, null);
		List<Event> events = new ArrayList<Event>();
		events.add(e);
		Mockito.when(eventService.selectEventsFromHall(10l)).thenReturn(Optional.of(events));
		Mockito.when(repository.save(ArgumentMatchers.any(Section.class))).thenReturn(sec);
		Mockito.when(repository.findById(10l)).thenReturn(Optional.of(sec));
		service.remove(10l);
	}
}
