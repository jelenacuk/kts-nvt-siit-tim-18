package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.itextpdf.text.DocumentException;
import com.tickettakeit.tickettakeit.dto.BuyTicketDTO;
import com.tickettakeit.tickettakeit.dto.TicketDTO;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationHasPassed;
import com.tickettakeit.tickettakeit.exceptions.EventApplicationNotFoundForGivenUser;
import com.tickettakeit.tickettakeit.exceptions.EventIsBlockedException;
import com.tickettakeit.tickettakeit.exceptions.EventNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionDoesNotBelongsToTheEventException;
import com.tickettakeit.tickettakeit.exceptions.EventSectionNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.GivenDateDoesntExistException;
import com.tickettakeit.tickettakeit.exceptions.NoLoggedUserException;
import com.tickettakeit.tickettakeit.exceptions.NoTicketsLeftException;
import com.tickettakeit.tickettakeit.exceptions.NotYourTimeYet;
import com.tickettakeit.tickettakeit.exceptions.SaleHasNotYetBeganException;
import com.tickettakeit.tickettakeit.exceptions.SalesDateHasPassedException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsBeyondBoundariesException;
import com.tickettakeit.tickettakeit.exceptions.SeatIsTakenException;
import com.tickettakeit.tickettakeit.exceptions.TicketNotExistsException;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.RegisteredUser;
import com.tickettakeit.tickettakeit.model.Ticket;

@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:ticket-integration-test.properties")
public class TicketServisIntegrationTest {

	@Autowired
	TicketService ticketService;

	// Data
	private Long loggedUserID;
	private Long eventSectionID;
	private Long eventID;
	private int seatRow;
	private int seatColumn;
	private Long forDay;
	private Ticket ticket;
	private BuyTicketDTO btDTO;
	private Date today;
	private HttpHeaders headers;

	private String token;

	@Before
	public void setUp() throws MalformedURLException, DocumentException, IOException {
		today = new Date();
		loggedUserID = new Long(10);
		eventSectionID = new Long(10);
		eventID = new Long(10);
		seatRow = 5;
		seatColumn = 5;
		forDay = 1602367200000L;

		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);

		/**
		 * Sets reserved Ticket for BuyReservedTicket ticket = new Ticket();
		 * ticket.setId(20L); ticket.setCode("10 20 "); ticket.setDayOfPurchase(new
		 * Date(today.getTime() - 2 * (1000 * 60 * 60 * 24))); ticket.setBought(false);
		 * ticket.setRow(6); ticket.setColumn(6);
		 * ticket.setPrice(eventSection.getPrice()); ticket.setForDay(new
		 * Date(today.getTime() + 12 * (1000 * 60 * 60 * 24)));
		 * ticket.setEventSection(eventSection); ticket.setEvent(event);
		 * ticket.setUser(loggedUser); loggedUser.getTickets().add(ticket);
		 * Mockito.when(ticketRepository.findById(ticket.getId())).thenReturn(Optional.of(ticket));
		 * 
		 **/
	}

	@Test
	public void whenGetTicket_thenReturnTicketDTO() throws TicketNotExistsException {
		TicketDTO ticketDTO = ticketService.getTicket(10L);

		assertEquals(new Long(10), ticketDTO.getId());
		assertEquals(new Long(10), ticketDTO.getEventID());
		assertEquals(new Long(10), ticketDTO.getEventSectionID());
		assertEquals(5, ticketDTO.getColumn());
		assertEquals(5, ticketDTO.getRow());
		assertEquals("10 10 Test", ticketDTO.getCode());
	}

	@Test(expected = TicketNotExistsException.class)
	public void whenGetTicketNotFound_thenThrow() throws TicketNotExistsException {
		ticketService.getTicket(11L);
	}

	// ===============================BuyTicket================================

	@Test
	public void whenSuccessfullyBuyTicket_thenReturnBoughtTicket() throws MalformedURLException, DocumentException,
			IOException, NoLoggedUserException, EventSectionNotExistsException, EventNotExistsException,
			SeatIsTakenException, SeatIsBeyondBoundariesException, NoTicketsLeftException, EventIsBlockedException,
			SaleHasNotYetBeganException, SalesDateHasPassedException, GivenDateDoesntExistException,
			EventSectionDoesNotBelongsToTheEventException, EventApplicationNotFoundForGivenUser, NotYourTimeYet, EventApplicationHasPassed {

		Ticket boughtTicket = ticketService.buyTicket(btDTO);

		assertEquals("Assert barcode: ", "10 10 " + boughtTicket.getDayOfPurchase(), boughtTicket.getCode());
		assertEquals("Assert Ticket bought for day: ", today.getTime() + 11 * (1000 * 60 * 60 * 24),
				boughtTicket.getForDay().getTime());
		assertTrue("Assert Ticket is bought: ", boughtTicket.isBought());
		assertEquals("Assert row number: ", seatRow, boughtTicket.getRow());
		assertEquals("Assert column number: ", seatColumn, boughtTicket.getColumn());
		assertEquals(200, boughtTicket.getPrice(), 1);
		assertEquals("Assert EventSection id: ", eventSectionID, boughtTicket.getEventSection().getId());
		assertEquals("Assert Event id: ", eventID, boughtTicket.getEvent().getId());
		assertEquals("Assert User id: ", loggedUserID, boughtTicket.getUser().getId());
	}

}
