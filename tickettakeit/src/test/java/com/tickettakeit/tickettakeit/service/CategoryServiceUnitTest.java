package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.exceptions.CategoryAlreadyExistsException;
import com.tickettakeit.tickettakeit.exceptions.CategoryNotExistsException;
import com.tickettakeit.tickettakeit.exceptions.ObjectCannotBeDeletedException;
import com.tickettakeit.tickettakeit.model.Category;
import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.repository.CategoryRepository;
import com.tickettakeit.tickettakeit.repository.EventRepository;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@SpringBootTest
public class CategoryServiceUnitTest {
	
	@MockBean
	CategoryRepository categoryRepository;
	
	@MockBean
	EventRepository eventRepository;
	
	@Autowired
	CategoryService categoryService;
	
	@Before
	public void setUp() {
		
		Category newCategory = new Category(1L, "Category 1", "color", "icon,path");
		Mockito.when(categoryRepository.findByName(newCategory.getName())).thenReturn(newCategory);	
		Mockito.when(categoryRepository.save(newCategory)).thenReturn(newCategory);
		Mockito.when(categoryRepository.findById(newCategory.getId())).thenReturn(Optional.of(newCategory));	
		
		Category category2 = new Category(2L, "Category 2", "color", "icon,path");
		Mockito.when(categoryRepository.findById(category2.getId())).thenReturn(Optional.of(category2));	
	}
	
	// ***** addCategory *****
	
	@Test
	@Transactional
	public void addCategory_success() throws CategoryAlreadyExistsException, FileNotFoundException, IOException {
		
		Category newCategory = new Category("New category", "color", "icon,path");
		Mockito.when(categoryRepository.save(ArgumentMatchers.any(Category.class))).thenReturn(newCategory);
		
		CategoryDTO dto = new CategoryDTO(null, "New category", "color", "icon,path");
		Category savedCategory = categoryService.addCategory(dto);
		
		assertEquals(dto.getName(), savedCategory.getName());
		assertEquals(dto.getColor(), savedCategory.getColor());
		assertEquals(dto.getIcon(), savedCategory.getIcon());
		
	}
	
	@Test(expected = CategoryAlreadyExistsException.class)
	@Transactional
	public void addCategory_nameExists() throws CategoryAlreadyExistsException, FileNotFoundException, IOException {
			
		CategoryDTO dto = new CategoryDTO(null, "Category 1", "color", "icon,path");
		categoryService.addCategory(dto);
		
	}
	
	// ***** updateCategory *****
	
	@Test
	@Transactional
	public void updateCategory_success() throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {
		
		Category updated = new Category(2L, "Category 2 updated", "color updated", "icon,path updated");
		Mockito.when(categoryRepository.save(ArgumentMatchers.any(Category.class))).thenReturn(updated);
		
		CategoryDTO dto = new CategoryDTO(2L, "Category 2 updated", "color updated", "icon,path updated");
		Category realUpdated = categoryService.updateCategory(dto);
		
		assertEquals(dto.getName(), realUpdated.getName());
		assertEquals(dto.getColor(), realUpdated.getColor());
	}
	
	@Test(expected = CategoryNotExistsException.class)
	@Transactional
	public void updateCategory_categoryNotExists() throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {
		
		CategoryDTO dto = new CategoryDTO(132L, "Category 2 updated", "color updated", "icon path updated");
		categoryService.updateCategory(dto);
		
	}
	
	@Test(expected = CategoryAlreadyExistsException.class)
	@Transactional
	public void updateCategory_nameExists() throws CategoryNotExistsException, CategoryAlreadyExistsException, IOException {	
		
		CategoryDTO dto = new CategoryDTO(2L, "Category 1", "color updated", "icon path updated");
		categoryService.updateCategory(dto);
				
	}
	
	// ***** deleteCategory *****
	@Test
	@Transactional
	public void deleteCategory_success() throws CategoryNotExistsException, CategoryAlreadyExistsException, ObjectCannotBeDeletedException {
		
		ArrayList<Long> categoryIdList = new ArrayList<>();
		categoryIdList.add(2L);
		List<Event> eventList = new ArrayList<Event>(); 
		Page<Event> page = new PageImpl<>(eventList);
		Mockito.when(eventRepository.findByCategory(PageRequest.of(1, 1), categoryIdList)).thenReturn(page);
		
		assertTrue(categoryService.deleteCategory(2L));
		
	}
	
	@Test(expected = ObjectCannotBeDeletedException.class)
	@Transactional
	public void deleteCategory_eventsExist() throws CategoryNotExistsException, ObjectCannotBeDeletedException {
		
		ArrayList<Long> categoryIdList = new ArrayList<>();
		categoryIdList.add(1L);
		
		List<Event> eventList = new ArrayList<Event>();
		eventList.add(new Event());
		Page<Event> page = new PageImpl<>(eventList);
		Mockito.when(eventRepository.findByCategory(PageRequest.of(1, 1), categoryIdList)).thenReturn(page);
		
		categoryService.deleteCategory(1L);
	}
	
	@Test(expected = CategoryNotExistsException.class)
	@Transactional
	public void deleteCategory_categoryNotExists() throws CategoryNotExistsException, ObjectCannotBeDeletedException {

		categoryService.deleteCategory(111L);
	}
	
	

}
