package com.tickettakeit.tickettakeit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.exceptions.AccountIsNotActiveException;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserAlredyExistsException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;
import com.tickettakeit.tickettakeit.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test-jelena.properties")
public class UserServiceIntegrationTest {

	@Autowired
	UserService userService;

	// Registred user in database
	private static final String USER_FIRST_NAME = "Mira";
	private static final String USER_LAST_NAME = "Miric";
	private static final String USER_PHONE = "023/848-888";
	private static final String USER_EMAIL = "mira@gmail.com";
	private static final String USER_USERNAME = "mira";
	private static final String USER_PASSWORD = "user";
	private static final String USER_PAYPAL = "paypal";
	private static final String NON_ACTIVE_USERNAME = "jana";
	private static final String NON_ACTIVE_PASSWORD = "jana";

	UserDTO registredUser;
	UserDTO newUser;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Before
	public void setUp() {

		registredUser = new UserDTO(USER_USERNAME, USER_PASSWORD, USER_EMAIL, USER_FIRST_NAME, USER_LAST_NAME,
				USER_PHONE, USER_PAYPAL);
		registredUser.setRepeatedPassword(USER_PASSWORD);

		// NonExisting user
		newUser = new UserDTO("pera", "pera", "pera@gmail.com", "Pera", "Peric", "023/848-988", "paypal");
		newUser.setRepeatedPassword("pera");
	}

////REGISTRATION

	@Test
	@Transactional
	@Rollback
	public void register_Successful() throws UserAlredyExistsException, PasswordConfirmationException {

		int usersSize_before = userService.findAll().size();
		
		UserDTO success = userService.register(newUser);
		assertNotNull(success);
		assertEquals("Number of users in db increased by 1.", usersSize_before + 1, userService.findAll().size());

		User registeredUser = userService.findOneByUsername(newUser.getUsername()).get();
		assertNotNull("User that we added exists in db", registeredUser);
		assertEquals(newUser.getFirstName(), registeredUser.getFirstName());
		assertEquals(newUser.getLastName(), registeredUser.getLastName());
		assertEquals(newUser.getEmail(), registeredUser.getEmail());
		assertEquals(newUser.getPhone(), registeredUser.getPhone());

	}

	@Test(expected = UserAlredyExistsException.class)
	@Transactional
	@Rollback
	public void register_ExistingUser() throws UserAlredyExistsException, PasswordConfirmationException {

		int usersSize_before = userService.findAll().size();
		UserDTO success = userService.register(registredUser);
		assertEquals(null, success, "Registration failed");
		assertEquals("Number of users in db remained unchanged.", usersSize_before, userService.findAll().size());
	}

	@Test(expected = PasswordConfirmationException.class)
	@Transactional
	@Rollback
	public void register_WrongRepeatedPassword() throws UserAlredyExistsException, PasswordConfirmationException {

		// Wrong repeated password!
		newUser.setRepeatedPassword(newUser.getPassword() + "123");

		int usersSize_before = userService.findAll().size();
		UserDTO success = userService.register(newUser);
		assertEquals(null, success, "Registration failed");
		assertEquals("Number of users in db remained unchanged.", usersSize_before, userService.findAll().size());

		// return value
		newUser.setRepeatedPassword(newUser.getPassword());
	}

////LOGIN

	@Test
	@Transactional
	@Rollback
	public void login_Successful() throws UserNotExistsException, AccountIsNotActiveException {

		LogInDTO loginDTO = new LogInDTO(USER_USERNAME, USER_PASSWORD);

		String token = userService.createAuthenticationToken(loginDTO);
		assertNotNull(token);

	}

	@Test(expected = UserNotExistsException.class)
	@Transactional
	@Rollback
	public void login_NonExistingUser() throws UserNotExistsException, AccountIsNotActiveException {

		// NonExisting user
		LogInDTO loginDTO = new LogInDTO(newUser.getUsername(), newUser.getPassword());

		String token = userService.createAuthenticationToken(loginDTO);
		assertEquals("Token is null.", null, token);

	}

	@Test(expected = AccountIsNotActiveException.class)
	@Transactional
	@Rollback
	public void login_NotActiveAccount() throws UserNotExistsException, AccountIsNotActiveException {

		LogInDTO loginDTO = new LogInDTO(NON_ACTIVE_USERNAME, NON_ACTIVE_PASSWORD);
		String token = userService.createAuthenticationToken(loginDTO);
		assertEquals("Token is null.", null, token);
	}

//// ACCOUNT CONFIRMATION

	@Test
	@Transactional
	@Rollback
	public void activateAccount_Successful() throws UnsupportedEncodingException, UserNotExistsException {

		// Set account activity on false.
		User user = userService.findOneByUsername(USER_USERNAME).get();
		user.setConfirmed(false);
		userService.save(user);
		// Encoded username 'mira'
		String encodedUsername = "bWlyYQ==";

		Boolean success = userService.activateAccount(encodedUsername);
		assertTrue(success);
		User registeredUser = userService.findOneByUsername(USER_USERNAME).get();
		assertEquals(true, registeredUser.isConfirmed());

	}

	@Test(expected = UserNotExistsException.class)
	@Transactional
	@Rollback
	public void activateAccount_NonExistingUser() throws UnsupportedEncodingException, UserNotExistsException {

		String encodedUsername = "encodedUsername";
		Boolean success = userService.activateAccount(encodedUsername);
		assertFalse(success);

	}

////EDIT USER PROFILE

	@Test
	@Transactional
	@Rollback
	public void editUserProfile_Successful() throws PasswordConfirmationException, UserNotExistsException {

		// Change first name from 'Mira' to 'Miroslava'
		registredUser.setFirstName("Miroslava");
		// Change email form mira@gmail.com to miroslava@gmail.com
		registredUser.setEmail("miroslava@gamil.com");
		// Change Password
		registredUser.setNewPassword(USER_PASSWORD + "123");
		registredUser.setRepeatedPassword(USER_PASSWORD + "123");

		Boolean success = userService.editUserProfile(registredUser);
		assertTrue("User profile successfuly updated", success);

		User updatedUser = userService.findOneByUsername(USER_USERNAME).get();
		assertEquals(registredUser.getFirstName(), updatedUser.getFirstName());
		assertEquals(registredUser.getEmail(), updatedUser.getEmail());
		assertTrue(encoder.matches(registredUser.getNewPassword(), updatedUser.getPassword()));

		// return values for userDTO
		registredUser.setFirstName(USER_FIRST_NAME);
		registredUser.setEmail(USER_EMAIL);
		registredUser.setPassword(USER_PASSWORD);
		registredUser.setRepeatedPassword(USER_PASSWORD);
	}

	@Test(expected = PasswordConfirmationException.class)
	@Transactional
	@Rollback
	public void editUserProfile_WrongRepeatedPassword() throws PasswordConfirmationException, UserNotExistsException {

		registredUser.setNewPassword(USER_PASSWORD + "321");
		registredUser.setRepeatedPassword(USER_PASSWORD + "123");

		Boolean success = userService.editUserProfile(registredUser);
		assertFalse(success);

		// return values to userDTO
		registredUser.setNewPassword(USER_PASSWORD);
		registredUser.setRepeatedPassword(USER_PASSWORD);
	}

	@Test(expected = UserNotExistsException.class)
	@Transactional
	@Rollback
	public void editUserProfile_NonExistingUser() throws PasswordConfirmationException, UserNotExistsException {

		// NonExisting user
		registredUser.setUsername("-1");

		Boolean success = userService.editUserProfile(registredUser);
		assertFalse(success);
		// return values to userDTO
		registredUser.setUsername(USER_USERNAME);
	}

}
