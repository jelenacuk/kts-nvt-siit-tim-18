package com.tickettakeit.tickettakeit.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.model.Event;
import com.tickettakeit.tickettakeit.model.EventSection;
import com.tickettakeit.tickettakeit.model.EventStatus;
import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.Ticket;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase()
@DataJpaTest
public class TicketRepositoryUnitTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private TicketRepository ticketRepository;

	private Long location1Id;
	private Long location2Id;

	private Long event1Id;
	private Long event2Id;
	private Long event3Id;

	private Long eventSection1Id;
	private Long eventSection2Id;

	private Long ticket1Id;

	@Before
	public void setUp() {

		// set up locations
		Location location1 = new Location("Location 1", "City 1", "Address 1", 100, 100, "", null, true);
		Location location2 = new Location("Location 2", "City 1", "Address 2", 100, 100, "", null, true);

		location1Id = entityManager.persistAndGetId(location1, Long.class);
		location2Id = entityManager.persistAndGetId(location2, Long.class);

		// set up event sections
		EventSection es1 = new EventSection("east", 100, 100, true, 0, 100);
		es1.setLeft(0);
		es1.setTop(0);
		EventSection es2 = new EventSection("west", 100, 100, true, 0, 100);
		es2.setLeft(0);
		es2.setTop(0);

		eventSection1Id = entityManager.persistAndGetId(es1, Long.class);
		eventSection2Id = entityManager.persistAndGetId(es2, Long.class);

		Set<EventSection> sections1 = new HashSet<>();
		Set<EventSection> sections2 = new HashSet<>();
		sections1.add(es1);
		sections2.add(es2);

		// Set up halls
		Hall hall1 = new Hall("Hall 1");
		Long hallID = entityManager.persistAndGetId(hall1, Long.class);

		Event event1 = new Event("Event 1", EventStatus.NORMAL, 0, false, hall1);
		event1.setLocation(location1);
		event1.setHall(hall1);
		event1.setEventSections(sections1);
		Event event2 = new Event("Event 2", EventStatus.NORMAL, 0, false, hall1);
		event2.setLocation(location1);
		event2.setHall(hall1);
		Event event3 = new Event("Event 3", EventStatus.NORMAL, 0, false, hall1);
		event3.setLocation(location2);
		event3.setHall(hall1);

		event1Id = entityManager.persistAndGetId(event1, Long.class);
		event2Id = entityManager.persistAndGetId(event2, Long.class);
		event3Id = entityManager.persistAndGetId(event3, Long.class);

		// Date dayOfPurchase, boolean bought, int row, int column, Date forDay, Event
		// event, price, code
		Ticket t1 = new Ticket(new GregorianCalendar(2019, Calendar.OCTOBER, 20).getTime(), true, 10, 10,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(), event1, 100, "code");
		t1.setEventSection(es1);
		Ticket t2 = new Ticket(new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(), true, 11, 10,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(), event1, 100, "code");
		Ticket t3 = new Ticket(null, false, 11, 10, new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(),
				event1, 100, "code");
		Ticket t4 = new Ticket(new GregorianCalendar(2019, Calendar.APRIL, 20).getTime(), true, 11, 10,
				new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime(), event3, 100, "code");

		ticket1Id = entityManager.persistAndGetId(t1, Long.class);
		entityManager.persist(t2);
		entityManager.persist(t3);
		entityManager.persist(t4);

	}

	// ***** findTicketByRowColumnEventSectionID *****
	@Test
	public void findTicketByRowColumnEventSectionID_ticketExists() {
		int row = 10;
		int column = 10;
		Ticket foundTicket = ticketRepository.findTicketByRowColumnEventSectionID(eventSection1Id, row, column);

		assertNotNull(foundTicket);
		assertEquals(ticket1Id, foundTicket.getId());
		assertEquals(row, foundTicket.getRow());
		assertEquals(column, foundTicket.getColumn());
		assertEquals(eventSection1Id, foundTicket.getEventSection().getId());
	}

	@Test
	public void findTicketByRowColumnEventSectionID_noTicketForTheSeat() {
		int row = 111;
		int column = 111;
		Ticket foundTicket = ticketRepository.findTicketByRowColumnEventSectionID(eventSection1Id, row, column);

		assertNull(foundTicket);

	}

	@Test
	public void findTicketByRowColumnEventSectionID_eventSectionHasNoTickets() {
		int row = 10;
		int column = 10;
		Ticket foundTicket = ticketRepository.findTicketByRowColumnEventSectionID(eventSection2Id, row, column);

		assertNull(foundTicket);

	}

	// ***** countSoldTickets *****
	@Test
	public void countSoldTickets_ticketsExist() {

		int result = ticketRepository.countSoldTickets(eventSection1Id);
		assertEquals(1, result);

	}

	@Test
	public void countSoldTickets_noTickets() {

		int result = ticketRepository.countSoldTickets(eventSection2Id);
		assertEquals(0, result);
	}

	// ***** findTicketByRowColumnEventSectionID *****
	@Test
	public void soldTicketsForLocation_findMultiple() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForLocation(location1Id,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 20).getTime());

		assertThat(foundTickets).hasSize(2)
				.allMatch(ticket -> ticket.getEvent().getLocation().getId().equals(location1Id))
				.allMatch(ticket -> ticket.isBought());

	}

	@Test
	public void soldTicketsForLocation_findOne() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForLocation(location2Id,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(1)
				.allMatch(ticket -> ticket.getEvent().getLocation().getId().equals(location2Id))
				.allMatch(ticket -> ticket.isBought());

	}

	@Test
	public void soldTicketsForLocation_noMatchingLocation() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForLocation(2222L,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);

	}

	@Test
	public void soldTicketsForLocation_noMatchingDate() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForLocation(location1Id,
				new GregorianCalendar(2017, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2017, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);

	}

	// ***** soldTicketsForEvent *****
	@Test
	public void soldTicketsForEvent_findMultiple() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEvent(event1Id,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 20).getTime());

		assertThat(foundTickets).hasSize(2).allMatch(ticket -> ticket.getEvent().getId().equals(event1Id))
				.allMatch(ticket -> ticket.isBought());

	}

	@Test
	public void soldTicketsForEvent_findOne() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEvent(event3Id,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(1).allMatch(ticket -> ticket.getEvent().getId().equals(event3Id))
				.allMatch(ticket -> ticket.isBought());

	}

	@Test
	public void soldTicketsForEvent_noMatchingEvent() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEvent(2222L,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);

	}

	@Test
	public void soldTicketsForEvent_noMatchingDate() {

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEvent(event1Id,
				new GregorianCalendar(2017, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2017, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);

	}

	// ***** soldTicketsForEventDay *****
	@Test
	public void soldTicketsForEventDay_findMultiple() {
		Date forDay = new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime();

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEventDay(event1Id, forDay,
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2019, Calendar.OCTOBER, 20).getTime());

		assertThat(foundTickets).hasSize(2).allMatch(ticket -> ticket.getEvent().getId().equals(event1Id))
				.allMatch(ticket -> ticket.getForDay().equals(forDay)).allMatch(ticket -> ticket.isBought());
	}

	@Test
	public void soldTicketsForEventDay_findOne() {
		Date forDay = new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime();

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEventDay(event3Id, forDay,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(1).allMatch(ticket -> ticket.getEvent().getId().equals(event3Id))
				.allMatch(ticket -> ticket.isBought());

	}

	@Test
	public void soldTicketsForEventDay_noMatchingEvent() {
		Date forDay = new GregorianCalendar(2020, Calendar.FEBRUARY, 20).getTime();

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEventDay(2222L, forDay,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);

	}

	@Test
	public void soldTicketsForEventDay_noMatchingDate() {
		Date forDay = new GregorianCalendar(2222, Calendar.FEBRUARY, 20).getTime();

		List<Ticket> foundTickets = ticketRepository.soldTicketsForEventDay(event1Id, forDay,
				new GregorianCalendar(2019, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2019, Calendar.SEPTEMBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);

	}

	// ***** findBoughtTicketsForDay *****
	@Test
	public void soldTicketsForDay_findMultiple() {

		List<Ticket> foundTickets = ticketRepository.findBoughtTicketsForDay(
				new GregorianCalendar(2020, Calendar.JANUARY, 20).getTime(),
				new GregorianCalendar(2020, Calendar.OCTOBER, 20).getTime());

		assertThat(foundTickets).hasSize(3).allMatch(ticket -> ticket.isBought());
	}

	@Test
	public void soldTicketsForDay_noResults() {

		List<Ticket> foundTickets = ticketRepository.findBoughtTicketsForDay(
				new GregorianCalendar(2017, Calendar.SEPTEMBER, 20).getTime(),
				new GregorianCalendar(2017, Calendar.OCTOBER, 20).getTime());

		assertThat(foundTickets).hasSize(0);
	}

}
