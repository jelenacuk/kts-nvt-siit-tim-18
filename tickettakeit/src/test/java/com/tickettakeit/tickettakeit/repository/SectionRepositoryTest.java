package com.tickettakeit.tickettakeit.repository;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Section;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SectionRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private SectionRepository sectionRepository;
	@Test
	public void findHallBySectionId() {
		Hall hall = new Hall(null, "Hall", new HashSet<Section>(), true);
		Section section = new Section();
		section.setActive(true);
		section.setColumns(5);
		section.setLeft(5);
		section.setName("Section");
		section.setNumberOfSeats(25);
		section.setNumerated(true);
		section.setRows(5);
		section.setTop(5);
		Long id = entityManager.persistAndGetId(section,Long.class);
		section.setId(id);
		hall.getSections().add(section);
		Long hallID = entityManager.persistAndGetId(hall, Long.class);
		Optional<Hall> oHall = sectionRepository.findHallBySectionId(id);
		assertTrue(oHall.isPresent());
		assertEquals(oHall.get().getId(), hallID);

	}

}
