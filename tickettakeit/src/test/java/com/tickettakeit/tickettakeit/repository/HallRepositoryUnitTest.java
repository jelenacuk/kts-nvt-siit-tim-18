package com.tickettakeit.tickettakeit.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;
import com.tickettakeit.tickettakeit.model.Section;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HallRepositoryUnitTest {
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private HallRepository hallRepository;
	@Autowired
	private LocationRepository locationRepository;

	@Test
	//When location exists
	public void whenFindHallsFromLocation_thenReturnHalls() {
		Set<Section> sections = new HashSet<Section>();
		Hall h = new Hall(null, "HallTest",sections,true);
		Hall hall = hallRepository.save(h);
		Set<Hall> halls = new HashSet<Hall>();
		halls.add(hall);
		Location location = locationRepository.save(new Location(null, "LocationTest", "", "", 0, 0, "", halls,true)); 
		Optional<List<Hall>> foundHalls = hallRepository.findHallsFromLocation(location.getId());
		
		assertEquals(true, foundHalls.isPresent());
	}
	@Test
	//When location not exists
	public void whenFindHallsFromLocation_thenNoHalls() {
		Optional<List<Hall>> foundHalls = hallRepository.findHallsFromLocation(999l);
		assertEquals(false, foundHalls.isPresent());
	}
	@Test
	public void whenfindHallsFromLocationAndHall_thenReturnHall() {
		Set<Section> sections = new HashSet<Section>();
		Hall h = new Hall(null, "HallTest",sections,true);
		Hall hall = hallRepository.save(h);
		Set<Hall> halls = new HashSet<Hall>();
		halls.add(hall);
		Location location = locationRepository.save(new Location(null, "LocationTest", "", "", 0, 0, "", halls,true)); 
		Hall foundHall = hallRepository.findHallsFromLocationAndHall(location.getId(),hall.getId()).get();
		
		assertEquals(hall.getId(),foundHall.getId());
	}
	@Test
	public void whenfindHallsFromLocationAndHall_thenReturnNoHall() {
		Set<Section> sections = new HashSet<Section>();
		Set<Hall> halls = new HashSet<Hall>();
		Location location = locationRepository.save(new Location(null, "LocationTest", "", "", 0, 0, "", halls,true)); 
		Hall foundHall = hallRepository.findHallsFromLocationAndHall(location.getId(),150l).orElse(null);
		
		assertEquals(null,foundHall);
	}

}
