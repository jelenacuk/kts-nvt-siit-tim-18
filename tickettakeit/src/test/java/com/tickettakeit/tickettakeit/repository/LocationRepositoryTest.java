package com.tickettakeit.tickettakeit.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.model.Hall;
import com.tickettakeit.tickettakeit.model.Location;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LocationRepositoryTest {
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private LocationRepository locationRepository;

	@Test
	public void getAllLocationNames() {
		Location l = new Location();
		l.setName("Name");
		l.setActive(true);
		l.setCity("Novi sad");
		l.setHalls(new HashSet<Hall>());
		l.setImage("");
		l.setLatitude(5);
		l.setLongitude(11);
		l.setAddress("Address");
		entityManager.persist(l);
		List<String> names = locationRepository.getAllLocationNames();
		assertEquals(1, names.size());
		assertEquals("Name", names.get(0));
	}
	
	@Test
	public void getAllLocationNoNames() {
		List<String> names = locationRepository.getAllLocationNames();
		assertEquals(0, names.size());
	}

}
