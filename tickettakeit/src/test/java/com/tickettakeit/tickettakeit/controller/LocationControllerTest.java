package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.HallDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LocationDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.exceptions.LocationNotExistsException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-mile.properties")
@Transactional
@Rollback(true)
public class LocationControllerTest {

	@Autowired
	private TestRestTemplate testRest;
	HttpHeaders headers;
	String token;

	@Before
	public void setUp() {
		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("pera", "peric");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRest.postForEntity("/auth/login", request, JwtResponseDTO.class);
		headers.add("Authorization", "Bearer " + response.getBody().getToken());
		token = "Bearer " + response.getBody().getToken();
		headers.add("Content-Type", "application/json");
	}

	@Test
	public void whengetLocations_getLocations() {
		ResponseEntity<LocationDTO[]> response = testRest.getForEntity("/api/locations?page=1&size=10",
				LocationDTO[].class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whengetLocation_getLocation() {
		ResponseEntity<LocationDTO> response = testRest.getForEntity("/api/locations/10", LocationDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whengetLocation_LocationNotExist() {
		ResponseEntity<LocationDTO> response = testRest.getForEntity("/api/locations/213", LocationDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whencreateLocation_createLocation() {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("New address");
		dto.setCity("New City");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(1l);
		dto.setImage(null);
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Location name");
		HttpEntity<LocationDTO> request = new HttpEntity<LocationDTO>(dto, headers);
		ResponseEntity<LocationDTO> response = testRest.postForEntity("/api/locations", request, LocationDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(dto.getName(), response.getBody().getName());
	}

	@Test
	public void whencreateLocation_LocationAlreadyExist() {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("Bulevar Arsenija Carnojevica 58");
		dto.setCity("Beograd");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setImage("");
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Beogradska arena");
		HttpEntity<LocationDTO> request = new HttpEntity<LocationDTO>(dto, headers);
		ResponseEntity<LocationDTO> response = testRest.postForEntity("/api/locations", request, LocationDTO.class);
		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenupdateLocation_UpdateLocation() {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("New address");
		dto.setCity("New City");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(1l);
		dto.setImage(null);
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Location name");
		dto.setActive(true);
		HttpEntity<LocationDTO> request = new HttpEntity<LocationDTO>(dto, headers);

		ResponseEntity<LocationDTO> response = testRest.exchange("/api/locations", HttpMethod.PUT, request,
				LocationDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(dto.getAddress(), response.getBody().getAddress());
		assertEquals(dto.getCity(), response.getBody().getCity());
		assertEquals(dto.getId(), response.getBody().getId());
		assertEquals(dto.getName(), response.getBody().getName());
	}

	@Test
	public void updateLocation_LocationNotExist() {
		LocationDTO dto = new LocationDTO();
		dto.setAddress("New address");
		dto.setCity("New City");
		dto.setHalls(new HashSet<HallDTO>());
		dto.setId(2l);
		dto.setImage("");
		dto.setLatitude(0);
		dto.setLongitude(0);
		dto.setName("Location name");
		HttpEntity<LocationDTO> request = new HttpEntity<LocationDTO>(dto, headers);
		ResponseEntity<LocationDTO> response = testRest.exchange("/api/locations", HttpMethod.PUT, request,
				LocationDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}
	
	@Test
	public void deleteLocation_deleteLocation() {
		HttpEntity<LocationDTO> request = new HttpEntity<LocationDTO>(null, headers);
		ResponseEntity<LocationDTO> response = testRest.exchange("/api/locations/21", HttpMethod.DELETE, request,
				LocationDTO.class);
		assertEquals(21, response.getBody().getId());
		assertFalse(response.getBody().getActive());
	}
	@Test
	public void deleteLocation_locationNotExists() {
		HttpEntity<LocationDTO> request = new HttpEntity<LocationDTO>(null, headers);
		ResponseEntity<LocationDTO> response = testRest.exchange("/api/locations/31", HttpMethod.DELETE, request,
				LocationDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

}
