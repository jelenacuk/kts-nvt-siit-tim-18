package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserAlredyExistsException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test-jelena.properties")
public class AuthenticationControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	// Registred user in database
	private static final String USER_FIRST_NAME = "Mira";
	private static final String USER_LAST_NAME = "Miric";
	private static final String USER_PHONE = "023/848-888";
	private static final String USER_EMAIL = "mira@gmail.com";
	private static final String USER_USERNAME = "mira";
	private static final String USER_PASSWORD = "user";
	private static final String USER_PAYPAL = "paypal";
	// Encoded username 'mira'
	private String encodedUsername = "bWlyYQ==";

	private static final String NOT_ACTIVE_USERNAME = "jana";
	private static final String NOT_ACTIVE_PASSWORD = "jana";

	private UserDTO newUser;
	private LogInDTO loginDTO;
	private UserDTO registredUser;

	@Before
	public void setUp() {

		// nonExisting user
		newUser = new UserDTO("pera", "pera", "pera@gmail.com", "Pera", "Peric", "023/848-111", "paypal");
		newUser.setRepeatedPassword(newUser.getPassword());
		// existing user
		registredUser = new UserDTO(USER_USERNAME, USER_PASSWORD, USER_EMAIL, USER_FIRST_NAME, USER_LAST_NAME,
				USER_PHONE, USER_PAYPAL);
		registredUser.setRepeatedPassword(USER_PASSWORD);
		loginDTO = new LogInDTO(USER_USERNAME, USER_PASSWORD);
	}

////REGISTERATION

	@Test
	@Transactional
	@Rollback
	public void register_Successful_OK() throws UserAlredyExistsException, PasswordConfirmationException {

		ResponseEntity<UserDTO> responseEntity = testRestTemplate.postForEntity("/auth/register", newUser,
				UserDTO.class);
		UserDTO added = responseEntity.getBody();
		assertEquals("Http status is OK", HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(newUser.getUsername(), added.getUsername());
		assertEquals(newUser.getFirstName(), added.getFirstName());
		assertEquals(newUser.getLastName(), added.getLastName());
	}

	@Test
	@Transactional
	@Rollback
	public void register_ExistingUser_UserAlredyExistsException()
			throws UserAlredyExistsException, PasswordConfirmationException {

		ResponseEntity<ErrorDetailsDTO> responseEntity = testRestTemplate.postForEntity("/auth/register", registredUser,
				ErrorDetailsDTO.class);
		ErrorDetailsDTO error = responseEntity.getBody();
		assertEquals("Http status is CONFLICT ", HttpStatus.CONFLICT, responseEntity.getStatusCode());
		assertEquals("User with username: " + registredUser.getUsername() + " already exists.", error.getMessage());
	}

	@Test
	@Transactional
	@Rollback
	public void register_WrongRepeatedPassword_PasswordConfirmationException()
			throws UserAlredyExistsException, PasswordConfirmationException {

		// wrong repeated password
		newUser.setRepeatedPassword(newUser.getPassword() + "123");
		ResponseEntity<ErrorDetailsDTO> responseEntity = testRestTemplate.postForEntity("/auth/register", newUser,
				ErrorDetailsDTO.class);
		ErrorDetailsDTO error = responseEntity.getBody();
		assertEquals("Http status is BAD_REQUEST ", HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("You must re-enter the password so that it matches with the original one.", error.getMessage());
		
		//return value
		newUser.setRepeatedPassword(newUser.getPassword());;
	}

////LOG IN

	@Test
	@Transactional
	@Rollback
	public void createAuthenticationToken_Successful_OK() {

		ResponseEntity<JwtResponseDTO> responseEntity = testRestTemplate.postForEntity("/auth/login", loginDTO,
				JwtResponseDTO.class);
		String token = responseEntity.getBody().getToken();
		assertEquals("Http status is OK ", HttpStatus.OK, responseEntity.getStatusCode());
		assertNotNull(token);
	}

	@Test
	@Transactional
	@Rollback
	public void createAuthenticationToken_NonExistingUser() {

		//NonExisting user
		LogInDTO dto = new LogInDTO( newUser.getUsername() + "a", newUser.getPassword());
		ResponseEntity<JwtResponseDTO> responseEntity = testRestTemplate.postForEntity("/auth/login", dto,
				JwtResponseDTO.class);
		String token = responseEntity.getBody().getToken();
		assertEquals("Http status is NOT_FOUND ", HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals(null, token);
	}

	@Test
	@Transactional
	@Rollback
	public void createAuthenticationToken_NotActiveAccount() {

		// set activity to false
		LogInDTO logInDTO = new LogInDTO(NOT_ACTIVE_USERNAME, NOT_ACTIVE_PASSWORD);
		ResponseEntity<JwtResponseDTO> responseEntity = testRestTemplate.postForEntity("/auth/login", logInDTO,
				JwtResponseDTO.class);
		String token = responseEntity.getBody().getToken();
		assertEquals("Http status is BAD_REQUEST.", HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals(null, token);
	}

////CONFIRM ACCOUNT

	@Test
	@Transactional
	@Rollback
	public void confirm_OK() {

		ResponseEntity<Boolean> responseEntity = testRestTemplate
				.getForEntity("/auth/registrationConfirmation/" + encodedUsername, Boolean.class);
		assertEquals("Http status is OK ", HttpStatus.FOUND, responseEntity.getStatusCode());
	}

	@Test
	@Transactional
	@Rollback
	public void confirm_NonExistingUser() {

		// Encoded nonExisting username 'djura'
		String encodedUsername = "ZGp1cmE=";

		ResponseEntity<ErrorDetailsDTO> responseEntity = testRestTemplate
				.getForEntity("/auth/registrationConfirmation/" + encodedUsername, ErrorDetailsDTO.class);
		ErrorDetailsDTO error = responseEntity.getBody();
		assertEquals("User with username: djura doesn't exists.", error.getMessage());
		assertEquals("Http status is NOT_FOUND ", HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
	}
}
