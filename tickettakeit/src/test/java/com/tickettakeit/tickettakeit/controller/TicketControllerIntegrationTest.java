package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.BuyTicketDTO;
import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.TicketDTO;
import com.tickettakeit.tickettakeit.exceptions.TicketNotExistsException;
import com.tickettakeit.tickettakeit.model.Ticket;

//Resets Application Context before every test.
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:ticket-integration-test.properties")
public class TicketControllerIntegrationTest {
	@Autowired
	private TestRestTemplate testRest;

	private HttpHeaders headers;

	private String token;

	// Data
	private Long loggedUserID;
	private Long eventSectionID;
	private Long eventID;
	private int seatRow;
	private int seatColumn;
	private Long forDay;
	private Ticket ticket;
	private BuyTicketDTO btDTO;
	private List<BuyTicketDTO> buyTicketDtos;
	private Date today;

	@Before
	public void setUp() {
		// Sets up Token for logged user (ADMIN user, user).
		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("user", "user");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRest.postForEntity("/auth/login", request, JwtResponseDTO.class);
		headers.add("Authorization", "Bearer " + response.getBody().getToken());
		token = "Bearer " + response.getBody().getToken();
		headers.add("Content-Type", "application/json");

		/**
		 * testRest.getRestTemplate().setInterceptors(Collections.singletonList((requestParams,
		 * body, execution) -> { requestParams.getHeaders().add("Authorization", "Bearer
		 * " + response.getBody().getToken());
		 * requestParams.getHeaders().add("Content-Type", "application/json"); return
		 * execution.execute(requestParams, body); }));
		 **/

		today = new Date();
		loggedUserID = new Long(10);
		eventSectionID = new Long(10);
		eventID = new Long(10);
		seatRow = 6;
		seatColumn = 6;
		forDay = 1602367200000L;

		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

	}

	@Test
	public void whenGetTicket_thenReturnTicketDTO() throws TicketNotExistsException {

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		ResponseEntity<TicketDTO> response = testRest.exchange("/api/tickets/10", HttpMethod.GET, httpEntity,
				TicketDTO.class);
		TicketDTO ticketDTO = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(new Long(10), ticketDTO.getId());
		assertEquals(new Long(10), ticketDTO.getEventID());
		assertEquals(new Long(10), ticketDTO.getEventSectionID());
		assertEquals(5, ticketDTO.getColumn());
		assertEquals(5, ticketDTO.getRow());
		assertEquals("10 10 Test", ticketDTO.getCode());
	}

	@Test
	public void whenGetTicketWrongId_thenReturnTicketNotExists() throws TicketNotExistsException {
		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		ResponseEntity<TicketDTO> response = testRest.exchange("/api/tickets/11", HttpMethod.GET, httpEntity,
				TicketDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	// ======================= BuyTicket ===============================

	@Test
	public void whenSuccessfullyBuyTicket_thenReturnTicketID() {

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<>(buyTicketDtos, headers);

		@SuppressWarnings({ "unchecked", "rawtypes" })
		ResponseEntity<Map<String, Object>> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				(Class<Map<String, Object>>) (Class) Map.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketSeatIsTaken_thenReturnSeatIsTaken() {
		seatColumn = 5;
		seatRow = 5;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);
		System.out.println("Date: " + new Date(forDay));
		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketRowIsOutOfBoundaries_thenReturnSeatIsOutOfBoundaries() {
		seatRow = 11;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketColumnIsOutOfBoundaries_thenReturnSeatIsOutOfBoundaries() {
		seatColumn = 11;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketGivenDateDoesntExist_thenReturnGivenDateDoesntExist() {
		forDay = 1602194400000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketEventSectionDoesntExist_thenReturnEventSectionNotExists() {
		eventSectionID = 11L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketEventDoesntExist_thenReturnEventNotExists() {
		eventID = 11L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketNoMoreTicketsLeft_thenReturnNoTicketLeft() {
		// Sets EventSection the one with not enumerated seats.
		eventSectionID = 20L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketEventSectionDoesNotBelongToTheEvent_thenReturnEventSectionDoesNotBelongsToTheEvent() {
		// Sets EventSection the one that does not belong to the Event.
		eventSectionID = 30L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketEventIsBlocked_thenReturnEventIsBlocked() {
		// Sets Event to the BLOCKED one.
		eventID = 20L;
		eventSectionID = 30L;
		forDay = 1605135600000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketSaleHasNotYetBegan_thenReturnSaleHasNotYetBegan() {
		// Sets Event to the one where sale has not yet began.
		eventID = 30L;
		eventSectionID = 40L;
		forDay = 1599688800000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	@Test
	public void whenBuyTicketSaleHasPassed_thenReturnSaleHasPassed() {
		// Sets Event to the one where sale has passed.
		eventID = 40L;
		eventSectionID = 50L;
		forDay = 1578178800000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/buy-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	// =======================ReserveTicket===============================

	@Test
	public void whenSuccessfullyReserveTicket_thenReturnTicketID() {
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		@SuppressWarnings({ "rawtypes", "unchecked" })
		ResponseEntity<List<Long>> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				(Class<List<Long>>) (Class) List.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketSeatIsTaken_thenReturnSeatIsTaken() {
		seatColumn = 5;
		seatRow = 5;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);

		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketRowIsOutOfBoundaries_thenReturnSeatIsOutOfBoundaries() {
		seatRow = 11;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketColumnIsOutOfBoundaries_thenReturnSeatIsOutOfBoundaries() {
		seatColumn = 11;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketGivenDateDoesntExist_thenReturnGivenDateDoesntExist() {
		forDay = 1602194400000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketEventSectionDoesntExist_thenReturnEventSectionNotExists() {
		eventSectionID = 11L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketEventDoesntExist_thenReturnEventNotExists() {
		eventID = 11L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketNoMoreTicketsLeft_thenReturnNoTicketLeft() {
		// Sets EventSection the one with not enumerated seats.
		eventSectionID = 20L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketEventSectionDoesNotBelongToTheEvent_thenReturnEventSectionDoesNotBelongsToTheEvent() {
		// Sets EventSection the one that does not belong to the Event.
		eventSectionID = 30L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketEventIsBlocked_thenReturnEventIsBlocked() {
		// Sets Event to the BLOCKED one.
		eventID = 20L;
		eventSectionID = 30L;
		forDay = 1605135600000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);
		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketSaleHasNotYetBegan_thenReturnSaleHasNotYetBegan() {
		// Sets Event to the one where sale has not yet began.
		eventID = 30L;
		eventSectionID = 40L;
		forDay = 1599688800000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);

		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	@Test
	public void whenReserveTicketSaleHasPassed_thenReturnSaleHasPassed() {
		// Sets Event to the one where sale has passed.
		eventID = 40L;
		eventSectionID = 50L;
		forDay = 1578178800000L;
		btDTO = new BuyTicketDTO(seatRow, seatColumn, forDay, eventSectionID, eventID);

		buyTicketDtos = new ArrayList<>();
		buyTicketDtos.add(btDTO);

		HttpEntity<List<BuyTicketDTO>> request = new HttpEntity<List<BuyTicketDTO>>(buyTicketDtos, headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.postForEntity("/api/tickets/reserve-ticket", request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	// =======================BuyReservedTicket===============================

	@Test
	public void whenSuccessfullyBuyReservedTicket_thenReturnTrue() {
		Long reservedTicketID = 30L;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		@SuppressWarnings({ "rawtypes", "unchecked" })
		ResponseEntity<Map<String, Object>> response = testRest.exchange(
				"/api/tickets/buy-reserved-ticket/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				(Class<Map<String, Object>>) (Class) Map.class);
		Map<String, Object> result = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("success", result.get("status"));
	}

	@Test
	public void whenBuyReservedTicketMissingTicket_thenReturnTicketNotExists() {
		Long reservedTicketID = 31L;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange(
				"/api/tickets/buy-reserved-ticket/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenyBuyReservedTicketEventIsBlocked_thenReturnEventIsBlocked() {
		Long reservedTicketID = 40L;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange(
				"/api/tickets/buy-reserved-ticket/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	@Test
	public void whenyBuyReservedTicketEventSaleHasPassed_thenReturnEventSaleHasPassed() {
		Long reservedTicketID = 50L;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange(
				"/api/tickets/buy-reserved-ticket/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.LOCKED, response.getStatusCode());
	}

	@Test
	public void whenyBuyReservedTicketUserDoesNotHaveTicket_thenReturnUserDoesNotHaveTheTicketException() {
		Long reservedTicketID = 60L;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange(
				"/api/tickets/buy-reserved-ticket/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	// =======================CancelTicketReservation===============================

	@Test
	public void whenCancelTicketReservation_thenReturnTrue() {

		Long reservedTicketID = 30l;
		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
		List<Long> ids = new ArrayList<>();
		ids.add(reservedTicketID);

		ResponseEntity<Boolean> response = testRest.exchange(
				"/api/tickets/cancel-ticket-reservation/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				Boolean.class);

		Boolean success = response.getBody();
		assertTrue(success);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whenCancelTicketReservation_NonExistingTicket_NOT_FOUND() {

		// NonExisting ticketId
		Long reservedTicketID = 11111l;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange(
				"/api/tickets/cancel-ticket-reservation/" + reservedTicketID, HttpMethod.PUT, httpEntity,
				ErrorDetailsDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenCancelTicketReservation_TicketIsBoughtException() {

		// Bought ticket
		Long boughtTicketID = 10l;

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange(
				"/api/tickets/cancel-ticket-reservation/" + boughtTicketID, HttpMethod.PUT, httpEntity,
				ErrorDetailsDTO.class);
		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}
}
