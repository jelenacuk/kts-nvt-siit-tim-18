package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.SectionDTO;
import com.tickettakeit.tickettakeit.exceptions.SectionAlreadyExistsException;
import com.tickettakeit.tickettakeit.model.Section;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-mile.properties")
@Transactional
@Rollback(true)
public class SectionControllerTest {
	@Autowired
	private TestRestTemplate testRest;
	HttpHeaders headers;
	String token;
	@Before
	public void setUp() {
		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("pera", "peric");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRest.postForEntity("/auth/login", request, JwtResponseDTO.class);
		headers.add("Authorization", "Bearer "+response.getBody().getToken());
		token = "Bearer "+response.getBody().getToken();
		headers.add("Content-Type", "application/json");
	}
	@Test
	public void whenCreateSection_thenCreateSection() throws Exception {
		SectionDTO dto = new SectionDTO(null, "Sec", 1, 1, false, 1,true);
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(dto, headers);
		ResponseEntity<SectionDTO> response = testRest.postForEntity("/api/sections/10", request, SectionDTO.class);
		assertEquals(response.getStatusCode(), HttpStatus.OK);
		assertEquals(response.getBody().getName(), dto.getName());
		assertEquals(response.getBody().getRows(), dto.getRows());
		assertEquals(response.getBody().getColumns(), dto.getColumns());
		assertEquals(response.getBody().getNumberOfSeats(), dto.getNumberOfSeats());
	}

	@Test
	public void whenCreateSection_thenSectionAlreadyExists() {
		SectionDTO dto = new SectionDTO(null, "east2", 1, 1, false, 1,true);
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(dto, headers);
		ResponseEntity<SectionDTO> response = testRest.postForEntity("/api/sections/10", request, SectionDTO.class);
		assertEquals(HttpStatus.CONFLICT,response.getStatusCode());
	}

	@Test
	public void whenGetSections_thenGetSections() {
		ResponseEntity<SectionDTO[]> response = testRest.getForEntity("/api/sections?size=1&limit=10",
				SectionDTO[].class);
		assertEquals(HttpStatus.OK,response.getStatusCode());
		assertTrue(response.getBody().length <= 10);
	}

	@Test
	public void whenGetSection_thenGetSection() {
		ResponseEntity<SectionDTO> response = testRest.getForEntity("/api/sections/10", SectionDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("east", response.getBody().getName());

	}

	@Test
	public void whenGetSection_thenSectionNotExists() {
		ResponseEntity<SectionDTO> response = testRest.getForEntity("/api/sections/30", SectionDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void whenUpdateSection_thenUpdateSection() {
		SectionDTO dto = new SectionDTO(10l, "sec", 1, 1, false,1,true);
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(dto, headers);
		ResponseEntity<SectionDTO> response = testRest.exchange("/api/sections/10", HttpMethod.PUT, request, SectionDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void whenUpdateSection_thenSectionNotFound() {
		SectionDTO dto = new SectionDTO(40l, "sec", 2, 1, false,1,true);
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(dto, headers);
		ResponseEntity<SectionDTO> response = testRest.exchange("/api/sections/40", HttpMethod.PUT, request, SectionDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	// If seats, rows or columns are less than 0
	public void whenUpdateSection_thenNotCorrectData() {
		SectionDTO dto = new SectionDTO(10l, "sec", -2, -1, false,0,true);
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(dto, headers);
		ResponseEntity<SectionDTO> response = testRest.exchange("/api/sections/12", HttpMethod.PUT, request, SectionDTO.class);
		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
	}

	@Test
	public void whenDeleteSection_thenDelete() {
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(null, headers);
		ResponseEntity<SectionDTO> response = testRest.exchange("/api/sections/10", HttpMethod.DELETE, request, SectionDTO.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertFalse(response.getBody().isActive());
	}

	@Test
	public void whenDeleteSection_thenSectionNotFound() {
		HttpEntity<SectionDTO> request = new HttpEntity<SectionDTO>(null, headers);
		ResponseEntity<ErrorDetailsDTO> response = testRest.exchange("/api/sections/110", HttpMethod.DELETE, request, ErrorDetailsDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	
}
