package com.tickettakeit.tickettakeit.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tickettakeit.tickettakeit.dto.CategoryDTO;
import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.ErrorResponseDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:ana-test.properties")
public class CategoryControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRestTemplate;

	HttpHeaders headers;

	String token;
    
	private static final String URL = "/api/categories/";
	private static final Long EXISTING_ID = 100L;
	private static final Long DELETE_ID = 102L;
	private static final Long NONEXISTENT_ID = 101010L;
	private static final String EXISTING_NAME = "fun";
	private static final String EXISTING_NAME2 = "sports";
	private static final String INVALID_NAME = "f345";

	private static final String NEW_NAME = "New Category";
	private static final String NEW_COLOR = "New Color";
	private static final String NEW_ICON = "New, Icon";

	@Before
	public void setUp() {

		headers = new HttpHeaders();
		LogInDTO dto = new LogInDTO("pera", "user");
		HttpEntity<LogInDTO> request = new HttpEntity<LogInDTO>(dto, headers);
		ResponseEntity<JwtResponseDTO> response = testRestTemplate.postForEntity("/auth/login", request,
				JwtResponseDTO.class);
		headers.add("Authorization", "Bearer " + response.getBody().getToken());
		token = "Bearer " + response.getBody().getToken();
		headers.add("Content-Type", "application/json");
	}

	// ***** getCategories *****

	@Test
	public void getCategories_getAll() {
		ResponseEntity<CategoryDTO[]> response = testRestTemplate.getForEntity(URL, CategoryDTO[].class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(3, response.getBody().length);
	}

	// ***** getCategory *****

	@Test
	public void getCategory_sucess() {
		ResponseEntity<CategoryDTO> response = testRestTemplate.getForEntity(URL + EXISTING_ID,
				CategoryDTO.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(EXISTING_ID, response.getBody().getId());
	}

	@Test
	public void getCategory_notExists() {
		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.getForEntity(URL + NONEXISTENT_ID,
				ErrorDetailsDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertTrue(response.getBody().getMessage().contains(NONEXISTENT_ID + ""));
	}

	// ***** addCategory *****

	@Test
	public void addCategory_success() {
		CategoryDTO dto = new CategoryDTO(null, NEW_NAME, NEW_COLOR, NEW_ICON);

		HttpEntity<CategoryDTO> request = new HttpEntity<CategoryDTO>(dto, headers);
		ResponseEntity<CategoryDTO> response = testRestTemplate.postForEntity(URL, request,
				CategoryDTO.class);

		CategoryDTO created = response.getBody();

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(NEW_NAME, created.getName());
		assertEquals(NEW_COLOR, created.getColor());
		assertEquals("category_icons/" + NEW_NAME + ".jpg", created.getIcon());
	}

	@Test
	public void addCategory_categoryNameExists() {
		CategoryDTO dto = new CategoryDTO(null, EXISTING_NAME, NEW_COLOR, NEW_ICON);

		HttpEntity<CategoryDTO> request = new HttpEntity<CategoryDTO>(dto, headers);
		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.postForEntity(URL, request,
				ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
		assertTrue(response.getBody().getMessage().contains(EXISTING_NAME));

	}
	
	@Test
	public void addCategory_invalidCategoryName() {
		CategoryDTO dto = new CategoryDTO(null, INVALID_NAME, NEW_COLOR, NEW_ICON);

		HttpEntity<CategoryDTO> request = new HttpEntity<CategoryDTO>(dto, headers);
		ResponseEntity<ErrorResponseDTO> response = testRestTemplate.postForEntity(URL, request,
				ErrorResponseDTO.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		
		// TODO
		//assertTrue(response.getBody().getErrors().get(0).getMessage().equals("Category name must contain only letters"));

	}
	
	//TODO Invalid color or icon??
	
	// ***** updateCategory ***** 
	@Test
	public void updateCategory_success() {
		CategoryDTO newCategoryDto = new CategoryDTO(EXISTING_ID, NEW_NAME, NEW_COLOR, NEW_ICON);

		HttpEntity<CategoryDTO> request = new HttpEntity<CategoryDTO>(newCategoryDto, headers);
		ResponseEntity<CategoryDTO> response = testRestTemplate.exchange(URL + EXISTING_ID, HttpMethod.PUT, request, CategoryDTO.class);
		
		CategoryDTO created = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(NEW_NAME, created.getName());
		assertEquals(NEW_COLOR, created.getColor());
		assertEquals("category_icons/" + NEW_NAME + ".jpg", created.getIcon());
	}
	
	@Test
	public void updateCategory_categoryNotExists() {
		CategoryDTO newCategoryDto = new CategoryDTO(NONEXISTENT_ID, NEW_NAME, NEW_COLOR, NEW_ICON);

		HttpEntity<CategoryDTO> request = new HttpEntity<CategoryDTO>(newCategoryDto, headers);
		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.exchange(URL + NONEXISTENT_ID, HttpMethod.PUT, request, ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertTrue(response.getBody().getMessage().contains(NONEXISTENT_ID + ""));
	}
	
	@Test
	public void updateCategory_categoryNameExists() {
		CategoryDTO newCategoryDto = new CategoryDTO(EXISTING_ID, EXISTING_NAME2, NEW_COLOR, NEW_ICON);

		HttpEntity<CategoryDTO> request = new HttpEntity<CategoryDTO>(newCategoryDto, headers);
		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.exchange(URL + EXISTING_ID, HttpMethod.PUT, request, ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());

	}
	
	// **** deleteCategory *****
	
	@Test
	public void deleteCategory_success() {
		HttpEntity<Object> request = new HttpEntity<Object>(null,headers);
		ResponseEntity<Boolean> response = testRestTemplate.exchange(URL + DELETE_ID, HttpMethod.DELETE, request, Boolean.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody());
	}

	@Test
	public void deleteCategory_categoryNotExists() {
		HttpEntity<Object> request = new HttpEntity<Object>(null, headers);
		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.exchange(URL + NONEXISTENT_ID,
				HttpMethod.DELETE, request, ErrorDetailsDTO.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertTrue(response.getBody().getMessage().contains(NONEXISTENT_ID + ""));
	}
	
	@Test
	public void deleteCategory_categoryCannotBeDeleted() {
		HttpEntity<Object> request = new HttpEntity<Object>(null, headers);
		ResponseEntity<ErrorDetailsDTO> response = testRestTemplate.exchange(URL + EXISTING_ID,
				HttpMethod.DELETE, request, ErrorDetailsDTO.class);

		assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
		assertTrue(response.getBody().getMessage().contains("cannot be deleted"));
	}
}
