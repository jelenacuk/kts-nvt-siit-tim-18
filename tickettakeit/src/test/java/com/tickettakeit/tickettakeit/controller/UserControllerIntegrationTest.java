package com.tickettakeit.tickettakeit.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tickettakeit.tickettakeit.dto.ErrorDetailsDTO;
import com.tickettakeit.tickettakeit.dto.JwtResponseDTO;
import com.tickettakeit.tickettakeit.dto.LogInDTO;
import com.tickettakeit.tickettakeit.dto.UserDTO;
import com.tickettakeit.tickettakeit.exceptions.PasswordConfirmationException;
import com.tickettakeit.tickettakeit.exceptions.UserNotExistsException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test-jelena.properties")
public class UserControllerIntegrationTest {

	@Autowired
	private TestRestTemplate testRestTemplate;


	private UserDTO registredUser;
	private String token;
	private UserDTO nonExistingUSer;

	// Registred user in database
	private static final String USER_FIRST_NAME = "Mira";
	private static final String USER_LAST_NAME = "Miric";
	private static final String USER_PHONE = "023/848-888";
	private static final String USER_EMAIL = "mira@gmail.com";
	private static final String USER_USERNAME = "mira";
	private static final String USER_PASSWORD = "user";
	private static final String USER_PAYPAL = "paypal";
	
	private HttpHeaders headers;

	@Before
	public void login() {

		ResponseEntity<JwtResponseDTO> result = testRestTemplate.postForEntity("/auth/login",
				new LogInDTO(USER_USERNAME, USER_PASSWORD), JwtResponseDTO.class);
		token = result.getBody().getToken();

		registredUser = new UserDTO(USER_USERNAME, USER_PASSWORD, USER_EMAIL, USER_FIRST_NAME, USER_LAST_NAME,
				USER_PHONE, USER_PAYPAL);
		registredUser.setRepeatedPassword("");
		registredUser.setNewPassword("");
		nonExistingUSer = new UserDTO("djura", "djura", "djura@gmail.com", "Djura", "Djuric", "023/848-111", "paypal");
		nonExistingUSer.setRepeatedPassword(nonExistingUSer.getPassword());
		
		headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + token);
	}

////EDIT USER PROFILE

	@Test
	@Transactional
	@Rollback
	public void editProfile_Successful_OK() throws PasswordConfirmationException, UserNotExistsException {
		
		HttpEntity<UserDTO> httpEntity = new HttpEntity<UserDTO>(registredUser, headers);
		ResponseEntity<Boolean> responseEntity = testRestTemplate.exchange("/api/user/editUserProfile", HttpMethod.POST,
				httpEntity, Boolean.class);
		Boolean success = responseEntity.getBody();
		assertEquals("Http status is OK.", HttpStatus.OK, responseEntity.getStatusCode());
		assertTrue("Response body has value TRUE", success);


	}

	@Test
	@Transactional
	@Rollback
	public void editProfile_WrongRepeatedPassword_BAD_REQUEST()
			throws PasswordConfirmationException, UserNotExistsException {

		registredUser.setRepeatedPassword(USER_PASSWORD + "123");
		HttpEntity<UserDTO> httpEntity = new HttpEntity<UserDTO>(registredUser, headers);
		ResponseEntity<ErrorDetailsDTO> responseEntity = testRestTemplate.exchange("/api/user/editUserProfile", HttpMethod.POST,
				httpEntity, ErrorDetailsDTO.class);
		ErrorDetailsDTO error = responseEntity.getBody();
		assertEquals("Http status is BAD_REQUEST", HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("You must re-enter the password so that it matches with the original one.", error.getMessage());

		// return values
		registredUser.setRepeatedPassword(USER_PASSWORD);
	}
	
	@Test
	@Transactional
	@Rollback
	public void editProfile_WrongRCurrentPassword_BAD_REQUEST()
			throws PasswordConfirmationException, UserNotExistsException {

		registredUser.setPassword("wrongCurrentPassword");
		HttpEntity<UserDTO> httpEntity = new HttpEntity<UserDTO>(registredUser, headers);
		ResponseEntity<ErrorDetailsDTO> responseEntity = testRestTemplate.exchange("/api/user/editUserProfile", HttpMethod.POST,
				httpEntity, ErrorDetailsDTO.class);
		ErrorDetailsDTO error = responseEntity.getBody();
		assertEquals("Http status is BAD_REQUEST", HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("You must re-enter the password so that it matches with the original one.", error.getMessage());
		// return values
		registredUser.setPassword(USER_PASSWORD);
	}

	@Test
	@Transactional
	@Rollback
	public void editProfile_NonExistingUser_NOT_FOUND() throws PasswordConfirmationException, UserNotExistsException {

		HttpEntity<UserDTO> httpEntity = new HttpEntity<UserDTO>(nonExistingUSer, headers);
		ResponseEntity<ErrorDetailsDTO> responseEntity = testRestTemplate.exchange("/api/user/editUserProfile", HttpMethod.POST,
				httpEntity, ErrorDetailsDTO.class);
		ErrorDetailsDTO error = responseEntity.getBody();
		assertEquals("Http status is NOT_FOUND", HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("User with username: djura doesn't exists.", error.getMessage());
	}

////GET LOGGED USER

	@Test
	@Transactional
	@Rollback
	public void getLogUser_Successful_OK() throws UserNotExistsException {

		HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);
		ResponseEntity<UserDTO> responseEntity = testRestTemplate.exchange("/api/user/getLogUser", HttpMethod.GET, httpEntity,
				UserDTO.class);
		UserDTO logged = responseEntity.getBody();
		assertEquals("Http status is OK ", HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(USER_USERNAME, logged.getUsername());
		assertEquals(USER_EMAIL, logged.getEmail());
		assertEquals(USER_FIRST_NAME, logged.getFirstName());
		assertEquals(USER_LAST_NAME, logged.getLastName());
		assertEquals(USER_PHONE, logged.getPhone());
	}

}
