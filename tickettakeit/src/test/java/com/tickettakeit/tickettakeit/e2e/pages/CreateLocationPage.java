package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateLocationPage {
	private WebDriver driver;

	@FindBy(id = "name")
	private WebElement name;
	@FindBy(id = "city")
	private WebElement city;
	@FindBy(id = "address")
	private WebElement address;
	@FindBy(id = "longitude")
	private WebElement longitude;
	@FindBy(id = "latitude")
	private WebElement latitude;
	@FindBy(id = "createButton")
	private WebElement createButton;
	@FindBy(id = "updateBt")
	private WebElement updateButton;
	

	public WebDriver getDriver() {
		return driver;
	}


	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}


	public WebElement getName() {
		return name;
	}


	public void setName(String name) {
		this.name.clear();
		this.name.sendKeys(name);
	}


	public WebElement getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city.clear();
		this.city.sendKeys(city);
	}


	public WebElement getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address.clear();
		this.address.sendKeys(address);
	}


	public WebElement getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude.clear();
		this.longitude.sendKeys(longitude);
	}


	public WebElement getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude.clear();
		this.latitude.sendKeys(latitude);
	}


	public WebElement getCreateButton() {
		return createButton;
	}
	

	

	public WebElement getUpdateButton() {
		return updateButton;
	}


	public void setUpdateButton(WebElement updateButton) {
		this.updateButton = updateButton;
	}


	public void setCreateButton(WebElement createButton) {
		this.createButton = createButton;
	}


	public void ensurecreateButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(createButton));
	}
	public void ensureUpdateButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(updateButton));
	}

}
