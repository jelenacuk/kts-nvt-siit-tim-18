package com.tickettakeit.tickettakeit.e2e;

import static com.tickettakeit.tickettakeit.e2e.constants.HomePageConstants.CATEGORY_SEARCH_HAS_RESULTS;
import static com.tickettakeit.tickettakeit.e2e.constants.HomePageConstants.CATEGORY_SEARCH_NO_RESULTS;
import static com.tickettakeit.tickettakeit.e2e.constants.HomePageConstants.EVENT_NAME_FOR_SEARCH;
import static com.tickettakeit.tickettakeit.e2e.constants.HomePageConstants.*;
import static com.tickettakeit.tickettakeit.e2e.constants.URLConstants.HOME_PAGE_URL;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.HomePage;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EventSearchE2E {

	private WebDriver browser;
	private HomePage homePage; 

	@BeforeEach
	public void setupSelenium() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		browser.navigate().to(HOME_PAGE_URL);

		homePage = PageFactory.initElements(browser, HomePage.class);
		homePage.setDriver(browser);

	}
	 
	// ********************** DETAILED SEARCH *****************************
	
	@Test 
	@Order(1)
	public void searchByEventName() {

		homePage.ensureSearchExpansionPanelIsDisplayed();
		homePage.getSearchExpansionPanel().click();

		homePage.ensureEventNameInputIsDisplayed();
		homePage.setEventNameSearch(EVENT_NAME_FOR_SEARCH);

		homePage.ensureSearchBtnIsDisplayed();
		homePage.getSearchBtn().click();

		// Refresh page - search results will be saved
		browser.navigate().to(HOME_PAGE_URL);
		homePage.ensureEventsIsDisplayed();

		assertThat(homePage.getEventCardEventNames())
				.allMatch(element -> element.getText().contains(EVENT_NAME_FOR_SEARCH));
	}

	@Test
	@Order(2)
	public void searchByLocationName() {

		homePage.ensureSearchExpansionPanelIsDisplayed();
		homePage.getSearchExpansionPanel().click();

		homePage.ensureLocationNameInputIsDisplayed();
		homePage.setLocationNameSearch(LOCATION_NAME_FOR_SEARCH);
		homePage.ensureSearchBtnIsDisplayed();
		homePage.getSearchBtn().click();

		// Refresh page - search results will be saved
		browser.navigate().to(HOME_PAGE_URL);
		homePage.ensureEventsIsDisplayed();

		assertThat(homePage.getEventCardLocationNames())
				.allMatch(element -> element.getText().contains(LOCATION_NAME_FOR_SEARCH));
	}

	@Test
	@Order(3)
	public void searchByCategory_hasResults() {

		homePage.ensureCategorySearchBtnsAreDisplayed();
		List<WebElement> categorySearchBtns = homePage.getCategorySearchBtns();

		WebElement categoryBtn = categorySearchBtns.stream()
				.filter(btn -> btn.getText().equals(CATEGORY_SEARCH_HAS_RESULTS)).findFirst().get();

		categoryBtn.click();
		// Refresh page - search results will be saved
		browser.navigate().to(HOME_PAGE_URL);
		homePage.ensureEventsIsDisplayed();

		assertThat(homePage.getCategoriesOfEvents())
				.allMatch(event -> event.getText().contains(CATEGORY_SEARCH_HAS_RESULTS));
	}

	@Test
	@Order(4)
	public void searchByCategory_noResults() {

		homePage.ensureCategorySearchBtnsAreDisplayed();
		List<WebElement> categorySearchBtns = homePage.getCategorySearchBtns();

		WebElement categoryBtn = categorySearchBtns.stream()
				.filter(btn -> btn.getText().equals(CATEGORY_SEARCH_NO_RESULTS)).findFirst().get();

		categoryBtn.click();
		// Refresh page - search results will be saved
		browser.navigate().to(HOME_PAGE_URL);
		homePage.ensureEventsIsDisplayed();

		assertThat(homePage.getEventCardLocationNames()).hasSize(0);
	} 
	
	// ********************** GENERAL SEARCH *****************************
	
	@Test
	@Order(5)
	public void generalSearch_matchLocation() {

		homePage.ensureGeneralSearchInutIsReady();
		homePage.setGeneralSearchInput(LOCATION_NAME_FOR_SEARCH);

		homePage.ensureGeneralSearchBtnIsDisplayed();
		homePage.getGeneralSearchBtn().click();

		// Refresh page - search results will be saved
		browser.navigate().to(HOME_PAGE_URL);
		homePage.ensureEventsIsDisplayed();

		assertThat(homePage.getEventCardLocationNames())
				.allMatch(element -> element.getText().contains(LOCATION_NAME_FOR_SEARCH));
	}
	
	@Test
	@Order(6)
	public void generalSearch_noResults() {

		homePage.ensureGeneralSearchInutIsReady();
		homePage.setGeneralSearchInput(GENERAL_SEARCH_NO_RESULTS);

		homePage.ensureGeneralSearchBtnIsDisplayed();
		homePage.getGeneralSearchBtn().click();

		// Refresh page - search results will be saved
		browser.navigate().to(HOME_PAGE_URL);
		homePage.ensureEventsIsDisplayed();

		assertThat(homePage.getEventCardLocationNames()).hasSize(0);
	}
	
	@AfterEach
	public void afterTest() {
		browser.close();
	}

}
