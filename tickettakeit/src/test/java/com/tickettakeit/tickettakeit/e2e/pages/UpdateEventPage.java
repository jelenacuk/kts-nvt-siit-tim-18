package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UpdateEventPage {

	private WebDriver driver;

	@FindBy(id = "name")
	private WebElement eventName;
	@FindBy(id = "nameBtn")
	private WebElement editNameBtn;
	@FindBy(id = "controllAccess")
	private WebElement controllAccess;
	@FindBy(id = "controllAccessBtn")
	private WebElement controllAccessBtn;
	// DATES
	@FindBy(xpath = "//*[@id=\"mat-expansion-panel-header-0\"]/span[2]")
	private WebElement dateExtensionPanel;
	@FindBy(id = "dateInput")
	private WebElement inputDate;
	@FindBy(id = "Mar 4, 2020, 2:40:00 PM")
	WebElement newDate;
	@FindBy(id = "addDateBtn")
	private WebElement addDateBtn;
	@FindBy(id = "changeEventDateBtn")
	private WebElement changeEventDateBtn;
	// SNACKBAR
	@FindBy(className = "cdk-overlay-container")
	private WebElement snackBar;

	// CATEGORIES
	@FindBy(xpath = "//*[@id=\"mat-expansion-panel-header-1\"]/span[2]")
	private WebElement categoryExtensionPanel;
	@FindBy(id = "categorySelect")
	WebElement selectCategory;
	@FindBy(id = "200")
	WebElement newCategory;
	@FindBy(id = "addCategoryBtn")
	private WebElement addCategoryBtn;
	

	public UpdateEventPage(WebDriver driver) {
		this.driver = driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getEventName() {
		return eventName;
	}

	public void setEventName(String value) {
		this.eventName.clear();
		this.eventName.sendKeys(value);
	}

	public WebElement getEditNameBtn() {
		return editNameBtn;
	}

	public void setControllAccess(int index) {
		this.controllAccess.click();
		WebElement optionToSelect = driver.findElement(By.xpath("//*[@id=\"mat-option-" + index + "\"]/span"));
		optionToSelect.click();
	}

	public WebElement getEditControllAccessBtn() {
		return controllAccessBtn;
	}

	public WebElement getNewDate() {
		return newDate;
	}

	public WebElement getAddDateBtn() {
		return addDateBtn;
	}

	public WebElement getChangeEventDateBtn() {
		return changeEventDateBtn;
	}

	public WebElement getSnackBar() {
		return snackBar;
	}

	public WebElement getInputDate() {
		return this.inputDate;
	}

	public void setInputDate(String value) {
		this.inputDate.clear();
		this.inputDate.sendKeys(value);
	}

	public WebElement getDateExtensionPanel() {
		return this.dateExtensionPanel;
	}
	
	public void setInputCategory(String option) {
		selectCategory.click();
		WebElement optionToSelect = driver.findElement(By.id("200ID"));
		optionToSelect.click();
	}
	public void myEnsurance(String value) {
		WebElement optionToSelect = driver.findElement(By.id("200ID"));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.textToBePresentInElement(optionToSelect, value));
	}

	public WebElement getCategoryExtensionPanel() {
		return categoryExtensionPanel;
	}

	public WebElement getNewCategory() {
		return newCategory;
	}

	public WebElement getAddCategoryBtn() {
		return addCategoryBtn;
	}

	public void ensureNameButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(editNameBtn));
	}

	public void ensureControllAccessButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(controllAccessBtn));
	}

	public void ensureAddDateButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(addDateBtn));
	}

	public void ensureChangeEventDateButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(changeEventDateBtn));
	}

	public void ensureAddedDateIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(newDate));
	}

	public void ensurePanelExpanded() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(dateExtensionPanel));
	}
	
	public void ensureAddCategoryButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(addCategoryBtn));
	}
	
	public void ensureAddedCategoryIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(newCategory));
	}

	public void ensureCategoryPanelExpanded() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(categoryExtensionPanel));
	}


	public void ensureSnackBarIsDisplayed(String text) {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.textToBePresentInElement(snackBar, text));
	}

	public void ensureSnackBarInvisible() {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.invisibilityOf(this.snackBar));
	}

	public String ensureErrorMessageIsDisplayed(String index) {
		WebElement error = driver.findElement(By.id("mat-error-" + index));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(error));
		return error.getText();
	}

}
