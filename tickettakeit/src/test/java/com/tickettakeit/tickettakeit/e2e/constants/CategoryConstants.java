package com.tickettakeit.tickettakeit.e2e.constants;

public class CategoryConstants {
	
	public static final String NEW_CATEGORY_NAME = "New Category test";
	public static final String NEW_ICON_PATH = "C:\\Users\\NikolaS\\Pictures\\Saved Pictures\\Red-Panda-Parker-001-Birmingham-Zoo-2-27-18-1024x801.jpg";
	public static final String CATEGORY_FOR_UPDATE_NAME = "Music Festival";
	public static final String UPDATED_CATEGORY_NAME = "Music";
	public static final String CATEGORY_FOR_UPDATE_NAME_CANCEL = "Art Exibition";
	public static final String UPDATED_CATEGORY_NAME_CANCEL = "Update Category cancel";
	public static final String CATEGORY_FOR_DELETION_NAME = "Cinema";

}
