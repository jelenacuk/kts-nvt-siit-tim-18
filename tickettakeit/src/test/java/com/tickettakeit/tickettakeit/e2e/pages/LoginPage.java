package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	
	private WebDriver driver;

	@FindBy(id = "username")
	private WebElement userName;
	@FindBy(id = "password")
	private WebElement password;
	@FindBy(id="loginBt")
	private WebElement loginButton;
	@FindBy(className = "cdk-overlay-container")
	private WebElement snackBar;
	
	public WebDriver getDriver() {
		return driver;
	}



	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}



	public WebElement getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName.sendKeys(userName);
	}



	public WebElement getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password.sendKeys(password);
	}



	public WebElement getLoginButton() {
		return loginButton;
	}



	public void setLoginButton(WebElement loginButton) {
		this.loginButton = loginButton;
	}

	public WebElement getSnackBar() {
		return this.snackBar;
	}

	public void ensureTableRowIsPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(loginButton));
	}
	
	public void ensureSnackBarIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(snackBar));
	}
	
	
	public String ensureErrorMessageIsDisplayed(String index) {
		WebElement error = driver.findElement(By.id("mat-error-" + index));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(error));
		return error.getText();
	}
	
	public void ensureSnackBarIsDisplayed(String text) {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.textToBePresentInElement(snackBar, text));
	}

	public void ensureSnackBarInvisible() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.invisibilityOf(this.snackBar));
	}
}
