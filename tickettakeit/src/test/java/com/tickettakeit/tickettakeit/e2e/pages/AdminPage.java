package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdminPage {
	private WebDriver driver;

	@FindBy(id = "mat-tab-label-0-2")
	private WebElement addEventTab;

	@FindBy(id = "mat-tab-label-0-3")
	private WebElement locationsTab;

	@FindBy(id = "mat-tab-label-0-4")
	private WebElement categoriesTab;

	@FindBy(id = "mat-tab-label-0-5")
	private WebElement reportsTab;

	public WebElement getAddEventTab() {
		return addEventTab;
	}

	public void setAddEventTab(WebElement addEventTab) {
		this.addEventTab = addEventTab;
	}

	public WebElement getLocationsTab() {
		return locationsTab;
	}

	public void setLocationsTab(WebElement locationsTab) {
		this.locationsTab = locationsTab;
	}

	public WebElement getCategoriesTab() {
		return categoriesTab;
	}

	public void setCategoriesTab(WebElement categoriesTab) {
		this.categoriesTab = categoriesTab;
	}

	public WebElement getReportsTab() {
		return reportsTab;
	}

	public void setReportsTab(WebElement reportsTab) {
		this.reportsTab = reportsTab;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void ensureAddEventTabIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(addEventTab));
	}

	public void ensurelocationsTabIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(locationsTab));
	}

	public void ensureCategoriesTabIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(categoriesTab));
	}

	public void ensureReportsTabIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(reportsTab));
	}
}
