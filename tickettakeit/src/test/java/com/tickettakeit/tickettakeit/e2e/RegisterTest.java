package com.tickettakeit.tickettakeit.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.RegisterPage;

public class RegisterTest {

	RegisterPage registerPage;
	WebDriver browser;

	private final String username = "mira";
	private final String password = "mira";
	private final String firstName = "Mira";
	private final String lastName = "Miric";
	private final String email = "mira@gmail.com";
	private final String phone = "023/848-848";
	private final String paypal = "45548432";
	private final String takenUsername = "user";
	private final String wrongRepeatedPassword = "xxxx";

	@Before
	public void setupSelenium() {

		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		browser.navigate().to("http://localhost:4200/register");
		registerPage = PageFactory.initElements(browser, RegisterPage.class);
		registerPage.setDriver(browser);
	}

	@Test
	public void registerTest() {

		// successfully
		registerPage.ensureRegisterButtonIsDisplayed();
		placeValuesInFields(username, password, password, firstName, lastName, email, phone);
		registerPage.getRegisterButton().click();
		registerPage.ensureSnackBarInvisible();
		registerPage.ensureRegisterMessageIsDisplayed();
		String textMessage = registerPage.getRegistrationMessage().getText();
		assertEquals("Successful registration. You will soon recieve email for confirmation your account.",
				textMessage);

	}

	@Test
	public void registerTest_ExistingUsername() {

		// all fields empty
		registerPage.ensureRegisterButtonIsDisplayed();
		registerPage.getRegisterButton().click();
		String text = registerPage.ensureErrorMessageIsDisplayed("0");
		assertEquals("Username: min 4 characters", text);

		// register with taken username
		registerPage.ensureRegisterButtonIsDisplayed();
		placeValuesInFields(takenUsername, password, password, firstName, lastName, email, phone);
		registerPage.getRegisterButton().click();
		registerPage.ensureSnackBarIsDisplayed();
		text = registerPage.getSnackBar().getText();
		assertEquals("User with username: " + takenUsername +" already exists.", text);
		assertEquals("http://localhost:4200/register", browser.getCurrentUrl());
		registerPage.ensureSnackBarInvisible();

		// set wrong First Name
		registerPage.setFirstName("us%e%r");
		registerPage.getRegisterButton().click();
		text = registerPage.ensureErrorMessageIsDisplayed("2");
		assertEquals("First Name can contain only letters.", text);

		// set wrong repeated pass
		registerPage.setRepeatedPassword(wrongRepeatedPassword);
		registerPage.setFirstName(firstName);
		registerPage.getRegisterButton().click();
		registerPage.ensureSnackBarIsDisplayed();
		text = registerPage.getSnackBar().getText();
		assertEquals("Wrong repeated password.", text);
	}

	private void placeValuesInFields(String _username, String _password, String _repeatedPassword, String _firstName,
			String _lastName, String _email, String _phone) {
		registerPage.setUsername(_username);
		registerPage.setPassword(_password);
		registerPage.setRepeatedPassword(_repeatedPassword);
		registerPage.setFirstName(_firstName);
		registerPage.setLastName(_lastName);
		registerPage.setEmail(_email);
		registerPage.setPhone(_phone);
		registerPage.setPaypal(paypal);
	}

	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
}
