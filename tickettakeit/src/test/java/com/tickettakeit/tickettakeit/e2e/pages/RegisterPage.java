package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {

	private WebDriver driver;

	@FindBy(id = "username")
	private WebElement username;
	@FindBy(id = "password")
	private WebElement password;
	@FindBy(id = "repeatedPassword")
	private WebElement repeatedPassword;
	@FindBy(id = "firstName")
	private WebElement firstName;
	@FindBy(id = "lastName")
	private WebElement lastName;
	@FindBy(id = "email")
	private WebElement email;
	@FindBy(id = "phone")
	private WebElement phone;
	@FindBy(id = "paypal")
	private WebElement paypal;
	@FindBy(id = "registerButton")
	private WebElement registerButton;
	@FindBy(id = "registerMessage")
	private WebElement registrationMessage;
	@FindBy(className = "cdk-overlay-container")
	private WebElement snackBar;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getUsername() {
		return username;
	}

	public void setUsername(String value) {
		WebElement el = getUsername();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getPassword() {
		return password;
	}

	public void setPassword(String value) {
		WebElement el = getPassword();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getRepeatedPassword() {
		return repeatedPassword;
	}

	public void setRepeatedPassword(String value) {
		WebElement el = getRepeatedPassword();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getFirstName() {
		return firstName;
	}

	public void setFirstName(String value) {
		WebElement el = getFirstName();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getLastName() {
		return lastName;
	}

	public void setLastName(String value) {
		WebElement el = getLastName();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getEmail() {
		return email;
	}

	public void setEmail(String value) {
		WebElement el = getEmail();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getPhone() {
		return phone;
	}

	public void setPhone(String value) {
		WebElement el = getPhone();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getPaypal() {
		return paypal;
	}

	public void setPaypal(String value) {
		WebElement el = getPaypal();
		el.clear();
		el.sendKeys(value);
	}

	public WebElement getRegisterButton() {
		return registerButton;
	}

	public WebElement getRegistrationMessage() {
		return registrationMessage;
	}
	
	public WebElement getSnackBar(){
		return snackBar;
	}

	public void ensureRegisterButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(registerButton));
	}
	
	public void ensureRegisterMessageIsDisplayed() {
		(new WebDriverWait(driver, 15)).until(ExpectedConditions.elementToBeClickable(registrationMessage));
	}
	
	public void ensureSnackBarIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(snackBar));
	}
	
	public void ensureSnackBarInvisible() {
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.invisibilityOf(this.snackBar));
	}
	
	public String ensureErrorMessageIsDisplayed(String index) {
		WebElement error = driver.findElement(By.id("mat-error-" + index));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(error));
		return error.getText();
	}
}
