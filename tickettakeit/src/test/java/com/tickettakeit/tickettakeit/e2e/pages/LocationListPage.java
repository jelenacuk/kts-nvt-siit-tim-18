package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LocationListPage {

	private WebDriver driver;

	@FindBy(id = "newLocationButton")
	private WebElement newLocationButton;
	@FindBy(xpath = "//*[@id='locationTable']/table/tbody/tr[1]/td[1]")
	private WebElement tableRowName;
	@FindBy(xpath = "//*[@id='Sava Centardeac']/button")
	private WebElement deactivateButton;
	@FindBy(xpath = "//*[@id='Sava Centarstatus']")
	private WebElement statusText;
	@FindBy(id = "Location 1")
	private WebElement addedLocationName;
	
	@FindBy(id = "Sava Centar")
	private WebElement selectLocation;
	

	public WebDriver getDriver() {
		return driver;
	}


	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}


	public WebElement getNewLocationButton() {
		return newLocationButton;
	}


	public void setNewLocationButton(WebElement newLocationButton) {
		this.newLocationButton = newLocationButton;
	}
	


	public WebElement getTableRow() {
		return tableRowName;
	}


	public void setTableRow(WebElement tableRow) {
		this.tableRowName = tableRow;
	}
	


	public WebElement getTableRowName() {
		return tableRowName;
	}


	public void setTableRowName(WebElement tableRowName) {
		this.tableRowName = tableRowName;
	}
	


	public WebElement getDeactivateButton() {
		return deactivateButton;
	}


	public void setDeactivateButton(WebElement deactivateButton) {
		this.deactivateButton = deactivateButton;
	}
	


	public WebElement getStatusText() {
		return statusText;
	}


	public void setStatusText(WebElement statusText) {
		this.statusText = statusText;
	}
	


	public WebElement getAddedLocationName() {
		return addedLocationName;
	}
	


	public WebElement getSelectLocation() {
		return selectLocation;
	}


	public void setSelectLocation(WebElement selectLocation) {
		this.selectLocation = selectLocation;
	}


	public void setAddedLocationName(WebElement addedLocationName) {
		this.addedLocationName = addedLocationName;
	}


	public void ensurenewLocationButtonIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(newLocationButton));
	}
	public void ensureTableRowIsPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(tableRowName));
	}
	public void ensureStatusIsPresend() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(statusText));
	}
	

}
