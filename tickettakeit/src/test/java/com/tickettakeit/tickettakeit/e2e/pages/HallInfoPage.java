package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HallInfoPage {
	
	@FindBy(id="hallName")
	private WebElement hallName;
	@FindBy(id="updateHall")
	private WebElement updateHallBt;
	@FindBy(id="createSection")
	private WebElement createSectionBt;
	
	private WebDriver driver;

	public WebElement getHallName() {
		return hallName;
	}

	public void setHallName(WebElement hallName) {
		this.hallName = hallName;
	}

	public WebElement getUpdateHallBt() {
		return updateHallBt;
	}

	public void setUpdateHallBt(WebElement updateHallBt) {
		this.updateHallBt = updateHallBt;
	}

	public WebDriver getDriver() {
		return driver;
	}
	

	public WebElement getCreateSectionBt() {
		return createSectionBt;
	}

	public void setCreateSectionBt(WebElement createSectionBt) {
		this.createSectionBt = createSectionBt;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	public void ensureUpdateBtIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(updateHallBt));
	}
	public void ensureCreateSectionIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(createSectionBt));
	}
	

}
