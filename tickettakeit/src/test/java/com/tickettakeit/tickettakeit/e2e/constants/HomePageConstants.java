package com.tickettakeit.tickettakeit.e2e.constants;

public class HomePageConstants {
	
	public static final String EVENT_NAME_FOR_SEARCH = "Lara Fabian";
	public static final String LOCATION_NAME_FOR_SEARCH = "Spens";
	public static final String CATEGORY_SEARCH_HAS_RESULTS = "Music Festival";
	public static final String CATEGORY_SEARCH_NO_RESULTS = "Cinema";
	public static final String GENERAL_SEARCH_NO_RESULTS = "fawrgerhebherthb";
		
} 
 