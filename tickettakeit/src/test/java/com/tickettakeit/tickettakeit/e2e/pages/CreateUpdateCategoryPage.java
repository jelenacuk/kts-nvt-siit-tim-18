package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateUpdateCategoryPage {

	private WebDriver driver;
	private JavascriptExecutor jsExecutor;
	private WebDriverWait wait;

	@FindBy(id = "name")
	private WebElement name;

	@FindBy(id = "color")
	private WebElement color;

	@FindBy(id = "fileInput")
	private WebElement icon;

	@FindBy(id = "add-category-btn")
	private WebElement addCategoryBtn;

	@FindBy(id = "update-category-btn")
	private WebElement updateCategoryBtn;

	@FindBy(id = "cancel-category-btn")
	private WebElement cancelCategoryBtn;

	@FindBy(className = "error-snack-bar")
	private WebElement errorSnackBar;
	
	@FindBy(id="add-category-form")
	private WebElement addCategoryForm;

	// ********************** GET AND SET METHODS ***********************
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 20);
		this.jsExecutor = (JavascriptExecutor) this.driver;
	}

	public WebElement getName() {
		return name;
	}

	public void setName(String name) {
		this.name.clear();
		this.name.sendKeys(name);
	}

	public WebElement getColor() {
		return color;
	}

	public void setColor(String color) {

		jsExecutor.executeScript("arguments[0].value='" + color + "';", this.color);
	}

	public WebElement getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon.sendKeys(icon);

	}

	public WebElement getAddCategoryBtn() {
		return addCategoryBtn;
	}

	public void setAddCategoryBtn(WebElement addCategoryBtn) {
		this.addCategoryBtn = addCategoryBtn;
	}

	public WebElement getUpdateCategoryBtn() {
		return updateCategoryBtn;
	}

	public void setUpdateCategoryBtn(WebElement updateCategoryBtn) {
		this.updateCategoryBtn = updateCategoryBtn;
	}

	public WebElement getCancelCategoryBtn() {
		return cancelCategoryBtn;
	}

	public WebElement getErrorSnackBar() {
		return errorSnackBar;
	}

	public void setErrorSnackBar(WebElement errorSnackBar) {
		this.errorSnackBar = errorSnackBar;
	}

	public void setCancelCategoryBtn(WebElement cancelCategoryBtn) {
		this.cancelCategoryBtn = cancelCategoryBtn;
	}

	// ************************** WAITS **********************************

	public void ensureAddCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOf(addCategoryBtn));
		wait.until(ExpectedConditions.elementToBeClickable(addCategoryBtn));
	}

	public void ensureUpdateCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOf(updateCategoryBtn));
		wait.until(ExpectedConditions.elementToBeClickable(updateCategoryBtn));
	}

	public void ensureCancelCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOfElementLocated((By.id("cancel-category-btn"))));
		wait.until(ExpectedConditions.elementToBeClickable(cancelCategoryBtn));
	}

	public void ensureSnackBarIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("error-snack-bar")));
	}
	
	public void ensureAddFormIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(addCategoryForm));
	}

}
