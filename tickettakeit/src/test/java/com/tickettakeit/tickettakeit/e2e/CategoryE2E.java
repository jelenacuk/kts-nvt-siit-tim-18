package com.tickettakeit.tickettakeit.e2e;

import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.CATEGORY_FOR_DELETION_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.CATEGORY_FOR_UPDATE_NAME_CANCEL;
import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.NEW_CATEGORY_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.NEW_ICON_PATH;
import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.UPDATED_CATEGORY_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.UPDATED_CATEGORY_NAME_CANCEL;
import static com.tickettakeit.tickettakeit.e2e.constants.URLConstants.LOGIN_PAGE_URL;
import static com.tickettakeit.tickettakeit.e2e.constants.UserConstants.ADMIN_PASSWORD;
import static com.tickettakeit.tickettakeit.e2e.constants.UserConstants.ADMIN_USERNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.AdminPage;
import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;
import com.tickettakeit.tickettakeit.e2e.pages.CategoriesListPage;
import com.tickettakeit.tickettakeit.e2e.pages.CreateUpdateCategoryPage;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CategoryE2E {

	private WebDriver browser;
	private JavascriptExecutor jsExecutor;
	private AdminPage adminPage;
	private LoginPage loginPage; 
	private CategoriesListPage categoriesListPage;
	private CreateUpdateCategoryPage createUpdateCategoryPage;

	@BeforeEach
	public void setupSelenium() { 
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		jsExecutor = (JavascriptExecutor) browser;
		browser.navigate().to(LOGIN_PAGE_URL);

		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(ADMIN_USERNAME);
		loginPage.setPassword(ADMIN_PASSWORD);
		loginPage.getLoginButton().click();
		adminPage = PageFactory.initElements(browser, AdminPage.class);
		adminPage.setDriver(browser);
		categoriesListPage = PageFactory.initElements(browser, CategoriesListPage.class);
		categoriesListPage.setDriver(browser);
		createUpdateCategoryPage = PageFactory.initElements(browser, CreateUpdateCategoryPage.class);
		createUpdateCategoryPage.setDriver(browser);

		adminPage.ensureCategoriesTabIsDisplayed();
		adminPage.getCategoriesTab().click();

	}

	// ************************ CREATE CATEGORY ************************************
	@Test
	@Order(1)
	public void createCategory_success() {
		categoriesListPage.ensureNewCategoryBtnIsDisplayed();
		categoriesListPage.getNewCategoryBtn().click();

		createUpdateCategoryPage.ensureAddFormIsDisplayed();
		createUpdateCategoryPage.setName(NEW_CATEGORY_NAME);

		createUpdateCategoryPage.setColor("#FAEBD7");

		String filename =  NEW_ICON_PATH;
		File file = new File(filename);
		String path = file.getAbsolutePath();

		createUpdateCategoryPage.setIcon(path);
		jsExecutor.executeScript("arguments[0].click();", createUpdateCategoryPage.getAddCategoryBtn());
		
		categoriesListPage.ensureSnackBarIsDisplayed();

		assertTrue(categoriesListPage.getSnackBar().getText().contains("added"));
		assertThat(categoriesListPage.getCategories())
				.anyMatch(element -> element.getText().equals(NEW_CATEGORY_NAME));

	}

	@Test
	@Order(2)
	public void createCategory_nameExists() {
		categoriesListPage.ensureNewCategoryBtnIsDisplayed();
		categoriesListPage.getNewCategoryBtn().click();

		createUpdateCategoryPage.ensureAddFormIsDisplayed();
		createUpdateCategoryPage.setName(NEW_CATEGORY_NAME);

		createUpdateCategoryPage.setColor("#FAEBD7");
		
		String filename = NEW_ICON_PATH;
		File file = new File(filename);
		String path = file.getAbsolutePath();
		createUpdateCategoryPage.setIcon(path);
		createUpdateCategoryPage.ensureAddCategoryBtnIsDisplayed();
		jsExecutor.executeScript("arguments[0].click();", createUpdateCategoryPage.getAddCategoryBtn());

		createUpdateCategoryPage.ensureSnackBarIsDisplayed();
		assertTrue(createUpdateCategoryPage.getErrorSnackBar().getText()
				.contains("Category with the name: " + NEW_CATEGORY_NAME + " already exists"));

	}

	// ************************* UPDATE CATEGORY *********************************
	@Test
	@Order(3)
	public void updateCategory_success() throws InterruptedException {

		categoriesListPage.ensureUpdateCategoryBtnIsDisplayed();
		categoriesListPage.getUpdateAddedCategoryBtn().click();
		
		Thread.sleep(3000);
		
		//createUpdateCategoryPage.ensureAddFormIsDisplayed();
		String oldName = createUpdateCategoryPage.getName().getText();
		createUpdateCategoryPage.setName(UPDATED_CATEGORY_NAME);

		jsExecutor.executeScript("arguments[0].click();", createUpdateCategoryPage.getUpdateCategoryBtn());

		categoriesListPage.ensureTableRowIsPresent();

		assertThat(categoriesListPage.getCategories()).allMatch(element -> !element.getText().equals(oldName));
		assertThat(categoriesListPage.getCategories())
				.anyMatch(element -> element.getText().equals(UPDATED_CATEGORY_NAME));

	}

	@Test
	@Order(4)
	public void updateCategory_canceled() {

		categoriesListPage.ensureUpdateCancelCategoryBtnIsDisplayed();
		categoriesListPage.getUpdateCancelCategoryBtn().click();

		createUpdateCategoryPage.ensureAddFormIsDisplayed();
		createUpdateCategoryPage.setName(UPDATED_CATEGORY_NAME_CANCEL);

		jsExecutor.executeScript("arguments[0].click();", createUpdateCategoryPage.getCancelCategoryBtn());

		categoriesListPage.ensureTableRowIsPresent();

		assertThat(categoriesListPage.getCategories())
				.allMatch(element -> !element.getText().equals(UPDATED_CATEGORY_NAME_CANCEL));
		assertThat(categoriesListPage.getCategories())
				.anyMatch(element -> element.getText().equals(CATEGORY_FOR_UPDATE_NAME_CANCEL));

	} 
	
	// ********************** DELETE CATEGORY **************************
	
	@Test
	@Order(5)
	public void deleteCategory_success() throws InterruptedException {
		
		categoriesListPage.ensureDeleteCategoryBtnIsDisplayed();
		categoriesListPage.getDeleteCategoryBtn().click();
		
		categoriesListPage.ensureDeleteConfirmationBtnIsDisplayed();
		categoriesListPage.getDeleteConfirmationBtn().click();

		categoriesListPage.ensureSnackBarIsDisplayed();
		
		assertTrue(categoriesListPage.getSnackBar().getText()
				.contains("deleted")); 
		assertThat(categoriesListPage.getCategories())
				.allMatch(element -> !element.getText().equals(CATEGORY_FOR_DELETION_NAME));
		
	}
	
	@AfterEach
	public void afterTest() {
		browser.close();
	}
}
