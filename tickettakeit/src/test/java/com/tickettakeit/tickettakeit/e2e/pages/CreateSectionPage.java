package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateSectionPage {

	@FindBy(id = "name")
	private WebElement name;
	@FindBy(id = "isNum")
	private WebElement isNum;
	@FindBy(id = "rows")
	private WebElement rows;
	@FindBy(id = "columns")
	private WebElement columns;
	@FindBy(id = "seats")
	private WebElement seats;
	@FindBy(id = "createBt")
	private WebElement createBt;
	@FindBy(id = "updateBt")
	private WebElement updateBt;
	private WebDriver driver;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getName() {
		return name;
	}

	public void setName(String name) {
		this.name.sendKeys(name);
	}

	public WebElement getIsNum() {
		return isNum;
	}

	public void setIsNum(String isNum) {
		this.isNum.sendKeys(isNum);
	}

	public WebElement getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows.sendKeys(rows);
	}

	public WebElement getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns.sendKeys(columns);
	}

	public WebElement getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats.sendKeys(seats);;
	}

	public WebElement getCreateBt() {
		return createBt;
	}

	public void setCreateBt(WebElement createBt) {
		this.createBt = createBt;
	}

	public WebElement getUpdateBt() {
		return updateBt;
	}

	public void setUpdateBt(WebElement updateBt) {
		this.updateBt = updateBt;
	}

	public void ensureUpdateBtIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(updateBt));
	}

	public void ensureCreateSectionIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(createBt));
	}

}
