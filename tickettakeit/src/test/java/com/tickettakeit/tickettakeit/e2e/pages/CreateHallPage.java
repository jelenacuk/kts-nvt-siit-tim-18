package com.tickettakeit.tickettakeit.e2e.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateHallPage {
	
	private WebDriver driver;

	
	@FindBy(id="name")
	private WebElement name;
	
	@FindBy(id="createHallBt")
	private WebElement createHall;
	
	@FindBy(id="updateHallBt")
	private WebElement updateButton;

	public WebElement getName() {
		return name;
	}

	public void setName(String name) {
		this.name.clear();
		this.name.sendKeys(name);
	}

	public WebElement getCreateHall() {
		return createHall;
	}

	public void setCreateHall(WebElement createHall) {
		this.createHall = createHall;
	}
		
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getUpdateButton() {
		return updateButton;
	}

	public void setUpdateButton(WebElement updateButton) {
		this.updateButton = updateButton;
	}

	public void ensureCreateHallIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(createHall));
	}
	public void ensureUpdateHallIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(updateButton));
	}
	

}
