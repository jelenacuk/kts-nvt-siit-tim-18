package com.tickettakeit.tickettakeit.e2e.pages;

import static com.tickettakeit.tickettakeit.e2e.constants.CategoryConstants.*;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CategoriesListPage {
	
	private WebDriver driver;
	
	private WebDriverWait wait; 
	
	@FindBy(id="new-category-btn")
	private WebElement newCategoryBtn;
	
	@FindBy(xpath = "//*[@id='categories-table']/tbody/tr[1]/td[1]")
	private WebElement tableRowName;
	
	@FindBy(id = CATEGORY_FOR_UPDATE_NAME)
	private WebElement categoryForUpdate;
	
	@FindBy(id = "update-category-" + CATEGORY_FOR_UPDATE_NAME)
	private WebElement updateCategoryBtn;
	
	@FindBy(id = "update-category-" + CATEGORY_FOR_UPDATE_NAME_CANCEL)
	private WebElement updateCancelCategoryBtn;
	
	@FindBy(id = "delete-category-" + CATEGORY_FOR_DELETION_NAME)
	private WebElement deleteCategoryBtn;
	
	@FindBy(className = "category-name")
	private List<WebElement> categories;
	
	@FindBy(id = "confirmation-confirm-btn")
	private WebElement deleteConfirmationBtn;
	
	@FindBy(className = "snack-bar")
	private WebElement snackBar;
	
	
	// ******************** GET AND SET METHODS *************************
	
	public WebElement getTableRowName() {
		return tableRowName;
	}

	public void setTableRowName(WebElement tableRowName) {
		this.tableRowName = tableRowName;
	}

	public WebElement getCategoryForUpdate() {
		return categoryForUpdate;
	}

	public void setCateogoryForUpdate(WebElement addedCategoryName) {
		this.categoryForUpdate = addedCategoryName;
	}

	public WebElement getUpdateAddedCategoryBtn() {
		return updateCategoryBtn;
	}

	public void setUpdateAddedCategoryBtn(WebElement updateAddedCategoryBtn) {
		this.updateCategoryBtn = updateAddedCategoryBtn;
	}

	public WebElement getUpdateCancelCategoryBtn() {
		return updateCancelCategoryBtn;
	}

	public void setUpdateCancelCategoryBtn(WebElement updateUpdatedCategoryBtn) {
		this.updateCancelCategoryBtn = updateUpdatedCategoryBtn;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 20);
	}

	public WebElement getNewCategoryBtn() {
		return newCategoryBtn;
	}

	public void setNewCategoryBtn(WebElement newCategoryBtn) {
		this.newCategoryBtn = newCategoryBtn;
	}
	
	public WebElement getDeleteCategoryBtn() {
		return deleteCategoryBtn;
	}

	public void setDeleteCategoryBtn(WebElement deleteCategoryBtn) {
		this.deleteCategoryBtn = deleteCategoryBtn;
	}
	
	public List<WebElement> getCategories() {
		return categories;
	}

	public void setCategories(List<WebElement> categories) {
		this.categories = categories;
	}
	
	public WebElement getDeleteConfirmationBtn() {
		return deleteConfirmationBtn;
	}

	public void setDeleteConfirmationBtn(WebElement deleteConfirmationBtn) {
		this.deleteConfirmationBtn = deleteConfirmationBtn;
	}
	
	public WebElement getSnackBar() {
		return snackBar;
	}

	public void setSnackBar(WebElement snackBar) {
		this.snackBar = snackBar;
	}
		
	//*************************** WAITS *********************************

	public void ensureNewCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOf(this.newCategoryBtn));
		wait.until(ExpectedConditions.elementToBeClickable(newCategoryBtn));
	}
	
	public void ensureTableRowIsPresent() {
		wait.until(ExpectedConditions.visibilityOf(tableRowName));
		//wait.until(ExpectedConditions.elementToBeClickable(tableRowName));
	}
	
	public void ensureUpdateCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOf(updateCategoryBtn));
		wait.until(ExpectedConditions.elementToBeClickable(updateCategoryBtn));
	}
	
	public void ensureDeleteCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOf(deleteCategoryBtn));
		wait.until(ExpectedConditions.elementToBeClickable(deleteCategoryBtn));
	}
	
	public void ensureUpdateCancelCategoryBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOf(updateCancelCategoryBtn));
		wait.until(ExpectedConditions.elementToBeClickable(updateCancelCategoryBtn));
	}
	
	public void ensureDeleteConfirmationBtnIsDisplayed() {
		//wait.until(ExpectedConditions.visibilityOfElementLocated((By.id("confirmation-confirm-btn"))));
		wait.until(ExpectedConditions.elementToBeClickable(deleteConfirmationBtn));
	}
	
	public void ensureSnackBarIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("snack-bar")));
	}
	
}
