package com.tickettakeit.tickettakeit.e2e.constants;

public class URLConstants {
	
	public static final String LOGIN_PAGE_URL = "http://localhost:4200/login";
	public static final String HOME_PAGE_URL = "http://localhost:4200/home";

}
