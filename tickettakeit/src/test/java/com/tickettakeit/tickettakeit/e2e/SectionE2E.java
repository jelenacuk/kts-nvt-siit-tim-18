package com.tickettakeit.tickettakeit.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.AdminPage;
import com.tickettakeit.tickettakeit.e2e.pages.CreateHallPage;
import com.tickettakeit.tickettakeit.e2e.pages.CreateLocationPage;
import com.tickettakeit.tickettakeit.e2e.pages.CreateSectionPage;
import com.tickettakeit.tickettakeit.e2e.pages.HallInfoPage;
import com.tickettakeit.tickettakeit.e2e.pages.LocationInfoPage;
import com.tickettakeit.tickettakeit.e2e.pages.LocationListPage;
import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;

public class SectionE2E {
	
	private AdminPage adminPage;
	private LocationListPage locationPage;
	private CreateLocationPage createLocationPage;
	private LocationInfoPage locationInfoPage;
	private LoginPage loginPage;
	private WebDriver browser;
	private CreateHallPage createHallPage;
	private HallInfoPage hallInfoPage;
	private CreateSectionPage createSectionPage;
	
	@Before
	public void setupSelenium() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-web-security");
        browser = new ChromeDriver(options);
		browser.navigate().to("http://localhost:4200/login");
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName("admin");
		loginPage.setPassword("user");
		loginPage.getLoginButton().click();
		adminPage = PageFactory.initElements(browser, AdminPage.class);
		adminPage.setDriver(browser);
		locationPage = PageFactory.initElements(browser, LocationListPage.class);
		locationPage.setDriver(browser);
		createLocationPage = PageFactory.initElements(browser, CreateLocationPage.class);
		createLocationPage.setDriver(browser);
		locationInfoPage = PageFactory.initElements(browser, LocationInfoPage.class);
		locationInfoPage.setDriver(browser);
		createHallPage = PageFactory.initElements(browser, CreateHallPage.class);
		createHallPage.setDriver(browser);
		hallInfoPage = PageFactory.initElements(browser, HallInfoPage.class);
		hallInfoPage.setDriver(browser);
		createSectionPage = PageFactory.initElements(browser, CreateSectionPage.class);
		createSectionPage.setDriver(browser);
		adminPage.ensurelocationsTabIsDisplayed();
		adminPage.getLocationsTab().click();
		locationPage.ensureTableRowIsPresend();
		locationPage.getTableRow().click();
		locationInfoPage.ensureHallIsPresend();
		locationInfoPage.getHallName().click();
	}
	@Test
	public void createSection() {
		hallInfoPage.ensureCreateSectionIsDisplayed();
		hallInfoPage.getCreateSectionBt().click();
		createSectionPage.ensureCreateSectionIsDisplayed();
		createSectionPage.setName("Sec");
		createSectionPage.setRows("15");
		createSectionPage.setColumns("10");
		createSectionPage.getCreateBt().click();
		hallInfoPage.ensureCreateSectionIsDisplayed();
		assertEquals(true, hallInfoPage.getCreateSectionBt().isDisplayed());
	}

}
