package com.tickettakeit.tickettakeit.e2e;

import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.DAILY_REPORT_EARNINGS_IMAGE;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.DAILY_REPORT_SOLD_TICKETS_IMAGE;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.EVENT_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.FROM_DATE_DAILY;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.FROM_DATE_MONTHLY;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.INVALID_EVENT_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.LOCATION_NAME;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.MONTHLY_REPORT_EARNINGS_IMAGE;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.MONTHLY_REPORT_SOLD_TICKETS_IMAGE;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.TO_DATE_DAILY;
import static com.tickettakeit.tickettakeit.e2e.constants.ReportConstants.TO_DATE_MONTHLY;
import static com.tickettakeit.tickettakeit.e2e.constants.URLConstants.LOGIN_PAGE_URL;
import static com.tickettakeit.tickettakeit.e2e.constants.UserConstants.ADMIN_PASSWORD;
import static com.tickettakeit.tickettakeit.e2e.constants.UserConstants.ADMIN_USERNAME;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.AdminPage;
import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;
import com.tickettakeit.tickettakeit.e2e.pages.ReportPage;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReportsE2E {
	
	private WebDriver browser;
	private AdminPage adminPage;
	private LoginPage loginPage;
	private ReportPage reportPage;
	
	@BeforeEach 
	public void setupSelenium() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		
		browser.navigate().to(LOGIN_PAGE_URL);

		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(ADMIN_USERNAME);
		loginPage.setPassword(ADMIN_PASSWORD);
		loginPage.getLoginButton().click(); 
		adminPage = PageFactory.initElements(browser, AdminPage.class);
		adminPage.setDriver(browser);
		reportPage = PageFactory.initElements(browser, ReportPage.class);
		reportPage.setDriver(browser);

		adminPage.ensureReportsTabIsDisplayed();
		adminPage.getReportsTab().click();

	}
	
	@Test
	@Order(1)
	public void createLocationDailyReport_success() throws InterruptedException, IOException {
		
		reportPage.ensureReportBoxIsDisplayed();
		reportPage.ensureLocationSlideBtnIsDisplayed();
		reportPage.getLocationSlideBtn().click();
		
		reportPage.ensureLocationInputIsReady();
		reportPage.setLocationInput(LOCATION_NAME);
		reportPage.setFromDateInput(FROM_DATE_DAILY);
		reportPage.setToDateInput(TO_DATE_DAILY);	
		
		WebElement reportTypeRadioBtn = reportPage.getReportTypeRadioBtns().stream()
				.filter(btn -> btn.getText().equals("daily")).findFirst().get();
		reportTypeRadioBtn.click();
		
		reportPage.ensureReportSubmitBtnIsDisplayed();
		reportPage.getReportSubmitBtn().click();
		
		reportPage.ensureChartsAreDisplayed();
		
		// resize the window so that only earnings chart is visible
		browser.manage().window().setSize(new Dimension(840, 490));
		
		assertTrue(reportPage.compareCharts(DAILY_REPORT_EARNINGS_IMAGE));
		assertTrue(reportPage.compareCharts(DAILY_REPORT_SOLD_TICKETS_IMAGE));
		
		
	}
	
	@Test
	@Order(2)
	public void createEventMonthlyReport_success() throws InterruptedException, IOException {
		
		reportPage.ensureReportBoxIsDisplayed();
		reportPage.ensureEventSlideBtnIsDisplayed();
		reportPage.getEventSlideBtn().click();
		
		reportPage.ensureEventInputIsReady();
		reportPage.setEventInput(EVENT_NAME);
		reportPage.setFromDateInput(FROM_DATE_MONTHLY);
		reportPage.setToDateInput(TO_DATE_MONTHLY);	
		
		WebElement reportTypeRadioBtn = reportPage.getReportTypeRadioBtns().stream()
				.filter(btn -> btn.getText().equals("monthly")).findFirst().get();
		reportTypeRadioBtn.click();
		
		reportPage.ensureReportSubmitBtnIsDisplayed();
		reportPage.getReportSubmitBtn().click();
		
		reportPage.ensureChartsAreDisplayed();
		
		// resize the window so that only earnings chart is visible
		browser.manage().window().setSize(new Dimension(840, 490)); 
		
		assertTrue(reportPage.compareCharts(MONTHLY_REPORT_EARNINGS_IMAGE));
		assertTrue(reportPage.compareCharts(MONTHLY_REPORT_SOLD_TICKETS_IMAGE));

	}
	
	@Test
	@Order(3)
	public void eventNameDoesNotExist() {
		
		reportPage.ensureReportBoxIsDisplayed();
		reportPage.ensureEventSlideBtnIsDisplayed();
		reportPage.getEventSlideBtn().click();
		
		reportPage.ensureEventInputIsReady();
		reportPage.setEventInput(INVALID_EVENT_NAME);
		reportPage.setFromDateInput(FROM_DATE_MONTHLY);
		reportPage.setToDateInput(TO_DATE_MONTHLY);	
		
		WebElement reportTypeRadioBtn = reportPage.getReportTypeRadioBtns().stream()
				.filter(btn -> btn.getText().equals("monthly")).findFirst().get();
		reportTypeRadioBtn.click();
		
		reportPage.ensureReportSubmitBtnIsDisplayed();
		reportPage.getReportSubmitBtn().click();
		
		reportPage.ensureErrorSnackBarIsDisplayed();
		
		assertTrue(reportPage.getErrorSnackBar().getText()
				.contains("You must pick an existing event name"));
	}
	
	@AfterEach
	public void afterTest() {
		browser.close();
	}  

}
