package com.tickettakeit.tickettakeit.e2e.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

	private WebDriver driver;

	private WebDriverWait wait;

	@FindBy(id = "event-name-search")
	private WebElement eventNameSearch;

	@FindBy(id = "city-search")
	private WebElement citySearch;
 
	@FindBy(id = "location-name-search") 
	private WebElement locationNameSearch;

	@FindBy(id = "from-date-search")
	private WebElement fromDateSearch;

	@FindBy(id = "to-date-search")
	private WebElement toDateSearch;

	@FindBy(id = "from-price-search")
	private WebElement fromPriceSearch;

	@FindBy(id = "to-price-search")
	private WebElement toPriceSearch;
	
	@FindBy(className = "search-expansion-panel")
	private WebElement searchExpansionPanel;

	@FindBy(id = "search-btn")
	private WebElement searchBtn;
	
	@FindBy(id = "reset-search-form-btn")
	private WebElement resetBtn;

	@FindBy(className = "category-search")
	private List<WebElement> categorySearchBtns;
	
	@FindBy(id = "search-input")
	private WebElement generalSearchInput;
	
	@FindBy(id = "search-icon-btn")
	private WebElement generalSearchBtn;
	
	@FindBy(className = "ticket-wrapper")
	private WebElement events;
	
	@FindBy(className = "event-card-event-name")
	private List<WebElement> eventCardEventNames;
	
	@FindBy(className = "event-card-location-name")
	private List<WebElement> eventCardLocationNames;
	
	@FindBy(className = "categories-tr")
	private List<WebElement> categoriesOfEvents;

	// ******************* GET AND SET METHODS *************************
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver, 20);
	}

	public WebElement getEventNameSearch() {
		return eventNameSearch;
	}

	public void setEventNameSearch(String eventNameSearch) {
		this.eventNameSearch.clear();
		this.eventNameSearch.sendKeys(eventNameSearch);
	}

	public WebElement getCitySearch() {
		return citySearch;
	}

	public void setCitySearch(String citySearch) {
		this.citySearch.clear();
		this.citySearch.sendKeys(citySearch);
	}

	public WebElement getLocationNameSearch() {
		return locationNameSearch;
	}

	public void setLocationNameSearch(String locationNameSearch) {
		this.locationNameSearch.clear();
		this.locationNameSearch.sendKeys(locationNameSearch);
	}

	public WebElement getFromDateSearch() {
		return fromDateSearch;
	}

	public void setFromDateSearch(WebElement fromDateSearch) {
		this.fromDateSearch = fromDateSearch;
	}

	public WebElement getToDateSearch() {
		return toDateSearch;
	}

	public void setToDateSearch(WebElement toDateSearch) {
		this.toDateSearch = toDateSearch;
	}

	public WebElement getFromPriceSearch() {
		return fromPriceSearch;
	}

	public void setFromPriceSearch(WebElement fromPriceSearch) {
		this.fromPriceSearch = fromPriceSearch;
	}

	public WebElement getTopriceSearch() {
		return toPriceSearch;
	}

	public void setTopriceSearch(WebElement topriceSearch) {
		this.toPriceSearch = topriceSearch;
	}
	
	public WebElement getSearchExpansionPanel() {
		return searchExpansionPanel;
	}

	public void setSearchExpansionPanel(WebElement searchExpansionPanel) {
		this.searchExpansionPanel = searchExpansionPanel;
	}

	public WebElement getSearchBtn() {
		return searchBtn;
	}
	
	public WebElement getResetBtn() {
		return resetBtn;
	}

	public List<WebElement> getCategorySearchBtns() {
		return categorySearchBtns;
	}
	
	public WebElement getGeneralSearchInput() {
		return generalSearchInput;
	}

	public void setGeneralSearchInput(String generalSearchInput) {
		this.generalSearchInput.clear();
		this.generalSearchInput.sendKeys(generalSearchInput);
		
	}

	public WebElement getGeneralSearchBtn() {
		return generalSearchBtn;
	}
	
	public WebElement getEvents() {
		return events;
	}

	public List<WebElement> getEventCardEventNames() {
		return eventCardEventNames;
	}
	
	public List<WebElement> getEventCardLocationNames() {
		return eventCardLocationNames;
	}
	
	public List<WebElement> getCategoriesOfEvents() {
		return categoriesOfEvents;
	}

	public void setCategoriesOfEvents(List<WebElement> categoriesOfEvents) {
		this.categoriesOfEvents = categoriesOfEvents;
	}
	
	
	
	// ********************** WAIT *****************************

	public void ensureEventNameInputIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(eventNameSearch));	
	}
	
	public void ensureLocationNameInputIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(locationNameSearch));	
	}
	
	public void ensureSearchExpansionPanelIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(searchExpansionPanel));
		wait.until(ExpectedConditions.elementToBeClickable(searchExpansionPanel));
	}
	
	public void ensureSearchBtnIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(searchBtn));
		wait.until(ExpectedConditions.elementToBeClickable(searchBtn));
	}
	
	public void ensureGeneralSearchInutIsReady() {
		wait.until(ExpectedConditions.elementToBeClickable(generalSearchInput));
	}
	
	public void ensureGeneralSearchBtnIsDisplayed() {
		wait.until(ExpectedConditions.elementToBeClickable(generalSearchBtn));
	
	}
	
	public void ensureCategorySearchBtnsAreDisplayed() {
		wait.until(ExpectedConditions.visibilityOfAllElements(categorySearchBtns));
	
	}
	
	public void ensureEventsIsDisplayed() {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ticket-wrapper")));	
	}
	
	

}
