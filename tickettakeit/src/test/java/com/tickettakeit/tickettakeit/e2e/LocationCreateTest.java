package com.tickettakeit.tickettakeit.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.AdminPage;
import com.tickettakeit.tickettakeit.e2e.pages.CreateLocationPage;
import com.tickettakeit.tickettakeit.e2e.pages.LocationInfoPage;
import com.tickettakeit.tickettakeit.e2e.pages.LocationListPage;
import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;

public class LocationCreateTest {

	private AdminPage adminPage;
	private LocationListPage locationPage;
	private CreateLocationPage createLocationPage;
	private LocationInfoPage locationInfoPage;
	private LoginPage loginPage;
	private WebDriver browser;

	@Before
	public void setupSelenium() {
		// instantiate browser
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		browser.navigate().to("http://localhost:4200/login");

		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName("admin");
		loginPage.setPassword("user");
		loginPage.getLoginButton().click();
		adminPage = PageFactory.initElements(browser, AdminPage.class);
		adminPage.setDriver(browser);
		locationPage = PageFactory.initElements(browser, LocationListPage.class);
		locationPage.setDriver(browser);
		createLocationPage = PageFactory.initElements(browser, CreateLocationPage.class);
		createLocationPage.setDriver(browser);
		locationInfoPage = PageFactory.initElements(browser, LocationInfoPage.class);
		locationInfoPage.setDriver(browser);
	}

	@Test
	public void createLocationTest() {

		adminPage.ensurelocationsTabIsDisplayed();
		adminPage.getLocationsTab().click();
		locationPage.ensurenewLocationButtonIsDisplayed();
		locationPage.getNewLocationButton().click();
		createLocationPage.ensurecreateButtonIsDisplayed();
		createLocationPage.setName("Location 1");
		createLocationPage.setCity("Beograd");
		createLocationPage.setAddress("Ulica 1");
		createLocationPage.setLatitude("42.5");
		createLocationPage.setLongitude("17.5");
		createLocationPage.getCreateButton().sendKeys(Keys.RETURN);
		locationPage.ensureTableRowIsPresend();
		assertEquals("Location 1", locationPage.getAddedLocationName().getText());
	}

	@Test
	public void updateLocationTest() {
		adminPage.ensurelocationsTabIsDisplayed();
		adminPage.getLocationsTab().click();
		locationPage.ensureTableRowIsPresend();
		locationPage.getTableRow().click();
		locationInfoPage.ensureUpdateButtonPresend();
		locationInfoPage.getUpdateButton().click();
		createLocationPage.ensureUpdateButtonIsDisplayed();
		createLocationPage.setName("Updated");
		createLocationPage.setLatitude("7");
		createLocationPage.setLongitude("8");
		createLocationPage.ensureUpdateButtonIsDisplayed();
		createLocationPage.getUpdateButton().sendKeys(Keys.RETURN);
		locationInfoPage.ensureUpdateButtonPresend();
		assertEquals("Updated", locationInfoPage.getLocationName().getText());
	}

	@Test
	public void deactivateLocationTest() {
		adminPage.ensurelocationsTabIsDisplayed();
		adminPage.getLocationsTab().click();
		locationPage.ensureStatusIsPresend();
		assertEquals("Active", locationPage.getStatusText().getText());
		locationPage.getDeactivateButton().click();
		browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		assertEquals("Not active", locationPage.getStatusText().getText());
	}

}
