package com.tickettakeit.tickettakeit.e2e;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import com.tickettakeit.tickettakeit.e2e.pages.LoginPage;
import com.tickettakeit.tickettakeit.e2e.pages.UpdateEventPage;

public class UpdateEventTest {

	private UpdateEventPage updateEventPage;
	private LoginPage loginPage;
	private WebDriver browser;
	
	private final String username = "admin";
	private final String password = "user";
	private final String eventId = "200";
	private final String newDate = "3/04/2020, 2:40 PM";
	private final String displayedDate = "Mar 4, 2020, 2:40:00 PM";
	private final String takenEventName = "Lara Fabian";
	private final String newName = "Brod Zeppelinnn";
	private final String newCategoryIndex = "3";
	private final String newCategoryName = "Art Exibition";

	@Before
	public void setupSelenium() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-web-security");
		browser = new ChromeDriver(options);
		browser.navigate().to("http://localhost:4200/login"); 

		// login
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		loginPage.setDriver(browser);
		loginPage.ensureTableRowIsPresend();
		loginPage.setUserName(username);
		loginPage.setPassword(password);
		loginPage.getLoginButton().click();
		loginPage.ensureSnackBarIsDisplayed();

		browser.navigate().to("http://localhost:4200/update-event/" + eventId);
		updateEventPage = PageFactory.initElements(browser, UpdateEventPage.class);
		updateEventPage.setDriver(browser);
	}

	@Test
	public void updateEvent_Date() {

		updateEventPage.getDateExtensionPanel().click();
		updateEventPage.ensurePanelExpanded();
		updateEventPage.ensureAddDateButtonIsDisplayed();

		// empty field
		updateEventPage.getAddDateBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("No date picked.");
		String text = updateEventPage.getSnackBar().getText();
		assertEquals("No date picked.", text);
		updateEventPage.ensureSnackBarInvisible();

		// add date
		updateEventPage.setInputDate(newDate);
		updateEventPage.getAddDateBtn().click();
		updateEventPage.ensureAddedDateIsDisplayed();
		text = updateEventPage.getNewDate().getText();
		assertEquals( displayedDate, text);
		updateEventPage.getChangeEventDateBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("Dates successfuly changed!");
		text = updateEventPage.getSnackBar().getText();
		assertEquals("Dates successfuly changed!", text);
		updateEventPage.ensureSnackBarInvisible();

		// add same date
		updateEventPage.getAddDateBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("Date already added.");
		text = updateEventPage.getSnackBar().getText();
		assertEquals("Date already added.", text);
		updateEventPage.ensureSnackBarInvisible();

		// remove date
		updateEventPage.getNewDate().click();
		updateEventPage.ensureSnackBarIsDisplayed("Date removed");
		text = updateEventPage.getSnackBar().getText();
		assertEquals("Date removed", text);
		updateEventPage.ensureSnackBarInvisible();

		// wrong date
		updateEventPage.setInputDate("2/26/2019, 2:40 PM");
		updateEventPage.getAddDateBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("Event date must be in the future.");
		text = updateEventPage.getSnackBar().getText();
	}

	@Test
	public void updateEvent_Name() {

		// invalid name
		updateEventPage.ensureNameButtonIsDisplayed();
		updateEventPage.setEventName("n%a*me");
		updateEventPage.getEditNameBtn().click();
		String text = updateEventPage.ensureErrorMessageIsDisplayed("0");
		assertEquals("Name is required and can contain only letters.", text);

		// event name is taken
		updateEventPage.setEventName(takenEventName);
		updateEventPage.getEditNameBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("Event name is taken.");
		text = updateEventPage.getSnackBar().getText();
		assertEquals("Event name is taken.", text);

		// successfully
		updateEventPage.setEventName(newName);
		updateEventPage.getEditNameBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("Name successfully updated!");
		text = updateEventPage.getSnackBar().getText();
		assertEquals("Name successfully updated!", text);
	}

	@Test
	public void updateEvent_Status() {

		updateEventPage.ensureChangeEventDateButtonIsDisplayed();
		updateEventPage.setControllAccess(1);
		updateEventPage.getEditControllAccessBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("ControllAccess successfuly changed!");
		String text = updateEventPage.getSnackBar().getText();
		assertEquals("ControllAccess successfuly changed!", text);
	}

	@Test
	public void updateEvent_Category() {

		updateEventPage.getCategoryExtensionPanel().click();
		updateEventPage.ensureCategoryPanelExpanded();
		updateEventPage.ensureAddCategoryButtonIsDisplayed();
		
		// empty field
		updateEventPage.getAddCategoryBtn().click();
		updateEventPage.ensureSnackBarIsDisplayed("No category picked.");
		String text = updateEventPage.getSnackBar().getText();
		assertEquals("No category picked.", text);
		
		updateEventPage.ensureSnackBarInvisible();
		updateEventPage.setInputCategory(newCategoryIndex);
		updateEventPage.myEnsurance(newCategoryName);
		updateEventPage.getAddCategoryBtn().click();
		updateEventPage.ensureAddedCategoryIsDisplayed();
		text = updateEventPage.getNewCategory().getText();
		assertEquals(newCategoryName, text);
		updateEventPage.ensureSnackBarIsDisplayed("Category successfuly updated!");
		text = updateEventPage.getSnackBar().getText();
		assertEquals("Category successfuly updated!", text);
	
		//remove category
		updateEventPage.ensureSnackBarInvisible();
		updateEventPage.getNewCategory().click();
		updateEventPage.ensureSnackBarIsDisplayed("Category successfully removed!");
		text =updateEventPage.getSnackBar().getText(); 
		assertEquals("Category successfully removed!", text);	
		
	}

	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
}
